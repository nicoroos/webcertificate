﻿<%@ Page Title="Qualification" Language="C#" MasterPageFile="~/Secure/CollegeMaster.Master" AutoEventWireup="true" CodeBehind="Qualification.aspx.cs" Inherits="GUICollege.Secure.Qualification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div id="main">
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Qualifications</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" id="search" class="form-control" placeholder="Search" data-bind='value: searchName' />
                    </div>
                    <button type="button" id="btnsearch" class="btn btn-primary">Search</button>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <%--Main qualification table--%>
        <div class="scrollableTable">
            <table id="tblQualification" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Qualification Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: qualifications">
                    <tr>
                        <td class="editHide" data-bind="text: QualName"></td>
                        <td class="editHide" data-bind="text: QualDescrip"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javaScript" runat="server">
    <script type="text/javascript">

        // View model for qualifications
        function QualificationListModel() {
            var self = this;

            // Qualification Search bar variable
            self.searchName = ko.observable();
            // Qualificatin list variable
            self.qualifications = ko.observableArray([]);

            // Get list of Qualifications
            $.getJSON("../Handlers/GetQualification.ashx", function (data) {
                self.qualifications(data);
            });

            // Search results
            $('#btnsearch').click(function () {
                var Param = { 'qualname': ko.toJS(self.searchName) };
                $.getJSON("../Handlers/GetQualificationSearchName.ashx", Param, function (data) {
                    self.qualifications(data);
                });
            });
        }
        // Apply the knockout binding.
        ko.applyBindings(new QualificationListModel());

    </script>
</asp:Content>
