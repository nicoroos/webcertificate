﻿<%@ Page Title="User" Language="C#" MasterPageFile="~/Secure/CollegeMaster.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="GUICollege.Secure.User" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div>
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Users</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="navbar-form navbar-nav" role="search">
                    <select id="cboCampus" class="form-control" data-bind="options: campuses, optionsText: 'CampusName', optionsValue: 'CampusID', optionsCaption: 'Select Campus..', value: selectedCampus"></select>
                </div>
                <div class="navbar-form navbar-nav" role="search">
                    <div class="form-group">
                        <input type="text" id="search" class=" form-control input" placeholder="Search" data-bind="value: clientSurname" />
                    </div>
                    <button type="button" id="btnsearch" class="btn btn-primary">Search</button>
                </div>
                <div class="navbar-form navbar-nav" role="search">
                    <p>
                        <label>
                            <input type='radio' name="type" value='all' data-bind='checked: typeToShow' />All</label>
                        <label>
                            <input type='radio' name="type" value='1' data-bind='checked: typeToShow' />Admin</label>
                        <label>
                            <input type='radio' name="type" value='2' data-bind='checked: typeToShow' />ColtAdmin</label>
                        <label>
                            <input type='radio' name="type" value='3' data-bind='checked: typeToShow' />ColtStaff</label>
                        <label>
                            <input type='radio' name="type" value='4' data-bind='checked: typeToShow' />CollAdmin</label>
                        <label>
                            <input type='radio' name="type" value='5' data-bind='checked: typeToShow' />CollStaff</label>
                        <label>
                            <input type='radio' name="type" value='6' data-bind='checked: typeToShow' />Users</label>
                    </p>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <br />
        <%-- Main Table with list of Clients --%>
        <div class="scrollableTable">
            <table id="tblClient" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Initials</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody data-bind='template: { foreach: clientsToShow, beforeRemove: hideClientElement, afterAdd: showClienttElement }'>
                    <tr data-bind='attr: { "class": +UserType }'>
                        <td data-bind="text: UserID"></td>
                        <td data-bind="text: Title"></td>
                        <td data-bind="text: Initials"></td>
                        <td data-bind="text: Name"></td>
                        <td data-bind="text: Surname"></td>
                        <td><a href="#myModal" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.editClient">View</a>
                            <a href="#myModal2" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.viewQuals">History</a></td>
                    </tr>
                </tbody>
            </table>
            </div>
            <input type="button" id="btnAddClient" class="btn btn-primary" value="Add new User" data-bind="click: addClient" />
        
        <!-- Modal View - Client Edit -->
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="myModalLabel">Client Details</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr>
                                <th>Name</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sName" data-bind="text: Name"></span>
                                        <div id="iptName">
                                            <input class="form-control inputName" data-bind="value: Name" type="text" placeholder="Name" />
                                            <div id="nameIn" class="icon-ok-sign in inputNameNotBlank"></div>
                                            <div id="nameEmpty" class="icon-remove-sign inputNameBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditName" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Surname</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sSurname" data-bind="text: Surname"></span>
                                        <div id="iptSurname">
                                            <input class="form-control inputSurname" data-bind="value: Surname" type="text" placeholder="Surname" />
                                            <div id="surnameIn" class="icon-ok-sign inputSurnameNotBlank"></div>
                                            <div id="surnameEmpty" class="icon-remove-sign inputSurnameBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditSurname" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Initials</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sInitials" data-bind="text: Initials"></span>
                                        <div id="iptInitials">
                                            <input class="form-control inputInitials" data-bind="value: Initials" type="text" placeholder="Initials" />
                                            <div id="initIn" class=" icon-ok-sign inputInitialsNotBlank"></div>
                                            <div id="initEmpty" class="icon-remove-sign inputInitialsBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditInitials" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Title</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sTitle" data-bind="text: Title"></span>
                                        <div id="selTitle">
                                            <select class="cboTitle form-control" data-bind="options: titles, optionsText: 'TitleName', optionsValue: 'TitleID', optionsCaption: 'Select a Title..', selectedOptions: selectedTitle, value: selectedTitle"></select>
                                            <div id="titleIn" class="icon-ok-sign inputTitleNotBlank"></div>
                                            <div id="titleEmpty" class="icon-remove-sign inputTitleBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditTitle" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sEmail" data-bind="text: Email"></span>
                                        <div id="iptEmail">
                                            <input class=" form-control inputEmail" data-bind="value: Email" type="email" placeholder="Email" />
                                            <div id="emailIn" class="icon-ok-sign inputEmailNotBlank"></div>
                                            <div id="emailEmpty" class="icon-remove-sign inputEmailBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditEmail" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Password</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sPassword" data-bind="text: Password"></span>
                                        <div id="iptPassword">
                                            <input class="form-control txtPassword" data-bind="value: Password" type="password" placeholder="Password" />
                                            <div id="passIn" class="icon-ok-sign inputVerifyPasswordNotBlank"></div>
                                            <div id="passEmpty" class="icon-remove-sign inputVerifyPasswordBlank"></div>
                                            <div id="passWrong" class="alert alert-danger txtPasswordlength">Password must be 6 characters or more!</div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditPassword" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr class="PasswordVerification">
                                <th>Verify Password</th>
                                <td>
                                    <div class="col-xs-10">
                                        <input class="form-control txtVerifyPassword" data-bind="value: PasswordVerification" type="password" placeholder="Password Verification" />
                                        <div id="pass2In" class=" icon-ok-sign"></div>
                                        <div id="pass2Empty" class="icon-remove-sign"></div>
                                        <div id="pass2Wrong" class="alert alert-danger txtVerifyStatus">Password Do not Match!</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>User type</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sType" data-bind="text: UserType"></span>
                                        <div id="selType">
                                            <select id="selUserType" class="selUserType form-control" data-bind="options: userTypes, optionsText: 'TypeName', optionsValue: 'TypeID', optionsCaption: 'Select a Type..', value: selectedType"></select>
                                            <div id="typeIn" class="icon-ok-sign userTypeNotBlank"></div>
                                            <div id="typeEmpty" class="icon-remove-sign userTypeBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditUserType" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Course Code</th>
                                <td>
                                    <div class="col-xs-10">
                                        <a href="#" id="AddCourscode">Add a Course Code</a>
                                        <div id="iptCourseCode">
                                            <input id="inputCourseCode" class="form-control " data-bind="value: ReferNum" type="text" placeholder="CourseCode" />
                                            <div id="courseCodeIn" class=" icon-ok-sign"></div>
                                            <div id="courseCodeEmpty" class="icon-remove-sign"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnAddCourse" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Location</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCollege" data-bind="text: College"></span>
                                        <select id="selCollege" class="cboCollege form-control " data-bind="options: cboColleges, optionsText: 'CollegeName', optionsValue: 'CollegeID', optionsCaption: 'Select College..', value: selectedCboCollege">
                                        </select>
                                    </div>
                                    <button type="button" id="btnEditCollege" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCampus" data-bind="text: Campus"></span>
                                        <div id="selCampus">
                                            <select class="inputCampus form-control" data-bind="options: cboCampuses, optionsText: 'CampusName', optionsValue: 'CampusID', optionsCaption: 'Select Campus..', value: selectedCboCampus">
                                            </select>
                                            <div id="campIn" class=" icon-ok-sign inputCampusNotBlank"></div>
                                            <div id="campEmpty" class="icon-remove-sign inputCampusBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditCampus" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Cellphone Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCellNo" data-bind="text: CellNo"></span>
                                        <div id="iptCellNo">
                                            <input class="form-control inputCelno" data-bind="value: CellNo" type="tel" placeholder="Cellphone Number" maxlength="10" onkeypress="return IsNumericcell(event);" ondrop="return false;"/>
                                            <div id="cellIn" class="icon-ok-sign inputCelnoNotBlank"></div>
                                            <div id="CellEmpty" class="icon-remove-sign inputCelnoBlank"></div>
                                            <br id="breaklinemodelcell" />
                                            <div id="nummodelcellerror" class="alert alert-danger">
                                                <a href="specialKeyscell" class="alert-link">Input digits (0 - 9)</a>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditCellno" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Telephone Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sTelNo" data-bind="text: TelNo"></span>
                                        <input id="inputTelNo" class="form-control inputTelNo " data-bind="value: TelNo" type="tel" placeholder="Telephone Number" maxlength="10" onkeypress="return IsNumerictel(event);" ondrop="return false;"/>
                                        <br id="breaklinemodeltel" />
                                            <div id="nummodeltelerror" class="alert alert-danger">
                                                <a href="specialKeystel" class="alert-link">Input digits (0 - 9)</a>
                                            </div>
                                    </div>
                                    <button type="button" id="btnEditTellno" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Fax Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sFax" data-bind="text: Fax"></span>
                                        <input id="inputFaxNo" class="form-control " data-bind="value: Fax" type="tel" placeholder="Fax Number" maxlength="10" onkeypress="return IsNumericfax(event);" ondrop="return false;"/>
                                        <br id="breaklinemodelfax" />
                                            <div id="nummodelfaxerror" class="alert alert-danger">
                                                <a href="specialKeysfax" class="alert-link">Input digits (0 - 9)</a>
                                            </div>
                                    </div>
                                    <button type="button" id="btnEditFax" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Residential Address</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sStreetNo" data-bind="text: Street1"></span>
                                        <input id="inputStreetNo" class="form-control " data-bind="value: Street1" type="text" placeholder="Street No" maxlength="10"/>
                                    </div>
                                    <button type="button" id="btnEditStreetNo" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sStreetName" data-bind="text: Street2"></span>
                                        <input id="inputStreetName" class="form-control " data-bind="value: Street2" type="text" placeholder="Street Name" maxlength="50"/>
                                    </div>
                                    <button type="button" id="btnEditStreetName" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sSuburb" data-bind="text: Street3"></span>
                                        <input id="inputSuburb" class="form-control " data-bind="value: Street3" type="text" placeholder="Suburb" maxlength="30"/>
                                    </div>
                                    <button type="button" id="btnEditSuburb" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCity" data-bind="text: Street4"></span>
                                        <input id="inputCity" class="form-control " data-bind="value: Street4" type="text" placeholder="City" maxlength="30"/>
                                    </div>
                                    <button type="button" id="btnEditCity" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Postal Code</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sPostalCode" data-bind="text: PostalCode"></span>
                                        <input id="iptPostalCode" class="form-control " data-bind="value: PostalCode" type="text" placeholder="Postal Code" maxlength="10"/>
                                    </div>
                                    <button type="button" id="btnEditPostalCode" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button class="btn btn-success" data-bind="click: upadate">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- Modal View - Client history -->
        <div id="myModal2" class="modal fade modal-wide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="H1">Qualifications&nbsp;(<span data-bind='text: qualification().length'></span>)</h3>
                        <p>for <span data-bind="text: userName"></span></p>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Client Qual ID</th>
                                    <th>Qualification</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Print Status</th>
                                    <th>CourseCode</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: qualification">
                                <tr>
                                    <td data-bind="text: UserQualID"></td>
                                    <td data-bind="text: Qual"></td>
                                    <td data-bind="text: StartDate"></td>
                                    <td data-bind="text: EndDate"></td>
                                    <td data-bind="text: PrintStatus"></td>
                                    <td data-bind="text: ReferNum"></td>
                                    <td class="clientHide">
                                        <input type="button" class="btn btn-inverse" data-bind="click: $parent.print" value="Print" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <%-- Client add form --%>
        <div class="form-horizontal" id="clientForm">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputName">Name</label>
                        <div class="col-xs-6">
                            <input class="inputName form-control" data-bind="value: Name" type="text" id="inputName" placeholder="Name" />
                            <div id="inputNameNotBlank" class="icon-ok-sign inputNameNotBlank"></div>
                            <div id="inputNameBlank" class="icon-remove-sign inputNameBlank"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputSurname">Surname</label>
                        <div class="col-xs-8">
                            <input id="inputSurname" data-bind="value: Surname" type="text" class="inputSurname form-control" placeholder="Surname" />
                            <div id="inputSurnameNotBlank" class="icon-ok-sign inputSurnameNotBlank"></div>
                            <div id="inputSurnameBlank" class="icon-remove-sign inputSurnameBlank"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputInitials">Initials</label>
                        <div class="col-xs-3">
                            <input data-bind="value: Initials" id="inputInitials" type="text" class="inputInitials form-control" placeholder="Initials" />
                            <div id="inputInitialsNotBlank" class=" icon-ok-sign inputInitialsNotBlank"></div>
                            <div id="inputInitialsBlank" class="icon-remove-sign inputInitialsBlank"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputTitle">Title</label>
                        <div class="col-xs-4">
                            <select id="cboTitle" class="cboTitle form-control" data-bind="options: titles, optionsText: 'TitleName', optionsValue: 'TitleID', optionsCaption: 'Select a Title..', value: selectedTitle"></select>
                            <div id="inputTitleNotBlank" class="icon-ok-sign inputTitleNotBlank"></div>
                            <div id="inputTitleBlank" class="icon-remove-sign inputTitleBlank"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputEmail">Email</label>
                        <div class="col-xs-6">
                            <input data-bind="value: Email" type="email" id="inputEmail" class="inputEmail form-control" placeholder="Email" />
                            <div id="inputEmailNotBlank" class="icon-ok-sign inputEmailNotBlank"></div>
                            <div id="inputEmailBlank" class="icon-remove-sign inputEmailBlank"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="txtPassword">Password</label>
                        <div class="col-xs-5">
                            <input data-bind="value: Password" id="txtPassword" type="password" class="txtPassword form-control" placeholder="Password" />
                            <div id="inputPasswordNotBlank" class="icon-ok-sign inputPasswordNotBlank"></div>
                            <div id="inputPasswordBlank" class="icon-remove-sign inputPasswordBlank"></div>
                            <div id="txtPasswordlength" class="alert alert-danger">Password must be 6 characters or more!</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputVerifyPassword">Password Verification</label>
                        <div class="col-xs-5">
                            <input data-bind="value: PasswordVerification" id="txtVerifyPassword" type="password" class="txtVerifyPassword form-control" placeholder="Password Verification" />
                            <div id="txtVerifyStatus" class="alert alert-danger txtVerifyStatus">Password Do not Match!</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputType">User Type</label>
                        <div class="col-xs-4">
                            <select id="cboUsertype" class="selUserType form-control" data-bind="options: userTypes, optionsText: 'TypeName', optionsValue: 'TypeID', optionsCaption: 'Select a Type..', value: selectedType"></select>
                            <div id="userTypeNotBlank" class="icon-ok-sign userTypeNotBlank"></div>
                            <div id="userTypeBlank" class="icon-remove-sign userTypeBlank"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputCampus">Campus</label>
                        <div class="col-xs-5">
                            <select id="inputCampus" class="inputCampus form-control cboCampuses" data-bind="options: campuses, optionsText: 'CampusName', optionsValue: 'CampusID', optionsCaption: 'Select Campus..', value: selectedCampus">
                            </select>
                            <div id="inputCampusNotBlank" class=" icon-ok-sign inputCampusNotBlank"></div>
                            <div id="inputCampusBlank" class="icon-remove-sign inputCampusBlank"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputCelno">Cell Number</label>
                        <div class="col-xs-5">
                            <input id="inputCelno" data-bind="value: CellNo" type="tel" class="inputCelno form-control" placeholder="Cellphone Number" maxlength="10" />
                            <div id="inputCelnoNotBlank" class="icon-ok-sign inputCelnoNotBlank"></div>
                            <div id="inputCelnoBlank" class="icon-remove-sign inputCelnoBlank"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputTelno">Phone Number</label>
                        <div class="col-xs-5">
                            <input data-bind="value: TelNo" class="form-control" type="tel" id="Tel1" placeholder="Telephone Number" maxlength="10" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputFax">Fax Number</label>
                        <div class="col-xs-5">
                            <input data-bind="value: Fax" class="form-control" type="tel" id="inputFax" placeholder="Fax Number" maxlength="10" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputStreet1">Your Address</label>
                        <div class="col-xs-3">
                            <input data-bind="value: Street1" class="form-control" type="text" id="inputStreet1" placeholder="Street No" maxlength="10"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputStreet2"></label>
                        <div class=" col-xs-7">
                            <input data-bind="value: Street2" class="form-control" type="text" id="inputStreet2" placeholder="Street Name" maxlength="50"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputStreet3"></label>
                        <div class="col-xs-5">
                            <input data-bind="value: Street3" class="form-control" type="text" id="inputStreet3" placeholder="Suburb" maxlength="30"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputStreet4"></label>
                        <div class="col-xs-5">
                            <input data-bind="value: Street4" class="form-control" type="text" id="inputStreet4" placeholder="City" maxlength="30"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="inputPostalCode">Postal Code</label>
                        <div class="col-xs-3">
                            <input data-bind="value: PostalCode" class="form-control" type="text" id="inputPostalCode" placeholder="Postal Code" />
                        </div>
                    </div>
                    <input type="button" id="btnSave" class="btn btn-success" value="Save Changes" data-bind="click: save" />
                    <input type="button" id="btnCancel" class="btn btn-danger" value="Cancel" data-bind="click: cancel" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javaScript" runat="server">
    <script type="text/javascript">
        // hide the validation errors
        function HideAlerts() {
            $('#txtPasswordlength').hide();
            $('.txtVerifyStatus').hide();
            $('.icon-ok-sign').hide();
            $('.icon-remove-sign').show();
            $('.PasswordVerification').hide();
            $('#nummodelcellerror').hide();
            $('#nummodeltelerror').hide();
            $('#nummodelfaxerror').hide();
            $('#breaklinemodelcell').hide();
            $('#breaklinemodeltel').hide();
            $('#breaklinemodelfax').hide();
        }

        // Animation validations
        // Name field Validation
        $('.inputName').blur(function () {
            if ($('.inputName').val() == "") {
                $('.inputNameBlank').show();
                $('.inputNameNotBlank').hide();
            }
            else {
                $('.inputNameNotBlank').show();
                $('.inputNameBlank').hide();
            }
        });
        // surname field validation
        $('.inputSurname').blur(function () {
            if ($('.inputSurname').val() == "") {
                $('.inputSurnameBlank').show();
                $('.inputSurnameNotBlank').hide();
            }
            else {
                $('.inputSurnameNotBlank').show();
                $('.inputSurnameBlank').hide();
            }
        });
        // initials field valition
        $('.inputInitials').blur(function () {
            if ($('.inputInitials').val() == "") {
                $('.inputInitialsBlank').show();
                $('.inputInitialsNotBlank').hide();
            }
            else {
                $('.inputInitialsNotBlank').show();
                $('.inputInitialsBlank').hide();
            }
        });
        // email field validation
        $('.inputEmail').blur(function () {
            if ($('.selectedEmail').val() == "") {
                $('.inputEmailBlank').show();
                $('.inputEmailNotBlank').hide();
            }
            else {
                $('.inputEmailNotBlank').show();
                $('.inputEmailBlank').hide();
            }
        });
        // verify password function
        function VerifyPassword() {
            if ($('.txtPassword').val() != $('.txtVerifyPassword').val()) {
                $('.txtVerifyStatus').show();
            }
            else {
                $('.txtVerifyStatus').hide();
            }
        }
        // Verify password length function
        function Passwordlength() {
            if ($('.txtPassword').val().length < 6) {
                $('.txtPasswordlength').show();
            }
            else {
                $('.txtPasswordlength').hide();
            }
        }
        // pasword verification field validation
        $('.txtVerifyPassword').blur(function () {
            VerifyPassword();
        })
        // Password field validation
        $('.txtPassword').blur(function () {
            Passwordlength();
        })

         // Numerical value validation: Cell Number
        var specialKeyscell = new Array();
        specialKeyscell.push(8); //Backspace
        function IsNumericcell(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((((((keyCode >= 48 && keyCode <= 57) || keyCode >= 8 && keyCode <= 9) || keyCode >= 13 && keyCode <= 16) || keyCode >= 37 && keyCode <= 40) || keyCode >= 46 && keyCode <= 46) || specialKeyscell.indexOf(keyCode) != -1);
            document.getElementById("nummodelcellerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinemodelcell").style.display = ret ? "none" : "inline";

            return ret;
        }

        // Numerical value validation: Tel Number
        var specialKeystel = new Array();
        specialKeystel.push(8); //Backspace
        function IsNumerictel(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((((((keyCode >= 48 && keyCode <= 57) || keyCode >= 8 && keyCode <= 9) || keyCode >= 13 && keyCode <= 16) || keyCode >= 37 && keyCode <= 40) || keyCode >= 46 && keyCode <= 46) || specialKeystel.indexOf(keyCode) != -1);
            document.getElementById("nummodeltelerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinemodeltel").style.display = ret ? "none" : "inline";

            return ret;
        }

        // Numerical value validation: Fax Number
        var specialKeysfax = new Array();
        specialKeysfax.push(8); //Backspace
        function IsNumericfax(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((((((keyCode >= 48 && keyCode <= 57) || keyCode >= 8 && keyCode <= 9) || keyCode >= 13 && keyCode <= 16) || keyCode >= 37 && keyCode <= 40) || keyCode >= 46 && keyCode <= 46) || specialKeysfax.indexOf(keyCode) != -1);
            document.getElementById("nummodelfaxerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinemodelfax").style.display = ret ? "none" : "inline";

            return ret;
        }


        // knockout view model
        function viewModel() {
            var self = this;

            // Navbar variables
            // College combobox variables
            self.selectedCollege = ko.observable();
            // Campus combox variables
            self.campuses = ko.observableArray([]);
            self.selectedCampus = ko.observable();
            // Search bar variable
            self.clientSurname = ko.observable("");

            // #tblClient list variable
            self.clients = ko.observableArray([]);

            // Modal view Client edit variables
            // Personal Details
            self.UserID = ko.observable();
            self.Title = ko.observable();
            self.Initials = ko.observable();
            self.Name = ko.observable();
            self.Surname = ko.observable();
            self.Password = ko.observable();
            self.PasswordVerification = ko.observable();
            self.UserType = ko.observable();
            // CampusLink Details
            self.College = ko.observable();
            self.Campus = ko.observable();
            // Contact Details
            self.ContactDetailsID = ko.observable();
            self.Street1 = ko.observable();
            self.Street2 = ko.observable();
            self.Street3 = ko.observable();
            self.Street4 = ko.observable();
            self.PostalCode = ko.observable();
            self.Email = ko.observable();
            self.CellNo = ko.observable();
            self.TelNo = ko.observable();
            self.Fax = ko.observable();
            // Varible to add client to a booking
            self.ReferNum = ko.observable();

            // College Combobox variables
            self.cboColleges = ko.observable();
            self.selectedCboCollege = ko.observable();

            // Campus Combox variables
            self.cboCampuses = ko.observable();
            self.selectedCboCampus = ko.observable();

            // Title Combobox variables
            self.titles = ko.observableArray([]);
            self.selectedTitle = ko.observable();

            // UserTypes ComboBox variables
            self.userTypes = ko.observableArray([]);
            self.selectedType = ko.observable();

            // Qualifcation
            self.qualification = ko.observableArray();
            self.userName = ko.observable();

            self.userid = ko.observable();

            // Load initial state from server.
            // Get method to get the realated college from the loggedin user.
            $.ajax({
                type: 'GET',
                url: '../Handlers/GetCollegeIDByUserID.ashx',
                success: function (data) {
                    self.selectedCollege(data.CollegeID);
                    self.getCampus()
                },
                error: function () {
                    alert('Cannot Retrieve Colleges from server');
                }
            });

            // Get method for Campuses navbar ComboBox
            self.getCampus = function () {
                var Param = { 'Id': ko.toJSON(self.selectedCollege()) };
                alert
                $.getJSON('../Handlers/GetCampus.ashx', Param, function (data) {
                    self.campuses(data);
                });
            }

            // Get method for the clients according to searchbar campus combobox selection
            $('#cboCampus').change(function () {
                // if this function is call it wil check if the selected campus has a value or not
                if ($('#cboCampus').val() > 0) {
                    var Param = { 'Id': $('#cboCampus').val() };
                    $.getJSON("../Handlers/GetClientByCampus.ashx", Param, function (data) {
                        self.clients(data);
                    });
                }
                else {
                    self.clients('');
                }
            });

            // Get method to update the clients list in a function
            self.getClients = function () {
                var Param = { 'Id': ko.toJSON(self.selectedCampus()) };
                $.getJSON("../Handlers/GetClientByCampus.ashx", Param, function (data) {
                    self.clients(data);
                });
            }
            // Get method for the Client details
            self.getClient = function () {
                $.getJSON("../Handlers/GetClient.ashx", function (data) {
                    self.clients(data);
                });
            }

            // View button function
            // Get data specific client's data
            self.editClient = function (data) {
                self.collegeList();
                self.titleList();
                self.userTypeList();
                var Param = { 'Id': ko.toJS(data.UserID) };
                $.getJSON("../Handlers/GetClientByID.ashx", Param, function (data) {
                    // User Details
                    self.UserID(data.UserID);
                    self.Title(data.Title);
                    self.Initials(data.Initials);
                    self.Name(data.Name);
                    self.Surname(data.Surname);
                    self.Password(data.Password);
                    self.PasswordVerification(data.PasswordVerification);
                    self.UserType(data.UserType);
                    // Campus link Details
                    self.College(data.College);
                    self.Campus(data.Campus);
                    //Contact Details
                    self.ContactDetailsID(data.ContactDetailsID);
                    self.Street1(data.Street1);
                    self.Street2(data.Street2);
                    self.Street3(data.Street3);
                    self.Street4(data.Street4);
                    self.PostalCode(data.PostalCode);
                    self.Email(data.Email);
                    self.CellNo(data.CellNo);
                    self.TelNo(data.TelNo);
                    self.Fax(data.Fax);
                    // Combo box selected values
                    self.selectedCboCollege();
                    self.selectedCboCampus();
                });
            }

            // Get method for colleges combobox options
            self.collegeList = function () {
                $.ajax({
                    type: 'GET',
                    url: '../Handlers/GetCollege.ashx',
                    success: function (response) {
                        self.cboColleges(response);
                    },
                    error: function () {
                        alert('Cannot Retrieve Colleges from server');
                    }
                });
            }

            // Get method for campuses combobox options
            $('#selCollege').blur(function () {
                if ($('#selCollege').val() > 0) {
                    var Param = { 'Id': $('#selCollege').val() };
                    $.getJSON('../Handlers/GetCampus.ashx', Param, function (data) {
                        self.cboCampuses(data);
                    });
                }
            });

            // Get method for titles combobox options
            self.titleList = function () {
                $.getJSON("../Handlers/GetTitle.ashx", function (data) {
                    self.titles(data);
                });
            }

            // Get method for the Types combobox options
            self.userTypeList = function () {
                $.getJSON("../Handlers/GetUserTypes.ashx", function (data) {
                    self.userTypes(data);
                });
            }

            // View the qualifications of the selected Client
            self.viewQuals = function (data) {
                if (ko.toJS(data.UserID) > 0) {
                    self.userid(data.UserID);
                    var Param = { 'Id': ko.toJS(data.UserID) };
                    $.getJSON("../Handlers/GetClientQual.ashx", Param, function (data) {
                        self.qualification(data);
                    });
                }
                self.userName(data.Name + " " + data.Surname);
            }

            // This method shows the Client add form.
            self.addClient = function () {
                $('#clientForm').show();
                self.collegeList();
                self.titleList();
                self.userTypeList();
            }

            // Method to clear the variables values
            self.clear = function () {
                self.UserID("")
                self.Name("");
                self.Surname("");
                self.Initials("");
                self.Title("");
                self.Password("");
                self.PasswordVerification("");
                self.UserType("");
                self.selectedType("");
                // Campus link Details
                self.selectedCboCollege("");
                self.selectedCboCampus("");
                //Contact Details
                self.ContactDetailsID("");
                self.Street1("");
                self.Street2("");
                self.Street3("");
                self.Street4("");
                self.PostalCode("");
                self.Email("");
                self.CellNo("");
                self.TelNo("");
                self.Fax("");
            }

            // This method hides the edit view
            self.modalClose = function () {
                hideModalEdit();
                self.clear();
                HideAlerts();
            }

            // ajax post method to Save the new user details 
            self.save = function () {
                if (Validate()) {
                    $.ajax({
                        type: "POST",
                        url: "../Handlers/SetClientDetails.ashx",
                        data: ko.mapping.toJSON(self),
                        success: function (response, status, xhr) {
                            alert("User Registration Successful!");
                            $('#clientForm').hide();
                            self.editClient(response);
                            hideModalEdit();
                            self.getClients();
                        },
                        error: function () {
                            alert("Save Failed!");
                        }
                    });
                }
                else {
                    alert("Please make sure all required fields in filled in");
                }
            };

            // Page refresh function
            self.refresh = function () {
                window.location.reload(true);
            }

            // ajax post method to update an existing user's details
            self.upadate = function () {
                $.ajax({
                    type: "POST",
                    url: "../Handlers/SetClientDetail.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function (response, status, xhr) {
                        alert("Update is successful!");
                        hideModalEdit();
                        self.editClient(response);
                        self.getClients();
                    },
                    error: function () {
                        alert("Update Failed!");
                    }
                });
            };

            // Get method for the search text
            $('#btnsearch').click(function () {
                // If this function is called it wil check if the clientSurname is empty and call functions
                if (ko.toJS(self.clientSurname) == "") {
                    self.getClients();
                }
                else {
                    var Param = { 'SearchText': ko.toJS(self.clientSurname) };
                    $.ajax({
                        type: "GET",
                        url: "../Handlers/GetClientSurname.ashx",
                        data: Param,
                        success: function (data) {
                            self.clients(data);
                        },
                        error: function () {
                            alert("No match found");
                        }
                    });
                }
            });

            // This method Closes the add Client Forms
            self.cancel = function () {
                $('#clientForm').hide();
                self.clear();
                HideAlerts()
            }

            // function to print the report
            self.print = function (data) {
                var values = { 'value1': ko.toJSON(1), 'value2': ko.toJSON(data.UserQualID), 'value3': ko.toJSON(self.userid) };
                $.ajax({
                    type: 'POST',
                    url: "../Handlers/SetReportParam.ashx",
                    data: values,
                    success: function () {
                        alert("Report page will be opened in new page or tab");
                        window.open("ReportPage.aspx");
                    },
                    error: function () {
                        alert("Print Failed!");
                    }
                });
            }

            // Hides Client add from on load
            $('#clientForm').hide();

            // Hide all the alerts on load
            HideAlerts();

            // Validation required field functions
            // Validate Title field
            $('.cboTitle').blur(function () {
                if (ko.toJS(self.selectedTitle) > 0) {
                    $('.inputTitleNotBlank').show();
                    $('.inputTitleBlank').hide();
                }
                else {
                    $('.inputTitleBlank').show();
                    $('.inputTitleNotBlank').hide();
                }
            });
            // Validate Course code field
            $('#inputCourseCode').blur(function () {
                if (ko.toJS(self.ReferNum) != "" && ko.toJS(self.ReferNum).length == 8) {
                    $('#courseCodeIn').show();
                    $('#courseCodeEmpty').hide();
                }
                else {
                    $('#courseCodeIn').hide();
                    $('#courseCodeEmpty').show();
                }
            });
            // Validate Campus field
            $('.inputCampus').blur(function () {
                if (ko.toJS(self.selectedCampus) > 0) {
                    $('.inputCampusNotBlank').show();
                    $('.inputCampusBlank').hide();
                }
                else {

                    $('.inputCampusBlank').show();
                    $('.inputCampusNotBlank').hide();
                }
            });
            // Validate usertype field
            $('.selUserType').blur(function () {
                if (ko.toJS(self.selectedType) > 0) {
                    $('.userTypeNotBlank').show();
                    $('.userTypeBlank').hide();
                }
                else {

                    $('.inputCampusBlank').show();
                    $('.inputCampusNotBlank').hide();
                }
            });
            // Validate input Celno field
            $('.inputCelno').blur(function () {
                if (ko.toJS(self.CellNo) != "" && $('.inputCelno').val().length == 10) {
                    $('.inputCelnoNotBlank').show();
                    $('.inputCelnoBlank').hide();
                }
                else {
                    $('.inputCelnoBlank').show();
                    $('.inputCelnoNotBlank').hide();
                }
            });
            // Validate password field
            $('.txtPassword').blur(function () {
                if ($('.txtPassword').val().length > 5) {
                    $('.inputPasswordNotBlank').show();
                    $('.inputPasswordBlank').hide();
                }
                else {
                    $('.inputPasswordBlank').show();
                    $('.inputPasswordNotBlank').hide();
                }
            });
            // Validate verify password field
            $('#txtVerifyPassword').blur(function () {
                if ($('.txtVerifyPassword').val().length > 5 && $('.txtVerifyPassword').val() == $('.txtPassword').val()) {
                    $('.inputVerifyPasswordNotBlank').show();
                    $('.inputVerifyPasswordBlank').hide();
                }
                else {
                    $('.inputVerifyPasswordBlank').show();
                    $('.inputVerifyPasswordNotBlank').hide();
                }
            });

            // Modal Window Validations
            function hideModalEdit() {
                // Hides all edit fields
                $('#iptName').hide();
                $('#iptSurname').hide();
                $('#iptInitials').hide();
                $('#selTitle').hide();
                $('#iptEmail').hide();
                $('#iptPassword').hide();
                $('#sVerifyPassword').hide();
                $('#selType').hide();
                $('#iptCourseCode').hide();
                $('#selCollege').hide();
                $('#selCampus').hide();
                $('#iptCellNo').hide();
                $('#inputTelNo').hide();
                $('#inputFaxNo').hide();
                $('#inputStreetNo').hide();
                $('#inputStreetName').hide();
                $('#inputSuburb').hide();
                $('#inputCity').hide();
                $('#iptPostalCode').hide();
                // Show Normal View
                $('#sName').show();
                $('#sSurname').show();
                $('#sInitials').show();
                $('#sTitle').show();
                $('#sEmail').show();
                $('#sPassword').show();
                $('#sType').show();
                $('#AddCourscode').show();
                $('#sCollege').show();
                $('#sCampus').show();
                $('#sCellNo').show();
                $('#sTelNo').show();
                $('#sFax').show();
                $('#sStreetNo').show();
                $('#sStreetName').show();
                $('#sSuburb').show();
                $('#sCity').show();
                $('#sPostalCode').show();
            }
            // call the method mentioned above
            hideModalEdit()

            // Functions shows the edit view on click in modal view
            // Name field
            $('#btnEditName').click(function () {
                $('#sName').hide();
                $('#iptName').show();
            });
            // Surname field
            $('#btnEditSurname').click(function () {
                $('#sSurname').hide();
                $('#iptSurname').show();
            });
            // Initials field
            $('#btnEditInitials').click(function () {
                $('#sInitials').hide();
                $('#iptInitials').show();
            });
            // Title field
            $('#btnEditTitle').click(function () {
                $('#sTitle').hide();
                $('#selTitle').show();
            });
            // email field
            $('#btnEditEmail').click(function () {
                $('#sEmail').hide();
                $('#iptEmail').show();
            });
            // Password field
            $('#btnEditPassword').click(function () {
                $('#sPassword').hide();
                $('#iptPassword').show();
                $('#sVerifyPassword').show();
                $('.PasswordVerification').show();
            });
            // usertype field
            $('#btnEditUserType').click(function () {
                $('#sType').hide();
                $('#selType').show();
            });
            // add course field
            $('#btnAddCourse').click(function () {
                $('#AddCourscode').hide();
                $('#iptCourseCode').show();
            });
            // college field
            $('#btnEditCollege').click(function () {
                $('#sCollege').hide();
                $('#selCollege').show();
            });
            // Campus field
            $('#btnEditCampus').click(function () {
                $('#sCampus').hide();
                $('#selCampus').show();
            });
            // Edit Cellno
            $('#btnEditCellno').click(function () {
                $('#sCellNo').hide();
                $('#iptCellNo').show();
            });
            // telephone no field
            $('#btnEditTellno').click(function () {
                $('#sTelNo').hide();
                $('#inputTelNo').show();
            });
            // Fax no field
            $('#btnEditFax').click(function () {
                $('#sFax').hide();
                $('#inputFaxNo').show();
            });
            // Street No field
            $('#btnEditStreetNo').click(function () {
                $('#sStreetNo').hide();
                $('#inputStreetNo').show();
            });
            // Street Name field
            $('#btnEditStreetName').click(function () {
                $('#sStreetName').hide();
                $('#inputStreetName').show();
            });
            // suburb Field
            $('#btnEditSuburb').click(function () {
                $('#sSuburb').hide();
                $('#inputSuburb').show();
            });
            // City field
            $('#btnEditCity').click(function () {
                $('#sCity').hide();
                $('#inputCity').show();
            });
            // Postal Code field
            $('#btnEditPostalCode').click(function () {
                $('#sPostalCode').hide();
                $('#iptPostalCode').show();
            });

            // Data input validations
            // Check that no text field is empty
            function Validate() {
                var valid = true;

                if ($("#inputName").val() == "") {
                    valid = false;
                }

                if ($("#inputSurname").val() == "") {
                    valid = false;
                }

                if ($("#inputInitials").val() == "") {
                    valid = false;
                }

                if ($("#cboTitle").val() < 1) {
                    valid = false;
                }

                if ($("#inputEmail").val() == "") {
                    valid = false;
                }

                if ($("#txtPassword").val() == "") {
                    valid = false;
                }

                if ($("#txtVerifyPassword").val() == "") {
                    valid = false;
                }

                if ($("#txtPassword").val() != $("#txtVerifyPassword").val()) {
                    valid = false;
                }

                if ($("#inputCollege").val() < 1) {
                    valid = false;
                }

                if ($("#inputCampus").val() < 1) {
                    valid = false;
                }

                if ($("#cboUsertype").val() < 1) {
                    valid = false;
                }

                if ($("#inputCelno").val() == "") {
                    valid = false;
                }

                return valid;
            }
            // radio button variable
            self.typeToShow = ko.observable("all");
            // checkbox variable
            self.displayAdvancedOptions = ko.observable(true);


            self.clientsToShow = ko.computed(function () {
                // Represents a filtered list of clients
                // i.e., only those matching the "typeToShow" condition
                var desiredType = self.typeToShow();
                if (desiredType == "all") return self.clients();
                return ko.utils.arrayFilter(self.clients(), function (data) {
                    return data.UserType == desiredType;
                });
            }, self);

            // Animation callbacks for the clients list
            self.showClienttElement = function (elem) { if (elem.nodeType === 1) $(elem).hide().slideDown() }
            self.hideClientElement = function (elem) { if (elem.nodeType === 1) $(elem).slideUp(function () { $(elem).remove(); }) }
        };

        // Here's a custom Knockout binding that makes elements shown/hidden via jQuery's fadeIn()/fadeOut() methods
        // Could be stored in a separate utility library
        ko.bindingHandlers.fadeVisible = {
            init: function (element, valueAccessor) {
                // Initially set the element to be instantly visible/hidden depending on the value
                var value = valueAccessor();
                $(element).toggle(ko.unwrap(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
            },
            update: function (element, valueAccessor) {
                // Whenever the value subsequently changes, slowly fade the element in or out
                var value = valueAccessor();
                ko.unwrap(value) ? $(element).fadeIn() : $(element).fadeOut();
            }
        };

        ko.applyBindings(new viewModel());
    </script>
</asp:Content>
