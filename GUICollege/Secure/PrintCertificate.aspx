﻿<%@ Page Title="PrintCertficate" Language="C#" MasterPageFile="~/Secure/CollegeMaster.Master" AutoEventWireup="true" CodeBehind="PrintCertificate.aspx.cs" Inherits="GUICollege.Secure.PrintCertificate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Print Certificate</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="navbar-form navbar-left" role="search">
                <select id="cboCampus" class="form-control" data-bind="options: campuses, optionsText: 'CampusName', optionsValue: 'CampusID', optionsCaption: 'Select Campus..', value: selectedCampus"></select>
            </div>
            <div class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" id="search" class="form-control" placeholder="Search" data-bind='value: searchText' />
                </div>
                <button type="button" id="btnsearch" class="btn btn-primary">Search</button>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <%--Booking main table--%>
    <div class="scrollableTable">
        <table id="tableView" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Booking ID</th>
                    <th>Location</th>
                    <th>Qualification</th>
                    <th>Trainer</th>
                    <th>StartDate</th>
                    <th>EndDate</th>
                    <th>Referance Code</th>
                    <th></th>
                </tr>
            </thead>
            <tbody data-bind="foreach: bookings">
                <tr class="data">
                    <td data-bind="text: BookingID"></td>
                    <td>
                        <span data-bind="text: College"></span>
                        <br />
                        <span data-bind="text: Campus"></span>
                    </td>
                    <td data-bind="text: Qualification"></td>
                    <td data-bind="text: Trainer"></td>
                    <td data-bind="text: StartDate"></td>
                    <td data-bind="text: EndDate"></td>
                    <td data-bind="text: CourseCode"></td>
                    <td id="btnbookview"><a href="#myModal" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.viewUsers">View</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div>
        <!-- Modal -->
        <div id="myModal" class="modal fade modal-wide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="myModalLabel">Booking Clients&nbsp;(<span data-bind='text: clients().length'></span>)</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Initials</th>
                                    <th>Surname</th>
                                    <th>Qualification</th>
                                    <th>Description</th>
                                    <th>StartDate</th>
                                    <th>EndDate</th>
                                    <th>Referance No</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: clients">
                                <tr>
                                    <td data-bind="text: UserID"></td>
                                    <td data-bind="text: CInitials"></td>
                                    <td data-bind="text: CSurname"></td>
                                    <td data-bind="text: Qual"></td>
                                    <td data-bind="text: QualDescrip"></td>
                                    <td data-bind="text: StartDate"></td>
                                    <td data-bind="text: EndDate"></td>
                                    <td data-bind="text: ReferNum"></td>
                                    <td class="hide" data-bind="text: PrintID">2</td>
                                    <td>
                                        <input type="button" class="btn btn-danger" data-bind="click: $parent.removePrint" value="Remove" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button class="btn btn-success" data-bind="click: saveChanges">Save changes</button>
                        <input type="button" id="txtPrint" value="Print" class="btn btn-primary" data-bind="click: print" />
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javaScript" runat="server">
    <script type="text/javascript">
        function viewModel() {
            var self = this;
            // Variable that contains the PrintID Value
            self.PrintID = ko.observable("3");
            // Selected College variable
            self.selectedCollegeFilter = ko.observable();
            // Variable for the list of campuses
            self.campuses = ko.observableArray([]);
            // Selected campus variable
            self.selectedCampus = ko.observable();
            // Client Search variable
            self.searchText = ko.observable("");
            // Variable for the list of all the bookings
            self.bookings = ko.observableArray([]);
            // Client Table variable
            self.clients = ko.observableArray([]);
            // PrintList variable
            self.printedlist = ko.observableArray([]);

            // View all the users link to the booking.
            self.viewUsers = function (data) {
                if (ko.toJS(data.BookingID) > 0) {
                    var Param = { 'Id': ko.toJS(data.BookingID) };
                    $.getJSON("../Handlers/GetClientQualByBooking.ashx", Param, function (data) {
                        self.clients(data);
                    });
                }
            }

            // Get method to get the realated college from the loggedin user.
            $.ajax({
                type: 'GET',
                url: '../Handlers/GetCollegeIDByUserID.ashx',
                success: function (data) {
                    self.selectedCollegeFilter(data.CollegeID);
                    self.getCampus()
                },
                error: function () {
                    alert('Cannot Retrieve Colleges from server');
                }
            });

            // Get method for Campuses navbar ComboBox
            self.getCampus = function () {
                var Param = { 'Id': ko.toJSON(self.selectedCollegeFilter()) };
                alert
                $.getJSON('../Handlers/GetCampus.ashx', Param, function (data) {
                    self.campuses(data);
                });
            }

            // Get method for the clients according to searchbar campus combobox selection
            $('#cboCampus').change(function () {
                $("#cboCampus option:selected").each(function () {
                    if ($('#cboCampus option:selected').val() > 0) {
                        var Param = { 'Id': $('#cboCampus option:selected').val() };
                        $.getJSON("../Handlers/GetBookingByCampus.ashx", Param, function (data) {
                            self.bookings(data);
                        });
                    }
                    else {
                        self.bookings("");
                    }
                });
            });

            // Client Search by Surname
            $('#btnsearch').click(function () {
                // If this function is called it wil check if the clientSurname is empty and call functions accordingly
                if (ko.toJS(self.searchText) == "") {
                    self.access();
                }
                else {
                    var Param = { 'Search': ko.toJS(self.searchText) };
                    $.getJSON("../Handlers/GetBookingByRef.ashx", Param, function (data) {
                        self.bookings(data);
                    });
                }
            });

            // Method to remove a row from the Printlist table
            self.removePrint = function (data) {
                self.clients.destroy(data);
            }

            // Method is called to hide the edit view in die modal window
            self.modalClose = function () {
                $.ajax({
                    type: "POST",
                    url: "../Handlers/SetClientQual.ashx",
                    data: ko.mapping.toJSON(self.printedlist),
                });
            }

            // Save the Changes made to the printlist
            self.saveChanges = function () {
                $.ajax({
                    type: "POST",
                    url: "../Handlers/SetBookLink.ashx",
                    data: ko.mapping.toJSON(self.clients),
                    success: function (data) {
                    }
                });
            }

            // Method to call the form with the crystal report viewer
            self.print = function () {
                $.ajax({
                    type: "POST",
                    url: "../Handlers/SetClientQual.ashx",
                    data: ko.mapping.toJSON(self.clients),
                    success: function (data) {
                        self.printedlist(data);
                        var values = { 'value1': ko.toJSON(2), 'value2': ko.toJSON(3), 'value3': 0 };
                        $.ajax({
                            type: 'POST',
                            url: "../Handlers/SetReportParam.ashx",
                            data: values,
                            success: function () {
                                window.open("ReportPage.aspx");
                                alert("Print Preview in new tab");
                            },
                            error: function () {
                                alert("Print Failed!");
                            }
                        });
                    },
                    error: function () {
                        alert("Print Failed\nMake sure you selected a qualification\nand that there are no blank fields");
                    }
                });
            }
        }
        // Knockout binding is applied
        ko.applyBindings(new viewModel());

    </script>
</asp:Content>
