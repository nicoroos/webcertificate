﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Secure/CollegeMaster.Master" AutoEventWireup="true" CodeBehind="Booking.aspx.cs" Inherits="GUICollege.Secure.Booking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div id="main">
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Bookings</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="navbar-form navbar-nav" role="search">
                    <select id="cboCampus" class="form-control" data-bind="options: campuses, optionsText: 'CampusName', optionsValue: 'CampusID', optionsCaption: 'Select Campus..', value: selectedCampus"></select>
                </div>
                <div class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" id="txtSearch" class="form-control" placeholder="Search" data-bind="value: bookingSearch" />
                    </div>
                    <button type="button" id="btnSearch" class="btn btn-primary">Search</button>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <%-- Booking Main Table --%>
        <div class="scrollableTable">
            <table id="mainTableView" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Location</th>
                        <th>Qualification</th>
                        <th>Trainer</th>
                        <th>StartDate</th>
                        <th>EndDate</th>
                        <th>Referance Code</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: bookings">
                    <tr class="data">
                        <td data-bind="text: BookingID"></td>
                        <td>
                            <span data-bind="text: College"></span>
                            <br />
                            <span data-bind="text: Campus"></span>
                        </td>
                        <td data-bind="text: Qualification"></td>
                        <td data-bind="text: Trainer"></td>
                        <td data-bind="text: StartDate"></td>
                        <td data-bind="text: EndDate"></td>
                        <td data-bind="text: CourseCode"></td>
                        <td class="status" data-bind="text: StatusName"></td>
                        <td id="btnbookview"><a href="#myModal" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.editingBooking">View</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <input id="btnAdd" type="button" class="btn btn-primary" data-bind='click: addBooking' value="Make new Booking" />
        <%-- Table add booking --%>
        <table id="tableEdit" class="table table-hover">
            <thead>
                <tr>
                    <th>Location</th>
                    <th>Qualification</th>
                    <th>Trainer</th>
                    <th>Date</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <select class="form-control" data-bind="options: campuses, optionsText: 'CampusName', optionsValue: 'CampusID', optionsCaption: 'Select Campus...', value: selectedCampus"></select>
                    </td>
                    <td>
                        <select class="form-control" data-bind="options: qualifications, optionsText: 'QualName', optionsValue: 'QualID', optionsCaption: 'Select Qualification...', value: selectedQualification"></select></td>
                    <td>
                        <select class="form-control" data-bind="options: trainers, optionsText: 'Name', optionsValue: 'UserID', optionsCaption: 'Select Trainer...', value: selectedTrainer"></select></td>
                    <td>
                        <div class="input-append" id="dp3">
                            <input class="dates form-control" type="text" data-bind="value: SStartDate" />
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                        <div class="input-append" id="Div1">
                            <input class="dates form-control" type="text" data-bind="value: SEndDate" />
                            <span class="add-on"><i class="icon-th"></i></span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <input id="btnSave" type="button" class="btn btn-success" data-bind='click: saveBooking' value="Save Changes" />
        <input id="btnCancel" type="button" class="btn btn-danger" data-bind="click: cancel" value="Cancel" />
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-bind="click: cancelEdit" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="myModalLabel">Booking Details</h3>
                </div>
                <div class="modal-body">
                    <table class="table table-hover table-striped">
                        <tr>
                            <th>Booking ID</th>
                            <td>
                                <div class="col-xs-10"><span data-bind="text: BookingID"></span></div>
                            </td>
                        </tr>
                        <tr>
                            <th>Location</th>
                            <td>
                                <div class="col-xs-10">
                                    <span class="locationNormal" data-bind="text: College"></span>
                                    <select class="College locationEdit form-control" data-bind="options: colleges, optionsText: 'CollegeName', optionsValue: 'CollegeID', optionsCaption: 'Select College...', value: selectedCollege"></select>
                                </div>
                                <div class="col-xs-10">
                                    <span class="locationNormal" data-bind="text: Campus"></span>
                                    <select class="locationEdit form-control" data-bind="options: campuses, optionsText: 'CampusName', optionsValue: 'CampusID', optionsCaption: 'Select Campus...', value: selectedCampus"></select>
                                </div>
                                <%--<button type="button" id="btnEditlocation" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>--%>
                            </td>
                        </tr>
                        <tr>
                            <th>Qualification</th>
                            <td>
                                <div class="col-xs-10">
                                    <span class="qualNormal" data-bind="text: Qualification"></span>
                                    <select class="qualEdit form-control" data-bind="options: qualifications, optionsText: 'QualName', optionsValue: 'QualID', optionsCaption: 'Select Qualification...', value: selectedQualification"></select>
                                </div>
                                <button type="button" id="btnEditQual" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                            </td>
                        </tr>
                        <tr>
                            <th>Trainer</th>
                            <td>
                                <div class="col-xs-10">
                                    <span class="trainNormal" data-bind="text: Trainer"></span>
                                    <select class="trainEdit form-control" data-bind="options: trainers, optionsText: 'Name', optionsValue: 'UserID', optionsCaption: 'Select Trainer...', value: selectedTrainer"></select>
                                </div>
                                <%--<button type="button" id="btnEditTrainer" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>--%>
                            </td>
                        </tr>
                        <tr>
                            <th>Start Date</th>
                            <td>
                                <div class="col-xs-10">
                                    <span class="dateNormal" data-bind="text: StartDate"></span>
                                    <div class="input-append date dateEdit" id="Div2">
                                        <input class="dateEdit dates form-control" type="text" data-bind="value: StartDate" />
                                        <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                </div>
                                <button type="button" id="btnEditStartDate" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                            </td>
                        </tr>
                        <tr>
                            <th>End Date</th>
                            <td>
                                <div class="col-xs-10">
                                    <span class="dateNormal" data-bind="text: EndDate"></span>
                                    <div class="input-append date dateEdit" id="Div3">
                                        <input class="dates form-control" type="text" data-bind="value: EndDate" />
                                        <span class="add-on"><i class="icon-th"></i></span>
                                    </div>
                                </div>
                                <%--<button type="button" id="btnEditEndDate" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>--%>
                            </td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                <div class="col-xs-10">
                                    <span class="statusNormal" data-bind="text: StatusName"></span>
                                    <select class="statusEdit form-control" data-bind="options: statuses, optionsText: 'StatusName', optionsValue: 'StatusID', optionsCaption: 'Select Status...', value: selectedStatus"></select>
                                </div>
                                <%--<button type="button" id="btnEditStatus" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>--%>
                            </td>
                        </tr>
                        <tr>
                            <th>Course Code</th>
                            <td>
                                <div class="col-xs-10">
                                    <span data-bind="text: CourseCode"></span>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" data-dismiss="modal" data-bind="click: cancelEdit" aria-hidden="true">Close</button>
                    <button class="btn btn-success" data-bind="click: UpdateBooking">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javaScript" runat="server">
    <script type="text/javascript">
        // Knockout view model
        function bookingViewModel(init) {
            showLocationNormal();
            showQualNormal();
            showTrainNormal();
            showDateNormal();
            showStatusNormal();

            // Data
            var self = this;

            // NavBar Booking combobox Variables
            self.colleges = ko.observableArray();
            // Variable to store selected college value
            self.selectedCollegeFilter = ko.observable();
            // Campus list variable
            self.campuses = ko.observableArray();
            // Selected campus value
            self.selectedCampus = ko.observable();
            // NavBar SearchBar variable
            self.bookingSearch = ko.observable("");
            // MainTable list variable
            self.bookings = ko.observableArray([]);
            // Edit/view Booking variables
            self.BookingID = ko.observable();
            self.College = ko.observable();
            self.Campus = ko.observable();
            self.Qualification = ko.observable();
            self.Trainer = ko.observable();
            self.StartDate = ko.observable();
            self.EndDate = ko.observable();
            self.StatusName = ko.observable();
            self.CourseCode = ko.observable();
            // Add new booking variables
            self.bookingSave = ko.observableArray([]);
            // variables inside the saveBooking array
            self.selectedCollege = ko.observable();
            self.campuses = ko.observableArray([]);
            self.selectedCampus = ko.observable();
            self.qualifications = ko.observableArray([]);
            self.selectedQualification = ko.observable();
            self.trainers = ko.observableArray([]);
            self.selectedTrainer = ko.observable();
            self.statuses = ko.observableArray([]);
            self.selectedStatus = ko.observable();
            self.SStartDate = ko.observable();
            self.SEndDate = ko.observable();
            self.CourseCode = ko.observable();
            // Usertype variable
            self.userType = ko.observable();

            // Load initial state from server, load the a list of Colleges from server, then populate self.Colleges variable
            // Get method to get the realated college from the loggedin user.
            $.ajax({
                type: 'GET',
                url: '../Handlers/GetCollegeIDByUserID.ashx',
                success: function (data) {
                    self.selectedCollegeFilter(data.CollegeID);
                    self.getCampus()
                },
                error: function () {
                    alert('Cannot Retrieve Colleges from server');
                }
            });

            // Get method for Campuses navbar ComboBox
            self.getCampus = function () {
                var Param = { 'Id': ko.toJSON(self.selectedCollegeFilter()) };
                alert
                $.getJSON('../Handlers/GetCampus.ashx', Param, function (data) {
                    self.campuses(data);
                });
            }

            //self.searchFilter = function () {
            $('#btnSearch').click(function () {
                var Param = { 'Search': ko.toJS(self.bookingSearch) };
                $.getJSON("../Handlers/GetBookingByRef.ashx", Param, function (data) {
                    self.bookings(data);
                });
            });

            // Get a list of campuses depending on the selected college
            $('.College').blur(function () {
                if (ko.toJS(self.selectedCollege()) > 0) {
                    var Param = { 'Id': ko.toJS(self.selectedCollege()) };
                    $.getJSON("../Handlers/GetCampus.ashx", Param, function (data) {
                        self.campuses(data);
                    });
                }
            });

            // Get a list of qualifications
            $.getJSON("../Handlers/GetQualification.ashx", function (data) {
                self.qualifications(data);
            });

            // Get a list of trainers
            $.getJSON("../Handlers/GetTrainer.ashx", function (data) {
                self.trainers(data);
            });

            // edit booking assign data to variable on click
            self.editingBooking = function (data) {
                self.BookingID(data.BookingID);
                self.College(data.College);
                self.Campus(data.Campus);
                self.Qualification(data.Qualification);
                self.Trainer(data.Trainer);
                self.StatusName(data.StatusName);
                self.StartDate(data.StartDate);
                self.EndDate(data.EndDate);
                self.CourseCode(data.CourseCode);
            }

            // Function to show the edit view
            function showEdit() {
                $('#tableEdit').show();
                $('#btnSave').show();
                $('#btnCancel').show();
            }
            // Function to hide edit view
            function hideEdit() {
                $('#tableEdit').hide();
                $('#btnSave').hide();
                $('#btnCancel').hide();
                $('#btnAdd').show();
            }

            //
            $('.editView').hide();
            hideEdit();

            // Method add a new booking.
            self.addBooking = function () {
                showEdit();
                $('#btnAdd').hide();
            }

            // Hierdie function clear al die variables
            self.cancel = function () {
                hideEdit();
                self.selectedCollege("");
                self.selectedCampus("");
                self.selectedQualification("");
                self.selectedTrainer("");
                self.SStartDate("");
                self.SEndDate("");
            }

            // Remove a row from the list
            self.removeBooking = function (data) {
                self.bookings.destroy(data);
            };

            // Updates the booking
            self.UpdateBooking = function () {
                $.ajax({
                    type: "POST",
                    url: "../Handlers/SetBookingUpdate.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function (response, status, xhr) {
                        alert("Booking Successfully Updated!");
                        showLocationNormal();
                        showQualNormal();
                        showTrainNormal();
                        showDateNormal();
                        showStatusNormal();
                        $.getJSON("../Handlers/GetBooking.ashx", function (data) {
                            self.bookings(data);
                            $('.editView').hide();
                        });
                        var Param = { 'Id': ko.toJSON(self.BookingID) }
                        $.getJSON("../Handlers/GetBookingByID.ashx", Param, function (data) {
                            self.BookingID(data.BookingID);
                            self.College(data.College);
                            self.Campus(data.Campus);
                            self.Qualification(data.Qualification);
                            self.Trainer(data.Trainer);
                            self.StatusName(data.StatusName);
                            self.CourseCode(data.CourseCode);
                        });
                    },
                    error: function () {
                        alert("Save Failed!");
                    }
                });
            };

            // Save a new booking
            self.saveBooking = function () {
                $.ajax({
                    type: "POST",
                    url: "../Handlers/SetBooking.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function (response, status, xhr) {
                        alert("Booking Successful!");
                        $.getJSON("../Handlers/GetBooking.ashx", function (data) {
                            self.bookings(data);
                            hideEdit();
                            $('.editView').hide();
                        });
                    },
                    error: function () {
                        alert("Save Failed!");
                    }
                });
            };

            // Populate the Bookings according to the Campus selection
            $('#cboCampus').change(function () {
                $("#cboCampus option:selected").each(function () {
                    if ($('#cboCampus option:selected').val() > 0) {
                        var Param = { 'Id': $('#cboCampus option:selected').val() };
                        $.getJSON("../Handlers/GetBookingByCampus.ashx", Param, function (data) {
                            self.bookings(data);
                        });
                    }
                    else {
                        self.bookings("");
                    }
                });
            });

            // Get a list of statuses from the server
            $.getJSON("../Handlers/GetStatus.ashx", function (data) {
                self.statuses(data);
            });

            // Hide edit view for location BookingEdit
            $('#btnEditlocation').click(function () {
                $('.locationEdit').show();
                $('.locationNormal').hide();
            });

            // Show edit view for location BookingEdit
            function showLocationNormal() {
                $('.locationEdit').hide();
                $('.locationNormal').show();
            }

            // Hide edit view for Booking Edit Qualification
            $('#btnEditQual').click(function () {
                $('.qualEdit').show();
                $('.qualNormal').hide();
            });
            // show edit view for Booking Edit qualificaiton 
            function showQualNormal() {
                $('.qualEdit').hide();
                $('.qualNormal').show();
            }

            // Hide edit view for Booking Edit trainer
            $('#btnEditTrainer').click(function () {
                $('.trainEdit').show();
                $('.trainNormal').hide();
            });
            // show edit view for Booking Edit trainer
            function showTrainNormal() {
                $('.trainEdit').hide();
                $('.trainNormal').show();
            }

            // Hide edit view for Booking Edit status
            $('#btnEditStatus').click(function () {
                $('.statusEdit').show();
                $('.statusNormal').hide();
            });

            // show edit view for Booking Edit status
            function showStatusNormal() {
                $('.statusEdit').hide();
                $('.statusNormal').show();
            }

            // Hide edit view for Booking Edit Date
            $('#btnEditStartDate').click(function () {
                $('.dateEdit').show();
                $('.dateNormal').hide();
            });

            // Show edit view for end date
            $('#btnEditEndDate').click(function () {
                $('.dateEdit').show();
                $('.dateNormal').hide();
            });

            // show edit view for Booking Edit Date
            function showDateNormal() {
                $('.dateEdit').hide();
                $('.dateNormal').show();
            }

            // Hides the edti view.
            self.cancelEdit = function () {
                showDateNormal();
                showTrainNormal();
                showQualNormal();
                showLocationNormal();
                showStatusNormal();
            };

            // Datepicker jqueryUI function
            $(function () {
                $(".dates").datepicker({ dateFormat: "yy-mm-dd" });
            });
        }
        // Apply Knockout bindings
        ko.applyBindings(new bookingViewModel());
    </script>
</asp:Content>
