﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetBookingByID
    /// </summary>
    public class GetBookingByID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Store querystring in a variable
            var jsonData = int.Parse(context.Request["Id"]);
            // retrieves requerst from BL and sends is to ajax mehtod
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(BL.ModifyBooking.GetBookingByBookingID(jsonData)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}