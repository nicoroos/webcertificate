﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetBooking
    /// </summary>
    public class GetBooking : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Get all the college Recoreds
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(BL.ModifyBooking.GeBookingBytUserID(GUI.Security.MemberShipProvider.LoggendInUserId)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}