﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetUserTypes
    /// </summary>
    public class GetUserTypes : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(BL.ModifyUserType.GetAllCollege()));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}