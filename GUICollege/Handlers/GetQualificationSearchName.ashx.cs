﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetQualificationSearchName
    /// </summary>
    public class GetQualificationSearchName : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var jsonData = Convert.ToString(context.Request["qualname"]);
            string qualification = new JavaScriptSerializer().Serialize(BL.ModifyQualification.GetQualName(jsonData.ToUpper()));
            context.Response.ContentType = "application/json";
            context.Response.Write(qualification);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}