﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for SetClientQual
    /// </summary>
    public class SetClientQual : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();
            var postedObject = new JavaScriptSerializer().Deserialize<List<BL.BLObjects.ClientQual>>(jsonData);
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(BL.ModifyUserQual.Save(postedObject)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}