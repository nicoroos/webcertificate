﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetTrainer
    /// </summary>
    public class GetTrainer : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int client = 1;
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(BL.ModifyUser.GetUserListByTrainer(client)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}