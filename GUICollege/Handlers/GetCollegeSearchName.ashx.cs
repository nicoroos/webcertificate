﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetCollegeSearchName
    /// </summary>
    public class GetCollegeSearchName : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // the param is save in the variable and send to the BL
            var jsonData = Convert.ToString(context.Request["Id"]);
            // Request data from the BL
            string college = new JavaScriptSerializer().Serialize(BL.ModifyCollege.GetCollegeByName(jsonData.ToUpper()));
            context.Response.ContentType = "application/json";
            context.Response.Write(college);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}