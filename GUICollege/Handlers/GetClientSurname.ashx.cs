﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetClientSurname
    /// </summary>
    public class GetClientSurname : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string jsonData = Convert.ToString(context.Request["SearchText"]);
            string campus = new JavaScriptSerializer().Serialize(BL.ModifyUser.GetUserBySurnameAndUserID(jsonData.ToUpper(), GUI.Security.MemberShipProvider.LoggendInUserId));
            context.Response.ContentType = "application/json";
            context.Response.Write(campus);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}