﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetBookings
    /// </summary>
    public class GetBookings : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Get all the college Recoreds
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(BL.ModifyBooking.GetAll()));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}