﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetUserType
    /// </summary>
    public class GetUserType : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(GUI.Security.MemberShipProvider.UserInType));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}