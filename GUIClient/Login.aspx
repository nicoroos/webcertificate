﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="GUIClient.Login1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Login</title>
    <link href="Stylesheets/OldBootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link href="Stylesheets/OldBootstrap/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">Login</a>
            <ul class="nav pull-right">
                <li><a href="Registration.aspx">Register Here</a></li>
            </ul>
        </div>
    </div>
    <form id="Form1" class="form-horizontal" runat="server" role="form">
        <asp:Login ID="Login" runat="server">
            <LayoutTemplate>
                <div class="control-group">
                    <label class="control-label" for="inputEmail">Email</label>
                    <div class="controls">
                        <asp:TextBox ID="username" runat="server" TextMode="Email"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="inputPassword">Password</label>
                    <div class="controls">
                        <asp:TextBox ID="password" runat="server" TextMode="Password"></asp:TextBox>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <label class="checkbox">
                            <input type="checkbox">
                            Remember me
                        </label>
                        <asp:Button ID="LoginButton" runat="server" Text="Login" CommandName="Login" ValidationGroup="Login" CssClass="btn btn-primary" />
                    </div>
                </div>
            </LayoutTemplate>
        </asp:Login>
    </form>
</body>
</html>
