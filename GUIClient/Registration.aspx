﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="GUIClient.Registration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Registration</title>
    <link href="Stylesheets/bootstrap.css" rel="stylesheet" />
    <link href="Stylesheets/bootstrap.min.css" rel="stylesheet" />
    <link href="Stylesheets/bootstrap-theme.css" rel="stylesheet" />
    <link href="Stylesheets/bootstrap-theme.min.css" rel="stylesheet" />
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Registration</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-right" role="search">
                <button type="button" class="btn btn-primary" data-bind="click: login">Login</button>
                <button type="button" class="btn btn-success" data-bind="click: save">Register</button>
            </form>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <%--Registration form--%>
    <form class="form-horizontal" id="form1" runat="server">
        <div class="row center-block">
            <div class="form-group">
                <label class="col-xs-3 control-label" for="inputTitle">Title</label>
                <div class="col-xs-3">
                    <select id="cboTitle" class="form-control" data-bind="options: titles, optionsText: 'TitleName', optionsValue: 'TitleID', optionsCaption: 'Select a Title..', value: selectedTitle"></select>
                    <div id="inputTitleNotBlank" class="icon-ok-sign"></div>
                    <div id="inputTitleBlank" class="icon-remove-sign"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="inputSurname">Surname</label>
                <div class="col-xs-7">
                    <input type="text" id="inputSurname" class="form-control" data-bind="value: Surname" placeholder="Surname" />
                    <div id="inputSurnameNotBlank" class="icon-ok-sign"></div>
                    <div id="inputSurnameBlank" class="icon-remove-sign"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="inputInitials">Initials</label>
                <div class="col-xs-2">
                    <input data-bind="value: Initials" class="form-control" type="text" id="inputInitials" placeholder="Initials" />
                    <div id="inputInitialsNotBlank" class=" icon-ok-sign"></div>
                    <div id="inputInitialsBlank" class="icon-remove-sign"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="inputName">Name</label>
                <div class="col-xs-5">
                    <input type="text" id="inputName" class="form-control" data-bind="value: Name" placeholder="Name" />
                    <div id="inputNameNotBlank" class="icon-ok-sign"></div>
                    <div id="inputNameBlank" class="icon-remove-sign"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="inputEmail">Email</label>
                <div class="col-xs-5">
                    <input data-bind="value: Email" class="form-control" type="email" id="inputEmail" placeholder="Email" />
                    <div id="inputEmailNotBlank" class="icon-ok-sign"></div>
                    <div id="inputEmailBlank" class="icon-remove-sign"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="inputPassword">Password</label>
                <div class="col-xs-5">
                    <input data-bind="value: Password" class="form-control" type="password" id="txtPassword" placeholder="Password" />
                    <div id="inputPasswordNotBlank" class="icon-ok-sign"></div>
                    <div id="inputPasswordBlank" class="icon-remove-sign"></div>
                    <div id="txtPasswordlength" class="alert alert-danger">Password must be 6 characters or more!</div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="inputVerifyPassword">Password Verification</label>
                <div class="col-xs-5">
                    <input data-bind="value: PasswordVerification" class="form-control" type="password" id="txtVerifyPassword" placeholder="Password Verification" />
                    <div id="inputVerifyPasswordNotBlank" class=" icon-ok-sign"></div>
                    <div id="inputVerifyPasswordBlank" class="icon-remove-sign"></div>
                    <div id="txtVerifyStatus" class="alert alert-danger">Password Do not Match!</div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="inputCollege">College</label>
                <div class="col-xs-4">
                    <select id="inputCollege" class="form-control" data-bind="options: colleges, optionsText: 'CollegeName', optionsValue: 'CollegeID', optionsCaption: 'Select College..', value: selectedCollege">
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="inputCampus">Campus</label>
                <div class="col-xs-4">
                    <select id="inputCampus" class="form-control" data-bind="options: campuses, optionsText: 'CampusName', optionsValue: 'CampusID', optionsCaption: 'Select Campus..', value: selectedCampus">
                    </select>
                    <div id="inputCampusNotBlank" class=" icon-ok-sign"></div>
                    <div id="inputCampusBlank" class="icon-remove-sign"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="inputCelno">Cellphone Number</label>
                <div class="col-xs-5">
                    <input data-bind="value: CellNo" class="form-control" type="tel" id="inputCelno" placeholder="Cellphone Number" maxlength="10" />
                    <div id="inputCelnoNotBlank" class="icon-ok-sign"></div>
                    <div id="inputCelnoBlank" class="icon-remove-sign"></div>
                </div>
            </div>
        </div>
    </form>
    <script src="Scripts/Jquery.js"></script>
    <script src="Scripts/Knockout_3.0.0.js"></script>
    <script src="Scripts/knockout.mapping-latest_2.4.1.js"></script>
    <script src="Scripts/bootstrap.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script type="text/javascript">

        // Hide all validation alerts functions
        function HideAlerts() {
            $('#txtPasswordlength').hide();
            $('#txtVerifyStatus').hide();
            $('.icon-ok-sign').hide();
        }
        // function to verify password
        function VerifyPassword() {
            if ($('#txtPassword').val() != $('#txtVerifyPassword').val()) {
                $('#txtVerifyStatus').show();
            }
            else {
                $('#txtVerifyStatus').hide();
            }
        }
        // Function to check the password length
        function Passwordlength() {
            if ($('#txtPassword').val() < 6) {
                $('#txtPasswordlength').show();
            }
            else {
                $('#txtPasswordlength').hide();
            }
        }
        // Method to call the verify password method
        $('#txtVerifyPassword').blur(function () {
            VerifyPassword();
        })
        // Method to call the password length method
        $('#txtPassword').blur(function () {
            Passwordlength();
        })
        // Method to show required field name is empty
        $('#inputName').blur(function () {
            if ($('#inputName').val() == "") {
                $('#inputNameBlank').show();
                $('#inputNameNotBlank').hide();
            }
            else {
                $('#inputNameNotBlank').show();
                $('#inputNameBlank').hide();
            }
        });
        // Method to show required field surname is empty
        $('#inputSurname').blur(function () {
            if ($('#inputSurname').val() == "") {
                $('#inputSurnameBlank').show();
                $('#inputSurnameNotBlank').hide();
            }
            else {
                $('#inputSurnameNotBlank').show();
                $('#inputSurnameBlank').hide();
            }
        });
        // Method to show required field initials is empty
        $('#inputInitials').blur(function () {
            if ($('#inputInitials').val() == "") {
                $('#inputInitialsBlank').show();
                $('#inputInitialsNotBlank').hide();
            }
            else {
                $('#inputInitialsNotBlank').show();
                $('#inputInitialsBlank').hide();
            }
        });
        // Method to show required field email is empty
        $('#inputEmail').blur(function () {
            if ($('#selectedEmail').val() == "") {
                $('#inputEmailBlank').show();
                $('#inputEmailNotBlank').hide();
            }
            else {
                $('#inputEmailNotBlank').show();
                $('#inputEmailBlank').hide();
            }
        });

        // Knockout viewModel
        function viewModel() {
            var self = this;

            // Personal Details
            self.ClientID = ko.observable();
            self.Name = ko.observable();
            self.Surname = ko.observable();
            self.Initials = ko.observable();
            self.Title = ko.observable();
            self.Email = ko.observable();
            self.Password = ko.observable();
            self.PasswordVerification = ko.observable();
            self.selectedType = ko.observable(6);

            //Contact Details
            self.CampusID = ko.observable();
            self.ContactDetailsID = ko.observable();
            self.Street1 = ko.observable();
            self.Street2 = ko.observable();
            self.Street3 = ko.observable();
            self.Street4 = ko.observable();
            self.PostalCode = ko.observable();
            self.CellNo = ko.observable();
            self.TelNo = ko.observable();
            self.Fax = ko.observable();

            self.titles = ko.observableArray([]);
            self.selectedTitle = ko.observable();

            self.colleges = ko.observableArray([]);
            self.selectedCollege = ko.observable();

            self.campuses = ko.observableArray([]);
            self.selectedCampus = ko.observable();

            // Get method for titles
            $.getJSON("../Handlers/GetTitle.ashx", function (data) {
                self.titles(data);
            });

            // Get method for Colleges
            $.getJSON('../Handlers/GetCollege.ashx', function (data) {
                self.colleges(data);
            });

            // Get method for Campuses combobox
            $('#inputCollege').blur(function () {
                if (ko.toJS(self.selectedCollege) > 0) {
                    var Param = { 'Id': ko.toJSON(self.selectedCollege) };
                    $.getJSON('../Handlers/GetCampus.ashx', Param, function (data) {
                        self.campuses(data);
                    });
                }
            });

            // ajax Save mehtod 
            self.save = function () {
                if (Validate()) {
                    $.ajax({
                        type: "POST",
                        url: "../Handlers/SetClientDetails.ashx",
                        data: ko.mapping.toJSON(self),
                        success: function (response, status, xhr) {
                            alert("Sign up Successful!");
                            window.location = "Login.aspx";
                        },
                        error: function () {
                            alert("Registration Failed!");
                        }
                    });
                }
                else {
                    alert("Please make sure all required fields in filled in");
                }
            };

            // Go to login Page
            self.login = function () {
                window.location = "Login.aspx";
            };

            // method is called to hide all alerts
            HideAlerts();

            // Validation functions
            // Check if the fields is blank.
            // Tilte field
            $('#cboTitle').blur(function () {
                if (ko.toJS(self.selectedTitle) > 0) {
                    $('#inputTitleNotBlank').show();
                    $('#inputTitleBlank').hide();
                }
                else {
                    $('#inputTitleBlank').show();
                    $('#inputTitleNotBlank').hide();
                }
            });
            // Campus field
            $('#inputCampus').blur(function () {
                if (ko.toJS(self.selectedCampus) > 0) {
                    $('#inputCampusNotBlank').show();
                    $('#inputCampusBlank').hide();
                }
                else {

                    $('#inputCampusBlank').show();
                    $('#inputCampusNotBlank').hide();
                }
            });
            // Celno field
            $('#inputCelno').blur(function () {
                if (ko.toJS(self.CellNo) != "" && $('#inputCelno').val().length == 10) {
                    $('#inputCelnoNotBlank').show();
                    $('#inputCelnoBlank').hide();
                }
                else {
                    $('#inputCelnoBlank').show();
                    $('#inputCelnoNotBlank').hide();
                }
            });
            // Password field
            $('#txtPassword').blur(function () {
                if ($('#txtPassword').val().length > 5) {
                    $('#inputPasswordNotBlank').show();
                    $('#inputPasswordBlank').hide();
                }
                else {
                    $('#inputPasswordBlank').show();
                    $('#inputPasswordNotBlank').hide();
                }
            });
            // Verify password field
            $('#txtVerifyPassword').blur(function () {
                if ($('#txtVerifyPassword').val().length > 5 && $('#txtVerifyPassword').val() == $('#txtPassword').val()) {
                    $('#inputVerifyPasswordNotBlank').show();
                    $('#inputVerifyPasswordBlank').hide();
                }
                else {
                    $('#inputVerifyPasswordBlank').show();
                    $('#inputVerifyPasswordNotBlank').hide();
                }
            });

            //data input validations
            //Check that no text field is empty
            function Validate() {
                var valid = true;

                if ($("#inputName").val() == "") {
                    valid = false;
                }

                if ($("#inputSurname").val() == "") {
                    valid = false;
                }

                if ($("#inputInitials").val() == "") {
                    valid = false;
                }

                if ($("#cboTitle").val() < 1) {
                    valid = false;
                }

                if ($("#inputEmail").val() == "") {
                    valid = false;
                }

                if ($("#txtPassword").val() == "") {
                    valid = false;
                }

                if ($("#txtVerifyPassword").val() == "") {
                    valid = false;
                }

                if ($("#inputCollege").val() < 1) {
                    valid = false;
                }

                if ($("#inputCampus").val() < 1) {
                    valid = false;
                }

                if ($("#inputCelno").val() == "") {
                    valid = false;
                }

                return valid;
            }
        }
        // Knockout bindings is applied 
        ko.applyBindings(new viewModel());
    </script>
</body>
</html>
