﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

namespace GUI.Secure
{
    public partial class ReportPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ReportDocument BooksReport = new ReportDocument();
            BooksReport.Load(Server.MapPath("~/Reports/ClientWeb.rpt"));
            BooksReport.SetDataSource(ds.Tables["ClientWeb"]);
            CrystalReportViewer1.ReportSource = BooksReport;
            BooksReport.SetDatabaseLogon("sa", "P@ssw0rd");
            BooksReport.SetParameterValue("@SELECTED", GUI.Handlers.SetReportParam.selected);
            BooksReport.SetParameterValue("@VALUE1", GUI.Handlers.SetReportParam.value1);
            BooksReport.SetParameterValue("@VALUE2", GUI.Handlers.SetReportParam.value2);
            CrystalReportViewer1.DataBind();
        }
    }
}