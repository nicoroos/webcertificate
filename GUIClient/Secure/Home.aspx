﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Secure/ClientMaster.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="GUIClient.Secure.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            <a class="navbar-brand" href="#">Home</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <div>
        <h4>Welcome <span data-bind="text: Name"></span>
        </h4>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javaScript" runat="server">
    <script type="text/javascript">
        function viewModel() {
            var self = this;
            self.Name = ko.observableArray([]);

            // get method for the Client own details
            self.getClient = function () {
                $.getJSON("../Handlers/GetClient.ashx", function (data) {
                    self.Name(data.Name);
                });
            }
            self.getClient();
        }
        ko.applyBindings(new viewModel());
    </script>
</asp:Content>
