﻿<%@ Page Title="User" Language="C#" MasterPageFile="~/Secure/ClientMaster.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="GUIClient.Secure.User" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div>
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Users</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <br />
        <%-- Main Table with list of Clients --%>
        <div>
            <table id="tblClient" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Initials</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: clients">
                    <tr>
                        <td data-bind="text: UserID"></td>
                        <td data-bind="text: Title"></td>
                        <td data-bind="text: Initials"></td>
                        <td data-bind="text: Name"></td>
                        <td data-bind="text: Surname"></td>
                        <td><a href="#myModal" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.editClient">View</a>
                            <a href="#myModal2" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.viewQuals">History</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- Modal View - Client Edit -->
        <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="myModalLabel">Client Details</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr>
                                <th>Name</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sName" data-bind="text: Name"></span>
                                        <div id="iptName">
                                            <input class="form-control inputName" data-bind="value: Name" type="text" placeholder="Name" />
                                            <div id="nameIn" class="icon-ok-sign in inputNameNotBlank"></div>
                                            <div id="nameEmpty" class="icon-remove-sign inputNameBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditName" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Surname</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sSurname" data-bind="text: Surname"></span>
                                        <div id="iptSurname">
                                            <input class="form-control inputSurname" data-bind="value: Surname" type="text" placeholder="Surname" />
                                            <div id="surnameIn" class="icon-ok-sign inputSurnameNotBlank"></div>
                                            <div id="surnameEmpty" class="icon-remove-sign inputSurnameBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditSurname" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Initials</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sInitials" data-bind="text: Initials"></span>
                                        <div id="iptInitials">
                                            <input class="form-control inputInitials" data-bind="value: Initials" type="text" placeholder="Initials" />
                                            <div id="initIn" class=" icon-ok-sign inputInitialsNotBlank"></div>
                                            <div id="initEmpty" class="icon-remove-sign inputInitialsBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditInitials" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Title</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sTitle" data-bind="text: Title"></span>
                                        <div id="selTitle">
                                            <select class="cboTitle form-control" data-bind="options: titles, optionsText: 'TitleName', optionsValue: 'TitleID', optionsCaption: 'Select a Title..', selectedOptions: selectedTitle, value: selectedTitle"></select>
                                            <div id="titleIn" class="icon-ok-sign inputTitleNotBlank"></div>
                                            <div id="titleEmpty" class="icon-remove-sign inputTitleBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditTitle" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sEmail" data-bind="text: Email"></span>
                                        <div id="iptEmail">
                                            <input class=" form-control inputEmail" data-bind="value: Email" type="email" placeholder="Email" />
                                            <div id="emailIn" class="icon-ok-sign inputEmailNotBlank"></div>
                                            <div id="emailEmpty" class="icon-remove-sign inputEmailBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditEmail" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Password</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sPassword" data-bind="text: Password"></span>
                                        <div id="iptPassword">
                                            <input class="form-control txtPassword" data-bind="value: Password" type="password" placeholder="Password" />
                                            <div id="passIn" class="icon-ok-sign inputVerifyPasswordNotBlank"></div>
                                            <div id="passEmpty" class="icon-remove-sign inputVerifyPasswordBlank"></div>
                                            <div id="passWrong" class="alert alert-danger txtPasswordlength">Password must be 6 characters or more!</div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditPassword" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr class="PasswordVerification">
                                <th>Verify Password</th>
                                <td>
                                    <div class="col-xs-10">
                                        <input class="form-control txtVerifyPassword" data-bind="value: PasswordVerification" type="password" placeholder="Password Verification" />
                                        <div id="pass2In" class=" icon-ok-sign"></div>
                                        <div id="pass2Empty" class="icon-remove-sign"></div>
                                        <div id="pass2Wrong" class="alert alert-danger txtVerifyStatus">Password Do not Match!</div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>User type</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sType" data-bind="text: UserType"></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Course Code</th>
                                <td>
                                    <div class="col-xs-10">
                                        <a href="#" id="AddCourscode">Add a Course Code</a>
                                        <div id="iptCourseCode">
                                            <input id="inputCourseCode" class="form-control " data-bind="value: ReferNum" type="text" placeholder="CourseCode" />
                                            <div id="courseCodeIn" class=" icon-ok-sign"></div>
                                            <div id="courseCodeEmpty" class="icon-remove-sign"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnAddCourse" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Location</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCollege" data-bind="text: College"></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCampus" data-bind="text: Campus"></span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Cellphone Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCellNo" data-bind="text: CellNo"></span>
                                        <div id="iptCellNo">
                                            <input class="form-control inputCelno" data-bind="value: CellNo" type="tel" placeholder="Cellphone Number" maxlength="10" onkeypress="return IsNumericcell(event);" ondrop="return false;"/>
                                            <div id="cellIn" class="icon-ok-sign inputCelnoNotBlank"></div>
                                            <div id="CellEmpty" class="icon-remove-sign inputCelnoBlank"></div>
                                        <br id="breaklinemodelcell" />
                                            <div id="nummodelcellerror" class="alert alert-danger">
                                                <a href="specialKeyscell" class="alert-link">Input digits (0 - 9)</a>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditCellno" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Telephone Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sTelNo" data-bind="text: TelNo"></span>
                                        <input id="inputTelNo" class="form-control inputTelNo " data-bind="value: TelNo" type="tel" placeholder="Telephone Number" maxlength="10" onkeypress="return IsNumerictel(event);" ondrop="return false;"/>
                                    <br id="breaklinemodeltel" />
                                            <div id="nummodeltelerror" class="alert alert-danger">
                                                <a href="specialKeystel" class="alert-link">Input digits (0 - 9)</a>
                                            </div>
                                    </div>
                                    <button type="button" id="btnEditTellno" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Fax Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sFax" data-bind="text: Fax"></span>
                                        <input id="inputFaxNo" class="form-control " data-bind="value: Fax" type="tel" placeholder="Fax Number" maxlength="10" onkeypress="return IsNumericfax(event);" ondrop="return false;"/>
                                   <br id="breaklinemodelfax" />
                                            <div id="nummodelfaxerror" class="alert alert-danger">
                                                <a href="specialKeysfax" class="alert-link">Input digits (0 - 9)</a>
                                            </div>
                                         </div>
                                    <button type="button" id="btnEditFax" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Residential Address</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sStreetNo" data-bind="text: Street1"></span>
                                        <input id="inputStreetNo" class="form-control " data-bind="value: Street1" type="text" placeholder="Street No" />
                                    </div>
                                    <button type="button" id="btnEditStreetNo" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sStreetName" data-bind="text: Street2"></span>
                                        <input id="inputStreetName" class="form-control " data-bind="value: Street2" type="text" placeholder="Street Name" />
                                    </div>
                                    <button type="button" id="btnEditStreetName" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sSuburb" data-bind="text: Street3"></span>
                                        <input id="inputSuburb" class="form-control " data-bind="value: Street3" type="text" placeholder="Suburb" />
                                    </div>
                                    <button type="button" id="btnEditSuburb" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCity" data-bind="text: Street4"></span>
                                        <input id="inputCity" class="form-control " data-bind="value: Street4" type="text" placeholder="City" />
                                    </div>
                                    <button type="button" id="btnEditCity" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Postal Code</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sPostalCode" data-bind="text: PostalCode"></span>
                                        <input id="iptPostalCode" class="form-control " data-bind="value: PostalCode" type="text" placeholder="Postal Code" maxlength="10"/>
                                    </div>
                                    <button type="button" id="btnEditPostalCode" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button class="btn btn-success" data-bind="click: upadate">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- Modal View - Client history -->
        <div id="myModal2" class="modal fade modal-wide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="H1">Qualifications&nbsp;(<span data-bind='text: qualification().length'></span>)</h3>
                        <p>for <span data-bind="text: userName"></span></p>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>Client Qual ID</th>
                                    <th>Qualification</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Print Status</th>
                                    <th>CourseCode</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: qualification">
                                <tr>
                                    <td data-bind="text: UserQualID"></td>
                                    <td data-bind="text: Qual"></td>
                                    <td data-bind="text: StartDate"></td>
                                    <td data-bind="text: EndDate"></td>
                                    <td data-bind="text: PrintStatus"></td>
                                    <td data-bind="text: ReferNum"></td>
                                    <td class="clientHide">
                                        <input type="button" class="btn btn-inverse" data-bind="click: $parent.print" value="Print" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="javaScript" runat="server">
    <script type="text/javascript">
        // Validation functions
        // This function hides the alerts
        function HideAlerts() {
            $('#txtPasswordlength').hide();
            $('.txtVerifyStatus').hide();
            $('.icon-ok-sign').hide();
            $('.icon-remove-sign').show();
            $('.PasswordVerification').hide();
            $('#nummodelcellerror').hide();
            $('#nummodeltelerror').hide();
            $('#nummodelfaxerror').hide();
            $('#breaklinemodelcell').hide();
            $('#breaklinemodeltel').hide();
            $('#breaklinemodelfax').hide();
        }

        // Animation validations
        // Name field Validation
        $('.inputName').blur(function () {
            if ($('.inputName').val() == "") {
                $('.inputNameBlank').show();
                $('.inputNameNotBlank').hide();
            }
            else {
                $('.inputNameNotBlank').show();
                $('.inputNameBlank').hide();
            }
        });
        // surname field Validation
        $('.inputSurname').blur(function () {
            if ($('.inputSurname').val() == "") {
                $('.inputSurnameBlank').show();
                $('.inputSurnameNotBlank').hide();
            }
            else {
                $('.inputSurnameNotBlank').show();
                $('.inputSurnameBlank').hide();
            }
        });
        // Initial field Validation
        $('.inputInitials').blur(function () {
            if ($('.inputInitials').val() == "") {
                $('.inputInitialsBlank').show();
                $('.inputInitialsNotBlank').hide();
            }
            else {
                $('.inputInitialsNotBlank').show();
                $('.inputInitialsBlank').hide();
            }
        });
        // Email field Validation
        $('.inputEmail').blur(function () {
            if ($('.selectedEmail').val() == "") {
                $('.inputEmailBlank').show();
                $('.inputEmailNotBlank').hide();
            }
            else {
                $('.inputEmailNotBlank').show();
                $('.inputEmailBlank').hide();
            }
        });
        // Function to VerifyPassword
        function VerifyPassword() {
            if ($('.txtPassword').val() != $('.txtVerifyPassword').val()) {
                $('.txtVerifyStatus').show();
            }
            else {
                $('.txtVerifyStatus').hide();
            }
        }
        // Function to check the password length
        function Passwordlength() {
            if ($('.txtPassword').val().length < 6) {
                $('.txtPasswordlength').show();
            }
            else {
                $('.txtPasswordlength').hide();
            }
        }
        // Verify Password field Validation
        $('.txtVerifyPassword').blur(function () {
            VerifyPassword();
        })
        // Password field Validation
        $('.txtPassword').blur(function () {
            Passwordlength();
        })

        // Numerical value validation: Cell Number
        var specialKeyscell = new Array();
        specialKeyscell.push(8); //Backspace
        function IsNumericcell(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((((((keyCode >= 48 && keyCode <= 57) || keyCode >= 8 && keyCode <= 9) || keyCode >= 13 && keyCode <= 16) || keyCode >= 37 && keyCode <= 40) || keyCode >= 46 && keyCode <= 46) || specialKeyscell.indexOf(keyCode) != -1);
            document.getElementById("nummodelcellerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinemodelcell").style.display = ret ? "none" : "inline";

            return ret;
        }

        // Numerical value validation: Tel Number
        var specialKeystel = new Array();
        specialKeystel.push(8); //Backspace
        function IsNumerictel(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((((((keyCode >= 48 && keyCode <= 57) || keyCode >= 8 && keyCode <= 9) || keyCode >= 13 && keyCode <= 16) || keyCode >= 37 && keyCode <= 40) || keyCode >= 46 && keyCode <= 46) || specialKeystel.indexOf(keyCode) != -1);
            document.getElementById("nummodeltelerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinemodeltel").style.display = ret ? "none" : "inline";

            return ret;
        }

        // Numerical value validation: Fax Number
        var specialKeysfax = new Array();
        specialKeysfax.push(8); //Backspace
        function IsNumericfax(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((((((keyCode >= 48 && keyCode <= 57) || keyCode >= 8 && keyCode <= 9) || keyCode >= 13 && keyCode <= 16) || keyCode >= 37 && keyCode <= 40) || keyCode >= 46 && keyCode <= 46) || specialKeysfax.indexOf(keyCode) != -1);
            document.getElementById("nummodelfaxerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinemodelfax").style.display = ret ? "none" : "inline";

            return ret;
        }


        // Knockout viewmodel
        function viewModel() {
            var self = this;

            // #tblClient list variable
            self.clients = ko.observableArray([]);

            // Modal view Client edit variables
            // Personal Details
            self.UserID = ko.observable();
            self.Title = ko.observable();
            self.Initials = ko.observable();
            self.Name = ko.observable();
            self.Surname = ko.observable();
            self.Password = ko.observable();
            self.PasswordVerification = ko.observable();
            self.UserType = ko.observable();
            // CampusLink Details
            self.College = ko.observable();
            self.Campus = ko.observable();
            // Contact Details
            self.ContactDetailsID = ko.observable();
            self.Street1 = ko.observable();
            self.Street2 = ko.observable();
            self.Street3 = ko.observable();
            self.Street4 = ko.observable();
            self.PostalCode = ko.observable();
            self.Email = ko.observable();
            self.CellNo = ko.observable();
            self.TelNo = ko.observable();
            self.Fax = ko.observable();
            // Varible to add client to a booking
            self.ReferNum = ko.observable();

            // College Combobox variables
            self.cboColleges = ko.observable();
            self.selectedCboCollege = ko.observable();

            // Campus Combox variables
            self.cboCampuses = ko.observable();
            self.selectedCboCampus = ko.observable();

            // Title Combobox variables
            self.titles = ko.observableArray([]);
            self.selectedTitle = ko.observable();

            // UserTypes ComboBox variables
            self.userTypes = ko.observableArray([]);
            self.selectedType = ko.observable();

            // Qualifcation
            self.qualification = ko.observableArray();
            self.userName = ko.observable();


            self.userid = ko.observable();

            // Load initial state from server.
            // Get method for the Client details
            self.getClient = function () {
                $.getJSON("../Handlers/GetClient.ashx", function (data) {
                    self.clients(data);
                });
            }
            self.getClient();

            // View button function
            // Get data specific client's data
            self.editClient = function (data) {
                self.collegeList();
                self.titleList();
                self.userTypeList();
                var Param = { 'Id': ko.toJS(data.UserID) };
                $.getJSON("../Handlers/GetClientByID.ashx", Param, function (data) {
                    // User Details
                    self.UserID(data.UserID);
                    self.Title(data.Title);
                    self.Initials(data.Initials);
                    self.Name(data.Name);
                    self.Surname(data.Surname);
                    self.Password(data.Password);
                    self.PasswordVerification(data.PasswordVerification);
                    self.UserType(data.UserType);
                    // Campus link Details
                    self.College(data.College);
                    self.Campus(data.Campus);
                    //Contact Details
                    self.ContactDetailsID(data.ContactDetailsID);
                    self.Street1(data.Street1);
                    self.Street2(data.Street2);
                    self.Street3(data.Street3);
                    self.Street4(data.Street4);
                    self.PostalCode(data.PostalCode);
                    self.Email(data.Email);
                    self.CellNo(data.CellNo);
                    self.TelNo(data.TelNo);
                    self.Fax(data.Fax);
                    // Combo box selected values
                    self.selectedCboCollege();
                    self.selectedCboCampus();
                    //self.selectedType()
                    //self.selectedTitle();

                });
            }

            // Get method for colleges combobox options
            self.collegeList = function () {
                $.ajax({
                    type: 'GET',
                    url: '../Handlers/GetCollege.ashx',
                    success: function (response) {
                        self.cboColleges(response);
                    },
                    error: function () {
                        alert('Cannot Retrieve Colleges from server');
                    }
                });
            }

            // Get method for campuses combobox options
            $('#selCollege').blur(function () {
                if ($('#selCollege').val() > 0) {
                    var Param = { 'Id': $('#selCollege').val() };
                    $.getJSON('../Handlers/GetCampus.ashx', Param, function (data) {
                        self.cboCampuses(data);
                    });
                }
            });

            // Get method for titles combobox options
            self.titleList = function () {
                $.getJSON("../Handlers/GetTitle.ashx", function (data) {
                    self.titles(data);
                });
            }

            // Get method for the Types combobox options
            self.userTypeList = function () {
                $.getJSON("../Handlers/GetUserTypes.ashx", function (data) {
                    self.userTypes(data);
                });
            }

            // View the qualifications of the selected Client
            self.viewQuals = function (data) {
                if (ko.toJS(data.UserID) > 0) {
                    self.userid(data.UserID);
                    var Param = { 'Id': ko.toJS(data.UserID) };
                    $.getJSON("../Handlers/GetClientQual.ashx", Param, function (data) {
                        self.qualification(data);
                    });
                }
                self.userName(data.Name + " " + data.Surname);
            }
                
            // Clears the fields
            self.clear = function () {
                self.UserID("")
                self.Name("");
                self.Surname("");
                self.Initials("");
                self.Title("");
                self.Password("");
                self.PasswordVerification("");
                self.UserType("");
                self.selectedType("");
                // Campus link Details
                self.selectedCboCollege("");
                self.selectedCboCampus("");
                //Contact Details
                self.ContactDetailsID("");
                self.Street1("");
                self.Street2("");
                self.Street3("");
                self.Street4("");
                self.PostalCode("");
                self.Email("");
                self.CellNo("");
                self.TelNo("");
                self.Fax("");
            }


            // This method hides the edit input and show the normal view
            self.modalClose = function () {
                hideModalEdit();
                self.clear();
                HideAlerts();
            }

            // Page refresh function
            self.refresh = function () {
                window.location.reload(true);
            }

            // ajax post method to update an existing user's details
            self.upadate = function () {
                $.ajax({
                    type: "POST",
                    url: "../Handlers/SetClientDetail.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function (response, status, xhr) {
                        alert("Update is successful!");
                        hideModalEdit();
                        self.editClient(response);
                        self.getClient();
                    },
                    error: function () {
                        alert("Update Failed!");
                    }
                });
            };

            // function to print the report
            self.print = function (data) {
                var values = { 'value1': ko.toJSON(1), 'value2': ko.toJSON(data.UserQualID), 'value3': ko.toJSON(self.userid) };
                $.ajax({
                    type: 'POST',
                    url: "../Handlers/SetReportParam.ashx",
                    data: values,
                    success: function () {
                        alert("Report page will be opened in new page or tab");
                        window.open("ReportPage.aspx");
                    },
                    error: function () {
                        alert("Print Failed!");
                    }
                });
            }

            // Hide all the alerts on load
            HideAlerts();

            // Validation mark functions
            $('.cboTitle').blur(function () {
                if (ko.toJS(self.selectedTitle) > 0) {
                    $('.inputTitleNotBlank').show();
                    $('.inputTitleBlank').hide();

                }
                else {
                    $('.inputTitleBlank').show();
                    $('.inputTitleNotBlank').hide();
                }
            });

            $('#inputCourseCode').blur(function () {
                if (ko.toJS(self.ReferNum) != "" && ko.toJS(self.ReferNum).length == 8) {
                    $('#courseCodeIn').show();
                    $('#courseCodeEmpty').hide();
                }
                else {
                    $('#courseCodeIn').hide();
                    $('#courseCodeEmpty').show();
                }
            });

            //$('.inputCampus').blur(function () {
            //    if (ko.toJS(self.selectedCampus) > 0) {
            //        $('.inputCampusNotBlank').show();
            //        $('.inputCampusBlank').hide();
            //    }
            //    else {

            //        $('.inputCampusBlank').show();
            //        $('.inputCampusNotBlank').hide();
            //    }
            //});

            //$('.selUserType').blur(function () {
            //    if (ko.toJS(self.selectedType) > 0) {
            //        $('.userTypeNotBlank').show();
            //        $('.userTypeBlank').hide();
            //    }
            //    else {

            //        $('.inputCampusBlank').show();
            //        $('.inputCampusNotBlank').hide();
            //    }
            //});

            $('.inputCelno').blur(function () {
                if (ko.toJS(self.CellNo) != "" && $('.inputCelno').val().length == 10) {
                    $('.inputCelnoNotBlank').show();
                    $('.inputCelnoBlank').hide();
                }
                else {
                    $('.inputCelnoBlank').show();
                    $('.inputCelnoNotBlank').hide();
                }
            });

            $('.txtPassword').blur(function () {
                if ($('.txtPassword').val().length > 5) {
                    $('.inputPasswordNotBlank').show();
                    $('.inputPasswordBlank').hide();
                }
                else {
                    $('.inputPasswordBlank').show();
                    $('.inputPasswordNotBlank').hide();
                }
            });

            $('#txtVerifyPassword').blur(function () {
                if ($('.txtVerifyPassword').val().length > 5 && $('.txtVerifyPassword').val() == $('.txtPassword').val()) {
                    $('.inputVerifyPasswordNotBlank').show();
                    $('.inputVerifyPasswordBlank').hide();
                }
                else {
                    $('.inputVerifyPasswordBlank').show();
                    $('.inputVerifyPasswordNotBlank').hide();
                }
            });

            // Modal Window Validations
            function hideModalEdit() {
                // Hides all edit fields
                $('#iptName').hide();
                $('#iptSurname').hide();
                $('#iptInitials').hide();
                $('#selTitle').hide();
                $('#iptEmail').hide();
                $('#iptPassword').hide();
                $('#sVerifyPassword').hide();
                $('#selType').hide();
                $('#iptCourseCode').hide();
                $('#selCollege').hide();
                $('#selCampus').hide();
                $('#iptCellNo').hide();
                $('#inputTelNo').hide();
                $('#inputFaxNo').hide();
                $('#inputStreetNo').hide();
                $('#inputStreetName').hide();
                $('#inputSuburb').hide();
                $('#inputCity').hide();
                $('#iptPostalCode').hide();
                // Show Normal View
                $('#sName').show();
                $('#sSurname').show();
                $('#sInitials').show();
                $('#sTitle').show();
                $('#sEmail').show();
                $('#sPassword').show();
                $('#sType').show();
                $('#AddCourscode').show();
                $('#sCollege').show();
                $('#sCampus').show();
                $('#sCellNo').show();
                $('#sTelNo').show();
                $('#sFax').show();
                $('#sStreetNo').show();
                $('#sStreetName').show();
                $('#sSuburb').show();
                $('#sCity').show();
                $('#sPostalCode').show();
            }
            // call the method mentioned above
            hideModalEdit()

            // Functions shows the edit view on click in modal view
            // Show the edit view for Name Field
            $('#btnEditName').click(function () {
                $('#sName').hide();
                $('#iptName').show();
            });
            // Show the edit view for surnamae Field
            $('#btnEditSurname').click(function () {
                $('#sSurname').hide();
                $('#iptSurname').show();
            });
            // Show the edit view for initials Field
            $('#btnEditInitials').click(function () {
                $('#sInitials').hide();
                $('#iptInitials').show();
            });
            // Show the edit view for title Field
            $('#btnEditTitle').click(function () {
                $('#sTitle').hide();
                $('#selTitle').show();
            });
            // Show the edit view for email Field
            $('#btnEditEmail').click(function () {
                $('#sEmail').hide();
                $('#iptEmail').show();
            });
            // Show the edit view for Password Field
            $('#btnEditPassword').click(function () {
                $('#sPassword').hide();
                $('#iptPassword').show();
                $('#sVerifyPassword').show();
                $('.PasswordVerification').show();
            });
            // Show the edit view for addcourse Field
            $('#btnAddCourse').click(function () {
                $('#AddCourscode').hide();
                $('#iptCourseCode').show();
            });
            // Show the edit view for Celno Field
            $('#btnEditCellno').click(function () {
                $('#sCellNo').hide();
                $('#iptCellNo').show();
            });
            // Show the edit view for telno Field
            $('#btnEditTellno').click(function () {
                $('#sTelNo').hide();
                $('#inputTelNo').show();
            });
            // Show the edit view for fax Field
            $('#btnEditFax').click(function () {
                $('#sFax').hide();
                $('#inputFaxNo').show();
            });
            // Show the edit view for streetno Field
            $('#btnEditStreetNo').click(function () {
                $('#sStreetNo').hide();
                $('#inputStreetNo').show();
            });
            // Show the edit view for Streetname Field
            $('#btnEditStreetName').click(function () {
                $('#sStreetName').hide();
                $('#inputStreetName').show();
            });
            // Show the edit view for suburb Field
            $('#btnEditSuburb').click(function () {
                $('#sSuburb').hide();
                $('#inputSuburb').show();
            });
            // Show the edit view for City Field
            $('#btnEditCity').click(function () {
                $('#sCity').hide();
                $('#inputCity').show();
            });
            // Show the edit view for postalcode Field
            $('#btnEditPostalCode').click(function () {
                $('#sPostalCode').hide();
                $('#iptPostalCode').show();
            });
            // Data input validations
            // Check that no text field is empty
            function Validate() {
                var valid = true;

                if ($("#inputName").val() == "") {
                    valid = false;
                }

                if ($("#inputSurname").val() == "") {
                    valid = false;
                }

                if ($("#inputInitials").val() == "") {
                    valid = false;
                }

                if ($("#cboTitle").val() < 1) {
                    valid = false;
                }

                if ($("#inputEmail").val() == "") {
                    valid = false;
                }

                if ($("#txtPassword").val() == "") {
                    valid = false;
                }

                if ($("#txtVerifyPassword").val() == "") {
                    valid = false;
                }

                if ($("#txtPassword").val() != $("#txtVerifyPassword").val()) {
                    valid = false;
                }

                if ($("#inputCollege").val() < 1) {
                    valid = false;
                }

                if ($("#inputCampus").val() < 1) {
                    valid = false;
                }

                if ($("#cboUsertype").val() < 1) {
                    valid = false;
                }

                if ($("#inputCelno").val() == "") {
                    valid = false;
                }

                return valid;
            }
        }
        //Knockout binding is applied
        ko.applyBindings(new viewModel());
    </script>
</asp:Content>