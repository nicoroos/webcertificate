﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetClientByCampus
    /// </summary>
    public class GetClientByCampus : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var jsonData = int.Parse(context.Request["Id"]);
            string client = new JavaScriptSerializer().Serialize(BL.ModifyUser.GetUserByCampID(jsonData));
            context.Response.ContentType = "application/json";
            context.Response.Write(client);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}