﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetCampus
    /// </summary>
    public class GetCampus : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            // Store querystring in a variable
            var jsonData = int.Parse(context.Request["Id"]);
            // retrieves requerst from BL and sends is to ajax mehtod
            string campus = new JavaScriptSerializer().Serialize(BL.ModifyCampus.GetCampusByCollegeID(jsonData));
            context.Response.ContentType = "application/json";
            context.Response.Write(campus);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}