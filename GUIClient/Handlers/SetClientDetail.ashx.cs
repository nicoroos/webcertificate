﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for SetClientDetail
    /// </summary>
    public class SetClientDetail : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();
            var postedObject = new JavaScriptSerializer().Deserialize<BL.BLObjects.ClientDetailUpdate>(jsonData);
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(BL.ModifyUserDetails.Update(postedObject)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}