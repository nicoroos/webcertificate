﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace GUI.Handlers
{
    /// <summary>
    /// Summary description for GetClientQual
    /// </summary>
    public class GetClientQual : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var jsonData = int.Parse(context.Request["Id"]);
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(BL.ModifyUserQual.GetID(jsonData)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}