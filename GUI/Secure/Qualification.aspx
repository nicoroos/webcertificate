﻿<%@ Page Title="Qualification - Coltech Online Certificates" Language="C#" MasterPageFile="~/Secure/Secure.Master" AutoEventWireup="true" CodeBehind="Qualification.aspx.cs" Inherits="GUI.Secure.AddQualification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="server">
    <div id="main">
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Qualification List</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" id="search" class="form-control" placeholder="Search" data-bind='value: searchQualName' />
                    </div>
                    <button type="button" id="btnsearch" class="btn btn-primary">Search</button>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <%--Qualification Main Table--%>
        <div class="scrollableTable">
            <table id="tblQualification" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>Qualification Name</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: qualifications">
                    <tr>
                        <td class="hide" data-bind="text: QUAL_QualID"></td>
                        <td data-bind="text: QUAL_Name"></td>
                        <td data-bind="text: QUAL_Description"></td>
                        <td>
                            <a href="#qualEdit" role="button" id="btnEditQual" class="btn btn-primary" data-toggle="modal" data-bind="click: $parent.editQual">View</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <a href="#saveQual" role="button" id="btnAddQual" class="btn btn-primary" data-toggle="modal" data-bind="click: addQual">Add Qualification</a>

        <!-- Modal View - Client Edit -->
        <div id="qualEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEdit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="myModalLabelEdit">Qualification Details</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr>
                                <th>Qualifiication</th>
                                <td>
                                    <div class="col-xs-9">
                                        <span id="sQualifiication" data-bind="text: QUAL_Name"></span>
                                        <input id="iptQualifiication" class="form-control" data-bind="value: QUAL_Name" type="text" placeholder="Qualification Name" />
                                    </div>
                                    <button type="button" id="btnEditQualifiication" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Qualification Decription</th>
                                <td>
                                    <div class="col-xs-9">
                                        <span id="sQualDecript" data-bind="text: QUAL_Description"></span>
                                       <input id="iptQualDecript" class="form-control" data-bind="value: QUAL_Description" type="text" placeholder="Qualification Decription" />
                                    </div>
                                    <button type="button" id="btnEditQualDecript" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button class="btn btn-success" data-bind="click: saveQual">Save Changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Modal View - Add Qual -->
        <div id="saveQual" class="modal fadef" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="myModalLabelAdd">Add New Qualification</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr id="trQual">
                                <th>Qualification</th>
                                <td>
                                    <div class="col-xs-10">
                                        <div id="iptQual">
                                            <input class="form-control" data-bind="value: QUAL_Name" type="text" placeholder="Qualification Name" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trQualDes">
                                <th>Qualification Decription</th>
                                <td>
                                    <div class="col-xs-10">
                                        <div id="iptQualDes">
                                            <input class="form-control" data-bind="value: QUAL_Description" type="text" placeholder="Qualification Decription" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button class="btn btn-success" data-bind="click: saveQual">Save Changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScript" runat="server">
    <script type="text/javascript">
        function QualificationListModel() {
            var self = this;

            // Qualification Search bar variable
            self.searchQualName = ko.observable();
            // Qualificatin list variable
            self.qualifications = ko.observableArray([]);
            // Qualification Edit variables
            self.QUAL_QualID = ko.observable();
            self.QUAL_Name = ko.observable();
            self.QUAL_Description = ko.observable();

            // Search results
            $('#btnsearch').click(function () {
                var Param = { 'QualName': ko.toJS(self.searchQualName) };
                $.getJSON("../Secure/Handlers/GetQualByName.ashx", Param, function (data) {
                    self.qualifications(data);
                    self.sortQualByName();
                });
            });

            // Get list of Qualifications
            self.getQualification = function () {
                $.ajax({
                    type: 'GET',
                    url: "../Secure/Handlers/GetQualifications.ashx",
                    success: function (data) {
                        self.qualifications(data);
                        self.sortQualByName();
                    }
                });
            }
            // Executes above function
            self.getQualification();

            // Remove the table row 
            self.editQual = function (data) {
                self.QUAL_QualID(data.QUAL_QualID);
                self.QUAL_Name(data.QUAL_Name);
                self.QUAL_Description(data.QUAL_Description);
                self.hideEdit();
            };

            self.modalClose = function () {
                self.hideEdit();
                self.clear();
            }

            self.clear = function () {
                self.QUAL_QualID(0);
                self.QUAL_Name("");
                self.QUAL_Description("");
            }

            self.addQual = function () {
                self.clear();
            }

            self.hideEdit = function () {
                $('#iptQualifiication').hide();
                $('#iptQualDecript').hide();
                $('#sQualifiication').show();
                $('#sQualDecript').show();
            }

            $('#btnEditQualifiication').click(function () {
                $('#sQualifiication').hide();
                $('#iptQualifiication').show();
            });

            $('#btnEditQualDecript').click(function () {
                $('#sQualDecript').hide();
                $('#iptQualDecript').show();
            });

            // This function saves all changes made to the list of qualifications
            self.saveQual = function () {
                $.ajax({
                    type: "POST",
                    url: "../Secure/Handlers/SetQualifications.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function (response, status, xhr) {
                        self.getQualification();
                        self.hideEdit();
                        self.searchQualName("");
                        alert("Saved Successful!");
                    }
                });
            };

            self.updateQual = function () {
                $.ajax({
                    type: "POST",
                    url: "../Secure/Handlers/SetQualifications.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function (response, status, xhr) {
                        self.getQualification();
                        self.hideEdit();
                        self.searchQualName("");
                        alert("Saved Successful!");
                    }
                });
            }

            // Sort function for qualification list
            self.sortQualByName = function () {
                self.qualifications.sort(function (left, right) {
                    return left.QUAL_Name == right.QUAL_Name ? 0 : (left.QUAL_Name < right.QUAL_Name ? -1 : 1)
                });
            }
        }

        // Knockout bindings is applied here
        ko.applyBindings(new QualificationListModel());

    </script>
</asp:Content>
