﻿<%@ Page Title="Bookings - Coltech Online Certificates" Language="C#" MasterPageFile="~/Secure/Secure.Master" AutoEventWireup="true" CodeBehind="Booking.aspx.cs" Inherits="GUI.Secure.Booking" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="server">
    <div id="main">
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Bookings</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <select id="cboCollege" class="form-control" data-bind="options: colleges, optionsText: 'COL_Name', optionsValue: 'COL_CollegeID', optionsCaption: 'Select College..', value: selectedCollegeFilter"></select>
                    </div>
                </div>
                <div class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" id="txtSearch" class="form-control" placeholder="Search RCC Code" data-bind="value: bookingSearch" />
                    </div>
                    <button type="button" id="btnSearch" class="btn btn-primary">Search</button>
                    <button type="button" id="btnClearFilters" class="btn btn-danger" data-bind="click: clearFilter">Clear Filters</button>
                </div>
                <div class="navbar-form navbar-nav" role="search">
                    <p data-bind='fadeVisible: displayAdvancedOptions'>
                        <label>
                            <input type='radio' name="type" value='all' data-bind='checked: typeToShow' />All</label>
                        <label>
                            <input type='radio' name="type" value='1' data-bind='checked: typeToShow' />Done</label>
                        <label>
                            <input type='radio' name="type" value='2' data-bind='checked: typeToShow' />Pending</label>
                        <label>
                            <input type='radio' name="type" value='3' data-bind='checked: typeToShow' />Cancelled</label>
                        <label>
                            <input type='radio' name="type" value='4' data-bind='checked: typeToShow' />Confirmed</label>
                        <label>
                            <input type='radio' name="type" value='5' data-bind='checked: typeToShow' />Printed</label>
                        <label>
                            <input type='radio' name="type" value='5' data-bind='checked: typeToShow' />Send</label>
                    </p>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <%-- Main Booking Table --%>
        <div class="scrollableTable">
            <table id="mainTableView" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Location</th>
                        <th>Qualification</th>
                        <th>Trainer</th>
                        <th>StartDate</th>
                        <th>EndDate</th>
                        <th>RCC Code</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody class="scrollableWindow" data-bind="template: { foreach: bookingsToShow, beforeRemove: hideBooking, afterAdd: showBooking }">

                    <tr class="data" data-bind='css: Coloring'>
                        <td data-bind="text: BOOK_BookingID"></td>
                        <td>
                            <span data-bind="text: COL_Name"></span>
                            <br />
                            <span data-bind="text: CAMP_Name"></span>
                        </td>
                        <td data-bind="text: QUAL_Name"></td>
                        <td data-bind="text: USER_Name"></td>
                        <td data-bind="text: BOOK_StartDate"></td>
                        <td data-bind="text: BOOK_EndDate"></td>
                        <td data-bind="text: CourseCode"></td>
                        <td data-bind=" text: STATUS_StatusName"></td>
                        <td id="btnbookview"><a href="#modalEditBooking" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.editBooking">View</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <a href="#modalAddBooking" id="btnAddBooking" role="button" class="btn btn-primary" data-toggle="modal" data-bind="click: clear">Make new Booking</a>

        <%-- Modal View - Add booking --%>
        <div id="modalAddBooking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-bind="click: modalClose" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="H1">Add New Booking</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr>
                                <th>Location</th>
                                <td>
                                    <div class="col-xs-10">
                                        <select class="cboCollege form-control" data-bind="options: colleges, optionsText: 'COL_Name', optionsValue: 'COL_CollegeID', optionsCaption: 'Select College...', value: selectedCollege"></select>
                                    </div>
                                    <div class="col-xs-10">
                                        <select class="form-control" data-bind="options: campuses, optionsText: 'CAMP_Name', optionsValue: 'CAMP_CampusID', optionsCaption: 'Select Campus...', value: CAMP_CampusID"></select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Qualification</th>
                                <td>
                                    <div class="col-xs-10">
                                        <select class="form-control" data-bind="options: qualifications, optionsText: 'QUAL_Name', optionsValue: 'QUAL_QualID', optionsCaption: 'Select Qualification...', value: QUAL_QualID"></select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Trainer</th>
                                <td>
                                    <div class="col-xs-10">
                                        <select class="form-control" data-bind="options: trainers, optionsText: 'USER_Name', optionsValue: 'USER_UserID', optionsCaption: 'Select Trainer...', value: USER_UserID"></select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Start Date</th>
                                <td>
                                    <div class="col-xs-10">
                                        <div class="input-append" id="Div5">
                                            <input class="dates form-control" type="text" data-bind="value: BOOK_StartDate" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>End Date</th>
                                <td>
                                    <div class="col-xs-10">
                                        <div class="input-append" id="Div6">
                                            <input class="dates form-control" type="text" data-bind="value: BOOK_EndDate" />
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <th>Order Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <div class="input-append" >
                                            <input class="form-control" type="text" data-bind="value: orderNumber" />
                                        </div>
                                    </div>
                                </td>
                            </tr>

                           <tr>
                                <th>Invoice Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <div class="input-append" >
                                            <input class="form-control" type="text" data-bind="value: invoiceNumber" />
                                        </div>
                                    </div>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" data-bind="click: modalClose" aria-hidden="true">Close</button>
                        <button id="btnSave" type="button" class="btn btn-success" data-bind='click: saveBooking'>Save</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- Modal View - Edit Booking -->
        <div id="modalEditBooking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-bind="click: modalClose" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="myModalLabel">Booking Details</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr>
                                <th>Booking ID</th>
                                <td>
                                    <div class="col-xs-10"><span data-bind="text: BOOK_BookingID"></span></div>
                                </td>
                            </tr>
                            <tr>
                                <th>Location</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span class="locationNormal" data-bind="text: COL_Name"></span>
                                        <select class="cboCollege locationEdit form-control" data-bind="options: colleges, optionsText: 'COL_Name', optionsValue: 'COL_CollegeID', optionsCaption: 'Select College...', value: selectedCollege"></select>
                                    </div>
                                    <div class="col-xs-10">
                                        <span class="locationNormal" data-bind="text: CAMP_Name"></span>
                                        <select class="locationEdit form-control" data-bind="options: campuses, optionsText: 'CAMP_Name', optionsValue: 'CAMP_CampusID', optionsCaption: 'Select Campus...', value: selectedCampus"></select>
                                    </div>
                                    <button type="button" id="btnEditlocation" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Qualification</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span class="qualNormal" data-bind="text: QUAL_Name"></span>
                                        <select class="qualEdit form-control" data-bind="options: qualifications, optionsText: 'QUAL_Name', optionsValue: 'QUAL_QualID', optionsCaption: 'Select Qualification...', value: QUAL_QualID"></select>
                                    </div>
                                    <button type="button" id="btnEditQual" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Trainer</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span class="trainNormal" data-bind="text: USER_Name"></span>
                                        <select class="trainEdit form-control" data-bind="options: trainers, optionsText: 'USER_Name', optionsValue: 'USER_UserID', optionsCaption: 'Select Trainer...', value: USER_UserID"></select>
                                    </div>
                                    <button type="button" id="btnEditTrainer" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Start Date</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span class="dateNormal" data-bind="text: BOOK_StartDate"></span>
                                        <div class="input-append date dateEdit" id="Div2">
                                            <input class="dateEdit dates form-control" type="text" data-bind="value: BOOK_StartDate" />
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditStartDate" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>End Date</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span class="dateNormal" data-bind="text: BOOK_EndDate"></span>
                                        <div class="input-append date dateEdit" id="Div3">
                                            <input class="dates form-control" type="text" data-bind="value: BOOK_EndDate" />
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditEndDate" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <th>Order Number</th>
                                <td>
                                    <div class="col-xs-10">
                                       <span class="orderNormal" data-bind="text: orderNumber"></span>
                                        <div class="input-append orderEdit">
                                            <input class="form-control" type="text" data-bind="value: orderNumber" />
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                        
                                    </div>
                                    <button type="button" id="btnEditOrder" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            

                            <tr>
                                <th>Status</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span class="statusNormal" data-bind="text: STATUS_StatusName"></span>
                                        <select class="statusEdit form-control" data-bind="options: statuses, optionsText: 'STATUS_StatusName', optionsValue: 'STATUS_StatusID', optionsCaption: 'Select Status...', value: selectedStatus"></select>
                                    </div>
                                    <button type="button" id="btnEditStatus" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>

                            <tr>
                                <th>Invoice Number</th>
                                <td>
                                    <div class="col-xs-10">
                                       <span class="invoiceNormal" data-bind="text: invoiceNumber"></span>
                                        <div class="input-append invoiceEdit">
                                            <input class="form-control " type="text" data-bind="value: invoiceNumber" />
                                            <span class="add-on"><i class="icon-th"></i></span>
                                        </div>
                                        
                                    </div>
                                    <button type="button" id="btnEditInvoice" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            
                            <tr>
                                <th>Course Code</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span data-bind="text: CourseCode"></span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" data-bind="click: modalClose" aria-hidden="true">Close</button>
                        <button class="btn btn-success" data-bind="click: updateBooking">Save Changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScript" runat="server">
    <script type="text/javascript">
        // Knockout viewmodel
        function bookingViewModel(init) {
            showLocationNormal();
            hideQualEdit();
            showTrainNormal();
            showDateNormal();
            showStatusNormal();
            showOrderNormal();
            showInvoiceNormal();

            // Data
            var self = this;
            // NavBar Booking combobox Variables
            self.colleges = ko.observableArray([]);
            self.selectedCollegeFilter = ko.observable();
            // NavBar SearchBar variable
            self.bookingSearch = ko.observable("");
            // MainTable list variable
            self.bookings = ko.observableArray([]);
            // Edit/view Booking variables
            self.BOOK_BookingID = ko.observable();
            self.COL_Name = ko.observable();
            self.CAMP_Name = ko.observable();
            self.QUAL_Name = ko.observable();
            self.USER_Name = ko.observable();
            self.BOOK_StartDate = ko.observable();
            self.BOOK_EndDate = ko.observable();
            self.STATUS_StatusName = ko.observable();
            self.CourseCode = ko.observable();
            self.orderNumber = ko.observable();
            self.invoiceNumber = ko.observable();
            // add new booking variables
            self.selectedCollege = ko.observable();
            self.campuses = ko.observableArray([]);
            self.selectedCampus = ko.observable();
            self.CAMP_CampusID = ko.observable();
            self.qualifications = ko.observableArray([]);
            self.QUAL_QualID = ko.observable();
            self.trainers = ko.observableArray([]);
            self.selectedTrainer = ko.observable();
            self.USER_UserID = ko.observable();
            self.statuses = ko.observableArray([]);
            self.selectedStatus = ko.observable();
            self.BOOK_StartDate = ko.observable();
            self.BOOK_EndDate = ko.observable();
            self.CourseCode = ko.observable();
            self.orderNumber = ko.observable();
            self.invoiceNumber = ko.observable();
            // Usertype variable
            self.userType = ko.observable();

            // Load initial state from server
            // Get a list of Colleges from server, then populate self.Colleges variable
            $.ajax({
                type: "GET",
                url: "../Secure/Handlers/GetColleges.ashx",
                success: function (data) {
                    self.colleges(data);
                    self.sortCollegeByName();
                }
            });

            // Get a list of bookings from the server
            self.getBooking = function () {
                $.getJSON("../Secure/Handlers/GetBookings.ashx", function (data) {
                    self.bookings(data);
                    self.sortBookingByDate();
                });
            };
            // Calls the method above
            self.getBooking();

            // populate the table data acoording to the navbar dropdown selection
            $('#cboCollege').change(function () {
                $("#cboCollege option:selected").each(function () {
                    if ($('#cboCollege option:selected').val() > 0) {
                        var Param = { 'CollegeID': $('#cboCollege option:selected').val() };
                        $.getJSON("../Secure/Handlers/GetBookingByCampus.ashx", Param, function (data) {
                            self.bookings(data);
                            self.sortBookingByDate();
                        });
                    }
                    else {
                        self.getBooking();
                    }
                });
            });

            // Navbar searchbar function
            $('#btnSearch').click(function () {
                var Param = { 'Search': ko.toJS(self.bookingSearch) };
                $.getJSON("../Secure/Handlers/GetBookingByRCC.ashx", Param, function (data) {
                    self.bookings(data);
                    self.sortBookingByDate();
                });
            });

            // Get a list of campuses depending on the selected college
            $('.cboCollege').blur(function () {
                if (ko.toJS(self.selectedCollege()) > 0) {
                    var Param = { 'CollegeID': ko.toJS(self.selectedCollege()) };
                    $.getJSON("../Secure/Handlers/GetCampuses.ashx", Param, function (data) {
                        self.campuses(data);
                        self.sortCampusByName();
                    });
                }
            });

            // Get a list of qualifications
            $.getJSON("../Secure/Handlers/GetQualifications.ashx", function (data) {
                self.qualifications(data);
                self.sortQualByName();
            });

            // Get a list of trainers
            $.getJSON("../Secure/Handlers/GetTrainers.ashx", function (data) {
                self.trainers(data);
                self.sortUserBySurname();
            });

            // Clear the search filters
            self.clearFilter = function () {
                self.selectedCollegeFilter("");
                self.bookingSearch("");
                self.getBooking();
            }

            // edit booking assign data to variable on click
            self.editBooking = function (data) {
                self.BOOK_BookingID(data.BOOK_BookingID);
                self.COL_Name(data.COL_Name);
                self.CAMP_Name(data.CAMP_Name);
                self.QUAL_Name(data.QUAL_Name);
                self.USER_Name(data.USER_Name);
                self.STATUS_StatusName(data.STATUS_StatusName);
                self.BOOK_StartDate(data.BOOK_StartDate);
                self.BOOK_EndDate(data.BOOK_EndDate);
                self.CourseCode(data.CourseCode);
                self.orderNumber(data.orderNumber);
                self.invoiceNumber(data.invoiceNumber);
            }

            //
            $('.editView').hide();

            // Clear some of the add form variables and hide the add form
            self.clear = function () {
                self.selectedCollege("");
                self.CAMP_CampusID("");
                self.QUAL_QualID("");
                self.USER_UserID("");
                self.BOOK_StartDate("");
                self.BOOK_EndDate("");
                self.orderNumber("");
                self.invoiceNumber("");
            }

            // Remove a row from the list
            self.removeBooking = function (data) {
                self.bookings.destroy(data);
            };

            // Updates the booking
            self.updateBooking = function () {
                $.ajax({
                    type: "POST",
                    url: "../Secure/Handlers/SetBooking.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function (response, status, xhr) {
                        alert("Booking Successfully Updated!");
                        showLocationNormal();
                        hideQualEdit();
                        showTrainNormal();
                        showDateNormal();
                        showStatusNormal();
                        showOrderNormal();
                        showInvoiceNormal();
                        var Param = { 'BookingID': ko.toJSON(self.BOOK_BookingID) }
                        $.getJSON("../Secure/Handlers/GetBookingByID.ashx", Param, function (data) {
                            self.BOOK_BookingID(data.BOOK_BookingID);
                            self.COL_Name(data.COL_Name);
                            self.CAMP_Name(data.CAMP_Name);
                            self.QUAL_Name(data.QUAL_Name);
                            self.USER_Name(data.USER_Name);
                            self.STATUS_StatusName(data.STATUS_StatusName);
                            self.BOOK_StartDate(data.BOOK_StartDate);
                            self.BOOK_EndDate(data.BOOK_EndDate);
                            self.CourseCode(data.CourseCode);
                            self.orderNumber(data.orderNumber);
                            self.invoiceNumber(data.invoiceNumber);
                        });
                        self.getBooking();
                    },
                    error: function () {
                        alert("Save Failed!");
                    }
                });
            };

            // Save the Changes to the DB
            self.saveBooking = function () {
                $.ajax({
                    type: "POST",
                    url: "../Secure/Handlers/SetBooking.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function (response, status, xhr) {
                        alert("Booking Successful!");
                        $.getJSON("../Secure/Handlers/GetBookings.ashx", function (data) {
                            self.bookings(data);

                            self.getBooking();
                            $('.editView').hide();
                        });
                        self.clear();
                    },
                    error: function () {
                        alert("Save Failed!");
                    }
                });
            };

            // Cancel the edit view when the modal window is closed
            self.modalClose = function () {
                showDateNormal();
                showTrainNormal();
                hideQualEdit();
                showLocationNormal();
                showStatusNormal();
                showOrderNormal();
                showInvoiceNormal();
                self.clear();
            };

            // Get a list of statuses from the DB
            $.getJSON("../Secure/Handlers/GetStatuses.ashx", function (data) {
                self.statuses(data);
            });

            // Hide edit view for location BookingEdit
            $('#btnEditlocation').click(function () {
                $('.locationEdit').show();
                $('.locationNormal').hide();
            });
            // show edit view for location BookingEdit
            function showLocationNormal() {
                $('.locationEdit').hide();
                $('.locationNormal').show();
            }

            // Hide edit view on edit Booking screen for Qualifications
            $('#btnEditQual').click(function () {
                $('.qualEdit').show();
                $('.qualNormal').hide();
            });
            // Show edit viewon edit Booking screen for qualificaiton
            function hideQualEdit() {
                $('.qualEdit').hide();
                $('.qualNormal').show();
            }

            // Hide edit view on edit Booking screen for trainer
            $('#btnEditTrainer').click(function () {
                $('.trainEdit').show();
                $('.trainNormal').hide();
            });
            // Show edit view on edit Booking screen for trainer
            function showTrainNormal() {
                $('.trainEdit').hide();
                $('.trainNormal').show();
            }

            // SHow edit view on edit Booking screen for status
            $('#btnEditStatus').click(function () {
                $('.statusEdit').show();
                $('.statusNormal').hide();
            });

            // Show edit view on edit Booking screen for Booking Edit status
            function showStatusNormal() {
                $('.statusEdit').hide();
                $('.statusNormal').show();
            }

            // Hide edit view on edit Booking screen for Date
            $('#btnEditStartDate').click(function () {
                $('.dateEdit').show();
                $('.dateNormal').hide();
            });

            // Show edit view on edit Booking screen for date
            $('#btnEditEndDate').click(function () {
                $('.dateEdit').show();
                $('.dateNormal').hide();
            });

            // Show edit view on edit Booking screen for Date
            function showDateNormal() {
                $('.dateEdit').hide();
                $('.dateNormal').show();
            }

            // Hide edit view on edit Booking for Order Number
            $('#btnEditOrder').click(function () {
                $('.orderEdit').show();
                $('.orderNormal').hide();
            });

            // Show edit view on edit Booking screen for Order Number
            function showOrderNormal() {
                $('.orderEdit').hide();
                $('.orderNormal').show();
            }

            // Hide edit view on edit Bookings for Invoice screen
            $('#btnEditInvoice').click(function () {
                $('.invoiceEdit').show();
                $('.invoiceNormal').hide();
            });

            // Show edit view on edit Booking screen for Invoice Number
            function showInvoiceNormal() {
                $('.invoiceEdit').hide();
                $('.invoiceNormal').show();
            }

            // Datepicker jqueryUI function
            $(function () {
                $(".dates").datepicker({ dateFormat: "yy-mm-dd" });
            });

            // Sort function
            self.sortBookingByDate = function () {
                self.bookings.sort(function (left, right) {
                    return left.BOOK_StartDate == right.BOOK_StartDate ? 0 : (left.BOOK_StartDate > right.BOOK_StartDate ? -1 : 1)
                });
            }
            // Sort function for college list
            self.sortCollegeByName = function () {
                self.colleges.sort(function (left, right) {
                    return left.COL_Name == right.COL_Name ? 0 : (left.COL_Name < right.COL_Name ? -1 : 1)
                });
            }
            // Sort function for campus list
            self.sortCampusByName = function () {
                self.campuses.sort(function (left, right) {
                    return left.CAMP_Name == right.CAMP_Name ? 0 : (left.CAMP_Name < right.CAMP_Name ? -1 : 1)
                });
            }
            // Sort function for college list
            self.sortQualByName = function () {
                self.qualifications.sort(function (left, right) {
                    return left.QUAL_Name == right.QUAL_Name ? 0 : (left.QUAL_Name < right.QUAL_Name ? -1 : 1)
                });
            }
            // Sort function trainer list
            self.sortUserBySurname = function () {
                self.trainers.sort(function (left, right) {
                    return left.USER_Surname == right.USER_Surname ? 0 : (left.USER_Surname < right.USER_Surname ? -1 : 1)
                });
            }


            self.typeToShow = ko.observable("all");
            self.displayAdvancedOptions = ko.observable(true);

            self.bookingsToShow = ko.computed(function () {
                // Represents a filtered list of bookings
                // i.e., only those matching the "typeToShow" condition
                var desiredType = self.typeToShow();
                if (desiredType == "all") return self.bookings();
                return ko.utils.arrayFilter(self.bookings(), function (data) {
                    return data.BOOK_StatusID == desiredType;
                });
            }, this);

            // Animation callbacks for the bookings list
            self.showBooking = function (booking) { if (booking.nodeType === 1) $(booking).hide().slideDown() }
            self.hideBooking = function (booking) { if (booking.nodeType === 1) $(booking).slideUp(function () { $(booking).remove(); }) }
        };

        // Here's a custom Knockout binding that makes elements shown/hidden via jQuery's fadeIn()/fadeOut() methods
        // Could be stored in a separate utility library
        ko.bindingHandlers.fadeVisible = {
            init: function (element, valueAccessor) {
                // Initially set the element to be instantly visible/hidden depending on the value
                var value = valueAccessor();
                $(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
            },
            update: function (element, valueAccessor) {
                // Whenever the value subsequently changes, slowly fade the element in or out
                var value = valueAccessor();
                ko.utils.unwrapObservable(value) ? $(element).fadeIn() : $(element).fadeOut();
            }
        };
        // Apply Knockout bindings
        ko.applyBindings(new bookingViewModel());
    </script>
</asp:Content>
