﻿<%@ Page Title="Campus - Coltech Online Certificates" Language="C#" MasterPageFile="~/Secure/Secure.Master" AutoEventWireup="true" CodeBehind="Campus.aspx.cs" Inherits="GUI.Secure.Campus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="server">
    <nav class="navbar navbar-default nav-justified" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            <a class="navbar-brand" href="#">Campus List</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="navbar-form navbar-left" role="search">
              <div class="form-group">
                <select id="College" class="input form-control" data-bind="options: colleges, optionsText: 'COL_Name', optionsValue: 'COL_CollegeID', value: CAMP_CollegID, optionsCaption: 'select a colleges...'"></select>
              </div>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <%--Campus main table--%>
    <div id="tblCampus" class="scrollableTable">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Campus Name</th>
                    <th>Description</th>
                    <th></th>
                </tr>
            </thead>
            <tbody id="tblCampusBody" data-bind="foreach: campuses">
                <tr>
                    <td class="hide" data-bind="text: CAMP_CampusID"></td>
                    <td data-bind="text: CAMP_Name"></td>
                    <td data-bind="text: CAMP_Description"></td>
                    <td>
                        <a href="#editCampus" role="button" id="btnEditCampus" class="btn btn-primary" data-toggle="modal" data-bind="click: $parent.editCampus">View</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    <a href="#saveCampus" role="button" id="btnAddCampus" class="btn btn-primary" data-toggle="modal" data-bind="click: addCampus">Add Campus</a>

    <!-- Modal View - Campus Edit -->
        <div id="editCampus" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEdit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="myModalLabelEdit">Campus Details</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr>
                                <th>Campus</th>
                                <td>
                                    <div class="col-xs-9">
                                        <span id="sCampus" data-bind="text: CAMP_Name"></span>
                                        <input id="iptCampus" class="form-control" data-bind="value: CAMP_Name" type="text" placeholder="Campus Name" />
                                    </div>
                                    <button type="button" id="btnEditCamp" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Campus Decription</th>
                                <td>
                                    <div class="col-xs-9">
                                        <span id="sCampDecript" data-bind="text: CAMP_Description"></span>
                                       <input id="iptCampDecript" class="form-control" data-bind="value: CAMP_Description" type="text" placeholder="Campus Decription" />
                                    </div>
                                    <button type="button" id="btnEditCampDecript" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button class="btn btn-success" data-bind="click: saveCampus">Save Changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Modal View - Add Qual -->
        <div id="saveCampus" class="modal fadef" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="myModalLabelAdd">Add New Campus</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr id="trCamp">
                                <th>Campus</th>
                                <td>
                                    <div class="col-xs-10">
                                        <div id="iptCamp">
                                            <input class="form-control" data-bind="value: CAMP_Name" type="text" placeholder="Campus Name" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trQualDes">
                                <th>Campus Decription</th>
                                <td>
                                    <div class="col-xs-10">
                                        <div id="iptCampDes">
                                            <input class="form-control" data-bind="value: CAMP_Description" type="text" placeholder="Campus Decription" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button class="btn btn-success" data-bind="click: saveCampus">Save Changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScript" runat="server">
    <script type="text/javascript">

        // Knockout view model
        function CampusListViewModel() {
            // Data
            var self = this;

            // Navbar Dropdownlist variables
            // College list vairable
            self.colleges = ko.observableArray([]);
            // Selected college variable
           // self.selectedCollege = ko.observable();

            // Campus list variable
            self.campuses = ko.observableArray([]);

            // User type variable
            self.userType = ko.observable();

            self.CAMP_CampusID = ko.observable();
            self.CAMP_Name = ko.observable();
            self.CAMP_Description = ko.observable();
            self.CAMP_CollegID = ko.observable();

            // Get a list of all the colleges for the dropdownlist
            $.ajax({
                type: 'GET',
                url: "../Secure/Handlers/GetColleges.ashx",
                success: function (data) {
                    self.colleges(data);
                    self.sortCollegeByName();
                }
            });

            // Get a list of campuses according to the Selected College
            $('#College').change(function () {
                $("select option:selected").each(function () {
                    if ($('option:selected').val() > 0) {
                        var Param = { 'CollegeID': $('option:selected').val() };
                        $.getJSON("../Secure/Handlers/GetCampuses.ashx", Param, function (data) {
                            self.campuses(data);
                            self.sortCampusByName();
                        });
                    }
                });
            });


            // This function calls the show edit function to view the edit view
            self.editCampus = function (data) {
                self.CAMP_CampusID(data.CAMP_CampusID);
                self.CAMP_Name(data.CAMP_Name);
                self.CAMP_Description(data.CAMP_Description);
                self.CAMP_CollegID(data.CAMP_CollegID);
                self.hideEdit();
            }

            self.modalClose = function () {
                self.hideEdit();
                self.clear();
            }

            // This function calls the hide edit function to hide the edit view
            self.clear = function () {
                self.CAMP_CampusID(0);
                self.CAMP_Name("");
                self.CAMP_Description("");
                //sefl.CAMP_CollegID(0);
            }


            // This function adds a new blank row in the table to add a new campus
            self.addCampus = function () {
                self.clear();
            };

            self.hideEdit = function () {
                $('#iptCampus').hide();
                $('#iptCampDecript').hide();
                $('#sCampus').show();
                $('#sCampDecript').show();
            }

            $('#btnEditCamp').click(function () {
                $('#sCampus').hide();
                $('#iptCampus').show();
            });

            $('#btnEditCampDecript').click(function () {
                $('#sCampDecript').hide();
                $('#iptCampDecript').show();
            });

            // Save the Changes to the DB
            self.saveCampus = function () {
                $.ajax({
                    type: "POST",
                    url: "../Secure/Handlers/SetCampus.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function (response, status, xhr) {
                        self.hideEdit();
                        var Param = { 'CollegeID': $('option:selected').val() };
                        $.getJSON("../Secure/Handlers/GetCampuses.ashx", Param, function (data) {
                            self.campuses(data);
                            self.sortCampusByName();
                        });
                        alert("Saved Successful!")
                    },
                    error: function () {
                        alert("Save Failed!");
                    }
                });
            };
            
            // Sort function for college list
            self.sortCollegeByName = function () {
                self.colleges.sort(function (left, right) {
                    return left.CollegeName == right.CollegeName ? 0 : (left.CollegeName < right.CollegeName ? -1 : 1)
                });
            }

            // Sort function for campus list
            self.sortCampusByName = function () {
                self.campuses.sort(function (left, right) {
                    return left.CampusName == right.CampusName ? 0 : (left.CampusName < right.CampusName ? -1 : 1)
                });
            }
        }

        // applay Knockout bindings
        ko.applyBindings(new CampusListViewModel());
    </script>
</asp:Content>
