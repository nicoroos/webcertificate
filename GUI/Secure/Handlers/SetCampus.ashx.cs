﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for SetCampus
    /// </summary>
    public class SetCampus : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();
            var postedObject = new JavaScriptSerializer().Deserialize<DALCMS.CAMPUS>(jsonData);
            DALCMS.CAMPUS campus = new DALCMS.CAMPUS();
            campus.CAMP_CampusID = postedObject.CAMP_CampusID;
            campus.CAMP_CollegID = postedObject.CAMP_CollegID;
            campus.CAMP_Name = postedObject.CAMP_Name.ToUpper();
            campus.CAMP_Description = postedObject.CAMP_Description.ToUpper();
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(Core.CMS.CAMPUS.SaveCampus(campus)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}