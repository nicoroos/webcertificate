﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for SetBookLink
    /// </summary>
    public class SetBookLink : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();
            var postedObject = new JavaScriptSerializer().Deserialize<List<Core.CMS.BO.UserQual>>(jsonData);
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(Core.CMS.BOOKLINK.DeleteBookLink(postedObject)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}