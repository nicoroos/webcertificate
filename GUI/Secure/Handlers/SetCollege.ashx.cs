﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for SetCollege
    /// </summary>
    public class SetCollege : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();
            var postedObject = new JavaScriptSerializer().Deserialize<DALCMS.COLLEGE>(jsonData);
            DALCMS.COLLEGE college = new DALCMS.COLLEGE();
            college.COL_CollegeID = postedObject.COL_CollegeID;
            college.COL_Name = postedObject.COL_Name.ToUpper();
            college.COL_Description = postedObject.COL_Description.ToUpper();
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(Core.CMS.COLLEGE.SaveCollege(college)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}