﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetTitles
    /// </summary>
    public class GetTitles : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.TITLE.Get().OrderBy(i => i.TITLE_Name)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}