﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetUserByID
    /// </summary>
    public class GetUserByID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var userID = int.Parse(context.Request["UserID"]);
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.UserDetials.GetUserByUserID(userID)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}