﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for SetUserQual
    /// </summary>
    public class SetUserQual : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int value2 = int.Parse(context.Request["value2"]);
            string jsonData = context.Request["printList"];
            var postedObject = new JavaScriptSerializer().Deserialize<List<Core.CMS.BO.UserQual>>(jsonData);
            if (value2 == 1)
            {
                context.Response.ContentType = "application/json";
                context.Response.Write(new JavaScriptSerializer().Serialize(Core.CMS.CLIENTQUAL.SaveClientQual(postedObject)));
            }
            else if(value2 == 2)
            {
                context.Response.ContentType = "application/json";
                context.Response.Write(new JavaScriptSerializer().Serialize(Core.CMS.CLIENTQUAL.SaveClientQualPrintList(postedObject)));
            }
            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}