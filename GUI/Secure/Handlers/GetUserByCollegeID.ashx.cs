﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetUserByCollegeID
    /// </summary>
    public class GetUserByCollegeID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var collegeID = int.Parse(context.Request["CollegeID"]);
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.USERS.GetUserByCollegeID(collegeID).OrderBy(i => i.USER_Surname)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}