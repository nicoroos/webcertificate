﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetRatePerKM
    /// </summary>
    public class GetRatePerKM : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "applicationn/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.RatePerKM.Get()));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}