﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetColleges
    /// </summary>
    public class GetColleges : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.COLLEGE.Get().OrderBy(i => i.COL_Name)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}