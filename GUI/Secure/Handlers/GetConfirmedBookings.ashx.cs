﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetConfirmedBookings
    /// </summary>
    public class GetConfirmedBookings : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.BOOKING.GetConfirmedBookings()));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}