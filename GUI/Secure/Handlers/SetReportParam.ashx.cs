﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for SetReportParam
    /// </summary>
    public class SetReportParam : IHttpHandler
    {
        public static int selected;
        public static int value1;
        public static int value2;

        public void ProcessRequest(HttpContext context)
        {
            // Store querystring in a variable
            selected = int.Parse(context.Request["value1"]);
            value1 = int.Parse(context.Request["value2"]);
            value2 = int.Parse(context.Request["value3"]);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}