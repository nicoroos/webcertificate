﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetBookingByID
    /// </summary>
    public class GetBookingByID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Store querystring in a variable
            var bookingID = int.Parse(context.Request["BookingID"]);
            // retrieves requerst from BL and sends is to ajax mehtod
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.BOOKING.GetBookingByBookingID(bookingID)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}