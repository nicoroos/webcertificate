﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetQualifications
    /// </summary>
    public class GetQualifications : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.QUALIFICATION.Get().OrderBy(i => i.QUAL_Name)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}