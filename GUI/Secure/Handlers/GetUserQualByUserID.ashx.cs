﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetClientQual
    /// </summary>
    public class GetUserQualByUserID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var userID = int.Parse(context.Request["Id"]);
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.CLIENTQUAL.GetUserQualByUserID(userID)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}