﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetUserBySurname
    /// </summary>
    public class GetUserBySurname : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string surname = Convert.ToString(context.Request["Surname"]);
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.USERS.GetUserBySurname(surname).OrderBy(i => i.USER_Surname)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}