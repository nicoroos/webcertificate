﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetBooking
    /// </summary>
    public class GetBookingByUserID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Get all the college Recoreds
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.BOOKING.GetBookingByUserID(GUI.Security.MemberShipProvider.LoggendInUserId)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}