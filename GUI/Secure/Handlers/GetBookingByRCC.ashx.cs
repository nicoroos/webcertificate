﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetBookingByRCC
    /// </summary>
    public class GetBookingByRCC : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Store querystring in a variable
            var rcc = (context.Request["Search"]).ToString();
            // retrieves requerst from BL and sends is to ajax mehtod
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.BOOKING.GetBookingByRCC(rcc.ToUpper())));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}