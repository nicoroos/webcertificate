﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetUserByCampusIDandUserID
    /// </summary>
    public class GetUserByCampusIDandUserID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var campusID = int.Parse(context.Request["CampusID"]);
            var surname = Convert.ToString(context.Request["surname"]);
            context.Response.ContentType = "text/plain";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.USERS.GetUserByCampusIDAndSurname(campusID, surname).OrderBy(i => i.USER_Surname)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}