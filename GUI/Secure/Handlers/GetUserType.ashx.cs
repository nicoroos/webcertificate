﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetUserType
    /// </summary>
    public class GetUserType : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(GUI.Security.MemberShipProvider.UserInType));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}