﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetUserByCampusID
    /// </summary>
    public class GetUserByCampusID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var campusID = int.Parse(context.Request["CampusID"]);
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.USERS.GetUserByCampusID(campusID).OrderBy(i => i.USER_Surname)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}