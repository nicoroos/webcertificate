﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for CollegeIDByUserId
    /// </summary>
    public class CollegeIDByUserId : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var UserID = int.Parse(context.Request["UserID"]);
            Core.CMS.BO.UserDetails userdetails = Core.CMS.UserDetials.GetUserByUserID(UserID);
            int CollegeID = userdetails.selectedCboCollege;

            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(CollegeID));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}