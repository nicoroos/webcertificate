﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetTimeSheet
    /// </summary>
    public class GetTimeSheet : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Store querystring in a variable
            var timeSheetID = int.Parse(context.Request["TimeSheetID"]);
            // retrieves requerst from BL and sends is to ajax mehtod
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.TIMESHEET.GetTimeSheetByID(timeSheetID)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}