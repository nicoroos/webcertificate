﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetLoggedInUser
    /// </summary>
    public class GetLoggedInUser : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.USERS.Get(GUI.Security.MemberShipProvider.LoggendInUserId)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}