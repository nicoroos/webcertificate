﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetUserQualByBookingID
    /// </summary>
    public class GetUserQualByBookingID : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var bookingID = int.Parse(context.Request["BookingID"]);
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.CLIENTQUAL.GetUserQualByBookingID(bookingID)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}