﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetQualificationSearchName
    /// </summary>
    public class GetQualificationByName : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var qualName = Convert.ToString(context.Request["qualname"]);
            string qualification = new JavaScriptSerializer().Serialize(BL.ModifyQualification.GetQualName(qualName.ToUpper()));
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.QUALIFICATION.GetQualByName(qualName.ToUpper())));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}