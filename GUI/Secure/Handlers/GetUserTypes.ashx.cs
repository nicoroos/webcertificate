﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetUserTypes
    /// </summary>
    public class GetUserTypes : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.USERTYPE.Get().OrderBy(i => i.TYPE_Name)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}