﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetQualByName
    /// </summary>
    public class GetQualByName : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var qualName = Convert.ToString(context.Request["QualName"]);
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.QUALIFICATION.GetQualByName(qualName.ToUpper())));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}