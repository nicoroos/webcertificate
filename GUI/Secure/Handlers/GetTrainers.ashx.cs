﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetTrainers
    /// </summary>
    public class GetTrainers : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.USERS.Get().Where(u => u.USER_TypeID <= 3).OrderBy(i => i.USER_Name)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}