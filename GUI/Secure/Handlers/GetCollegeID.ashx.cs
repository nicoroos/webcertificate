﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetCollegeID
    /// </summary>
    public class GetCollegeID : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            var collegeID = int.Parse(context.Request["Id"]);
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.COLLEGE.Get().SingleOrDefault(c => c.COL_CollegeID == collegeID)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}