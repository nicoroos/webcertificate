﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetUsers
    /// </summary>
    public class GetUsers : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.USERS.Get().OrderBy(i => i.USER_Surname)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}