﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetStatuses
    /// </summary>
    public class GetStatuses : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.STATUS.Get().OrderBy(i => i.STATUS_StatusName)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}