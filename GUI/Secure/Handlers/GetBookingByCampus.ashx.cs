﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetBookingByCampus
    /// </summary>
    public class GetBookingByCampus : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            // Store querystring in a variable
            var collegeID = int.Parse(context.Request["CollegeID"]);
            // retrieves requerst from BL and sends is to ajax mehtod
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.BOOKING.GetBookingByCollegeID(collegeID)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}