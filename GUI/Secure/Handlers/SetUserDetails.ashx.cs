﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for SetUserDetails
    /// </summary>
    public class SetUserDetails : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int type = int.Parse(context.Request["type"]);
            int userID = int.Parse(context.Request["UserId"]);
            string name = context.Request["UserName"];
            string surname = context.Request["UserSurname"];
            string initials = context.Request["Initials"];
            int titleID = int.Parse(context.Request["TitleID"]);
            string email = context.Request["Email"];
            int userType = int.Parse(context.Request["UserType"]);
            string courseCode = context.Request["Course"];
            int collegeID = int.Parse(context.Request["CollegeID"]);
            int campusID = int.Parse(context.Request["CampusID"]);
            string cellNo = context.Request["Celno"];
            string telNo = context.Request["Telno"];
            string faxNo = context.Request["Faxno"];
            string streetNo = context.Request["Streetno"];
            string streetName = context.Request["StreetName"];
            string suburb = context.Request["Suburb"];
            string city = context.Request["City"];
            string postalCode = context.Request["PostalCode"];
            
            Core.CMS.BO.UserDetails postedObject = new Core.CMS.BO.UserDetails();
            postedObject.USER_UserID = userID;
            postedObject.USER_Name = name;
            postedObject.USER_Surname = surname;
            postedObject.USER_Initials = initials;
            postedObject.USER_TitleID = titleID;
            postedObject.CONDET_Email = email;
            postedObject.USER_TypeID = userType;
            postedObject.selectedCourse = courseCode;
            postedObject.selectedCollege = collegeID;
            postedObject.selectedCampus = campusID;
            postedObject.CONDET_CellNo = cellNo;
            postedObject.CONDET_TelNo = telNo;
            postedObject.CONDET_FaxNo = faxNo;
            postedObject.CONDET_StreetNo = streetNo;
            postedObject.CONDET_StreetName = streetName;
            postedObject.CONDET_Suburb = suburb;
            postedObject.CONDET_City = city;
            postedObject.CONDET_PostalCode = postalCode;

            if (type == 2)// 2 = update and 1 = new save;
            {
                string Password = context.Request["Password"];
                int ContactDetailsID = int.Parse(context.Request["ContactDetailsID"]);
                
                postedObject.USER_Password = Password;
                postedObject.CONDET_CONTACTDETAILSID = ContactDetailsID;
            }
           
            context.Response.ContentType = "application/json";
            context.Response.Write(new JavaScriptSerializer().Serialize(Core.CMS.UserDetials.SaveUserDetails(postedObject)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}