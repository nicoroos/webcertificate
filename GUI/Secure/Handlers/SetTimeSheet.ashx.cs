﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for SetTimeSheet
    /// </summary>
    public class SetTimeSheet : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //Kry die JSon object uit die context se inputstream uit.
            string jsonData = new StreamReader(context.Request.InputStream).ReadToEnd();
            //Convert Json data na die C# Business Layer object.
            var postedObject = new JavaScriptSerializer().Deserialize<Core.CMS.BO.TimeSheet>(jsonData);
            //Spesifiseer dat die data wat die generic handler gaan return n JSon object is.
            context.Response.ContentType = "application/json";
            //Stuur die C# BusinessObject na die save method op die Business Layer en convert die C# BusinessObject wat terug kom na n JSon Object,
            //dan word die converted JSon object in die context se response in terug geskryf vir die UI om verder te verwerk. (Dit sal dan weer aan
            //die knockout viewmodal assign word)
            context.Response.Write(new JavaScriptSerializer().Serialize(Core.CMS.TIMESHEET.SaveTimeSheet(postedObject, GUI.Security.MemberShipProvider.LoggendInUserId)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}