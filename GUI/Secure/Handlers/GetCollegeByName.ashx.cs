﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetCollegeByName
    /// </summary>
    public class GetCollegeByName : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var collegeName = Convert.ToString(context.Request["CollegeName"]);
            context.Response.ContentType = "application/json";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.COLLEGE.GetCollegeByName(collegeName.ToUpper())));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}