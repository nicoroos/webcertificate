﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace GUI.Secure.Handlers
{
    /// <summary>
    /// Summary description for GetTimeSheetByDates
    /// </summary>
    public class GetTimeSheetByDates : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var dateStart = (context.Request["Start"]).ToString();
            DateTime startDate = DateTime.Parse(dateStart);

            var dateEnd = (context.Request["End"]).ToString();
            DateTime endDate = DateTime.Parse(dateEnd);

            context.Response.ContentType = "text/plain";
            context.Response.Write(JsonConvert.SerializeObject(Core.CMS.TIMESHEET.GetTimesheetListByDateAndUserID(startDate, endDate, GUI.Security.MemberShipProvider.LoggendInUserId)));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}