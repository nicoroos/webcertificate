﻿<%@ Page Title="College - Coltech Online Certificates" Language="C#" MasterPageFile="~/Secure/Secure.Master" AutoEventWireup="true" CodeBehind="College.aspx.cs" Inherits="GUI.Secure.College" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="server">
    <div id="main">
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">College List</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" id="search" class="form-control" placeholder="Search" data-bind='value: searchbartext' />
                    </div>
                    <button type="button" id="btnsearch" class="btn btn-primary">Search</button>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <%--College Main Table--%>
        <div class="scrollableTable">
            <table id="tblCollege" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>College Name</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: colleges">
                    <tr>
                        <td class="hide" data-bind="text: COL_CollegeID"></td>
                        <td data-bind="text: COL_Name"></td>
                        <td data-bind="text: COL_Description"></td>
                        <td>
                            <a href="#editCollege" role="button" id="btnEditCollege" class="btn btn-primary" data-toggle="modal" data-bind="click: $parent.editCollege">View</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <a href="#saveCollege" role="button" id="btnAddCollege" class="btn btn-primary" data-toggle="modal" data-bind="click: addCollege">Add College</a>

        <!-- Modal View - Client Edit -->
        <div id="editCollege" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEdit" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="myModalLabelEdit">College Details</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr>
                                <th>College</th>
                                <td>
                                    <div class="col-xs-9">
                                        <span id="sCollege" data-bind="text: COL_Name"></span>
                                        <input id="iptCollege" class="form-control" data-bind="value: COL_Name" type="text" placeholder="College Name" />
                                    </div>
                                    <button type="button" id="btnEditColl" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>College Decription</th>
                                <td>
                                    <div class="col-xs-9">
                                        <span id="sCollegeDesc" data-bind="text: COL_Description"></span>
                                       <input id="iptCollegeDesc" class="form-control" data-bind="value: COL_Description" type="text" placeholder="College Decription" />
                                    </div>
                                    <button type="button" id="btnEditCollegeDesc" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button class="btn btn-success" data-bind="click: saveCollege">Save Changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Modal View - Add Qual -->
        <div id="saveCollege" class="modal fadef" tabindex="-1" role="dialog" aria-labelledby="myModalLabelAdd" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="myModalLabelAdd">Add New College</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr id="trQual">
                                <th>College</th>
                                <td>
                                    <div class="col-xs-10">
                                        <div id="iptCol">
                                            <input class="form-control uppercase" data-bind="value: COL_Name" type="text" placeholder="College Name" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trQualDes">
                                <th>College Decription</th>
                                <td>
                                    <div class="col-xs-10">
                                        <div id="iptColDesc">
                                            <input class="form-control uppercase" data-bind="value: COL_Description" type="text" placeholder="College Decription" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button class="btn btn-success" data-bind="click: saveCollege">Save Changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScript" runat="server">
    <script type="text/javascript">

        // Knockout viewmodel
        function CollegeListViewModel() {
            // Data
            var self = this;
            // Search bar textfield variable
            self.searchbartext = ko.observable();
            // list variable to populate the college table
            self.colleges = ko.observableArray([]);
            // Usertype variable
            self.userType = ko.observable();
            // edit view variables
            self.COL_CollegeID= ko.observable();
            self.COL_Name = ko.observable();
            
            self.COL_Description = ko.observable();
            
            // Get a list of all the colleges containing the word or phrase in the search bar
            $('#btnsearch').click(function () {
                var Param = { 'CollegeName': ko.toJS(self.searchbartext) };
                $.ajax({
                    type: "GET",
                    url: "../Secure/Handlers/GetCollegeByName.ashx",
                    data: Param,
                    success: function (data) {
                        self.colleges(data);
                        self.sortCollegeByName();
                    }
                });
            });
            

            // Get method to get a list of all the colleges from the DB
            self.getColleges = function () {
                $.ajax({
                    type: "GET",
                    url: "../Secure/Handlers/GetColleges.ashx",
                    success: function (data) {
                        self.colleges(data);
                        self.sortCollegeByName();
                    },
                });
            }
            // Executes the function above
            self.getColleges();
            
            // Shows the edit layout
            self.editCollege = function (data) {
                self.COL_CollegeID(data.COL_CollegeID);
                self.COL_Name(data.COL_Name);
                self.COL_Description(data.COL_Description);
                self.hideEdit();
            }

            // Hides the edit loyout
            self.modalClose = function () {
                self.hideEdit();
                self.clear();
            }

            self.clear = function () {
                self.COL_CollegeID(0);
                self.COL_Name("");
                self.COL_Description("");
            }

            // Adds a new empty row in the college table
            self.addCollege = function () {
                self.clear();
            };

            self.hideEdit = function () {
                $('#iptCollege').hide();
                $('#iptCollegeDesc').hide();
                $('#sCollege').show();
                $('#sCollegeDesc').show();
            }

            $('#btnEditColl').click(function () {
                $('#sCollege').hide();
                $('#iptCollege').show();
            });

            $('#btnEditCollegeDesc').click(function () {
                $('#sCollegeDesc').hide();
                $('#iptCollegeDesc').show();
            });

            // Save the Changes to the DB
            self.saveCollege = function () {
                $.ajax({
                    type: "POST",
                    url: "../Secure/Handlers/SetCollege.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function (response, status, xhr) {
                        self.getColleges();
                        self.hideEdit();
                        self.searchbartext("");
                        alert("Saved Succussful!");
                    },
                    error: function () {
                        alert("Save faild!");
                    }
                });
            };

            // Sort function for college list
            self.sortCollegeByName = function () {
                self.colleges.sort(function (left, right) {
                    return left.COL_Name == right.COL_Name ? 0 : (left.COL_Name < right.COL_Name ? -1 : 1)
                });
            }
        }

        // knockout bindings is applied here
        ko.applyBindings(new CollegeListViewModel());
    </script>
</asp:Content>
