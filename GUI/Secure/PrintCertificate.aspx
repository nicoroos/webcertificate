﻿<%@ Page Title="Coltech Certificates" Language="C#" MasterPageFile="~/Secure/Secure.Master" AutoEventWireup="true" CodeBehind="PrintCertificate.aspx.cs" Inherits="GUI.Secure.PrintCertificate" %>

<%--<meta http-equiv="X-UA-Compatible" content="IE=9,chrome=1" /> --%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="server">
    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Print Certificate</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <div class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <select id="dboCollege" class="form-control" data-bind="options: colleges, optionsText: 'COL_Name', optionsValue: 'COL_CollegeID', optionsCaption: 'Select College..', value: selectedCollegeFilter"></select>
                </div>
            </div>
            <div class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input type="text" id="txtSearch" class="form-control" placeholder="Search Referance Number" data-bind='value: bookingSearch' />
                </div>
                <button type="button" id="btnsearch" class="btn btn-primary" data-bind="click: search">Search</button>
                <button type="button" id="btnClearFilters" class="btn btn-danger" data-bind="click: clearFilter">Clear Filters</button>
            </div>
            <div class="navbar-form navbar-nav" role="search">
                    <p data-bind='fadeVisible: displayAdvancedOptions'>
                        <label>
                            <input type='radio' name="type" value='all' data-bind='checked: typeToShow' />All</label>
                        <label>
                            <input type='radio' name="type" value='1' data-bind='checked: typeToShow' />Done</label>
                        <label>
                            <input type='radio' name="type" value='2' data-bind='checked: typeToShow' />Pending</label>
                        <label>
                            <input type='radio' name="type" value='3' data-bind='checked: typeToShow' />Cancelled</label>
                        <label>
                            <input type='radio' name="type" value='4' data-bind='checked: typeToShow' />Confirmed</label>
                        <label>
                            <input type='radio' name="type" value='5' data-bind='checked: typeToShow' />Printed</label>
                        <label>
                            <input type='radio' name="type" value='5' data-bind='checked: typeToShow' />Send</label>
                    </p>
                </div>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <%--Main booking table--%>
    <div class="scrollableTable">
        <table id="tableView" class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Booking ID</th>
                    <th>Location</th>
                    <th>Qualification</th>
                    <th>Trainer</th>
                    <th>StartDate</th>
                    <th>EndDate</th>
                    <th>Referance Code</th>
                    <th></th>
                </tr>
            </thead>
            <tbody data-bind="template: { foreach: bookingsToShow, beforeRemove: hideBooking, afterAdd: showBooking }">
                <tr class="data" data-bind='css: Coloring'>
                    <td data-bind="text: BOOK_BookingID"></td>
                    <td>
                        <span data-bind="text: COL_Name"></span>
                        <br />
                        <span data-bind="text: CAMP_Name"></span>
                    </td>
                    <td data-bind="text: QUAL_Name"></td>
                    <td data-bind="text: USER_Name"></td>
                    <td data-bind="text: BOOK_StartDate"></td>
                    <td data-bind="text: BOOK_EndDate"></td>
                    <td data-bind="text: CourseCode"></td>
                    <td id="btnbookview"><a href="#modalUserList" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.viewUsers">View</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div>
        <!-- Modal Print Certificate -->
        <div id="modalUserList" class="modal fade modal-wide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: statusChange">×</button>
                        <h3 id="myModalLabel">Booking Clients&nbsp;(<span data-bind='text: clients().length'></span>)</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Initials</th>
                                    <th>Surname</th>
                                    <th>Qualification</th>
                                    <th>Description</th>
                                    <th>StartDate</th>
                                    <th>EndDate</th>
                                    <th>Referance No</th>
                                    <th>Print Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: clients">
                                <tr>
                                    <td data-bind="text: USER_UserID"></td>
                                    <td data-bind="text: USER_Initials"></td>
                                    <td data-bind="text: USER_Surname"></td>
                                    <td data-bind="text: QUAL_Name"></td>
                                    <td data-bind="text: QUAL_Description"></td>
                                    <td data-bind="text: CLIQUAL_StartDate"></td>
                                    <td data-bind="text: CLIQUAL_EndDate"></td>
                                    <td data-bind="text: CourseCode"></td>
                                    <td data-bind="text: PRINT_Status"></td>
                                    <td class="hide" data-bind="text: PRINT_PrintID">3</td><%--class="hide" --%>
                                    <td>
                                        <input type="button" class="btn btn-danger" data-bind="click: $parent.removePrint" value="Remove" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: statusChange">Close</button>
                        <button class="btn btn-success" data-bind="click: saveChanges">Save Changes</button>
                        <button id="btnPrint" class="btn btn-primary" type="button"  data-bind="click: print">Print</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
</asp:Content>
<asp:Content ID="JavaScript" ContentPlaceHolderID="JavaScript" runat="server">
    <script src="../JavaScript/KnockoutsimpleGrid.1.3.js"></script>
    <script type="text/javascript">
        function viewModel() {
            var self = this;

            // Navbar variable to store a list of colleges
            self.colleges = ko.observableArray([]);
            // Navbar variable to store the selected colleges ID
            self.selectedCollegeFilter = ko.observable();
            // Client search variable
            self.bookingSearch = ko.observable("");
            // a List variable to store a list of the bookings
            self.bookings = ko.observableArray([]);
            // variable to store a list of clients in the modal window table
            self.clients = ko.observableArray([]);
            // variable to store the PrintID
            self.PRINT_PrintID = ko.observable("3");
            // PrintList variable
            // variable to store a list of the printed clients
            self.printedlist = ko.observableArray([]);

            // Load initial state from server.
            // Get a list of all the bookings
            self.getBookings = function () {
                $.getJSON("../Secure/Handlers/GetBookings.ashx", function (data) {
                    self.bookings(data);
                    self.sortBookingByDate();
                });
            }
            // Executes the method above
            self.getBookings();

            // Load initial state from server
            // Method to view a list of uers to be printed
            self.viewUsers = function (Bookingdata) {
                if (ko.toJS(Bookingdata.BOOK_BookingID) > 0) {
                    if (Bookingdata.BOOK_StatusID != 1) {
                        $('#btnPrint').hide();
                    }
                    else {
                        $('#btnPrint').show();
                    };
                    var Param = { 'BookingID': ko.toJS(Bookingdata.BOOK_BookingID) };
                    $.getJSON("../Secure/Handlers/GetUserQualByBookingID.ashx", Param, function (data) {
                        self.clients(data);
                        self.sortUserBySurname();
                    });
                }
            }

            // Method to get a list of college from the server to populate the navbar combobox
            $.getJSON("../Secure/Handlers/GetColleges.ashx", function (data) {
                self.colleges(data);
                self.sortCollegeByName();
            });

            // Method to fileter the bookings list according to the selected value from the navbar combo box
            $('#dboCollege').change(function () {
                $("select option:selected").each(function () {
                    if ($('option:selected').val() > 0) {
                        var Param = { 'CollegeID': $('option:selected').val() };
                        $.getJSON("../Secure/Handlers/GetBookingByCampus.ashx", Param, function (data) {
                            self.bookings(data);
                            self.sortBookingByDate();
                        });
                    }
                    else {
                        self.getBookings();
                    }
                });
            });

            // Client Search by Surname or referance code
            self.search = function () {
                // If this function is called it wil check if the clientSurname is empty and call functions accordingly
                if (ko.toJS(self.bookingSearch) == "") {
                    self.access();
                }
                else {
                    var Param = { 'Search': ko.toJS(self.bookingSearch) };
                    $.getJSON("../Secure/Handlers/GetBookingByRCC.ashx", Param, function (data) {
                        self.bookings(data);
                        self.sortBookingByDate();
                    });
                }
            }
            // Method to remove a row from the Printlist table in the modal window
            self.removePrint = function (data) {
                self.clients.destroy(data);
            }

            // Clear the search filters
            self.clearFilter = function () {
                self.selectedCollegeFilter("");
                self.bookingSearch("");
                self.getBooking();
            }

            // Method called to change the Printstatus on the modal window close
            self.statusChange = function () {
                var values = { 'printList': ko.mapping.toJSON(self.printedlist), 'value2': 2 };
                $.ajax({
                    type: "POST",
                    url: "../Secure/Handlers/SetUserQual.ashx",
                    data: values,
                });
            }

            // Method to save the changes made on the Printlist
            self.saveChanges = function () {
                $.ajax({
                    type: "POST",
                    url: "../Secure/Handlers/SetBookLink.ashx",
                    data: ko.mapping.toJSON(self.clients),
                    success: function (data) {
                        alert("Saved Successfull!");
                    }
                });
            }

            // Method to call the form with the crystal report viewer
            self.print = function () {
                var valDat = { 'printList': ko.mapping.toJSON(self.clients), 'value2': 1 };
                $.ajax({
                    type: "POST",
                    url: "../Secure/Handlers/SetUserQual.ashx",
                    data: valDat,
                    success: function (data) {
                        self.printedlist(data);
                        var values = { 'value1': ko.toJSON(2), 'value2': ko.toJSON(3), 'value3': 0 };
                        $.ajax({
                            type: 'POST',
                            url: "../Secure/Handlers/SetReportParam.ashx",
                            data: values,
                            success: function () {
                                alert("The Print Preview will be displayed in a new browser window");
                                window.open("ReportPage.aspx");
                            },
                            error: function () {
                                alert("Print Failed!");
                            }
                        });
                    },
                    error: function () {
                        alert("Print Failed\nMake sure you selected a qualification\nand that there are no blank fields");
                    }
                });
            }
            // Sort function
            self.sortBookingByDate = function () {
                self.bookings.sort(function (left, right) {
                    return left.BOOK_StartDate == right.BOOK_StartDate ? 0 : (left.BOOK_StartDate > right.BOOK_StartDate ? -1 : 1)
                });
            }
            // Sort function for college list
            self.sortCollegeByName = function () {
                self.colleges.sort(function (left, right) {
                    return left.COL_Name == right.COL_Name ? 0 : (left.COL_Name < right.COL_Name ? -1 : 1)
                });
            }
            // Sort function trainer list
            self.sortUserBySurname = function () {
                self.clients.sort(function (left, right) {
                    return left.USER_Name == right.USER_Name ? 0 : (left.USER_Name < right.USER_Name ? -1 : 1)
                });
            }

            // filtering of the list function
            self.typeToShow = ko.observable("all");
            self.displayAdvancedOptions = ko.observable(true);

            self.bookingsToShow = ko.computed(function () {
                // Represents a filtered list of bookings
                // i.e., only those matching the "typeToShow" condition
                var desiredType = this.typeToShow();
                if (desiredType == "all") return this.bookings();
                return ko.utils.arrayFilter(self.bookings(), function (data) {
                    return data.BOOK_StatusID == desiredType;
                });
            }, this);

            // Animation callbacks for the bookings list
            self.showBooking = function (booking) { if (booking.nodeType === 1) $(booking).hide().slideDown() }
            self.hideBooking = function (booking) { if (booking.nodeType === 1) $(booking).slideUp(function () { $(booking).remove(); }) }
        };

        // Here's a custom Knockout binding that makes elements shown/hidden via jQuery's fadeIn()/fadeOut() methods
        // Could be stored in a separate utility library
        ko.bindingHandlers.fadeVisible = {
            init: function (element, valueAccessor) {
                // Initially set the element to be instantly visible/hidden depending on the value
                var value = valueAccessor();
                $(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
            },
            update: function (element, valueAccessor) {
                // Whenever the value subsequently changes, slowly fade the element in or out
                var value = valueAccessor();
                ko.utils.unwrapObservable(value) ? $(element).fadeIn() : $(element).fadeOut();
            }
        };
        // Knockout binding is applied.
        ko.applyBindings(new viewModel());
    </script>
</asp:Content>
