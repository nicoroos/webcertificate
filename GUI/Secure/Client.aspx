﻿<%@ Page Title="Client - Coltech Online Certificates" Language="C#" MasterPageFile="~/Secure/Secure.Master" AutoEventWireup="true" CodeBehind="Client.aspx.cs" Inherits="GUI.Secure.Client" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="server">
    <div>
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Users</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="navbar-form navbar-left" role="search">
                    <select id="cboCollege" class="form-control" data-bind="options: colleges, optionsText: 'COL_Name', optionsValue: 'COL_CollegeID', optionsCaption: 'Select College..', value: selectedCollege"></select>
                </div>
                <div class="navbar-form navbar-nav" role="search">
                    <select id="cboCampus" class="form-control" data-bind="options: campuses, optionsText: 'CAMP_Name', optionsValue: 'CAMP_CampusID', optionsCaption: 'Select Campus..', value: selectedCampus"></select>
                </div>
                <div class="navbar-form navbar-nav" role="search">
                    <div class="form-group">
                        <input type="text" id="search" class=" form-control input" placeholder="Search Surname" data-bind="value: clientSurname" />
                    </div>
                    <%--<button type="button" id="btnsearch" class="btn btn-primary" data-bind="click: getClients">Search</button>--%>
                    <button type="button" id="btnClear" class="btn btn-danger" data-bind="click: clearFilters">Clear Filters</button>
                </div>
                <div class="navbar-form navbar-nav" role="search">
                    <p>
                        <label>
                            <input type='radio' name="type" value='all' data-bind='checked: typeToShow' />All</label>
                        <label>
                            <input type='radio' name="type" value='1' data-bind='checked: typeToShow' />Admin</label>
                        <label>
                            <input type='radio' name="type" value='2' data-bind='checked: typeToShow' />ColtAdmin</label>
                        <label>
                            <input type='radio' name="type" value='3' data-bind='checked: typeToShow' />ColtStaff</label>
                        <label>
                            <input type='radio' name="type" value='4' data-bind='checked: typeToShow' />CollAdmin</label>
                        <label>
                            <input type='radio' name="type" value='5' data-bind='checked: typeToShow' />CollStaff</label>
                        <label>
                            <input type='radio' name="type" value='6' data-bind='checked: typeToShow' />Users</label>
                    </p>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <br />
        <%-- Main Table with list of Clients --%>
        <div class="scrollableTable">
            <table id="tblClient" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Initials</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody data-bind='template: { foreach: clientsToShow, beforeRemove: hideClientElement, afterAdd: showClienttElement }'>
                    <tr data-bind='attr: { "class": +USER_TypeID }'>
                        <td data-bind="text: USER_UserID"></td>
                        <td data-bind="text: USER_Initials"></td>
                        <td data-bind="text: USER_Name"></td>
                        <td data-bind="text: USER_Surname"></td>
                        <td><a href="#myModal" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.editClient">View</a>
                            <a href="#myModal2" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.viewQuals">History</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <a href="#myModal3" role="button" id="btnAddClient" class="btn btn-primary" data-toggle="modal" data-bind="click: addClient">Add User</a>

        <!-- Modal View - Client Edit -->
        <div id="myModal" class="modal fadef" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="myModalLabel">Client Details</h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <tr id="trName">
                                <th>Name</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sName" data-bind="text: USER_Name"></span>
                                        <div id="iptName">
                                            <input class="form-control inputName has-error" data-bind="value: USER_Name" type="text" placeholder="Name" />
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditName" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr id="trSurname">
                                <th>Surname</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sSurname" data-bind="text: USER_Surname"></span>
                                        <div id="iptSurname">
                                            <input class="form-control inputSurname" data-bind="value: USER_Surname" type="text" placeholder="Surname" />
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditSurname" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr id="trInitials">
                                <th>Initials</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sInitials" data-bind="text: USER_Initials"></span>
                                        <div id="iptInitials">
                                            <input class="form-control inputInitials" data-bind="value: USER_Initials" type="text" placeholder="Initials" />
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditInitials" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr id="trTitle">
                                <th>Title</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sTitle" data-bind="text: TITLE_Name"></span>
                                        <div id="selTitle">
                                            <select class="cboTitle form-control" data-bind="options: titles, optionsText: 'TITLE_Name', optionsValue: 'TITLE_TitleID', optionsCaption: 'Select a Title..', selectedOptions: selectedTitle, value: selectedTitle"></select>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditTitle" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr id="trEmail">
                                <th>Email</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sEmail" data-bind="text: CONDET_Email"></span>
                                        <div id="iptEmail">
                                            <input class=" form-control inputEmail" data-bind="value: CONDET_Email" type="email" placeholder="Email" />
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditEmail" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr id="trPassword">
                                <th>Password</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sPassword" data-bind="text: USER_Password"></span>
                                        <div id="iptPassword">
                                            <input class="form-control txtPassword" data-bind="value: USER_Password" type="password" placeholder="Password" />
                                            <div id="passIn" class="glyphicon glyphicon-ok-sign inputVerifyPasswordNotBlank"></div>
                                            <div id="passEmpty" class="glyphicon glyphicon-remove inputVerifyPasswordBlank"></div>
                                            <div id="passWrong" class="alert alert-danger txtPasswordlength">Password must be 6 characters or more!</div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditPassword" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr class="PasswordVerification">
                                <th>Verify Password</th>
                                <td>
                                    <div class="col-xs-10">
                                        <input class="form-control txtVerifyPassword" data-bind="value: PasswordVerification" type="password" placeholder="Password Verification" />
                                        <div id="pass2In" class=" glyphicon glyphicon-ok-sign"></div>
                                        <div id="pass2Empty" class="glyphicon glyphicon-remove"></div>
                                        <div id="pass2Wrong" class="alert alert-danger txtVerifyStatus">Password Do not Match!</div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="trUserType">
                                <th>User type</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sType" data-bind="text: TYPE_Name"></span>
                                        <div id="selType">
                                            <select id="selUserType" class="selUserType form-control" data-bind="options: userTypes, optionsText: 'TYPE_Name', optionsValue: 'TYPE_TypeID', optionsCaption: 'Select a Type..', selectedOptions: selectedType, value: selectedType"></select>
                                            <div id="typeIn" class="glyphicon glyphicon-ok-sign userTypeNotBlank"></div>
                                            <div id="typeEmpty" class="glyphicon glyphicon-remove userTypeBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditUserType" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr id="trCourseCode">
                                <th>Course Code</th>
                                <td>
                                    <div class="col-xs-10">
                                        <label id="AddCourscode">Add a Course Code</label>
                                        <div id="iptCourseCode">
                                            <select id="selCourseCode" class="selCourseCode form-control" data-bind="options: bookings, optionsText: 'QUAL_Name', optionsValue: 'CourseCode', optionsCaption: 'selected a Type...', value: selectedCourse"></select>
                                            <div id="courseCodeIn" class=" glyphicon glyphicon-ok-sign"></div>
                                            <div id="courseCodeEmpty" class="glyphicon glyphicon-remove"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnAddCourse" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr id="trCollege">
                                <th>Location</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCollege" data-bind="text: COL_Name"></span>
                                        <select id="selCollege" class="cboCollege form-control " data-bind="options: cboColleges, optionsText: 'COL_Name', optionsValue: 'COL_CollegeID', optionsCaption: 'Select College..', selectedOptions: selectedCboCollege, value: selectedCboCollege">
                                        </select>
                                    </div>
                                    <button type="button" id="btnEditCollege" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr id="trCampus">
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCampus" data-bind="text: CAMP_Name"></span>
                                        <div id="selCampus">
                                            <select class="inputCampus form-control" data-bind="options: cboCampuses, optionsText: 'CAMP_Name', optionsValue: 'CAMP_CampusID', optionsCaption: 'Select Campus..', selectedOptions: selectedCboCampus, value: selectedCboCampus">
                                            </select>
                                            <div id="campIn" class=" glyphicon glyphicon-ok-sign inputCampusNotBlank"></div>
                                            <div id="campEmpty" class="glyphicon glyphicon-remove inputCampusBlank"></div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditCampus" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr id="trCellNo">
                                <th>Cellphone Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCellNo" data-bind="text: CONDET_CellNo"></span>
                                        <div id="iptCellNo">
                                            <input class="form-control inputCelno" data-bind="value: CONDET_CellNo" type="tel" placeholder="Cellphone Number" maxlength="10" onkeypress="return IsNumericcell(event);" ondrop="return false;" />
                                            <div id="cellIn" class="glyphicon glyphicon-ok-sign inputCelnoNotBlank"></div>
                                            <div id="CellEmpty" class="glyphicon glyphicon-remove inputCelnoBlank"></div>
                                            <br id="breaklinemodelcell" />
                                            <div id="nummodelcellerror" class="alert alert-danger">
                                                <a href="specialKeyssell" class="alert-link">Input digits (0 - 9)</a>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditCellno" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Telephone Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sTelNo" data-bind="text: CONDET_TelNo"></span>
                                        <input id="inputTelNo" class="form-control inputTelNo " data-bind="value: CONDET_TelNo" type="tel" placeholder="Telephone Number" maxlength="10" onkeypress="return IsNumerictel(event);" ondrop="return false;" />
                                        <br id="breaklinemodeltel" />
                                        <div id="nummodeltelerror" class="alert alert-danger">
                                            <a href="specialKeystel" class="alert-link">Input digits (0 - 9)</a>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditTellno" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Fax Number</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sFax" data-bind="text: CONDET_FaxNo"></span>
                                        <input id="inputFaxNo" class="form-control " data-bind="value: CONDET_FaxNo" type="tel" placeholder="Fax Number" maxlength="10" onkeypress="return IsNumericfax(event);" ondrop="return false;" />
                                        <br id="breaklinemodelfax" />
                                        <div id="nummodelfaxerror" class="alert alert-danger">
                                            <a href="specialKeysfax" class="alert-link">Input digits (0 - 9)</a>
                                        </div>
                                    </div>
                                    <button type="button" id="btnEditFax" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Residential Address</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sStreetNo" data-bind="text: CONDET_StreetNo"></span>
                                        <input id="inputStreetNo" class="form-control " data-bind="value: CONDET_StreetNo" type="text" placeholder="Street No" />
                                    </div>
                                    <button type="button" id="btnEditStreetNo" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sStreetName" data-bind="text: CONDET_StreetName"></span>
                                        <input id="inputStreetName" class="form-control " data-bind="value: CONDET_StreetName" type="text" placeholder="Street Name" />
                                    </div>
                                    <button type="button" id="btnEditStreetName" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sSuburb" data-bind="text: CONDET_Suburb"></span>
                                        <input id="inputSuburb" class="form-control " data-bind="value: CONDET_Suburb" type="text" placeholder="Suburb" />
                                    </div>
                                    <button type="button" id="btnEditSuburb" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sCity" data-bind="text: CONDET_City"></span>
                                        <input id="inputCity" class="form-control " data-bind="value: CONDET_City" type="text" placeholder="City" />
                                    </div>
                                    <button type="button" id="btnEditCity" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                            <tr>
                                <th>Postal Code</th>
                                <td>
                                    <div class="col-xs-10">
                                        <span id="sPostalCode" data-bind="text: CONDET_PostalCode"></span>
                                        <input id="iptPostalCode" class="form-control " data-bind="value: CONDET_PostalCode" type="text" placeholder="Postal Code" maxlength="10" />
                                    </div>
                                    <button type="button" id="btnEditPostalCode" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button class="btn btn-success" data-bind="click: upadate">Save Changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Modal View - Client history -->
        <div id="myModal2" class="modal fade modal-wide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="H1">Qualifications&nbsp;(<span data-bind='text: qualification().length'></span>)</h3>
                        <p>for <span data-bind="text: userName"></span></p>
                    </div>
                    <div class="modal-body">
                        <table class="table table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>User Qual ID</th>
                                    <th>Qualification</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Print Status</th>
                                    <th>CourseCode</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: qualification">
                                <tr>
                                    <td data-bind="text: CLIQUAL_UserQualID"></td>
                                    <td data-bind="text: QUAL_Name"></td>
                                    <td data-bind="text: CLIQUAL_StartDate"></td>
                                    <td data-bind="text: CLIQUAL_EndDate"></td>
                                    <td data-bind="text: PRINT_Status"></td>
                                    <td data-bind="text: CourseCode"></td>
                                    <td class="clientHide">
                                        <input type="button" class="btn btn-inverse" data-bind="click: $parent.print" value="Print" /></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <%-- Modal View - Client add form --%>
        <div id="myModal3" class="modal fade modal-wide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">×</button>
                        <h3 id="H2">Add new client</h3>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal" id="clientForm">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group formName">
                                        <label class="col-xs-3 control-label" for="inputName">Name</label>
                                        <div class="col-xs-6">
                                            <input class="inputName form-control" data-bind="value: USER_Name" type="text" id="inputName" placeholder="Name" />
                                        </div>
                                    </div>
                                    <div class="form-group formSurname">
                                        <label class="col-xs-3 control-label" for="inputSurname">Surname</label>
                                        <div class="col-xs-8">
                                            <input id="inputSurname" data-bind="value: USER_Surname" type="text" class="inputSurname form-control" placeholder="Surname" />
                                        </div>
                                    </div>
                                    <div class="form-group fromInitial">
                                        <label class="col-xs-3 control-label" for="inputInitials">Initials</label>
                                        <div class="col-xs-3">
                                            <input data-bind="value: USER_Initials" id="inputInitials" type="text" class="inputInitials form-control" placeholder="Initials" />
                                        </div>
                                    </div>
                                    <div class="form-group formTitle">
                                        <label class="col-xs-3 control-label" for="inputTitle">Title</label>
                                        <div class="col-xs-4">
                                            <select id="cboTitle" class="cboTitle form-control" data-bind="options: titles, optionsText: 'TITLE_Name', optionsValue: 'TITLE_TitleID', optionsCaption: 'Select a Title..', value: selectedTitle"></select>
                                        </div>
                                    </div>
                                    <div class="form-group formEmail">
                                        <label class="col-xs-3 control-label" for="inputEmail">Email</label>
                                        <div class="col-xs-6">
                                            <input data-bind="value: CONDET_Email" type="email" id="inputEmail" class="inputEmail form-control" placeholder="Email" />
                                        </div>
                                    </div>
                                    <div class="form-group formType">
                                        <label class="col-xs-3 control-label" for="inputType">User Type</label>
                                        <div class="col-xs-4">
                                            <select id="cboUsertype" class="selUserType form-control" data-bind="options: userTypes, optionsText: 'TYPE_Name', optionsValue: 'TYPE_TypeID', optionsCaption: 'Select a Type..', value: selectedType"></select>
                                        </div>
                                    </div>
                                    <div class="form-group formCourseCode">
                                        <label class="col-xs-3 control-label" for="cboCourseCode">Course</label>
                                        <div class="col-xs-6">
                                            <select id="cboCourseCode" class="selCourseCode form-control" data-bind="options: bookings, optionsText: 'QUAL_Name', optionsValue: 'CourseCode', optionsCaption: 'selected a Type...', value: selectedCourse"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group formCollege">
                                        <label class="col-xs-3 control-label" for="inputCollege">College</label>
                                        <div class="col-xs-5">
                                            <select class="cboColleges form-control" id="inputCollege" data-bind="options: colleges, optionsText: 'COL_Name', optionsValue: 'COL_CollegeID', optionsCaption: 'Select College..', value: selectedCollege">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group formCampus">
                                        <label class="col-xs-3 control-label" for="inputCampus">Campus</label>
                                        <div class="col-xs-5">
                                            <select id="inputCampus" class="inputCampus form-control cboCampuses" data-bind="options: campuses, optionsText: 'CAMP_Name', optionsValue: 'CAMP_CampusID', optionsCaption: 'Select Campus..', value: selectedCampus">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group formCellNo">
                                        <label class="col-xs-3 control-label" for="inputCelno">Cell Number</label>
                                        <div class="col-xs-5">
                                            <input id="inputCelno" data-bind="value: CONDET_CellNo" type="tel" class="inputCelno form-control" placeholder="Cellphone Number" maxlength="10" onkeypress="return IsNumericcell(event);" ondrop="return false;" />
                                            <br id="breaklinecell" />
                                            <div id="numcellerror" class="alert alert-danger">
                                                <a href="specialKeyssell" class="alert-link">Input digits (0 - 9)</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-xs-3 control-label" for="inputTelno">Phone Number</label>
                                        <div class="col-xs-5">
                                            <input data-bind="value: CONDET_TelNo" class="form-control" type="tel" id="Tel1" placeholder="Telephone Number" maxlength="10" onkeypress="return IsNumerictel(event);" ondrop="return false;" />
                                            <br id="breaklinetel" />
                                            <div id="numtelerror" class="alert alert-danger">
                                                <a href="specialKeystel" class="alert-link">Input digits (0 - 9)</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-xs-3 control-label" for="inputFax">Fax Number</label>
                                        <div class="col-xs-5">
                                            <input data-bind="value: CONDET_FaxNo" class="form-control" type="tel" id="inputFax" placeholder="Fax Number" maxlength="10" onkeypress="return IsNumericfax(event);" ondrop="return false;" />
                                            <br id="breaklinefax" />
                                            <div id="numfaxerror" class="alert alert-danger">
                                                <a href="specialKeysfax" class="alert-link">Input digits (0 - 9)</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-xs-3 control-label" for="inputStreet1">Your Address</label>
                                        <div class="col-xs-3">
                                            <input data-bind="value: CONDET_StreetNo" class="form-control" type="text" id="inputStreet1" placeholder="Street No" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-xs-3 control-label" for="inputStreet2"></label>
                                        <div class=" col-xs-7">
                                            <input data-bind="value: CONDET_StreetName" class="form-control" type="text" id="inputStreet2" placeholder="Street Name" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-xs-3 control-label" for="inputStreet3"></label>
                                        <div class="col-xs-5">
                                            <input data-bind="value: CONDET_Suburb" class="form-control" type="text" id="inputStreet3" placeholder="Suburb" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-xs-3 control-label" for="inputStreet4"></label>
                                        <div class="col-xs-5">
                                            <input data-bind="value: CONDET_City" class="form-control" type="text" id="inputStreet4" placeholder="City" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-xs-3 control-label" for="inputPostalCode">Postal Code</label>
                                        <div class="col-xs-3">
                                            <input data-bind="value: CONDET_PostalCode" class="form-control" type="text" id="inputPostalCode" placeholder="Postal Code" maxlength="10" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btnModalClose" class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalClose">Close</button>
                        <button type="button" id="btnSave" class="btn btn-success" value="Save Changes" data-bind="click: save">Save</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScript" runat="server">
    <script type="text/javascript">

        // Animation validations
        // Name field Validation
        $('.inputName').blur(function () {
            if ($('.inputName').val() == "") {
                $('.formName').addClass('has-error');
                $('.formName').removeClass('has-success');
            }
            else {
                $('.formName').removeClass('has-error');
                $('.formName').addClass('has-success');
            }
        });
        // Surname field Validation
        $('.inputSurname').blur(function () {
            if ($('.inputSurname').val() == "") {
                $('.formSurname').addClass('has-error');
                $('.formSurname').removeClass('has-success');
            }
            else {
                $('.formSurname').removeClass('has-error');
                $('.formSurname').addClass('has-success');
            }
        });
        // Initial field validation
        $('.inputInitials').blur(function () {
            if ($('.inputInitials').val() == "") {
                $('.fromInitial').addClass('has-error');
                $('.fromInitial').removeClass('has-success');
            }
            else {
                $('.fromInitial').removeClass('has-error');
                $('.fromInitial').addClass('has-success');
            }
        });
        // Email field validation
        $('.inputEmail').blur(function () {
            if ($('.inputEmail').val() == "") {
                $('.formEmail').addClass('has-error');
                $('.formEmail').removeClass('has-success');
            }
            else {
                $('.formEmail').removeClass('has-error');
                $('.formEmail').addClass('has-success');
            }
        });
        // Verify password field validation
        function VerifyPassword() {
            if ($('.txtPassword').val() != $('.txtVerifyPassword').val()) {
                $('.txtVerifyStatus').show();
            }
            else {
                $('.txtVerifyStatus').hide();
            }
        }
        // password field leght validation
        function Passwordlength() {
            if ($('.txtPassword').val().length < 6) {
                $('.txtPasswordlength').show();
            }
            else {
                $('.txtPasswordlength').hide();
            }
        }
        // mehtod that calls the verifypassword function
        $('.txtVerifyPassword').blur(function () {
            VerifyPassword();
        })
        // Mehtod that calls the passwordlength function
        $('.txtPassword').blur(function () {
            Passwordlength();
        })

        // Numerical value validation: Cell Number
        var specialKeyscell = new Array();
        specialKeyscell.push(8); //Backspace
        function IsNumericcell(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((((((keyCode >= 48 && keyCode <= 57) || keyCode >= 8 && keyCode <= 9) || keyCode >= 13 && keyCode <= 16) || keyCode >= 37 && keyCode <= 40) || keyCode >= 46 && keyCode <= 46) || specialKeyscell.indexOf(keyCode) != -1);
            document.getElementById("numcellerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinecell").style.display = ret ? "none" : "inline";
            document.getElementById("nummodelcellerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinemodelcell").style.display = ret ? "none" : "inline";

            return ret;
        }

        // Numerical value validation: Tel Number
        var specialKeystel = new Array();
        specialKeystel.push(8); //Backspace
        function IsNumerictel(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((((((keyCode >= 48 && keyCode <= 57) || keyCode >= 8 && keyCode <= 9) || keyCode >= 13 && keyCode <= 16) || keyCode >= 37 && keyCode <= 40) || keyCode >= 46 && keyCode <= 46) || specialKeystel.indexOf(keyCode) != -1);
            document.getElementById("numtelerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinetel").style.display = ret ? "none" : "inline";
            document.getElementById("nummodeltelerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinemodeltel").style.display = ret ? "none" : "inline";

            return ret;
        }

        // Numerical value validation: Fax Number
        var specialKeysfax = new Array();
        specialKeysfax.push(8); //Backspace
        function IsNumericfax(e) {
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((((((keyCode >= 48 && keyCode <= 57) || keyCode >= 8 && keyCode <= 9) || keyCode >= 13 && keyCode <= 16) || keyCode >= 37 && keyCode <= 40) || keyCode >= 46 && keyCode <= 46) || specialKeysfax.indexOf(keyCode) != -1);
            document.getElementById("numfaxerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinefax").style.display = ret ? "none" : "inline";
            document.getElementById("nummodelfaxerror").style.display = ret ? "none" : "inline";
            document.getElementById("breaklinemodelfax").style.display = ret ? "none" : "inline";

            return ret;
        }

        // Knockout viewmodel
        function viewModel() {
            var self = this;
            // Navbar variables
            // College combobox variables
            self.colleges = ko.observableArray([]);
            self.selectedCollege = ko.observable();
            // Campus combox variables
            self.campuses = ko.observableArray([]);
            self.selectedCampus = ko.observable();
            // Search bar variable
            self.clientSurname = ko.observable("");

            // #tblClient list variable
            self.clients = ko.observableArray([]);

            // Modal view Client edit variables
            // Personal Details
            self.USER_UserID = ko.observable();
            self.TITLE_Name = ko.observable();
            self.USER_Initials = ko.observable();
            self.USER_Name = ko.observable();
            self.USER_Surname = ko.observable();
            self.USER_Password = ko.observable();
            self.PasswordVerification = ko.observable();
            self.TYPE_Name = ko.observable();
            self.USER_TypeID = ko.observable();
            // CampusLink Details
            self.COL_Name = ko.observable();
            self.CAMP_Name = ko.observable();
            // Contact Details
            self.CONDET_CONTACTDETAILSID = ko.observable();
            self.CONDET_StreetNo = ko.observable();
            self.CONDET_StreetName = ko.observable();
            self.CONDET_Suburb = ko.observable();
            self.CONDET_City = ko.observable();
            self.CONDET_PostalCode = ko.observable();
            self.CONDET_Email = ko.observable();
            self.CONDET_CellNo = ko.observable();
            self.CONDET_TelNo = ko.observable();
            self.CONDET_FaxNo = ko.observable();
            // Varible to add client to a booking
            self.CourseCode = ko.observable();

            // College Combobox variables
            self.cboColleges = ko.observable();
            self.selectedCboCollege = ko.observable();

            // Campus Combox variables
            self.cboCampuses = ko.observable();
            self.selectedCboCampus = ko.observable();

            // ConfirmedBooking List
            self.bookings = ko.observableArray();
            self.selectedCourse = ko.observable();

            // Title Combobox variables
            self.titles = ko.observableArray([]);
            self.selectedTitle = ko.observable();

            // UserTypes ComboBox variables
            self.userTypes = ko.observableArray([]);
            self.selectedType = ko.observable();

            // Qualifcation
            self.qualification = ko.observableArray();
            self.userName = ko.observable();

            // Userid variable
            self.userid = ko.observable();

            // Load initial state from server.
            // Get method for Colleges  navbar ComboBox
            $.ajax({
                type: 'GET',
                url: '../Secure/Handlers/GetColleges.ashx',
                success: function (response) {
                    self.colleges(response);
                    self.sortCollegeByName();
                },
                error: function () {
                    alert('Cannot Retrieve Colleges from server');
                }
            });

            // Get method for Campuses navbar ComboBox
            $('#cboCollege').change(function () {
                if ($('#cboCollege').val() > 0) {
                    var Param = { 'CollegeID': $('#cboCollege').val() };
                    GetClientsByCollegeID(Param);
                    GetCampus(Param);
                    //$.getJSON('../Secure/Handlers/GetCampuses.ashx', Param, function (data) {
                    //    self.campuses(data);
                    //    self.sortCampusByName();
                    //});
                }
            });

            function GetClientsByCollegeID(param) {
                $.ajax({
                    type: 'GET',
                    url: '../Secure/Handlers/GetUserByCollegeID.ashx',
                    data: param,
                    success: function (data) {
                        self.clients(data);
                        self.sortUserBySurname();
                    }
                })
            };

            function GetCampus(param) {
                $.ajax({
                    type: 'GET',
                    url: '../Secure/Handlers/GetCampuses.ashx',
                    data: param,
                    success: function (data) {
                        self.campuses(data);
                        self.sortUserBySurname();
                    }
                })
            };

            // Get method for Campuses add new client ComboBox
            $('#inputCollege').change(function () {
                if ($('#inputCollege').val() > 0) {
                    var Param = { 'CollegeID': $('#inputCollege').val() };
                    $.getJSON('../Secure/Handlers/GetCampuses.ashx', Param, function (data) {
                        self.campuses(data);
                        self.sortCampusByName();
                    });
                }
            });

            // Get method for the clients according to searchbar campus combobox selection
            $('#cboCampus').change(function () {
                // if this function is call it wil check if the selected campus has a value or not
                if ($('#cboCampus').val() > 0) {
                    var Param = { 'CampusID': $('#cboCampus').val() };
                    $.getJSON("../Secure/Handlers/GetUserByCampusID.ashx", Param, function (data) {
                        self.clients(data);
                        self.sortUserBySurname();
                    });
                }
                else {
                    self.clients('');
                }
            });



            // Get method to get a list of all clients
            self.getClients = function () {
                var CollegeID = $('#cboCollege').val();
                var CampusID = $('#cboCampus').val();
                var SearchText = $('#search').val();
                var param = { 'CampusID': $('#cboCampus').val() };

                if (CollegeID > 0 && CampusID > 0 && SearchText.length > 0) {
                    self.getByUserSurname();
                    self.sortUserBySurname();
                }
                else if (CampusID > 0 && CollegeID > 0 && SearchText.length == 0) {
                    $.getJSON("../Secure/Handlers/GetUserByCampusID.ashx", param, function (data) {
                        self.clients(data);
                        self.sortUserBySurname();
                    });
                }
                else if (CollegeID > 0 && CampusID < 1 && SearchText.length == 0) {
                    $.getJSON('../Secure/Handlers/GetUserByCollegeID.ashx', CollegeID, function (data) {
                        self.clients(data);
                        self.sortUserBySurname();
                    });
                    $.getJSON('../Secure/Handlers/GetCampuses.ashx', CollegeID, function (data) {
                        self.campuses(data);
                        self.sortCampusByName();
                    });
                }
                else if (CollegeID < 1 && CampusID < 1 && SearchText.length > 0) {
                    self.getByUserSurname();
                }
                else {
                    $.ajax({
                        type: 'GET',
                        url: "../Secure/Handlers/GetUsers.ashx",
                        success: function (data) {
                            self.clients(data);
                            self.sortUserBySurname();
                        }
                    });
                }
            }

            // View button function
            // Get data specific client's data
            self.editClient = function (data) {
                self.collegeList();
                self.titleList();
                self.userTypeList();
                var Param = { 'UserID': ko.toJS(data.USER_UserID) };
                $.ajax({
                    type: 'GET',
                    url: "../Secure/Handlers/CollegeIDByUserId.ashx",
                    data: Param,
                    success: function (CollegeID) {
                        var Param2 = { 'CollegeID': ko.toJSON(CollegeID) };
                        $.getJSON('../Secure/Handlers/GetCampuses.ashx', Param2, function (cdata) {
                            self.cboCampuses(cdata);
                        });

                        $.getJSON("../Secure/Handlers/GetUserByID.ashx", Param, function (data) {
                            // User Details
                            self.USER_UserID(data.USER_UserID);
                            self.USER_Name(data.USER_Name);
                            self.USER_Surname(data.USER_Surname);
                            self.USER_Initials(data.USER_Initials);
                            self.selectedTitle(data.selectedTitle)
                            self.TITLE_Name(data.TITLE_Name);
                            self.USER_Password(data.USER_Password);
                            self.PasswordVerification(data.PasswordVerification);
                            self.TYPE_Name(data.TYPE_Name);
                            self.selectedType(data.selectedType);
                            // Campus link Details
                            self.COL_Name(data.COL_Name);
                            self.CAMP_Name(data.CAMP_Name);
                            //Contact Details
                            self.CONDET_CONTACTDETAILSID(data.CONDET_CONTACTDETAILSID);
                            self.CONDET_StreetNo(data.CONDET_StreetNo);
                            self.CONDET_StreetName(data.CONDET_StreetName);
                            self.CONDET_Suburb(data.CONDET_Suburb);
                            self.CONDET_City(data.CONDET_City);
                            self.CONDET_PostalCode(data.CONDET_PostalCode);
                            self.CONDET_Email(data.CONDET_Email);
                            self.CONDET_CellNo(data.CONDET_CellNo);
                            self.CONDET_TelNo(data.CONDET_TelNo);
                            self.CONDET_FaxNo(data.CONDET_FaxNo);
                            // Combo box selected values
                            self.selectedCboCollege(data.selectedCboCollege);
                            self.selectedCboCampus(data.selectedCboCampus);
                            self.CONDET_CONTACTDETAILSID(data.CONDET_CONTACTDETAILSID);
                        });
                    }
                });
                

                

            }

            // Get method for colleges combobox options
            self.collegeList = function () {
                $.ajax({
                    type: 'GET',
                    url: '../Secure/Handlers/GetColleges.ashx',
                    success: function (response) {
                        self.cboColleges(response);
                    },
                    error: function () {
                        alert('Cannot Retrieve Colleges from server');
                    }
                });
            }

            // Get method for campuses combobox options
            $('#selCollege').blur(function () {
                if ($('#selCollege').val() > 0) {
                    var Param = { 'CollegeID': $('#selCollege').val() };
                    $.getJSON('../Secure/Handlers/GetCampuses.ashx', Param, function (data) {
                        self.cboCampuses(data);
                    });
                }
            });

            // Get method to get Confirmed Bookings
            self.ConfirmedBookings = function () {
                $.ajax({
                    type: 'GET',
                    url: '../Secure/Handlers/GetConfirmedBookings.ashx',
                    success: function (response) {
                        self.bookings(response);
                    }
                })
            }
            self.ConfirmedBookings();

            // Get method for titles combobox options
            self.titleList = function () {
                $.getJSON("../Secure/Handlers/GetTitles.ashx", function (data) {
                    self.titles(data);
                });
            }

            // Get method for the usertype combobox options
            self.userTypeList = function () {
                $.getJSON("../Secure/Handlers/GetUserTypes.ashx", function (data) {
                    self.userTypes(data);
                });
            }

            // View a list of the selected user's qualifications
            self.viewQuals = function (data) {
                if (ko.toJS(data.USER_UserID) > 0) {
                    self.userid(data.USER_UserID);
                    var Param = { 'UserID': ko.toJS(data.USER_UserID) };
                    $.getJSON("../Secure/Handlers/GetUserQual.ashx", Param, function (data) {
                        self.qualification(data);
                    });
                }
                self.userName(data.USER_Name + " " + data.USER_Surname);
            }

            // This method shows the Client add form.
            self.addClient = function () {
                $('#clientForm').show();
                self.collegeList();
                self.titleList();
                self.userTypeList();
                self.hideAlerts();
            }

            self.clearFilters = function () {
                self.refresh();
            }

            // Method clears the variable values
            self.clear = function () {
                self.USER_UserID("")
                self.USER_Name("");
                self.USER_Surname("");
                self.USER_Initials("");
                self.TITLE_Name("");
                self.USER_Password("");
                self.PasswordVerification("");
                self.USER_TypeID(0);
                self.selectedCourse(0);
                self.selectedType(0);
                self.selectedTitle(0);
                // Campus link Details
                //self.selectedCboCollege(0);
                //self.selectedCboCampus(0);
                //self.selectedCollege(0);
                //self.selectedCampus(0);
                //Contact Details
                self.CONDET_CONTACTDETAILSID(0);
                self.CONDET_StreetNo("");
                self.CONDET_StreetName("");
                self.CONDET_Suburb("");
                self.CONDET_City("");
                self.CONDET_PostalCode("");
                self.CONDET_Email("");
                self.CONDET_CellNo("");
                self.CONDET_TelNo("");
                self.CONDET_FaxNo("");
                // CourseCOde
                self.CourseCode("");
            }

            // This method hides the edit input and show the normal view
            self.modalClose = function () {
                hideModalEdit();
                self.clear();
                self.hideAlerts();

            }

            // Get method to get all users
            self.getAllUsers = function () {
                $.ajax({
                    type: 'GET',
                    url: "../Secure/Handlers/GetUsers.ashx",
                    success: function (data) {
                        self.clients(data);
                        self.sortUserBySurname();
                    }
                });
            }
            // calls the method above
            self.getAllUsers();

            // ajax post method to Save the new user details 
            self.save = function () {
                var values = { 'UserId': ko.toJSON(0), 'type': ko.toJSON(1), 'UserName': ko.toJS(self.USER_Name()), 'UserSurname': ko.toJS(self.USER_Surname()), 'Initials': ko.toJS(self.USER_Initials()), 'TitleID': ko.toJS(self.selectedTitle()), 'Email': ko.toJS(self.CONDET_Email()), 'UserType': ko.toJSON(self.selectedType()), 'CollegeID': ko.toJSON(self.selectedCollege()), 'CampusID': ko.toJSON(self.selectedCampus()), 'Celno': ko.toJS(self.CONDET_CellNo()), 'Telno': ko.toJS(self.CONDET_TelNo()), 'Faxno': ko.toJS(self.CONDET_FaxNo()), 'Streetno': ko.toJS(self.CONDET_StreetNo()), 'StreetName': ko.toJS(self.CONDET_StreetName()), 'Suburb': ko.toJS(self.CONDET_Suburb()), 'City': ko.toJS(self.CONDET_City()), 'PostalCode': ko.toJS(self.CONDET_PostalCode()), 'Course': ko.toJS(self.selectedCourse()) };
                if (Validate()) {
                    $.ajax({
                        type: "POST",
                        url: "../Secure/Handlers/SetUserDetails.ashx",
                        //data: ko.mapping.toJSON(self.viewModel()),
                        data: values,
                        success: function (response, status, xhr) {
                            alert("User Registration Successful!");
                            self.getClients();
                            self.hideAlerts();
                            self.clear();
                        },
                        error: function () {
                            alert("Save failed!");
                        }
                    });
                }
                else {
                    alert("Please make sure all required fields in filled in");
                }
            };

            // Page refresh function
            self.refresh = function () {
                window.location.reload(true);
            }

            // ajax post method to update an existing user's details
            self.upadate = function () {
                var values = { 'UserId': ko.toJSON(self.USER_UserID), 'type': ko.toJS(2), 'UserName': ko.toJS(self.USER_Name()), 'UserSurname': ko.toJS(self.USER_Surname()), 'Initials': ko.toJS(self.USER_Initials()), 'TitleID': ko.toJS(self.selectedTitle()), 'Email': ko.toJS(self.CONDET_Email()), 'UserType': ko.toJSON(self.selectedType()), 'CollegeID': ko.toJSON(self.selectedCboCollege()), 'CampusID': ko.toJSON(self.selectedCboCampus()), 'Celno': ko.toJS(self.CONDET_CellNo()), 'Telno': ko.toJS(self.CONDET_TelNo()), 'Faxno': ko.toJS(self.CONDET_FaxNo()), 'Streetno': ko.toJS(self.CONDET_StreetNo()), 'StreetName': ko.toJS(self.CONDET_StreetName()), 'Suburb': ko.toJS(self.CONDET_Suburb()), 'City': ko.toJS(self.CONDET_City()), 'PostalCode': ko.toJS(self.CONDET_PostalCode()), 'Password': ko.toJS(self.USER_Password()), 'Course': ko.toJS(self.selectedCourse()), 'ContactDetailsID': ko.toJSON(self.CONDET_CONTACTDETAILSID) };
                if (ValidateUpdate()) {
                    $.ajax({
                        type: "POST",
                        url: "../Secure/Handlers/SetUserDetails.ashx",
                        data: values,
                        success: function (response, status, xhr) {
                            alert("Update is successful!");
                            hideModalEdit();
                            self.getClients();

                        },
                        error: function () {
                            alert("Update Failed!");
                        }
                    });
                }
                else {
                    alert("Please make sure all required fields in filled in");
                }
            };

            $("#search").keyup(function () {
                self.getClients();
            });

            self.getByUserSurname = function () {
                // If this function is called it wil check if the clientSurname is empty and call functions
                if (($('#search').val()).length == 0) {
                    self.getClients();
                }
                else if (ko.toJS(self.selectedCampus()) > 0) {
                    var Param = { 'campusID': $('#cboCampus').val(), 'surname': $('#search').val() }
                    $.ajax({
                        type: 'GET',
                        url: '../Secure/Handlers/GetUserByCampusIDAndUserID.ashx',
                        data: Param,
                        success: function (data) {
                            self.clients(data);
                        }
                    });
                }
                else {
                    var Param = { 'Surname': $('#search').val() };
                    $.ajax({
                        type: "GET",
                        url: "../Secure/Handlers/GetUserBySurname.ashx",
                        data: Param,
                        success: function (data) {
                            self.clients(data);
                        }
                    });
                }
            };

            // function to print the report
            self.print = function (data) {
                var values = { 'value1': ko.toJSON(1), 'value2': ko.toJSON(data.CLIQUAL_UserQualID), 'value3': ko.toJSON(self.userid) };
                $.ajax({
                    type: 'POST',
                    url: "../Secure/Handlers/SetReportParam.ashx",
                    data: values,
                    success: function () {
                        alert("Report page will be opened in new page or tab");
                        window.open("ReportPage.aspx");
                    },
                    error: function () {
                        alert("Print Failed!");
                    }
                });
            }

            // Function hides Validation functions
            self.hideAlerts = function () {
                $('#txtPasswordlength').hide();
                $('.txtVerifyStatus').hide();
                $('.glyphicon-ok-sign').hide();
                $('.glyphicon-remove').hide();
                $('.PasswordVerification').hide();
                $('#numcellerror').hide();
                $('#nummodelcellerror').hide();
                $('#numtelerror').hide();
                $('#nummodeltelerror').hide();
                $('#numfaxerror').hide();
                $('#nummodelfaxerror').hide();
                $('#breaklinemodelcell').hide();
                $('#breaklinetel').hide();
                $('#breaklinemodeltel').hide();
                $('#breaklinefax').hide();
                $('#breaklinemodelfax').hide();

                $('.formName').removeClass('has-success');
                $('.formName').removeClass('has-error');
                $('.formSurname').removeClass('has-success');
                $('.formSurname').removeClass('has-error');
                $('.fromInitial').removeClass('has-success');
                $('.fromInitial').removeClass('has-error');
                $('.formEmail').removeClass('has-success');
                $('.formEmail').removeClass('has-error');
                $('.formTitle').removeClass('has-success');
                $('.formTitle').removeClass('has-error');
                $('.formEmail').removeClass('has-success');
                $('.formEmail').removeClass('has-error');
                $('.formCollege').removeClass('has-success');
                $('.formCollege').removeClass('has-error');
                $('.formCampus').removeClass('has-success');
                $('.formCampus').removeClass('has-error');
                $('.formType').removeClass('has-success');
                $('.formType').removeClass('has-error');
                $('.formCourseCode').removeClass('has-success');
                $('.formCourseCode').removeClass('has-error');
                $('.formCellNo').removeClass('has-success');
                $('.formCellNo').removeClass('has-error');
                $('.formPassword').removeClass('has-success');
                $('.formPassword').removeClass('has-error');
                $('.formVerifyPassword').removeClass('has-success');
                $('.formVerifyPassword').removeClass('has-error');

                $('#trName').removeClass('danger');
                $('#trSurname').removeClass('danger');
                $('#trInitials').removeClass('danger');
                $('#trTitle').removeClass('danger');
                $('#trEmail').removeClass('danger');
                $('#trCollege').removeClass('danger');
                $('#trCampus').removeClass('danger');
                $('#trUserType').removeClass('danger');
                $('#trCellNo').removeClass('danger');
            };
            // Hide all the alerts on load
            self.hideAlerts();

            // Validation functions
            // Check if the title combo box is empty
            $('.cboTitle').blur(function () {
                if (ko.toJS(self.selectedTitle) > 0) {
                    $('.formTitle').removeClass('has-error');
                    $('.formTitle').addClass('has-success');
                }
                else {
                    $('.formTitle').addClass('has-error');
                    $('.formTitle').removeClass('has-success');
                }
            });
            // Check if the course code combo box is empty
            $('#inputCourseCode').blur(function () {
                if (ko.toJS(self.ReferNum) != "" && ko.toJS(self.ReferNum).length == 8) {
                    $('.formEmail').addClass('has-error');
                    $('.formEmail').removeClass('has-success');
                }
                else {
                    $('.formEmail').removeClass('has-error');
                    $('.formEmail').addClass('has-success');
                }
            });
            // Check if the campus textbox is empty
            $('.cboColleges').blur(function () {
                if (ko.toJS(self.selectedCollege) > 0) {
                    $('.formCollege').removeClass('has-error');
                    $('.formCollege').addClass('has-success');
                }
                else {
                    $('.formCollege').addClass('has-error');
                    $('.formCollege').removeClass('has-success');
                }
            });
            // Check if the campus textbox is empty
            $('.inputCampus').blur(function () {
                if (ko.toJS(self.selectedCampus) > 0) {
                    $('.formCampus').removeClass('has-error');
                    $('.formCampus').addClass('has-success');
                }
                else {
                    $('.formCampus').addClass('has-error');
                    $('.formCampus').removeClass('has-success');
                }
            });
            //Check if the usertype textbox is empty
            $('.selUserType').blur(function () {
                if (ko.toJS(self.selectedType) > 0) {
                    $('.formType').removeClass('has-error');
                    $('.formType').addClass('has-success');
                }
                else {

                    $('.formType').addClass('has-error');
                    $('.formType').removeClass('has-success');
                }
            });
            // Check if the cellno textbox is empty
            $('.inputCelno').blur(function () {
                if (ko.toJS(self.CellNo) != "" && $('.inputCelno').val().length == 10) {
                    $('.formCellNo').removeClass('has-error');
                    $('.formCellNo').addClass('has-success');
                }
                else {
                    $('.formCellNo').addClass('has-error');
                    $('.formCellNo').removeClass('has-success');
                }
            });

            // Modal Window Validations
            function hideModalEdit() {
                // Hides all edit fields
                $('#iptName').hide();
                $('#iptSurname').hide();
                $('#iptInitials').hide();
                $('#selTitle').hide();
                $('#iptEmail').hide();
                $('#iptPassword').hide();
                $('#sVerifyPassword').hide();
                $('#selType').hide();
                $('#iptCourseCode').hide();
                $('#selCollege').hide();
                $('#selCampus').hide();
                $('#iptCellNo').hide();
                $('#inputTelNo').hide();
                $('#inputFaxNo').hide();
                $('#inputStreetNo').hide();
                $('#inputStreetName').hide();
                $('#inputSuburb').hide();
                $('#inputCity').hide();
                $('#iptPostalCode').hide();
                // Show Normal View
                $('#sName').show();
                $('#sSurname').show();
                $('#sInitials').show();
                $('#sTitle').show();
                $('#sEmail').show();
                $('#sPassword').show();
                $('#sType').show();
                $('#AddCourscode').show();
                $('#sCollege').show();
                $('#sCampus').show();
                $('#sCellNo').show();
                $('#sTelNo').show();
                $('#sFax').show();
                $('#sStreetNo').show();
                $('#sStreetName').show();
                $('#sSuburb').show();
                $('#sCity').show();
                $('#sPostalCode').show();
            }
            // call the method mentioned above
            hideModalEdit()

            // Functions shows the edit view on click in modal view
            // View edit view for Name field
            $('#btnEditName').click(function () {
                $('#sName').hide();
                $('#iptName').show();
            });
            // View Edit view for surname field
            $('#btnEditSurname').click(function () {
                $('#sSurname').hide();
                $('#iptSurname').show();
            });
            // View edit view for intials field
            $('#btnEditInitials').click(function () {
                $('#sInitials').hide();
                $('#iptInitials').show();
            });
            // View edit view for title field
            $('#btnEditTitle').click(function () {
                $('#sTitle').hide();
                $('#selTitle').show();
            });
            // View edit view for email field
            $('#btnEditEmail').click(function () {
                $('#sEmail').hide();
                $('#iptEmail').show();
            });
            // View edit view for passowrd field
            $('#btnEditPassword').click(function () {
                $('#sPassword').hide();
                $('#iptPassword').show();
                $('#sVerifyPassword').show();
                $('.PasswordVerification').show();
            });
            // View edit view for usertype field
            $('#btnEditUserType').click(function () {
                $('#sType').hide();
                $('#selType').show();
            });
            // View edit view for add course field
            $('#btnAddCourse').click(function () {
                $('#AddCourscode').hide();
                $('#iptCourseCode').show();
            });
            // View edit view for College field
            $('#btnEditCollege').click(function () {
                $('#sCollege').hide();
                $('#selCollege').show();
            });
            // View edit view for campus field
            $('#btnEditCampus').click(function () {
                $('#sCampus').hide();
                $('#selCampus').show();
            });
            // View edit view for cellphone field
            $('#btnEditCellno').click(function () {
                $('#sCellNo').hide();
                $('#iptCellNo').show();
            });
            // View edit view for telephone field
            $('#btnEditTellno').click(function () {
                $('#sTelNo').hide();
                $('#inputTelNo').show();
            });
            // View edit view for fax field
            $('#btnEditFax').click(function () {
                $('#sFax').hide();
                $('#inputFaxNo').show();
            });
            // View edit view for streetno field
            $('#btnEditStreetNo').click(function () {
                $('#sStreetNo').hide();
                $('#inputStreetNo').show();
            });
            // View edit view for streetname field
            $('#btnEditStreetName').click(function () {
                $('#sStreetName').hide();
                $('#inputStreetName').show();
            });
            // View edit view for suburb field
            $('#btnEditSuburb').click(function () {
                $('#sSuburb').hide();
                $('#inputSuburb').show();
            });
            // View edit view for city field
            $('#btnEditCity').click(function () {
                $('#sCity').hide();
                $('#inputCity').show();
            });
            // View edit view for postalcode field
            $('#btnEditPostalCode').click(function () {
                $('#sPostalCode').hide();
                $('#iptPostalCode').show();
            });

            // Data input validations
            // Check that no required text field is empty
            function Validate() {
                var valid = true;

                if ($("#inputName").val() == "") {
                    valid = false;
                }

                if ($("#inputSurname").val() == "") {
                    valid = false;
                }

                if ($("#inputInitials").val() == "") {
                    valid = false;
                }

                if ($("#cboTitle").val() < 1) {
                    valid = false;
                }

                if ($("#inputEmail").val() == "") {
                    valid = false;
                }

                if ($("#inputCollege").val() < 1) {
                    valid = false;
                }

                if ($("#inputCampus").val() < 1) {
                    valid = false;
                }

                if ($("#cboUsertype").val() < 1) {
                    valid = false;
                }

                if ($("#inputCelno").val() == "") {
                    valid = false;
                }

                return valid;
            }

            // Data input validations
            // Check that no required text field is empty
            function ValidateUpdate() {
                var valid = true;

                if ($("#sName").val() == "" && $(".inputName").val() == "") {
                    valid = false;
                    $('#trName').addClass('danger');
                }
                else {
                    $('#trName').removeClass('danger');
                }

                if ($("#sSurname").val() == "" && $('.inputSurname').val() == "") {
                    valid = false;
                    $('#trSurname').addClass('danger');
                }
                else {
                    $('#trSurname').removeClass('danger');
                }

                if ($("#sInitials").val() == "" && $("inputInitials").val() == "") {
                    valid = false;
                    $('#trInitials').addClass('danger');
                }
                else {
                    $('#trInitials').removeClass('danger');
                }

                if ($("#sTitle").val() == "" && $(".cboTitle").val() < 1) {
                    valid = false;
                    $('#trTitle').addClass('danger');
                }
                else {
                    $('#trTitle').removeClass('danger');
                }

                if ($("#sEmail").val() == "" && $(".inputEmail").val() == "") {
                    valid = false;
                    $('#trEmail').addClass('danger');
                }
                else {
                    $('#trEmail').removeClass('danger');
                }

                if ($(".inputCollege").val() < 1 && $('#sCollege').val() == "") {
                    valid = false;
                    $('#trCollege').addClass('danger');
                    $('#trCampus').addClass('danger');
                }
                else {
                    $('#trCollege').removeClass('danger');
                    $('#trCampus').removeClass('danger');
                }

                if ($(".inputCelno").val() == "" && $("#sCellNo").val() == "") {
                    valid = false;
                    $('#trCellNo').addClass('danger');
                }
                else {
                    $('#trCellNo').removeClass('danger');
                }

                return valid;
            }

            // Sort function user list
            self.sortUserBySurname = function () {
                self.clients.sort(function (left, right) {
                    return left.USER_Surname == right.USER_Surname ? 0 : (left.USER_Surname < right.USER_Surname ? -1 : 1)
                });
            }

            // Sort function for college list
            self.sortCollegeByName = function () {
                self.colleges.sort(function (left, right) {
                    return left.CollegeName == right.CollegeName ? 0 : (left.CollegeName < right.CollegeName ? -1 : 1)
                });
            }

            // Sort function for campus list
            self.sortCampusByName = function () {
                self.campuses.sort(function (left, right) {
                    return left.CampusName == right.CampusName ? 0 : (left.CampusName < right.CampusName ? -1 : 1)
                });
            }


            // radio button variable
            self.typeToShow = ko.observable("all");
            // checkbox variable
            self.displayAdvancedOptions = ko.observable(true);


            self.clientsToShow = ko.computed(function () {
                // Represents a filtered list of clients
                // i.e., only those matching the "typeToShow" condition
                var desiredType = self.typeToShow();
                if (desiredType == "all") return self.clients();
                return ko.utils.arrayFilter(self.clients(), function (planet) {
                    return planet.USER_TypeID == desiredType;
                });
            }, self);

            // Animation callbacks for the clients list
            self.showClienttElement = function (elem) { if (elem.nodeType === 1) $(elem).hide().slideDown() }
            self.hideClientElement = function (elem) { if (elem.nodeType === 1) $(elem).slideUp(function () { $(elem).remove(); }) }
        };

        // Here's a custom Knockout binding that makes elements shown/hidden via jQuery's fadeIn()/fadeOut() methods
        // Could be stored in a separate utility library
        ko.bindingHandlers.fadeVisible = {
            init: function (element, valueAccessor) {
                // Initially set the element to be instantly visible/hidden depending on the value
                var value = valueAccessor();
                $(element).toggle(ko.unwrap(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
            },
            update: function (element, valueAccessor) {
                // Whenever the value subsequently changes, slowly fade the element in or out
                var value = valueAccessor();
                ko.unwrap(value) ? $(element).fadeIn() : $(element).fadeOut();
            }
        };

        // Knockout bindings are applied here
        ko.applyBindings(new viewModel());
    </script>
</asp:Content>
