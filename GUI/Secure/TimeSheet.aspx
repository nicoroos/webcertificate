﻿<%@ Page Title="Time Sheet" Language="C#" MasterPageFile="~/Secure/Secure.Master" AutoEventWireup="true" CodeBehind="TimeSheet.aspx.cs" Inherits="GUI.Secure.TimeSheet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Body" runat="server">
    <div>
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Time Sheet</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control dates" placeholder="Start Date" data-bind="value: sDateSearch" />
                        <input type="text" class="form-control dates" placeholder="End Date" data-bind="value: eDateSearch" />
                    </div>
                    <button type="button" id="btnsearch" class="btn btn-primary" data-bind="click: Search">Search</button>
                    <button type="button" id="Button1" class="btn btn-primary" data-bind="click: searchClear">Clear</button>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <%-- Main Table with list of Clients --%>
        <div>
            <div>
                <table class="table table-hover table-striped table-bordered">
                    <thead>
                        <tr>
                            <th rowspan="2">Date</th>
                            <%--<th colspan="2">Time</th>--%>
                            <th rowspan="2">Hours</th>
                            <th colspan="2">Location</th>
                            <th rowspan="2">ODO</th>
                            <%--<th rowspan="2">Rates Per KM</th>--%>
                            <%--<th rowspan="2">Other</th>--%>
                            <%--<th rowspan="2">Toll</th>--%>
                            <th rowspan="2">Nights</th>
                            <%--<th rowspan="2">Food</th>--%>
                            <%--<th colspan="2">Flight</th>--%>
                            <th rowspan="2">Total
                            <br />
                                Expense</th>
                            <th rowspan="2"></th>
                        </tr>
                        <tr>
                            <%--<th>Start</th>
                            <th>End</th>--%>
                            <th>College</th>
                            <th>Campus</th>
                            <%--<th>Start</th>
                            <th>End</th>--%>
                            <%--<th>Parking</th>
                            <th>Transport</th>--%>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: timesheetEntries">
                        <tr>
                            <td class="normalView" data-bind="text: TIME_Date"></td>
                            <%--<td class="normalView" data-bind="text: StartTime"></td>
                            <td class="normalView" data-bind="text: EndTime"></td>--%>
                            <td class="normalView" data-bind="text: TIME_Hours"></td>
                            <td class="normalView" data-bind="text: COL_Name"></td>
                            <td class="normalView" data-bind="text: CAMP_Name"></td>
                            <%--<td class="normalView" data-bind="text: ODOStart"></td>
                            <td class="normalView" data-bind="text: ODOEnd"></td>--%>
                            <td class="normalView" data-bind="text: KMTraveled"></td>
                            <%--<td><span data-bind="text: RatesName"></span>&nbsp;R<span data-bind="    text: RatesPerKM"></span></td>--%>
                            <%--<td class="normalView">R<span data-bind="text: Other"></span></td>--%>
                            <%--<td class="normalView">R<span data-bind="text: Toll"></span></td>--%>
                            <td class="normalView"><span data-bind="text: TIME_Nights"></span></td>
                            <%-- <td class="normalView">R<span data-bind="text: Food"></span></td>--%>
                            <%--<td class="normalView">R<span data-bind="text: FlightParking"></span></td>
                            <td class="normalView">R<span data-bind="text: FlightTransport"></span></td>--%>
                            <td><strong>R<span data-bind="text: formattedPrice"></span></strong></td>
                            <td><a href="#modalEditEntry" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.editClient">View</a></td>
                        </tr>
                    </tbody>
                </table>
                <a href="#modalAddEntry" role="button" class="btn btn-primary" data-toggle="modal" data-bind="click: addEntry">Add New Entry</a>
            </div>
            <!-- Modal - Edit View -->
            <div id="modalEditEntry" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalNormalview">×</button>
                            <h3 id="myModalLabel">Entry Details</h3>
                        </div>
                        <div class="modal-body">
                            <table class="table table-hover table-striped">
                                <tr>
                                    <th>Date</th>
                                    <td>
                                        <div class="col-xs-10">
                                            <span id="sDate" data-bind="text: TIME_Date"></span>
                                            <input id="txtDate" class="form-control" data-bind="value: TIME_Date" type="text" placeholder="Date" />
                                        </div>
                                        <button type="button" id="editDate" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Start Time</th>
                                    <td>
                                        <div class="col-xs-10">
                                            <span id="sStartTime" data-bind="text: TIME_StartTime"></span>
                                            <input class="form-control inputStartTime" data-bind="value: TIME_StartTime" type="text" placeholder="Start Time" />
                                        </div>
                                        <button type="button" id="editStartTime" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>End Time</th>
                                    <td>
                                        <div class="col-xs-10">
                                            <span id="sEndTime" data-bind="text: TIME_EndTime"></span>
                                            <input class="form-control inputEndTime" data-bind="value: TIME_EndTime" type="text" placeholder="End Time" />
                                        </div>
                                        <button type="button" id="editEndTime" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>College</th>
                                    <td>
                                        <div class="col-xs-10">
                                            <span id="sCollege" data-bind="text: COL_Name"></span>
                                            <select class="cboCollege form-control" id="cboCollege" data-bind="options: colleges, optionsText: 'COL_Name', optionsValue: 'COL_CollegeID', optionsCaption: 'Select College..', value: selectedCollege"></select>
                                        </div>
                                        <button type="button" id="editCollege" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Campus</th>
                                    <td>
                                        <div class="col-xs-10">
                                            <span id="sCampus" data-bind="text: CAMP_Name"></span>
                                            <select class="cboCampus form-control" data-bind="options: campuses, optionsText: 'CAMP_Name', optionsValue: 'CAMP_CampusID', optionsCaption: 'Select Campus..', value: selectedCampus"></select>
                                        </div>
                                        <button type="button" id="editCampus" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ODO Start</th>
                                    <td>
                                        <div class="col-xs-10">
                                            <span id="sODOStart" data-bind="text: TIME_ODOStart"></span>KM
                                        <input class="form-control inputODOStart" maxlength="10" data-bind="value: TIME_ODOStart" type="text" placeholder="ODO Start" />
                                        </div>
                                        <button type="button" id="editODOStart" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>ODO End</th>
                                    <td>
                                        <div class="col-xs-10">
                                            <span id="sODOEnd" data-bind="text: TIME_ODOEnd"></span>KM
                                        <input class="form-control inputODOEnd" maxlength="10" data-bind="value: TIME_ODOEnd" type="text" placeholder="ODO End" />
                                        </div>
                                        <button type="button" id="editODOEnd" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Other</th>
                                    <td>
                                        <div class="col-xs-10">
                                            R <span id="sOther" data-bind="text: COST_Other"></span>
                                            <input class="form-control inputOther" data-bind="value: COST_Other" type="text" placeholder="Other" />
                                        </div>
                                        <button type="button" id="editOther" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Toll</th>
                                    <td>
                                        <div class="col-xs-10">
                                            R<span id="sToll" data-bind="text: COST_Toll"></span>
                                            <input class="form-control inputToll" data-bind="value: COST_Toll" type="text" placeholder="Toll" />
                                        </div>
                                        <button type="button" id="editToll" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Nights</th>
                                    <td>
                                        <div class="col-xs-10">
                                            <span id="sNights" data-bind="text: TIME_Nights"></span>
                                            <select class=" form-control inputNights" data-bind="value: TIME_Nights">
                                                <option>0</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                                <option>7</option>
                                                <option>8</option>
                                                <option>9</option>
                                                <option>10</option>
                                                <option>11</option>
                                                <option>12</option>
                                                <option>13</option>
                                                <option>14</option>
                                                <option>15</option>
                                            </select>
                                        </div>
                                        <button type="button" id="editNights" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Food</th>
                                    <td>
                                        <div class="col-xs-10">
                                            R<span id="sFood" data-bind="text: COST_Food"></span>
                                            <input class="form-control inputFood" data-bind="value: COST_Food" type="text" placeholder="Food" />
                                        </div>
                                        <button type="button" id="editFood" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Flight Parking</th>
                                    <td>
                                        <div class="col-xs-10">
                                            R<span id="sFlightParking" data-bind="text: COST_FlightParking"></span>
                                            <input class="form-control inputFlightParking" data-bind="value: COST_FlightParking" type="text" id="iptFlightParking" placeholder="Flight Parking" />
                                        </div>
                                        <button type="button" id="editFlightParking" class="pull-right btn"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Flight Transport</th>
                                    <td>
                                        <div class="col-xs-10">
                                            R<span id="sFlightTransport" data-bind="text: COST_FlightTransport"></span>
                                            <input class="form-control inputFlightTransport" data-bind="value: COST_FlightTransport" type="text" id="iptFlightTransport" placeholder="FlightTransport" />
                                        </div>
                                        <button type="button" id="editFlightTransport" class="btn pull-right"><span class="glyphicon glyphicon-pencil"></span></button>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Total</th>
                                    <td>
                                        <div class="col-xs-10">
                                            R<span id="sTotal" data-bind="text: Total"></span>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalNormalview">Close</button>
                            <button class="btn btn-success" data-bind="click: update">Save changes</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <%-- Modal - Add Entry --%>
            <br />
            <div id="modalAddEntry" class="modal fade modal-wide" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" data-bind="click: modalNormalview">×</button>
                            <h3 id="H1">Add Entry Details</h3>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal" id="addSheet">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputDate">Date:</label>
                                            <div class="col-xs-5">
                                                <input type="date" id="inputDate" class="form-control dates" placeholder="Date" data-bind="value: TIME_Date" />
                                                <%--<div id="inputNameNotBlank" class="icon-ok-sign inputNameNotBlank"></div>
                                <div id="inputNameBlank" class="icon-remove-sign inputNameBlank"></div>--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputStartTime">Start Time:</label>
                                            <div class="col-xs-5">
                                                <input type="text" id="inputStartTime" class="form-control" placeholder="Start Time" data-bind="value: TIME_StartTime" />
                                                <%--<div id="inputSurnameNotBlank" class="icon-ok-sign inputSurnameNotBlank"></div>
                                <div id="inputSurnameBlank" class="icon-remove-sign inputSurnameBlank"></div>--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputEndTime">End Time:</label>
                                            <div class="col-xs-5">
                                                <input type="text" id="inputEndTime" class="form-control" placeholder="End Time" data-bind="value: TIME_EndTime" />
                                                <%--<div id="inputInitialsNotBlank" class=" icon-ok-sign inputInitialsNotBlank"></div>
                                <div id="inputInitialsBlank" class="icon-remove-sign inputInitialsBlank"></div>--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputCollege">College:</label>
                                            <div class="col-xs-5">
                                                <select class="cboCollege form-control" id="inputCollege" data-bind="options: colleges, optionsText: 'COL_Name', optionsValue: 'COL_CollegeID', optionsCaption: 'Select College..', value: selectedCollege">
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputCampus">Campus:</label>
                                            <div class="col-xs-5">
                                                <select class="inputCampus form-control" data-bind="options: campuses, optionsText: 'CAMP_Name', optionsValue: 'CAMP_CampusID', optionsCaption: 'Select Campus..', value: selectedCampus">
                                                </select>
                                                <%--<div id="inputCampusNotBlank" class=" icon-ok-sign inputCampusNotBlank"></div>
                                                <div id="inputCampusBlank" class="icon-remove-sign inputCampusBlank"></div>--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputCampus">Rates:</label>
                                            <div class="col-xs-5">
                                                <select class="input-small form-control" data-bind="options: $root.ratePerKM, optionsText: 'RATES_Name', optionsValue: 'RATES_RatePerKM', optionsCaption: 'Other', value: selectedRate"></select>
                                                <%--<div id="inputCampusNotBlank" class=" icon-ok-sign inputCampusNotBlank"></div>
                                                <div id="inputCampusBlank" class="icon-remove-sign inputCampusBlank"></div>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputODOStart">ODO Start:</label>
                                            <div class="col-xs-5">
                                                <input type="text" id="inputODOStart" class="form-control" placeholder="ODO Start" data-bind="value: TIME_ODOStart" maxlength="10" />
                                                <%--<div id="inputCelnoNotBlank" class="icon-ok-sign inputCelnoNotBlank"></div>
                                                <div id="inputCelnoBlank" class="icon-remove-sign inputCelnoBlank"></div>--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputODOEnd">ODO End:</label>
                                            <div class="col-xs-5">
                                                <input type="text" id="inputODOEnd" class="form-control" placeholder="ODO End" data-bind="value: TIME_ODOEnd" maxlength="10" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="">Extra KM:</label>
                                            <div class="col-xs-5">
                                                <input type="checkbox" id="isExtraSelected" class="checkbox" />
                                            </div>
                                        </div>
                                        <div id="divExtraKm" class="form-group" style="display: none">
                                            <label class="col-xs-3 control-label" for="">Extra KM:</label>
                                            <div class="col-xs-5">
                                                <input type="text" id="txtExtraKm" class="form-control" placeholder="Extra Kilometers" />
                                            </div>
                                        </div>
                                        <div id="divReason" class="form-group" style="display: none">
                                            <label class="col-xs-3 control-label" for="">Reason:</label>
                                            <div class="col-xs-5">
                                                <input type="text" id="txtReason" class="form-control" placeholder="Reason" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputOther">Other:</label>
                                            <div class="col-xs-5">
                                                <input type="text" id="inputOther" class="form-control" placeholder="Other" data-bind="value: COST_Other" maxlength="7" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputNights">Nights:</label>
                                            <div class="col-xs-5">
                                                <select class="form-control" data-bind="value: TIME_Nights">
                                                    <option>0</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    <option>6</option>
                                                    <option>7</option>
                                                    <option>8</option>
                                                    <option>9</option>
                                                    <option>10</option>
                                                    <option>11</option>
                                                    <option>12</option>
                                                    <option>13</option>
                                                    <option>14</option>
                                                    <option>15</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputToll">Toll:</label>
                                            <div class="col-xs-5">
                                                <input type="text" id="inputToll" class="form-control" placeholder="Toll" data-bind="value: COST_Toll" maxlength="7" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputFood">Food:</label>
                                            <div class="col-xs-5">
                                                <input type="text" id="inputFood" class="form-control" placeholder="Food" data-bind="value: COST_Food" maxlength="7" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputFlight">Flight</label>
                                            <div class="col-xs-5">
                                                <input type="text" id="inputFlight" class="form-control" placeholder="Transport" data-bind="value: COST_FlightTransport" maxlength="7" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-3 control-label" for="inputParking"></label>
                                            <div class="col-xs-5">
                                                <input type="text" id="inputParking" class="form-control" placeholder="Parking" data-bind="value: COST_FlightParking" maxlength="7" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true" data-bind="click: modalNormalview">Close</button>
                            <button type="button" id="btnSave" class="btn btn-success" data-bind="click: save">Save</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScript" runat="server">
    <script type="text/javascript">
        // Class to represent a row in the timesheet table
        function TimeSheetEntry(data) {
            var self = this;

            self.TIME_TimeSheetID = ko.observable(data.TIME_TimeSheetID);
            self.TIME_Date = ko.observable(data.TIME_Date);
            self.TIME_StartTime = ko.observable(data.TIME_StartTime);
            self.TIME_EndTime = ko.observable(data.TIME_EndTime);
            self.TIME_Hours = ko.observable(data.TIME_Hours);
            self.COL_Name = ko.observable(data.COL_Name);
            self.CAMP_Name = ko.observable(data.CAMP_Name);

            self.RATES_Name = ko.observable(data.RATES_Name);
            self.RATES_RatePerKM = ko.observable(data.RATES_RatePerKM);

            self.COST_Other = ko.observable(data.COST_Other);
            self.COST_Toll = ko.observable(data.COST_Toll);
            self.TIME_Nights = ko.observable(data.TIME_Nights);
            self.COST_Food = ko.observable(data.COST_Food);
            self.COST_FlightParking = ko.observable(data.COST_FlightParking);
            self.COST_FlightTransport = ko.observable(data.COST_FlightTransport);

            self.TIME_ODOStart = ko.observable(data.TIME_ODOStart);
            self.TIME_ODOEnd = ko.observable(data.TIME_ODOEnd);
            self.selectedRate = ko.observable(data.initialMeal);
            self.KMTraveled = ko.computed(function () {
                var total = self.TIME_ODOEnd() - self.TIME_ODOStart();
                return total;
            });
            self.formattedPrice = ko.computed(function () {
                var price = self.RATES_RatePerKM();
                var km = self.KMTraveled();
                var traveltotal = price * km;
                var expenses = self.COST_Other() + self.COST_Toll() + self.COST_Food() + self.COST_FlightParking() + self.COST_FlightTransport();
                return traveltotal + expenses;
            });
        }

        // Overall viewmodel for this screen, along with initial state
        function ViewModel() {
            var self = this;
            // variables reprenting the new time sheet entry data
            self.TIME_TimeSheetID = ko.observable();
            self.TIME_Date = ko.observable();
            self.TIME_StartTime = ko.observable();
            self.TIME_EndTime = ko.observable();
            self.TIME_Hours = ko.computed(function () {
                return parseFloat(self.TIME_EndTime()) - parseFloat(self.TIME_StartTime());
            });

            self.selectedCollege = ko.observable();
            self.COL_Name = ko.observable();
            self.selectedCampus = ko.observable();
            self.CAMP_Name = ko.observable();

            self.selectedRate = ko.observable();
            self.RATES_RatePerKM = ko.observable();
            self.RATES_Name = ko.observable();

            self.COST_Other = ko.observable();
            self.COST_Toll = ko.observable();
            self.TIME_Nights = ko.observable();
            self.COST_Food = ko.observable();
            self.COST_FlightParking = ko.observable();
            self.COST_FlightTransport = ko.observable();

            self.TIME_ODOStart = ko.observable();
            self.TIME_ODOEnd = ko.observable();

            self.Total = ko.computed(function () {
                var Distance = (self.TIME_ODOEnd() - self.TIME_ODOStart()) * self.RATES_RatePerKM();
                var Price = self.COST_Other() + self.COST_Toll() + self.COST_Food() + self.COST_FlightParking() + self.COST_FlightTransport();
                var hhh = Distance + Price;
                var ra = self.RATES_RatePerKM();
                return hhh;
            });

            // Search variables
            self.sDateSearch = ko.observable();
            self.eDateSearch = ko.observable();

            // Editable catalog data - would come from server
            self.timesheetEntries = ko.observableArray([]);

            // Non-editable catalog data - would come from the server
            self.ratePerKM = ko.observableArray([]);

            // Non-editable catalog data - would come from the server
            self.colleges = ko.observableArray([]);

            // Non-editable catalog data - would come from the server
            self.campuses = ko.observableArray([]);

            // Load initial state from server, then populate self.colleges
            $.ajax({
                type: "GET",
                url: "../Secure/Handlers/GetColleges.ashx",
                success: function (data) {
                    self.colleges(data);
                }
            });


            // Load initial state from server, then populate self.campuses
            $('.cboCollege').blur(function () {
                if (ko.toJS(self.selectedCollege) > 0) {
                    var Param = { 'CollegeID': ko.toJSON(self.selectedCollege) };
                    $.getJSON('../Secure/Handlers/GetCampuses.ashx', Param, function (data) {
                        self.campuses(data);
                    });
                }
            });

            // Load initial state from server, then populate self.ratePerKM
            $.ajax({
                type: "GET",
                url: "../Secure/Handlers/GetRatePerKM.ashx",
                success: function (data) {
                    self.ratePerKM(data);
                }
            });

            // Load initial state from server, convert it to Task instances, then populate self.tasks
            self.getTimeSheet = function () {
                $.getJSON("../Secure/Handlers/GetTimeSheets.ashx", function (data) {
                    var mappedTasks = $.map(data, function (item) { return new TimeSheetEntry(item) });
                    self.timesheetEntries(mappedTasks);
                });
            };

            self.getTimeSheet();

            self.Search = function () {
                if (ko.toJS(self.sDateSearch()) != null || ko.toJS(self.eDateSearch()) != null) {
                    var values = { 'Start': ko.toJS(self.sDateSearch()), 'End': ko.toJS(self.eDateSearch()) };
                    $.getJSON("../Secure/Handlers/GetTimeSheetByDates.ashx", values, function (data) {
                        var mappedTasks = $.map(data, function (item) { return new TimeSheetEntry(item) });
                        self.timesheetEntries(mappedTasks);
                    });
                }
                else {
                    self.timesheetEntries();
                }
            };

            // Send data to the server, send all the data in die timesheetentries array to the server
            self.save = function () {
                $.ajax({
                    type: "POST",
                    url: "../Secure/Handlers/SetTimeSheet.ashx",
                    data: ko.mapping.toJSON(self),
                    success: function () {
                        alert("Saved Successfully");
                        self.getTimeSheet();
                        self.cancel();
                    },
                    error: function () {
                        alert("Save Failed");
                    }
                });
            };
        

        self.update = function () {
            $.ajax({
                type: "POST",
                url: "../Secure/Handlers/SetTimeSheet.ashx",
                data: ko.mapping.toJSON(self),
                success: function (response) {
                    alert("Changes Save Successfully");
                    self.getTimeSheet();
                    self.cancel();
                    self.modalNormalview();
                    self.editClient(response);
                }
            });
        };

        // Get data to Edit and view the specific record's Data
        self.editClient = function (data) {
            var Param = { 'TimeSheetID': ko.toJS(data.TIME_TimeSheetID) };
            $.getJSON("../Secure/Handlers/GetTimeSheet.ashx", Param, function (data) {
                self.TIME_TimeSheetID(data.TIME_TimeSheetID);
                self.TIME_Date(data.TIME_Date);
                self.TIME_StartTime(data.TIME_StartTime);
                self.TIME_EndTime(data.TIME_EndTime);
                self.COL_Name(data.COL_Name);
                self.CAMP_Name(data.CAMP_Name);

                self.TIME_ODOStart(data.TIME_ODOStart);
                self.TIME_ODOEnd(data.TIME_ODOEnd);

                self.COST_Other(data.COST_Other);
                self.COST_Toll(data.COST_Toll);
                self.TIME_Nights(data.TIME_Nights);
                self.COST_Food(data.COST_Food);
                self.COST_FlightParking(data.COST_FlightParking);
                self.COST_FlightTransport(data.COST_FlightTransport);

                self.RATES_RatePerKM(data.RATES_RatePerKM);
                self.RATES_Name(data.RATES_Name);
            });
        }

        // Operations
        self.cancel = function () {
            $("#addSheet").hide();
        };

        // Operations
        self.addEntry = function () {
            $("#addSheet").show();

        }
        // Hide en show certain views
        function normalView() {
            $(".normalView").show();
            $(".editView").hide();
            $("#addSheet").hide();

        }

        normalView();

        function editview() {
            $(".normalView").hide();
            $(".editView").show();
        }

        self.clear = function () {

            self.TIME_Date("");
            self.TIME_StartTime("");
            self.TIME_EndTime("");
            self.COL_Name("");
            self.CAMP_Name("");

            self.TIME_ODOStart("");
            self.TIME_ODOEnd("");

            self.COST_Other("");
            self.COST_Toll("");
            self.TIME_Nights("");
            self.COST_Food("");
            self.COST_FlightParking("");
            self.COST_FlightTransport("");

            self.RATES_RatePerKM("");
            self.RATES_Name("");
        };

        self.searchClear = function () {
            self.getTimeSheet();
            self.sDateSearch("");
            self.eDateSearch("");
        };

        // Modal window normal view
        self.modalNormalview = function () {
            $("#txtDate").hide();
            $("#sDate").show();
            $(".inputStartTime").hide();
            $("#sStartTime").show();
            $(".inputEndTime").hide();
            $("#sEndTime").show();
            $("#cboCollege").hide();
            $("#sCollege").show();
            $(".cboCampus").hide();
            $("#sCampus").show();
            $(".inputODOStart").hide();
            $("#sODOStart").show();
            $(".inputODOEnd").hide();
            $("#sODOEnd").show();
            $(".inputOther").hide();
            $("#sOther").show();
            $(".inputToll").hide();
            $("#sToll").show();
            $(".inputNights").hide();
            $("#sNights").show();
            $(".inputFood").hide();
            $("#sFood").show();
            $(".inputFlightParking").hide();
            $("#sFlightParking").show();
            $(".inputFlightTransport").hide();
            $("#sFlightTransport").show();
            self.clear();
        };
        self.modalNormalview();

        // Modal window edit view
        $("#editDate").click(function () {
            $("#sDate").hide();
            $("#txtDate").show();
        });

        $("#editStartTime").click(function () {
            $("#sStartTime").hide();
            $(".inputStartTime").show();
        });

        $("#editEndTime").click(function () {
            $("#sEndTime").hide();
            $(".inputEndTime").show();
        });

        $("#editCollege").click(function () {
            $("#sCollege").hide();
            $("#cboCollege").show();
        });

        $("#editCampus").click(function () {
            $("#sCampus").hide();
            $(".cboCampus").show();
        });

        $("#editODOStart").click(function () {
            $("#sODOStart").hide();
            $(".inputODOStart").show();
        });

        $("#editODOEnd").click(function () {
            $("#sODOEnd").hide();
            $(".inputODOEnd").show();
        });

        $("#editOther").click(function () {
            $("#sOther").hide();
            $(".inputOther").show();
        });

        $("#editToll").click(function () {
            $("#sToll").hide();
            $(".inputToll").show();
        });

        $("#editNights").click(function () {
            $("#sNights").hide();
            $(".inputNights").show();
        });

        $("#editFood").click(function () {
            $("#sFood").hide();
            $(".inputFood").show();
        });

        $("#editFlightTransport").click(function () {
            $("#sFlightTransport").hide();
            $(".inputFlightTransport").show();
        });

        $("#editFlightParking").click(function () {
            $("#sFlightParking").hide();
            $(".inputFlightParking").show();
        });

        // Datepicker jqueryUI function
        $(function () {
            $(".dates").datepicker({ dateFormat: "yy-mm-dd" });
        });

        // if checkbox is checked the fields visible
        $('#isExtraSelected').click(function () {
            $("#divExtraKm").toggle(this.checked);
            $("#divReason").toggle(this.checked);
        });
        }

        ko.applyBindings(new ViewModel());

    </script>
</asp:Content>
