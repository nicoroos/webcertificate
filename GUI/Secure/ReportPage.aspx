﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportPage.aspx.cs" Inherits="GUI.Secure.ReportPage" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="/crystalreportviewers13/js/crviewer/crv.js"></script>
    <script type="text/javascript" src="/crystalreportviewers/js/crviewer/crv.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <table>
            <tr>
                <td>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/img/PDFDownload.jpg" OnClick="ImageButton1_Click" /></td>
                <td>
                    <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/img/Print.jpg" OnClick="ImageButton2_Click" /></td>
            </tr>
        </table>
        <div id="print">
            <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="True" EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" ToolPanelView="None" HasToggleGroupTreeButton="False" ToolPanelWidth="250px" Width="350px" HasExportButton="False" HasPrintButton="False" />
        </div>
        <script src="../JavaScript/Jquery.js"></script>
    </form>
</body>
</html>
