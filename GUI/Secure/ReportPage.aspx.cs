﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Drawing.Printing;
using System.IO;


namespace GUI.Secure
{
    public partial class ReportPage : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            ReportDocument BooksReport = new ReportDocument();
            BooksReport.Load(Server.MapPath("~/Reports/ClientWeb.rpt"));
            BooksReport.SetDataSource(ds.Tables["ClientWeb"]);
            CrystalReportViewer1.ReportSource = BooksReport;
            BooksReport.SetDatabaseLogon("sa", "P@ssw0rd");
            BooksReport.SetParameterValue("@SELECTED", GUI.Secure.Handlers.SetReportParam.selected);
            BooksReport.SetParameterValue("@VALUE1", GUI.Secure.Handlers.SetReportParam.value1);
            BooksReport.SetParameterValue("@VALUE2", GUI.Secure.Handlers.SetReportParam.value2);
            CrystalReportViewer1.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            DataSet ds = new DataSet();
            ReportDocument rpt = new ReportDocument();
            rpt.Load(Server.MapPath("~/Reports/ClientWeb.rpt"));
            rpt.SetDataSource(ds.Tables["ClientWeb"]);
            rpt.SetDatabaseLogon("sa", "P@ssw0rd");
            rpt.SetParameterValue("@SELECTED", GUI.Secure.Handlers.SetReportParam.selected);
            rpt.SetParameterValue("@VALUE1", GUI.Secure.Handlers.SetReportParam.value1);
            rpt.SetParameterValue("@VALUE2", GUI.Secure.Handlers.SetReportParam.value2);

            rpt.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, true, "CCT-Certificate");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            DataSet ds = new DataSet();
            ReportDocument rpt = new ReportDocument();
            rpt.Load(Server.MapPath("~/Reports/ClientWeb.rpt"));
            rpt.SetDataSource(ds.Tables["ClientWeb"]);
            rpt.SetDatabaseLogon("sa", "P@ssw0rd");
            rpt.SetParameterValue("@SELECTED", GUI.Secure.Handlers.SetReportParam.selected);
            rpt.SetParameterValue("@VALUE1", GUI.Secure.Handlers.SetReportParam.value1);
            rpt.SetParameterValue("@VALUE2", GUI.Secure.Handlers.SetReportParam.value2);

            rpt.PrintOptions.PrinterName = GetDefaultPrinter();

            rpt.PrintToPrinter(1, false, 0, 0);
        }

        string GetDefaultPrinter()
        {
            PrinterSettings settings = new PrinterSettings();
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                settings.PrinterName = printer;
                if (settings.IsDefaultPrinter)
                    return printer;
            }
            return string.Empty;
        }

        private string GetDefaultprinterName()
        {
            throw new NotImplementedException();
        }
    }
}