﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test 2.aspx.cs" Inherits="GUI.Test.test_2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Stylesheets/jQueryUI.css" rel="stylesheet" />
    <link href="../Stylesheets/bootstrap.css" rel="stylesheet" />
    <link href="../Stylesheets/bootstrap-theme.css" rel="stylesheet" />
</head>
<body>
    <div>
        <nav class="navbar navbar-default" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Users</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="navbar-form navbar-nav" role="search">
                    <div class="form-group">
                        <input class="form-control" placeholder="Search…" type="search" data-bind="value: clientsLIst, valueUpdate: 'keyup'" autocomplete="off" />
                    </div>
                </div>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <br />
        <%-- Main Table with list of Clients --%>
        <div class="scrollableTable">
            <table id="tblClient" class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Initials</th>
                        <th>Name</th>
                        <th>Surname</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody data-bind='template: { foreach: clients }'>
                    <tr>
                        <%--data-bind='attr: { "class": +UserType }'>--%>
                       
                       <td data-bind="text: UserID"></td>
                        <td data-bind="text: Title"></td>
                        <td data-bind="text: Initials"></td>
                        <td data-bind="text: Name"></td>
                        <td data-bind="text: Surname"></td>
                        <td><a href="#myModal" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.editClient">View</a>
                            <a href="#myModal2" role="button" class="btn btn-info" data-toggle="modal" data-bind="click: $parent.viewQuals">History</a></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <script type="text/html" id="client">
            <li>
                <strong data-bind="text: name"></strong>–
        <span data-bind="text: brewery"></span>–
        <span data-bind="text: style"></span>
            </li>
        </script>

        <script src="../JavaScript/Jquery.js"></script>
        <script src="../JavaScript/JqueryUI.js"></script>
        <script src="../JavaScript/bootstrap.js"></script>
        <script src="../JavaScript/Knockout.js"></script>
        <script type="text/javascript">

            function viewModel() {
                var self = this;

                self.clientsLIst = ko.observableArray();

                self.getClients = function () {
                    $.ajax({
                        type: 'GET',
                        url: "../Handlers/GetClients.ashx",
                        success: function (data) {
                            self.clients(data);
                            self.sortBookingByDate();
                        }
                    });
                }
                // calls the method above
                self.getClients();

                self.clients = ko.dependentObservable(function () {
                    var search = self.clientsLIst();
                    return ko.utils.arrayFilter(self.clients, function (client) {
                        return client.nameque.indexOf(search) >= 0;
                    });
                }, this);
            }
            ko.applyBindings(new viewModel());
        </script>
</body>
</html>
