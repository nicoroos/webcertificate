﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="GUI.Test.test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test Page</title>
    <link href="../Stylesheets/bootstrap-responsive.css" rel="stylesheet" />
    <link href="../Stylesheets/bootstrap.css" rel="stylesheet" />
    <link href="test.css" rel="stylesheet" />
</head>
<body>
    <div id="main">
       <div class="navbar">
        <div class="navbar-inner">
            <a class="brand" href="#">Campus</a>
            <div class="navbar-search">
                <div>
                    <select id="College" data-bind="options: colleges, optionsText: 'CollegeName', optionsValue: 'CollegeID', value: selectedCollege, optionsCaption: 'select a colleges...'">
                    </select>
                </div>
            </div>
            <div class="navbar-search">
                <button type="button" class="btn" data-bind="click: refresh"><span class="icon-refresh"></span></button>
            </div>
        </div>
    </div>
        <div class="row-fluid">
            <div class="span6">
                <table id="collegeTable" class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>College Name</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody data-bind="foreach: college">
                        <tr>
                            <td class="editCollege">
                                <input type="text" class="input" data-bind="value: CollegeName" />
                            </td>
                            <td class="viewCollege" data-bind="text: CollegeName"></td>
                            <td class="editCollege">
                                <input type="text" class="input" data-bind="value: CollegeDescription" />
                            </td>
                            <td class="viewCollege" data-bind="text: CollegeDescription"></td>
                            <td>
                                <input type="button" class="btn btn-danger" data-bind="click: $parent.removeCollege" value="Delete" />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input id="btnEditCollege" type="button" class="btn btn-warning" data-bind="click: editCollege" value="Edit" />
                <input id="btnAddCollege" type="button" class="btn btn-primary" data-bind='click: addCollege' value="Add College" />
                <input id="btnSaveCollege" type="button" class="btn btn-success" data-bind='click: saveCollege' value="Save Changes" />
            </div>
            <div class="span6">
                <table id="campusTable" class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Campus Name</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="TableBody" data-bind="foreach: campus">
                        <tr>
                            <td class="editCampus">
                                <input type="text" class="input" data-bind="value: CampusName" />
                            </td>
                            <td class="viewCampus" data-bind="text: CampusName"></td>
                            <td class="editCampus">
                                <input type="text" class="input" data-bind="value: CampusDescription" />
                            </td>
                            <td class="viewCampus" data-bind="text: CampusDescription"></td>
                            <td class="inputHide">
                                <input id='btnDelete' type='button' class='btn btn-danger editCampus' value='Delete' data-bind='click: $parent.removeCampus' />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <input id="btnEditCampus" type="button" class="btn btn-warning" value="Edit" data-bind="click: editCampus" />
                <input id="btnAddCampus" type="button" class="btn btn-primary" value="Add Campus" data-bind="click: addCampus" />
                <input id="btnSaveCampus" type='button' class="btn btn-success" value='Save Changes' data-bind="click: saveCampus" />
            </div>
        </div>
    </div>
    <script src="../JavaScript/Jquery.js"></script>
    <script src="../JavaScript/bootstrap.js"></script>
    <script src="../JavaScript/Knockout.js"></script>
    <script src="../JavaScript/Knockout.mapping-latest.js"></script>
    <script src="JavaScript1.js"></script>
    <script type="text/javascript">
        
        function CollegeListViewModel() {
            // Data
            var self = this;
            // College combobox variable
            self.colleges = ko.observableArray([]);
            self.selectedCollege = ko.observable();

            // College variable for College Table
            self.college = ko.observableArray([]);

            // Campus variable for Campus Table
            self.campus = ko.observableArray([]);

            // Get metohod to populate the combobox 
            $.getJSON("../Handlers/GetCollege.ashx", function (data) {
                self.colleges(data);
            });

            // Get method to populate the Campus and College Tables
            self.getLocationDetails = function () {
                $('#College').change(function () {
                    $("select option:selected").each(function () {
                        if ($('option:selected').val() > 0) {
                            var Param = { 'Id': $('option:selected').val() };
                            $.ajax({
                                type: "GET",
                                url: "../Handlers/GetCollegeID.ashx",
                                data: Param,
                                success: function (data) {
                                    self.college(data);
                                    var Param = { 'Id': ko.toJSON(self.selectedCollege()) };
                                    $.getJSON("../Handlers/GetCampus.ashx", Param, function (data) {
                                        self.campus(data);
                                    });
                                },
                                error: function () {
                                    alert("Save Failed");
                                }
                            });
                        }
                    });
                });
            }
            self.getLocationDetails();

            // Show Normal view for college table
            self.viewCollege = function () {
                $('.editCollege').hide();
                $('.viewCollege').show();
                $('#btnAddCollege').hide();
                $('#btnSaveCollege').hide();
            };
            self.viewCollege();

            // Show Edit view for college table
            self.editCollege = function () {
                $('.editCollege').show();
                $('.viewCollege').hide();
                $('#btnAddCollege').show();
                $('#btnSaveCollege').show();
            };

            // Show Normal view for campus table
            self.viewCampus = function () {
                $('.editCampus').hide();
                $('.viewCampus').show();
                $('#btnAddCampus').hide();
                $('#btnSaveCampus').hide();
            };
            self.viewCampus();

            // Show Edit view for campus table
            self.editCampus = function () {
                $('.editCampus').show();
                $('.viewCampus').hide();
                $('#btnAddCampus').show();
                $('#btnSaveCampus').show();
            };

            // Operations
            self.addCollege = function () {
                self.college.push({
                    CollegeName: "",
                    CollegeDescription: ""
                });
                self.editCollege();
            };

            // Remove a row from the list
            self.removeCollege = function (data) {
                self.colleges.destroy(data);
            };

            // Save the Changes to the DB
            self.saveCollege = function () {
                $.ajax({
                    type: "POST",
                    url: "../Handlers/SetCollege.ashx",
                    data: ko.mapping.toJSON(self.college),
                    success: function (response, status, xhr) {
                        self.college(response);
                        self.selectedCollege(response.CollegeID);
                        self.viewCollege();
                    },
                    error: function () {
                        alert("Save faild!");
                    }
                });
            };

            // Campus 
            // Operations
            self.addCampus = function () {
                self.campus.push({
                    CollegeID: self.selectedCollege,
                    CampusName: "",
                    CampusDescription: ""
                });
                self.editCampus()
            };

            // Remove a row from the list
            self.removeCampus = function (data) {
                self.campus.destroy(data);
            };

            // Save the Changes to the DB
            self.saveCampus = function () {
                $.ajax({
                    type: "POST",
                    url: "../Handlers/SetCampus.ashx",
                    data: ko.mapping.toJSON(self.campus),
                    success: function (response, status, xhr) {
                        self.viewCampus();
                        var Param = { 'Id': ko.toJSON(self.selectedCollege) };
                        $.getJSON("../Handlers/GetCampus.ashx", Param, function (data) {
                            self.campus(data);
                        });
                    },
                    error: function () {
                        alert("Save Failed!");
                    }
                });
            };
            // Page refresh function
            self.refresh = function () {
                window.location.reload(true);
            }
        }

        ko.applyBindings(new CollegeListViewModel());
    </script>
</body>
</html>
