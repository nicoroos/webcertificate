﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Test3.aspx.cs" Inherits="GUI.Test.Test3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Test Page</title>
    <link href="../Stylesheets/bootstrap.css" rel="stylesheet" />
    <link href="../Stylesheets/bootstrap-theme.css" rel="stylesheet" />
    <link href="test.css" rel="stylesheet" />
</head>
    
<body>
    <div> 
    <table class="table table-hover table-striped">
        <thead>
            <tr>
                <th>College Name</th>
                <th>College Description</th>
                <th>Campus(es)</th>
                <th></th>
            </tr>
        </thead>
        <tbody data-bind="foreach: colleges">
            <tr>
                <td data-bind="text: CollegeName"></td>
                <td data-bind="text: CollegeDescription"></td>
                <td>
                    <table>
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody data-bind="foreach: campuses">
                            <tr>
                                <td data-bind="text: CampusName"></td>
                                <td><input id='btnDelete' type='button' class='btn btn-danger editCampus' value='X' data-bind='click: $parent.removeCampus' /></td>
                            </tr>
                            
                        </tbody>
                    </table>
                </td>
                <td>
                    <button class="btn btn-primary" data-bind='click: $root.addCampus'>Add Campus</button>
                </td>
            </tr>
        </tbody>
    </table>
    </div>
    <button class="btn btn-primary" data-bind='click: addCollege'>Add College</button>
    <button class="btn btn-success" data-bind='click: saveChanges'>Save Changes</button>
    <script src="../JavaScript/Jquery.js"></script>
    <script src="../JavaScript/bootstrap.js"></script>
    <script src="../JavaScript/Knockout.js"></script>
    <script src="../JavaScript/Knockout.mapping-latest.debug.js"></script>
    <script src="JavaScript1.js"></script>
    <script type="text/javascript">
        var resumeDataViewModel;

        function College(data) {
            var self = this;

            self.CollegeID = ko.observable(data.CollegeID);
            self.CollegeName = ko.observable(data.CollegeName);
            self.CollegeDescription = ko.observable(data.CollegeDescription);

            self.campuses = ko.observableArray();
            // Get a list of campus from the DB
            $.ajax({
                type: "GET",
                url: "../Handlers/GetCampus.ashx",
                data: { 'Id': ko.toJSON(self.CollegeID()) },
                success: function (data) {
                    var mappedCampuses = $.map(data, function (item) { return new Campus(item) });
                    self.campuses(mappedCampuses);
                }
            });

           

            // Remove a campus(row) from the Campus list
            self.removeCampus = function (data) {
                self.campuses.destroy(data);
            };
        }

        function Campus(data) {
            var self = this;

            self.CampusID = ko.observable(data.CampusID);
            self.CampusName = ko.observable(data.CampusName);
            self.CampusDescription = ko.observable(data.CampusDescription);

        }

        function ResumeDataViewModel() {
            var self = this;

            self.colleges = ko.observableArray();

            // Get College Data
            self.getCollege = function () {
                $.ajax({
                    type: "GET",
                    url: "../Handlers/GetCollege.ashx",
                    success: function (data) {
                        var mappedColleges = $.map(data, function (item) { return new College(item) });
                        self.colleges(mappedColleges);
                    }
                });
            }
            self.getCollege();

            self.addCollege = function () {
                self.colleges.push(new College({
                    CollegeName: "New",
                    CollegeDescription: "College"
                }));
            }

            // add a new campus to the campus list
            self.addCampus = function (category) {
                category.campuses.push(new Campus({
                    CampusName: 'new ',
                    CampusDescription: 'Campus'
                }));
            }

            // 
            // Save the Changes to the DB
            self.saveChanges = function () {
                $.ajax({
                    type: "POST",
                    url: "../Handlers/SetCampus.ashx",
                    data: ko.mapping.toJSON(self.colleges()),
                    success: function (response, status, xhr) {
                        alert("success");
                        self.getCollege();
                    },
                    error: function () {
                        alert("Save Failed!");
                    }
                });
            };
            
        }

        ko.applyBindings(new ResumeDataViewModel());
    </script>
</body>
</html>
