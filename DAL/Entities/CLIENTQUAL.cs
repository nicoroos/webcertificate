//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class CLIENTQUAL
    {
        public int CLIQUAL_UserQualID { get; set; }
        public int CLIQUAL_UserID { get; set; }
        public int CLIQUAL_QualID { get; set; }
        public System.DateTime CLIQUAL_StartDate { get; set; }
        public System.DateTime CLIQUAL_EndDate { get; set; }
        public int CLIQUAL_PrintID { get; set; }
        public int CLIQUAL_BookingID { get; set; }
    
        public virtual QUALIFICATION QUALIFICATION { get; set; }
        public virtual USERS USERS { get; set; }
        public virtual PRINTSTATUS PRINTSTATUS { get; set; }
        public virtual BOOKING BOOKING { get; set; }
    }
}
