//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class USERTYPE
    {
        public USERTYPE()
        {
            this.USERS = new HashSet<USERS>();
        }
    
        public int TYPE_TypeID { get; set; }
        public string TYPE_Name { get; set; }
    
        public virtual ICollection<USERS> USERS { get; set; }
    }
}
