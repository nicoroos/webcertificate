﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the USERS partial class which contains all the methods the BL uses to retrieve and save data to and from the USERS table of the DB.
    /// </summary>
    public partial class USERS
    {
        /// <summary>
        /// Get a specific record from the USERS table of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a record from DB which match the param.
        /// </returns>
        public static USERS GetUserByID(int userID)
        {
            try
            {
                return new WebCertificateEntities().USERS.SingleOrDefault(s => s.USER_UserID == userID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the USERS table of the DB.
        /// </summary>
        /// <param name="trainer"></param>
        /// <returns>
        /// Returns a record from DB which match the param.
        /// </returns>
        public static USERS GetUserByNameAndSurname(string trainer)
        {
            try
            {
                return new WebCertificateEntities().USERS.SingleOrDefault(s => s.USER_Name + " " + s.USER_Surname == trainer);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the USERS table of the DB.
        /// </summary>
        /// <param name="userType"></param>
        /// <returns>
        /// Returns a record from DB which match the param.
        /// </returns>
        public static USERS GetUserByType(int userType)
        {
            try
            {
                return new WebCertificateEntities().USERS.SingleOrDefault(u => u.USER_TypeID == userType);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USERS table of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of records from the DB which match the param.
        /// </returns>
        public static List<USERS> GetUserListByUserID(int userID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<USERS> result = new List<USERS>(ctx.USERS.Where(u => u.USER_UserID == userID));

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USERS table of the DB.
        /// </summary>
        /// <param name="userType"></param>
        /// <returns>
        /// Returns a list of records from the DB which match the param.
        /// </returns>
        public static List<USERS> GetUserListByType(int userType)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<USERS> result = new List<USERS>(ctx.USERS.Where(u => u.USER_TypeID <= userType));

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USERS table of the DB.
        /// </summary>
        /// <param name="trainer"></param>
        /// <returns>
        /// Returns a list of records from the DB which match the param.
        /// </returns>
        public static List<USERS> GetUserLIstByNameAndSurname(string trainer)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<USERS> result = new List<USERS>(ctx.USERS.Where(u => (u.USER_Name + " " + u.USER_Surname).Contains(trainer)));

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USERS table of the DB.
        /// </summary>
        /// <param name="surname"></param>
        /// <returns>
        /// Returns a list of records from the DB which match the param.
        /// </returns>
        public static List<USERS> GetUserListBySurname(string surname)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<USERS> result = new List<USERS>(ctx.USERS.Where(u => u.USER_Surname.Contains(surname)));

                    return result;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USERS table of the DB.
        /// </summary>
        /// <returns>
        /// Returns all the records from the USERS table
        /// </returns>
        public static List<USERS> GetAllUsers()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<USERS> result = new List<USERS>(ctx.USERS);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a new record or updates a existing record in the BOOKING table of the DB.
        /// </summary>
        /// <param name="user"></param>
        /// <returns>
        /// Returns the saved or updated data
        /// </returns>
        public static USERS SaveUser(USERS user)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    USERS u = ctx.USERS.SingleOrDefault(s => s.USER_UserID == user.USER_UserID);

                    if (u == null)
                    {
                        u = new USERS();
                        u.USER_Name = user.USER_Name;
                        u.USER_Surname = user.USER_Surname;
                        u.USER_Initials = user.USER_Initials;
                        u.USER_TitleID = user.USER_TitleID;
                        u.USER_TypeID = user.USER_TypeID;
                        u.USER_Password = user.USER_Password;

                        ctx.USERS.Add(u);
                    }
                    else
                    {
                        u.USER_Name = user.USER_Name;
                        u.USER_Surname = user.USER_Surname;
                        u.USER_Initials = user.USER_Initials;
                        u.USER_TitleID = user.USER_TitleID;
                        u.USER_TypeID = user.USER_TypeID;
                        u.USER_Password = user.USER_Password;
                    }

                    ctx.SaveChanges();

                    return u;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Removes the data from the DB.
        /// </summary>
        /// <param name="user"></param>
        public static void Delete(USERS user)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    USERS uUsers = ctx.USERS.Where(c => c.USER_UserID == user.USER_UserID).SingleOrDefault();

                    ctx.USERS.Remove(uUsers);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}