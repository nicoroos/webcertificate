﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the COSTS partial class which contains all the methods used by the BL to store and retrieve data from and to the COSTS table of the DB.
    /// </summary>
    public partial class COSTS
    {
        /// <summary>
        /// Get a specific record from the COSTS table of the DB.
        /// </summary>
        /// <param name="costID"></param>
        /// <returns>
        /// Returns a specific record from the COST table matching the param.
        /// </returns>
        public static COSTS GetCostByCostID(int costID)
        {
            try
            {
                return new WebCertificateEntities().COSTS.SingleOrDefault(s => s.COST_CostID == costID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the COSTS table of the DB.
        /// </summary>
        /// <returns>
        /// Returns a list of all the records in the COSTS table.
        /// </returns>
        public static List<COSTS> GetAllCosts()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<COSTS> result = new List<COSTS>(ctx.COSTS);

                    return result;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Save a new record or update an existig record in the CONTACTDETAILS tables.
        /// </summary>
        /// <param name="costs"></param>
        /// <returns>
        /// Returns the saved record
        /// </returns>
        public static COSTS SaveCosts(COSTS costs)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    COSTS c = ctx.COSTS.SingleOrDefault(s => s.COST_CostID == costs.COST_CostID);

                    if (c == null)
                    {
                        c = new COSTS();
                        c.COST_RatesPerKMID = costs.COST_RatesPerKMID;
                        c.COST_Toll = costs.COST_Toll;
                        c.COST_FlightParking = costs.COST_FlightParking;
                        c.COST_FlightTransport = costs.COST_FlightTransport;
                        c.COST_Other = costs.COST_Other;
                        c.COST_Food = costs.COST_Food;

                        ctx.COSTS.Add(c);
                    }

                    else
                    {
                        c.COST_RatesPerKMID = costs.COST_RatesPerKMID;
                        c.COST_Toll = costs.COST_Toll;
                        c.COST_FlightParking = costs.COST_FlightParking;
                        c.COST_FlightTransport = costs.COST_FlightTransport;
                        c.COST_Other = costs.COST_Other;
                        c.COST_Food = costs.COST_Food;
                    }

                    ctx.SaveChanges();

                    return c;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Removes the row from the DB.
        /// </summary>
        /// <param name="costs"></param>
        public static void Delete(COSTS costs)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    COSTS cCosts = ctx.COSTS.Where(c => c.COST_CostID == costs.COST_CostID).SingleOrDefault();

                    ctx.COSTS.Remove(cCosts);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
