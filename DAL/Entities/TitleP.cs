﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the TITLE partial class which contains all the methods used by the BL to store and retrieve data from and to the TITLE table of the DB.
    /// </summary>
    public partial class TITLE
    {
        /// <summary>
        /// Get a specific record from the TITLE table of the DB.
        /// </summary>
        /// <param name="titleID"></param>
        /// <returns>
        /// Returns a record from DB which match the param.
        /// </returns>
        public static TITLE GetTitleByID(int titleID)
        {
            try
            {
                return new WebCertificateEntities().TITLE.SingleOrDefault(s => s.TITLE_TitleID == titleID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the TITLE table of the DB.
        /// </summary>
        /// <returns>
        /// Returns a list of all the records in the TITLE table.
        /// </returns>
        public static List<TITLE> GetAll()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<TITLE> result = new List<TITLE>(ctx.TITLE);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
