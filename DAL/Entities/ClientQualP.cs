﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the CLIENTQUAL partial class which contains all the methods used by the BL to store and retrieve data from and to the CLIENTQUAL table of the DB.
    /// </summary>
    public partial class CLIENTQUAL
    {
        /// <summary>
        /// Get a specific record from the CLIENTQUAL table of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="bookingID"></param>
        /// <returns>
        /// Return a record matching both the params.
        /// </returns>
        public static CLIENTQUAL GetClientQualByUserIDAndBookingID(int userID, int bookingID)
        {
            try
            {
                return new WebCertificateEntities().CLIENTQUAL.SingleOrDefault(c => c.CLIQUAL_UserID == userID && c.CLIQUAL_BookingID == bookingID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the CLIENTQUAL table of the DB.
        /// </summary>
        /// <param name="bookingID"></param>
        /// <returns>
        /// Returns a list of records matching the param.
        /// </returns>
        public static List<CLIENTQUAL> GetClientQualByBookingID(int bookingID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<CLIENTQUAL> result = new List<CLIENTQUAL>(ctx.CLIENTQUAL.Where(cq => cq.CLIQUAL_BookingID == bookingID));

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records form the CLIENTQUAL table of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of records from the DB matching the param
        /// </returns>
        public static List<CLIENTQUAL> GetClientQualListByUserID(int userID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<CLIENTQUAL> result = new List<CLIENTQUAL>(ctx.CLIENTQUAL.Where(cq => cq.CLIQUAL_UserID == userID));

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the record from the CLIENTQUAL table of the DB.
        /// </summary>
        /// <returns>
        /// Returns all the records from the CLIENTQUAL table.
        /// </returns>
        public static List<CLIENTQUAL> GetALL()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {

                    List<CLIENTQUAL> result = new List<CLIENTQUAL>(ctx.CLIENTQUAL);

                    return result;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        /// <summary>
        /// Save a new records or update a existing record in the CLIENTQUAL table of the DB
        /// </summary>
        /// <param name="clientQual"></param>
        /// <returns>
        /// Returns all the saved records
        /// </returns>
        public static CLIENTQUAL SaveClientQual(CLIENTQUAL clientQual)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    CLIENTQUAL cq = ctx.CLIENTQUAL.SingleOrDefault(c => c.CLIQUAL_UserQualID == clientQual.CLIQUAL_UserQualID);

                    if (cq == null)
                    {
                        cq = new CLIENTQUAL();
                        cq.CLIQUAL_UserID = clientQual.CLIQUAL_UserID;
                        cq.CLIQUAL_QualID = clientQual.CLIQUAL_QualID;
                        cq.CLIQUAL_StartDate = clientQual.CLIQUAL_StartDate;
                        cq.CLIQUAL_EndDate = clientQual.CLIQUAL_EndDate;
                        cq.CLIQUAL_PrintID = clientQual.CLIQUAL_PrintID;
                        cq.CLIQUAL_BookingID = clientQual.CLIQUAL_BookingID;

                        ctx.CLIENTQUAL.Add(cq);
                    }
                    else
                    {
                        cq.CLIQUAL_UserID = clientQual.CLIQUAL_UserID;
                        cq.CLIQUAL_QualID = clientQual.CLIQUAL_QualID;
                        cq.CLIQUAL_StartDate = clientQual.CLIQUAL_StartDate;
                        cq.CLIQUAL_EndDate = clientQual.CLIQUAL_EndDate;
                        cq.CLIQUAL_PrintID = clientQual.CLIQUAL_PrintID;
                        cq.CLIQUAL_BookingID = clientQual.CLIQUAL_BookingID;
                    }

                    ctx.SaveChanges();

                    return cq;
                }

            }
            catch (Exception)
            {
                
                throw;
            }
        }

        /// <summary>
        /// Delete the record form the DB
        /// </summary>
        /// <param name="college"></param>
        public static void Delete(CLIENTQUAL clientQual)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    CLIENTQUAL cClientQual = ctx.CLIENTQUAL.Where(c => c.CLIQUAL_UserQualID == clientQual.CLIQUAL_UserQualID).SingleOrDefault();
                    if (cClientQual == null)
                    {
                        ctx.CLIENTQUAL.Remove(cClientQual);
                        ctx.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
