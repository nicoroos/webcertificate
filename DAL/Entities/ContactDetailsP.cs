﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the CONTACTDETAILS partial class which contains all the methods used by the BL to store and retrieve data from and to the CONTACTDETAILS table of the DB.
    /// </summary>
    public partial class CONTACTDETAILS
    {
        /// <summary>
        /// Get a specific record from the CONTACTDETAILS table of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Return a record from the DB matching the param.
        /// </returns>
        public static CONTACTDETAILS GetClientByUserID(int userID)
        {
            try
            {
                return new WebCertificateEntities().CONTACTDETAILS.SingleOrDefault(s => s.CONDET_UserID == userID);  
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the CONTACTDETAILS table of the DB
        /// </summary>
        /// <param name="email"></param>
        /// <returns>
        /// Return a record from the DB matching the param
        /// </returns>
        public static CONTACTDETAILS GetClientByEmail(string email)
        {
            try
            {
                return new WebCertificateEntities().CONTACTDETAILS.SingleOrDefault(s => s.CONDET_Email == email);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the CONTACTDETAILS table of the DB.
        /// </summary>
        /// <returns>
        /// Returns a list of all the records in the CONTACTDETAILS table.
        /// </returns>
        public static List<CONTACTDETAILS> GetAllContactDetails()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<CONTACTDETAILS> result = new List<CONTACTDETAILS>(ctx.CONTACTDETAILS);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a new record or update an existig record in the CONTACTDETAILS tables.
        /// </summary>
        /// <param name="contactDetails"></param>
        /// <returns>
        /// Returns the saved record
        /// </returns>
        public static CONTACTDETAILS SaveContactDetails(CONTACTDETAILS contactDetails)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    CONTACTDETAILS cd = ctx.CONTACTDETAILS.SingleOrDefault(s => s.CONDET_CONTACTDETAILSID == contactDetails.CONDET_CONTACTDETAILSID);

                    if (cd == null)
                    {
                        cd = new CONTACTDETAILS();

                        cd.CONDET_UserID = contactDetails.CONDET_UserID;
                        cd.CONDET_StreetNo = contactDetails.CONDET_StreetNo;
                        cd.CONDET_StreetName = contactDetails.CONDET_StreetName;
                        cd.CONDET_Suburb = contactDetails.CONDET_Suburb;
                        cd.CONDET_City = contactDetails.CONDET_City;
                        cd.CONDET_PostalCode = contactDetails.CONDET_PostalCode;
                        cd.CONDET_TelNo = contactDetails.CONDET_TelNo;
                        cd.CONDET_FaxNo = contactDetails.CONDET_FaxNo;
                        cd.CONDET_CellNo = contactDetails.CONDET_CellNo;
                        cd.CONDET_Email = contactDetails.CONDET_Email;

                        ctx.CONTACTDETAILS.Add(cd);
                    }
                    else
                    {
                        cd.CONDET_UserID = contactDetails.CONDET_UserID;
                        cd.CONDET_StreetNo = contactDetails.CONDET_StreetNo;
                        cd.CONDET_StreetName = contactDetails.CONDET_StreetName;
                        cd.CONDET_Suburb = contactDetails.CONDET_Suburb;
                        cd.CONDET_City = contactDetails.CONDET_City;
                        cd.CONDET_PostalCode = contactDetails.CONDET_PostalCode;
                        cd.CONDET_TelNo = contactDetails.CONDET_TelNo;
                        cd.CONDET_FaxNo = contactDetails.CONDET_FaxNo;
                        cd.CONDET_CellNo = contactDetails.CONDET_CellNo;
                        cd.CONDET_Email = contactDetails.CONDET_Email;
                    }

                    ctx.SaveChanges();

                    return cd;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Removes the row from the DB.
        /// </summary>
        /// <param name="contactDetails"></param>
        public static void Delete(CONTACTDETAILS contactDetails)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    CONTACTDETAILS cContactdetails = ctx.CONTACTDETAILS.Where(c => c.CONDET_UserID == contactDetails.CONDET_UserID).SingleOrDefault();
                    if (cContactdetails != null)
                    {
                        ctx.CONTACTDETAILS.Remove(cContactdetails);
                        ctx.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
