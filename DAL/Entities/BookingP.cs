﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the BOOKING partial class which contains all the methods the BL uses to retrieve and save data to and from the BOOKING table of the DB.
    /// </summary>
    public partial class BOOKING
    {
        /// <summary>
        /// Get a specific record from the BOOKING table of the DB.
        /// </summary>
        /// <param name="bookingID"></param>
        /// <returns>
        /// Returns a record from the BOOKING table which match the param BookingID.
        /// </returns>
        public static BOOKING GetBookingByID(int bookingID)
        {
            try
            {
                return new WebCertificateEntities().BOOKING.SingleOrDefault(s => s.BOOK_BookingID == bookingID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the BOOKING table of the DB.
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="randomCode"></param>
        /// <returns>
        /// Returns a record from the BOOKING table which match the param CourseCode(which is a Random generated Course Code RCC).
        /// </returns>
        public static BOOKING GetBookingByRCC(string year, string month, string randomCode)
        {
            try
            {
                return new WebCertificateEntities().BOOKING.SingleOrDefault(s => s.BOOK_Year == year && s.BOOK_Month == month && s.BOOK_RandomCode == randomCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the BOOKING table of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a record from the DB which match the param UserID.
        /// </returns>
        public static BOOKING GetBookingByUserID(int userID)
        {
            try
            {
                return new WebCertificateEntities().BOOKING.SingleOrDefault(b => b.BOOK_UserID == userID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the BOOKING Table of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of records from the DB which match the param UserID.
        /// </returns>
        public static List<BOOKING> GetBookingListByUserID(int userID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<BOOKING> result = new List<BOOKING>(ctx.BOOKING.Where(b => b.BOOK_UserID == userID));
                    
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the BOOKING table of the DB.
        /// </summary>
        /// <param name="campusID"></param>
        /// <returns>
        /// Returns a list of records from the DB which match the param CampusID.
        /// </returns>
        public static List<BOOKING> GetBookingListByCampusID(int campusID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<BOOKING> result = new List<BOOKING>(ctx.BOOKING.Where(b => b.BOOK_CampusID == campusID));
                    
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from BOOKING tabel of the DB.
        /// </summary>
        /// <returns>
        /// Returns all the rows from the BOOKING Table.
        /// </returns>
        public static List<BOOKING> GetAllBookings()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<BOOKING> result = new List<BOOKING>(ctx.BOOKING);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a new record or updates a existing record in the BOOKING table of the DB.
        /// </summary>
        /// <param name="booking"></param>
        /// <returns>
        /// Returns the saved or updated data
        /// </returns>
        public static BOOKING SaveBooking(BOOKING booking)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    BOOKING b = ctx.BOOKING.SingleOrDefault(s => s.BOOK_BookingID == booking.BOOK_BookingID);

                    if (b == null)
                    {
                        b = new BOOKING();
                        b.BOOK_UserID = booking.BOOK_UserID;
                        b.BOOK_QualID = booking.BOOK_QualID;
                        b.BOOK_CampusID = booking.BOOK_CampusID;
                        b.BOOK_StartDate = booking.BOOK_StartDate;
                        b.BOOK_EndDate = booking.BOOK_EndDate;
                        b.BOOK_Year = booking.BOOK_Year;
                        b.BOOK_Month = booking.BOOK_Month;
                        b.BOOK_RandomCode = booking.BOOK_RandomCode;
                        b.BOOK_StatusID = booking.BOOK_StatusID;

                        ctx.BOOKING.Add(b);
                    }
                    else
                    {
                        b.BOOK_UserID = booking.BOOK_UserID;
                        b.BOOK_QualID = booking.BOOK_QualID;
                        b.BOOK_CampusID = booking.BOOK_CampusID;
                        b.BOOK_StartDate = booking.BOOK_StartDate;
                        b.BOOK_EndDate = booking.BOOK_EndDate;
                        b.BOOK_Year = booking.BOOK_Year;
                        b.BOOK_Month = booking.BOOK_Month;
                        b.BOOK_RandomCode = booking.BOOK_RandomCode;
                        b.BOOK_StatusID = booking.BOOK_StatusID;
                    }

                    ctx.SaveChanges();

                    return b;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete selected data from the BOOKING table of DB.
        /// </summary>
        /// <param name="booking"></param>
        /// <returns>
        /// This is a void method so there is no return type.
        /// </returns>
        public static void DeleteBooking(BOOKING booking)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    BOOKING bBooking = ctx.BOOKING.Where(c => c.BOOK_BookingID == booking.BOOK_BookingID).SingleOrDefault();

                    ctx.BOOKING.Remove(bBooking);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
