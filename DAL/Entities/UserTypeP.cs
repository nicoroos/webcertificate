﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the USERTYPE partial class which contains all the methods the BL uses to retrieve and save data to and from the USERTYPE table of the DB.
    /// </summary>
    public partial class USERTYPE
    {
        /// <summary>
        /// Get a specific record from the USERTYPE table of the DB.
        /// </summary>
        /// <param name="typeID"></param>
        /// <returns>
        /// Returns a row from DB which match the param.
        /// </returns>
        public static USERTYPE GetUserTypeByID(int typeID)
        {
            try
            {
                return new WebCertificateEntities().USERTYPE.SingleOrDefault(s => s.TYPE_TypeID == typeID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from USERTYPE tabel of the DB.
        /// </summary>
        /// <returns>
        /// Returns all the record form the USERTYPE table.
        /// </returns>
        public static List<USERTYPE> GetAll()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<USERTYPE> result = new List<USERTYPE>(ctx.USERTYPE);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
