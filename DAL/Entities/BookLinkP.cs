﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the BOOKLINK partial class which contains all the methods used by the BL to store and retrieve data from and to the BOOKLINK table of the DB.
    /// </summary>
    public partial class BOOKLINK
    {
        /// <summary>
        /// Get a specific record from the BOOKLINK table of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a record from DB which match the param userID.
        /// </returns>
        public static BOOKLINK GetUserID(int userID)
        {
            try
            {
                return new WebCertificateEntities().BOOKLINK.SingleOrDefault(s => s.BOOKLINK_UserID == userID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the BOOKLINK table of the DB.
        /// </summary>
        /// <param name="bookingID"></param>
        /// <returns>
        /// Returns a record from DB which match the param bookingID.
        /// </returns>
        public static BOOKLINK GetBookID(int bookingID)
        {
            try
            {
                return new WebCertificateEntities().BOOKLINK.SingleOrDefault(s => s.BOOKLINK_BookingID == bookingID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the BOOKLINK table of the DB.
        /// </summary>
        /// <param name="bookingID"></param>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a record from the DB matching both the parameters bookingID and userID.
        /// </returns>
        public static BOOKLINK GetBookIDAndUserID(int bookingID, int userID)
        {
            try
            {
                return new WebCertificateEntities().BOOKLINK.SingleOrDefault(s => s.BOOKLINK_BookingID == bookingID && s.BOOKLINK_UserID == userID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the BOOKLINK table of the DB
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of records from the DB matching the param userID.
        /// </returns>
        public static List<BOOKLINK> GetUserIDList(int userID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<BOOKLINK> result = new List<BOOKLINK>(ctx.BOOKLINK.Where(b => b.BOOKLINK_UserID == userID));

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the BOOKLINK table of the DB
        /// </summary>
        /// <param name="bookingID"></param>
        /// <returns>
        /// Returns a list of records from the DB containing the param value of bookingID
        /// </returns>
        public static List<BOOKLINK> GetBookIDList(int bookingID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<BOOKLINK> result = new List<BOOKLINK>(ctx.BOOKLINK.Where(b => b.BOOKLINK_BookingID == bookingID));

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the BOOKLINK table of the DB.
        /// </summary>
        /// <returns>
        /// Returns all the records from the BOOKLINK Table.
        /// </returns>
        public static List<BOOKLINK> GetAll()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<BOOKLINK> result = new List<BOOKLINK>(ctx.BOOKLINK);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a new record or update an existing record in the BOOKLINK table of the DB.
        /// </summary>
        /// <param name="bookLink"></param>
        /// <returns>
        /// Returns the the saved data
        /// </returns>
        public static BOOKLINK Save(BOOKLINK bookLink)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    BOOKLINK bl = ctx.BOOKLINK.SingleOrDefault(s => s.BOOKLINK_BooklinkID == bookLink.BOOKLINK_BooklinkID);

                    if (bl == null)
                    {
                        bl = new BOOKLINK();
                        bl.BOOKLINK_BookingID = bookLink.BOOKLINK_BookingID;
                        bl.BOOKLINK_UserID = bookLink.BOOKLINK_UserID;

                        ctx.BOOKLINK.Add(bl);
                    }
                    else
                    {
                        bl.BOOKLINK_BookingID = bookLink.BOOKLINK_BookingID;
                        bl.BOOKLINK_UserID = bookLink.BOOKLINK_UserID;
                    }

                    ctx.SaveChanges();

                    return bl;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Removes the data from the DB.
        /// </summary>
        /// <param name="bookLink"></param>
        public static void Delete(BOOKLINK bookLink)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    BOOKLINK bl = ctx.BOOKLINK.Where(c => c.BOOKLINK_BooklinkID == bookLink.BOOKLINK_BooklinkID).SingleOrDefault();

                    ctx.BOOKLINK.Remove(bl);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
