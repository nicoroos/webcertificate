﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the CAMPUSLINK partial class which contains all the methods used by the BL to store and retrieve data from and to the CAMPUSLINK table of the DB.
    /// </summary>
    public partial class CAMPUSLINK
    {
        /// <summary>
        /// Get a specific record from the CAMPUSLINK of hte DB.
        /// </summary>
        /// <param name="campusID"></param>
        /// <returns>
        /// Returns a record from DB which match the param campusID.
        /// </returns>
        public static CAMPUSLINK GetCampusLinkByCampusID(int campusID)
        {
            try
            {
                return new WebCertificateEntities().CAMPUSLINK.SingleOrDefault(s => s.CAMPLINK_CampusID == campusID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the CAMPUSLINK of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a record from DB which match the param userID.
        /// </returns>
        public static CAMPUSLINK GetCampusLinkByUserID(int userID)
        {
            try
            {
                return new WebCertificateEntities().CAMPUSLINK.SingleOrDefault(s => s.CAMPLINK_UserID == userID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the CAMPUSLINK of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="campusID"></param>
        /// <returns>
        /// Returns a record from DB which match both the params
        /// </returns>
        public static CAMPUSLINK GetCampusLinkByUserIDAndeCampusID(int userID, int campusID)
        {
            try
            {
                return new WebCertificateEntities().CAMPUSLINK.SingleOrDefault(s => s.CAMPLINK_UserID == userID && s.CAMPLINK_CampusID == campusID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the CAMPUSLINK table of the DB.
        /// </summary>
        /// <returns>
        /// Returns all the records from the CAMPUSLINK Table
        /// </returns>
        public static List<CAMPUSLINK> GetAll()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<CAMPUSLINK> result = new List<CAMPUSLINK>(ctx.CAMPUSLINK);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get specific records from the CAMPUSLINK table of the DB
        /// </summary>
        /// <param name="campusID"></param>
        /// <returns>
        /// Returns a record from the DB matching the param campusID
        /// </returns>
        public static List<CAMPUSLINK> GetCampusID(int campusID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<CAMPUSLINK> result = new List<CAMPUSLINK>(ctx.CAMPUSLINK.Where(s => s.CAMPLINK_CampusID == campusID));
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a new record or update an existing record in the CAMPUSLINK table of the DB.
        /// </summary>
        /// <param name="campusLink"></param>
        /// <returns>
        /// Returns the saved data
        /// </returns>
        public static CAMPUSLINK CampusLinkSave(CAMPUSLINK campusLink)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    CAMPUSLINK cl = ctx.CAMPUSLINK.SingleOrDefault(s => s.CAMPLINK_CampLinkID == campusLink.CAMPLINK_CampLinkID);

                    if (cl == null)
                    {
                        cl = new CAMPUSLINK();
                        cl.CAMPLINK_UserID = campusLink.CAMPLINK_UserID;
                        cl.CAMPLINK_CampusID = campusLink.CAMPLINK_CampusID;

                        ctx.CAMPUSLINK.Add(cl);
                    }
                    else
                    {
                        cl.CAMPLINK_UserID = campusLink.CAMPLINK_UserID;
                        cl.CAMPLINK_CampusID = campusLink.CAMPLINK_CampusID;
                    }

                    ctx.SaveChanges();

                    return cl;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Removes the data from the CAMPUSLINK table of the DB.
        /// </summary>
        /// <param name="campusLink"></param>
        public static void Delete(CAMPUSLINK campusLink)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    CAMPUSLINK cCampuslink = ctx.CAMPUSLINK.Where(c => c.CAMPLINK_CampLinkID == campusLink.CAMPLINK_CampLinkID).SingleOrDefault();
                    if (cCampuslink != null)
                    {
                        ctx.CAMPUSLINK.Remove(cCampuslink);
                        ctx.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}