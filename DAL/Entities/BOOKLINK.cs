//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class BOOKLINK
    {
        public int BOOKLINK_BooklinkID { get; set; }
        public int BOOKLINK_UserID { get; set; }
        public int BOOKLINK_BookingID { get; set; }
    
        public virtual BOOKING BOOKING { get; set; }
        public virtual USERS USERS { get; set; }
    }
}
