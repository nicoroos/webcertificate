﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the PRINTSTATUS partial class which contains all the methods used by the BL to store and retrieve data from and to the PRINTSTATUS table of the DB.
    /// </summary>
    public partial class PRINTSTATUS
    {
        /// <summary>
        /// Get a specific record from the PRINTSTATUS table of the DB.
        /// </summary>
        /// <param name="printID"></param>
        /// <returns>
        /// Retruns a specific record matching the param.
        /// </returns>
        public static PRINTSTATUS GetPrintStatusByPrintID(int printID)
        {
            try
            {
                return new WebCertificateEntities().PRINTSTATUS.SingleOrDefault(s => s.PRINT_PrintID == printID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the PRINTSTATUS table of the DB.
        /// </summary>
        /// <param name="printStatus"></param>
        /// <returns>
        /// Returns a specific record matchin the param.
        /// </returns>
        public static PRINTSTATUS GetPrintStatusByStatusName(string printStatus)
        {
            try
            {
                return new WebCertificateEntities().PRINTSTATUS.SingleOrDefault(s => s.PRINT_Status == printStatus);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the PRINTSTATUS table of the DB.
        /// </summary>
        /// <returns>
        /// Returns a list of all the records in the PRINTSTATUS table.
        /// </returns>
        public static List<PRINTSTATUS> GetAll()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<PRINTSTATUS> result = new List<PRINTSTATUS>(ctx.PRINTSTATUS);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
