﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the RatePerKM partial class which contains all the methods used by the BL to store and retrieve data from and to the RatePerKM table of the DB.
    /// </summary>
    public partial class RatePerKM
    {
        /// <summary>
        /// Get a specific record from the RatePerKM table of the DB.
        /// </summary>
        /// <param name="rateID"></param>
        /// <returns>
        /// Returns a specific recording matching the param
        /// </returns>
        public static RatePerKM GetRatePerKMByID(int rateID)
        {
            try
            {
                return new WebCertificateEntities().RatePerKM.SingleOrDefault(s => s.RATES_RatePerKMID == rateID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the RatePerKM table of the DB.
        /// </summary>
        /// <param name="rates"></param>
        /// <returns>
        /// Returns a specific recording matching the param
        /// </returns>
        public static RatePerKM GetRatePerKMByRate(decimal rates)
        {
            try
            {
                return new WebCertificateEntities().RatePerKM.SingleOrDefault(s => s.RATES_RatePerKM == rates);
            }
            catch (Exception)
            {

                throw;
            }
        }


        /// <summary>
        /// Get all the records from the RatePerKM table of the DB.
        /// </summary>
        /// <returns>
        /// Returns a list of all the records in the RatePerKM table.
        /// </returns>
        public static List<RatePerKM> GetAll()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<RatePerKM> result = new List<RatePerKM>(ctx.RatePerKM);

                    return result;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Save a new record or update an existig record in the RatePerKM tables.
        /// </summary>
        /// <param name="costs"></param>
        /// <returns>
        /// Returns the saved record.
        /// </returns>
        public static RatePerKM SaveRatePerKm(RatePerKM rates)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    RatePerKM r = ctx.RatePerKM.SingleOrDefault(s => s.RATES_RatePerKMID == rates.RATES_RatePerKMID);

                    if (r == null)
                    {
                        r.RATES_RatePerKMID = rates.RATES_RatePerKMID;
                        r.RATES_Name = rates.RATES_Name;
                        r.RATES_RatePerKM = rates.RATES_RatePerKM;

                        ctx.RatePerKM.Add(r);
                    }

                    else
                    {
                        r.RATES_RatePerKMID = rates.RATES_RatePerKMID;
                        r.RATES_Name = rates.RATES_Name;
                        r.RATES_RatePerKM = rates.RATES_RatePerKM;
                    }
                    ctx.SaveChanges();

                    return r;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete the record form the DB.
        /// </summary>
        /// <param name="Rates"></param>
        public static void Delete(RatePerKM Rates)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    RatePerKM rRates = ctx.RatePerKM.Where(c => c.RATES_RatePerKMID == Rates.RATES_RatePerKMID).SingleOrDefault();

                    ctx.RatePerKM.Remove(rRates);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
