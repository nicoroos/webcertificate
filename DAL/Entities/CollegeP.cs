﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the COLLEGE partial class which contains all the methods used by the BL to store and retrieve data from and to the COLLEGE table of the DB.
    /// </summary>
    public partial class COLLEGE
    {
        /// <summary>
        /// Get a specific record from the COLLEGE table of the DB.
        /// </summary>
        /// <param name="collegeID"></param>
        /// <returns>
        /// Returns a specific record from the DB matching the param.
        /// </returns>
        public static COLLEGE GetCollegeByID(int collegeID)
        {
            try
            {
                return new WebCertificateEntities().COLLEGE.SingleOrDefault(s => s.COL_CollegeID == collegeID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the COLLEGE table of the DB.
        /// </summary>
        /// <param name="collegeName"></param>
        /// <returns>
        /// Returns a specific record from the DB matching the param
        /// </returns>
        public static COLLEGE GetCollegeByName(string collegeName)
        {
            try
            {
                return new WebCertificateEntities().COLLEGE.SingleOrDefault(s => s.COL_Name == collegeName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the COLLEGE Table of the DB.
        /// </summary>
        /// <returns>
        /// Returns all the records from the COLLEGE table
        /// </returns>
        public static List<COLLEGE> GetAll()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<COLLEGE> result = new List<COLLEGE>(ctx.COLLEGE);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a new record or update an existing record in the COLLEGE table of the DB.
        /// </summary>
        /// <param name="college"></param>
        /// <returns>
        /// Returns the saved record
        /// </returns>
        public static COLLEGE SaveCollege(COLLEGE college)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    COLLEGE c = ctx.COLLEGE.SingleOrDefault(s => s.COL_CollegeID == college.COL_CollegeID);

                    if (c == null)
                    {
                        // record doesnt exist, create a new record
                        c = new COLLEGE();

                        c.COL_Name = college.COL_Name;
                        c.COL_Description = college.COL_Description;

                        ctx.COLLEGE.Add(c);
                    }
                    else
                    {
                        // record exists, update existing record

                        c.COL_Name = college.COL_Name;
                        c.COL_Description = college.COL_Description;
                    }
                    ctx.SaveChanges();

                    return c;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete the row from the DB.
        /// </summary>
        /// <param name="college"></param>
        public static void Delete(COLLEGE college)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    COLLEGE cCollege = ctx.COLLEGE.Where(c => c.COL_CollegeID == college.COL_CollegeID).SingleOrDefault();

                    ctx.COLLEGE.Remove(cCollege);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}