﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the STATUS partial class which contains all the methods used by the BL to store and retrieve data from and to the STATUS table of the DB.
    /// </summary>
    public partial class STATUS
    {
        /// <summary>
        /// Get a specific record from the STATUS table of the DB.
        /// </summary>
        /// <param name="statusID"></param>
        /// <returns>
        /// Returns a record from DB which match the param.
        /// </returns>
        public static STATUS GetStatusByID(int statusID)
        {
            try
            {
                return new WebCertificateEntities().STATUS.SingleOrDefault(s => s.STATUS_StatusID == statusID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the STATUS table of the DB.
        /// </summary>
        /// <returns>
        /// Returns a list of all the records in the STATUS table.
        /// </returns>
        public static List<STATUS> GetAll()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<STATUS> result = new List<STATUS>(ctx.STATUS);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a new record or update an existig record in the QUALIFICATION tables.
        /// </summary>
        /// <param name="status"></param>
        /// <returns>
        /// Rerturn the saved data
        /// </returns>
        public static STATUS SaveStatus(STATUS status)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    STATUS s = ctx.STATUS.SingleOrDefault(x => x.STATUS_StatusID == status.STATUS_StatusID);

                    if (s == null)
                    {
                        s = new STATUS();
                        s.STATUS_StatusName = status.STATUS_StatusName;

                        ctx.STATUS.Add(s);
                    }
                    else
                    {
                        s.STATUS_StatusName = status.STATUS_StatusName;
                    }

                    ctx.SaveChanges();

                    return s;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Removes the data from the DB.
        /// </summary>
        /// <param name="status"></param>
        public static void Delete(STATUS status)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    STATUS sStatus = ctx.STATUS.Where(c => c.STATUS_StatusID == status.STATUS_StatusID).SingleOrDefault();

                    ctx.STATUS.Remove(sStatus);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
