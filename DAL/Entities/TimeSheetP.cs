﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the TIMESHEET partial class which contains all the methods used by the BL to store and retrieve data from and to the TIMESHEET table of the DB.
    /// </summary>
    public partial class TIMESHEET
    {
        /// <summary>
        /// Get a specific record from the TIMESHEET table of the DB.
        /// </summary>
        /// <param name="timeSheetID"></param>
        /// <returns>
        /// Returns a specific recording matching the param.
        /// </returns>
        public static TIMESHEET GetTimeSheetByID(int timeSheetID)
        {
            try
            {
                return new WebCertificateEntities().TIMESHEET.SingleOrDefault(s => s.TIME_TimeSheetID == timeSheetID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the TIMESHEET table of the DB.
        /// </summary>
        /// <returns>
        /// Returns a list of all the records in the TIMESHEET table.
        /// </returns>
        public static List<TIMESHEET> GetAll()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<TIMESHEET> result = new List<TIMESHEET>(ctx.TIMESHEET);

                    return result;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the TIMESHEET table of the DB.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of records from the DB which match all of the params.
        /// </returns>
        public static List<TIMESHEET> GetTimeSheetListByDatesAndUserID(DateTime startDate, DateTime endDate, int userID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<TIMESHEET> result = new List<TIMESHEET>(ctx.TIMESHEET.Where(s=> s.TIME_Date >= startDate && s.TIME_Date <= endDate && s.TIME_UserID == userID));

                    return result;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the TIMESHEET table of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of records from the DB which match the param.
        /// </returns>
        public static List<TIMESHEET> GetTimeSheetListByUserID(int userID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<TIMESHEET> result = new List<TIMESHEET>(ctx.TIMESHEET.Where(s => s.TIME_UserID == userID));

                    return result;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Save a new record or updates a existing record in the TIMESHEET table of the DB.
        /// </summary>
        /// <param name="timeSheet"></param>
        /// <returns>
        /// Returns the saved or updated data.
        /// </returns>
        public static TIMESHEET Save(TIMESHEET timeSheet)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    TIMESHEET t = ctx.TIMESHEET.SingleOrDefault(s => s.TIME_TimeSheetID == timeSheet.TIME_TimeSheetID);

                    if (t == null)
                    {
                        t = new TIMESHEET();

                        t.TIME_UserID = timeSheet.TIME_UserID;
                        t.TIME_Date = timeSheet.TIME_Date;
                        t.TIME_StartTime = timeSheet.TIME_StartTime;
                        t.TIME_EndTime = timeSheet.TIME_EndTime;
                        t.TIME_Hours = timeSheet.TIME_Hours;
                        t.TIME_CampusID = timeSheet.TIME_CampusID;
                        t.TIME_Nights = timeSheet.TIME_Nights;
                        t.TIME_CostID = timeSheet.TIME_CostID;
                        t.TIME_ODOStart = timeSheet.TIME_ODOStart;
                        t.TIME_ODOEnd = timeSheet.TIME_ODOEnd;

                        ctx.TIMESHEET.Add(t);
                    }

                    else
                    {
                        t.TIME_UserID = timeSheet.TIME_UserID;
                        t.TIME_Date = timeSheet.TIME_Date;
                        t.TIME_StartTime = timeSheet.TIME_StartTime;
                        t.TIME_EndTime = timeSheet.TIME_EndTime;
                        t.TIME_Hours = timeSheet.TIME_Hours;
                        t.TIME_CampusID = timeSheet.TIME_CampusID;
                        t.TIME_Nights = timeSheet.TIME_Nights;
                        t.TIME_CostID = timeSheet.TIME_CostID;
                        t.TIME_ODOStart = timeSheet.TIME_ODOStart;
                        t.TIME_ODOEnd = timeSheet.TIME_ODOEnd;
                    }

                    ctx.SaveChanges();

                    return t;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Delete selected data from the BOOKING table of DB.
        /// </summary>
        /// <param name="timesheet"></param>
        public static void Delete(TIMESHEET timesheet)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    TIMESHEET cTimesheet = ctx.TIMESHEET.Where(c => c.TIME_TimeSheetID == timesheet.TIME_TimeSheetID).SingleOrDefault();

                    ctx.TIMESHEET.Remove(cTimesheet);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
