﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the QUALIFICATION partial class which contains all the methods used by the BL to store and retrieve data from and to the QUALIFICATION table of the DB.
    /// </summary>
    public partial class QUALIFICATION
    {
        /// <summary>
        /// Get a specific record from the QUALIFICATION table of the DB.
        /// </summary>
        /// <param name="qualID"></param>
        /// <returns>
        /// Returns a specific recording matching the param.
        /// </returns>
        public static QUALIFICATION GetQualByQualID(int qualID)
        {
            try
            {
                return new WebCertificateEntities().QUALIFICATION.SingleOrDefault(s => s.QUAL_QualID == qualID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the QUALIFICATION table of the DB.
        /// </summary>
        /// <param name="qualName"></param>
        /// <returns>
        /// Returns a record matching the param
        /// </returns>
        public static QUALIFICATION GetQualByName(string qualName)
        {
            try
            {
                {
                    return new WebCertificateEntities().QUALIFICATION.SingleOrDefault(q => q.QUAL_Name == qualName);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the QUALIFICATION table of the DB.
        /// </summary>
        /// <returns>
        /// Returns a list of all the records in the QUALIFICATION table.
        /// </returns>
        public static List<QUALIFICATION> GetAllQualifications()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<QUALIFICATION> result = new List<QUALIFICATION>(ctx.QUALIFICATION);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a new record or update an existig record in the QUALIFICATION tables.
        /// </summary>
        /// <param name="qual"></param>
        /// <returns>
        /// Returns the saved record.
        /// </returns>
        public static QUALIFICATION SaveQual(QUALIFICATION qual)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    QUALIFICATION q = ctx.QUALIFICATION.SingleOrDefault(s => s.QUAL_QualID == qual.QUAL_QualID);

                    if (q == null)
                    {
                        // record doesnt exist, create a new record
                        q = new QUALIFICATION();

                        q.QUAL_Name = qual.QUAL_Name;
                        q.QUAL_Description = qual.QUAL_Description;

                        ctx.QUALIFICATION.Add(q);
                    }
                    else
                    {
                        // record exists, update existing record

                        q.QUAL_Name = qual.QUAL_Name;
                        q.QUAL_Description = qual.QUAL_Description;
                    }
                    ctx.SaveChanges();

                    return q;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete the record form the DB.
        /// </summary>
        /// <param name="qual"></param>
        public static void Delete(QUALIFICATION qual)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    QUALIFICATION cCollege = ctx.QUALIFICATION.Where(c => c.QUAL_QualID == qual.QUAL_QualID).SingleOrDefault();

                    ctx.QUALIFICATION.Remove(cCollege);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
