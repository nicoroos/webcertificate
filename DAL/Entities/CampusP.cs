﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    /// <summary>
    /// This is the CAMPUS partial class which contains all the methods used by the BL to store and retrieve data from and to the CAMPUS table of the DB.
    /// </summary>
    public partial class CAMPUS
    {
        /// <summary>
        /// Get a specific record from the Campus table of the DB.
        /// </summary>
        /// <param name="campusID"></param>
        /// <returns>
        /// Returns a record from the DB matching the param.
        /// </returns>
        public static CAMPUS GetCampusByID(int campusID)
        {
            try
            {
                return new WebCertificateEntities().CAMPUS.SingleOrDefault(s => s.CAMP_CampusID == campusID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the Campus table of the DB
        /// </summary>
        /// <param name="campusName"></param>
        /// <returns>
        /// Returns a record from the DB matching the param
        /// </returns>
        public static CAMPUS GetCampusByName(string campusName)
        {
            try
            {
                return new WebCertificateEntities().CAMPUS.SingleOrDefault(s => s.CAMP_Name == campusName);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the Campus table of the DB.
        /// </summary>
        /// <param name="collegeID"></param>
        /// <returns>
        /// Returns a list of records from the DB matching the param.
        /// </returns>
        public static List<CAMPUS> GetCampusByCollegeID(int collegeID)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<CAMPUS> result = new List<CAMPUS>(ctx.CAMPUS.Where(s => s.CAMP_CollegID == collegeID));
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from Campus table of the DB.
        /// </summary>
        /// <returns>
        /// Returns all the records from the Campus table
        /// </returns>
        public static List<CAMPUS> GetAll()
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    List<CAMPUS> result = new List<CAMPUS>(ctx.CAMPUS);

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save the record in the DB
        /// </summary>
        /// <param name="campus"></param>
        /// <returns>
        /// Returns the data saved in DB</returns>
        public static CAMPUS CampusSave(CAMPUS campus)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    CAMPUS c = ctx.CAMPUS.SingleOrDefault(s => s.CAMP_CampusID == campus.CAMP_CampusID);

                    if (c == null)
                    {
                        // record doesnt exist, create a new record
                        c = new CAMPUS();

                        c.CAMP_Name = campus.CAMP_Name;
                        c.CAMP_Description = campus.CAMP_Description;
                        c.CAMP_CollegID = campus.CAMP_CollegID;

                        ctx.CAMPUS.Add(c);
                    }
                    else
                    {
                        // record exists, update existing record

                        c.CAMP_Name = campus.CAMP_Name;
                        c.CAMP_Description = campus.CAMP_Description;
                        c.CAMP_CollegID = campus.CAMP_CollegID;
                    }
                    ctx.SaveChanges();

                    return c;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete the record form the DB
        /// </summary>
        /// <param name="campus"></param>
        public static void Delete(CAMPUS campus)
        {
            try
            {
                using (WebCertificateEntities ctx = new WebCertificateEntities())
                {
                    CAMPUS cCampus = ctx.CAMPUS.Where(c => c.CAMP_CampusID == campus.CAMP_CampusID).SingleOrDefault();

                    ctx.CAMPUS.Remove(cCampus);
                    ctx.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
