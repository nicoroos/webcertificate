//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DAL.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class COSTS
    {
        public COSTS()
        {
            this.TIMESHEET = new HashSet<TIMESHEET>();
        }
    
        public int COST_CostID { get; set; }
        public Nullable<int> COST_RatesPerKMID { get; set; }
        public Nullable<decimal> COST_Toll { get; set; }
        public Nullable<decimal> COST_Other { get; set; }
        public Nullable<decimal> COST_Food { get; set; }
        public Nullable<decimal> COST_FlightParking { get; set; }
        public Nullable<decimal> COST_FlightTransport { get; set; }
    
        public virtual RatePerKM RatePerKM { get; set; }
        public virtual ICollection<TIMESHEET> TIMESHEET { get; set; }
    }
}
