using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core
{
public static class Globals
{
static string cmsCon;

/// <summary>
/// The cmsConnection string for entity framework.
/// </summary>
/// <seealso cref="DALCMS.globals.Connection"/>
public static string cmsConnection
{
get
{
return cmsCon;
}
set
{
DALCMS.globals.Connection = value;
cmsCon = value;
}
}
}
}
