using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
    /// <summary>
    /// This class extends the CAMPUSLINK Business Layer methods.
    /// </summary>
    public partial class CAMPUSLINK
    {
        /// <summary>
        /// This code validates the bussiness logik of the CAMPUSLINK table.
        /// </summary>
        /// <param name="CAMPUSLINK">CAMPUSLINK Object.</param>
        /// <param name="blType">The action to validate.</param>
        /// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
        public static string BussinesLogik(DALCMS.CAMPUSLINK CAMPUSLINK, COMMON.Enums.BLType blType)
        {
            //NB: Please apply the CAMPUSLINK Business validation Logic here

            StringBuilder sb = new StringBuilder();

            switch (blType)
            {
                case COMMON.Enums.BLType.Save:
                    break;
                case COMMON.Enums.BLType.Read:
                    break;
                case COMMON.Enums.BLType.Delete:
                    break;
                default:
                    break;
            }

            return sb.ToString();
        }

        // Please add extended methods here

        /// <summary>
        /// Get Campus Link By UserID
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>Returns Campus Link</returns>
        public static DALCMS.CAMPUSLINK GetCampusLinkByUserID(int userID)
        {
            try
            {
                return Get().SingleOrDefault(cl => cl.CAMPLINK_UserID == userID);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
