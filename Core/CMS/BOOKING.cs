using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace Core.CMS
{
    /// <summary>
    /// This class extends the BOOKING Business Layer methods.
    /// </summary>
    public partial class BOOKING
    {
        /// <summary>
        /// This code validates the bussiness logik of the BOOKING table.
        /// </summary>
        /// <param name="BOOKING">BOOKING Object.</param>
        /// <param name="blType">The action to validate.</param>
        /// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
        public static string BussinesLogik(DALCMS.BOOKING BOOKING, COMMON.Enums.BLType blType)
        {
            //NB: Please apply the BOOKING Business validation Logic here

            StringBuilder sb = new StringBuilder();

            switch (blType)
            {
                case COMMON.Enums.BLType.Save:
                    break;
                case COMMON.Enums.BLType.Read:
                    break;
                case COMMON.Enums.BLType.Delete:
                    break;
                default:
                    break;
            }

            return sb.ToString();
        }

        // Please add extended methods here

        public static List<BO.Booking> GetAll()
        {
            try
            {
                List<BO.Booking> result = new List<BO.Booking>();

                foreach (DALCMS.BOOKING book in Core.CMS.BOOKING.Get())
                {
                    // Returns data for...
                    // booking matching bookingid
                    DALCMS.BOOKING booking = Get(book.BOOK_BookingID);
                    // campus matching campusID
                    DALCMS.CAMPUS campus = Core.CMS.CAMPUS.Get(booking.BOOK_CampusID);
                    // college matching collegeID
                    DALCMS.COLLEGE college = Core.CMS.COLLEGE.Get(campus.CAMP_CollegID);
                    // qualification matching qualid
                    DALCMS.QUALIFICATION qualification = Core.CMS.QUALIFICATION.Get(booking.BOOK_QualID);
                    // trainter matching userid
                    DALCMS.USERS trainer = Core.CMS.USERS.Get(booking.BOOK_UserID);
                    // status mathcing the status id
                    DALCMS.STATUS status = Core.CMS.STATUS.Get(booking.BOOK_StatusID);
                    // declare a course code string variable
                    string courseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;

                    BO.Booking bBooking = new BO.Booking();
                    bBooking.BOOK_BookingID = booking.BOOK_BookingID;
                    bBooking.COL_Name = college.COL_Name;
                    bBooking.CAMP_Name = campus.CAMP_Name;
                    bBooking.QUAL_Name = qualification.QUAL_Name;
                    bBooking.USER_Name = trainer.USER_Name + " " + trainer.USER_Surname;
                    bBooking.BOOK_StartDate = booking.BOOK_StartDate.ToShortDateString();
                    bBooking.BOOK_EndDate = booking.BOOK_EndDate.ToShortDateString();
                    bBooking.BOOK_StatusID = booking.BOOK_StatusID;
                    bBooking.STATUS_StatusName = status.STATUS_StatusName;
                    bBooking.CourseCode = courseCode;
                    bBooking.orderNumber = booking.BOOK_OrderNo;
                    bBooking.invoiceNumber = booking.BOOK_InvoiceNo;

                    if (status.STATUS_StatusName == "DONE")
                        bBooking.Coloring = "success";
                    else if (status.STATUS_StatusName == "CONFIRMED")
                        bBooking.Coloring = "info";
                    else if (status.STATUS_StatusName == "CANCELED")
                        bBooking.Coloring = "warning";
                    else if (status.STATUS_StatusName == "PENDING")
                        bBooking.Coloring = "danger";

                    result.Add(bBooking);
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the booklink table matching the param value.
        /// Then foreach booklink record get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of bookings to be populated in the bookings table matching the param
        /// </returns>
        public static List<BO.Booking> GetBookingByUserID(int userID)
        {
            try
            {
                List<BO.Booking> result = new List<BO.Booking>();

                foreach (DALCMS.BOOKLINK bookLink in Core.CMS.BOOKLINK.Get().Where(bl => bl.BOOKLINK_UserID == userID))
                {
                    // Returns data for...
                    // booking matching bookingid
                    DALCMS.BOOKING booking = Get().SingleOrDefault(b => b.BOOK_BookingID == bookLink.BOOKLINK_BookingID);
                    // campus matching campusID
                    DALCMS.CAMPUS campus = Core.CMS.CAMPUS.Get().SingleOrDefault(c => c.CAMP_CampusID == booking.BOOK_CampusID);
                    // college matching collegeID
                    DALCMS.COLLEGE college = Core.CMS.COLLEGE.Get().SingleOrDefault(c => c.COL_CollegeID == campus.CAMP_CollegID);
                    // qualification matching qualid
                    DALCMS.QUALIFICATION qualification = Core.CMS.QUALIFICATION.Get().SingleOrDefault(q => q.QUAL_QualID == booking.BOOK_QualID);
                    // trainter matching userid
                    DALCMS.USERS trainer = Core.CMS.USERS.Get().SingleOrDefault(t => t.USER_UserID == booking.BOOK_UserID);
                    // status mathcing the status id
                    DALCMS.STATUS status = Core.CMS.STATUS.Get().SingleOrDefault(s => s.STATUS_StatusID == booking.BOOK_StatusID);
                    // declare a course code string variable
                    string courseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;

                    BO.Booking bBooking = new BO.Booking();
                    bBooking.BOOK_BookingID = booking.BOOK_BookingID;
                    bBooking.COL_Name = college.COL_Name;
                    bBooking.CAMP_Name = campus.CAMP_Name;
                    bBooking.QUAL_Name = qualification.QUAL_Name;
                    bBooking.USER_Name = trainer.USER_Name + " " + trainer.USER_Surname;
                    bBooking.BOOK_StartDate = booking.BOOK_StartDate.ToShortDateString();
                    bBooking.BOOK_EndDate = booking.BOOK_EndDate.ToShortDateString();
                    bBooking.STATUS_StatusName = status.STATUS_StatusName;
                    bBooking.BOOK_StatusID = booking.BOOK_StatusID;
                    bBooking.CourseCode = courseCode;
                    bBooking.orderNumber = booking.BOOK_OrderNo;
                    bBooking.invoiceNumber = booking.BOOK_InvoiceNo;

                    if (status.STATUS_StatusName == "DONE")
                        bBooking.Coloring = "success";
                    else if (status.STATUS_StatusName == "CONFIRMED")
                        bBooking.Coloring = "info";
                    else if (status.STATUS_StatusName == "CANCELED")
                        bBooking.Coloring = "warning";
                    else if (status.STATUS_StatusName == "PENDING")
                        bBooking.Coloring = "danger";

                    result.Add(bBooking);
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the CAMPUS table that match the collegeID param, then get a list of records from the BOOKING table that match the campusID.
        /// Then foreach booking record get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="collegeID"></param>
        /// <returns>
        /// Returns a list of Booking to be populated in the bookings table that match the param.
        /// </returns>
        public static List<BO.Booking> GetBookingByCollegeID(int collegeID)
        {
            try
            {
                List<BO.Booking> result = new List<BO.Booking>();

                foreach (DALCMS.CAMPUS campusid in Core.CMS.CAMPUS.Get().Where(c => c.CAMP_CollegID == collegeID))
                {
                    foreach (DALCMS.BOOKING booking in Core.CMS.BOOKING.Get().Where(b => b.BOOK_CampusID == campusid.CAMP_CampusID))
                    {
                        // Returns data for...
                        // campus matching campusID
                        DALCMS.CAMPUS campus = Core.CMS.CAMPUS.Get().SingleOrDefault(c => c.CAMP_CampusID == booking.BOOK_CampusID);
                        // college matching collegeID
                        DALCMS.COLLEGE college = Core.CMS.COLLEGE.Get().SingleOrDefault(c => c.COL_CollegeID == campus.CAMP_CollegID);
                        // qualification matching qualid
                        DALCMS.QUALIFICATION qualification = Core.CMS.QUALIFICATION.Get().SingleOrDefault(q => q.QUAL_QualID == booking.BOOK_QualID);
                        // trainter matching userid
                        DALCMS.USERS trainer = Core.CMS.USERS.Get().SingleOrDefault(t => t.USER_UserID == booking.BOOK_UserID);
                        // status mathcing the status id
                        DALCMS.STATUS status = Core.CMS.STATUS.Get().SingleOrDefault(s => s.STATUS_StatusID == booking.BOOK_StatusID);
                        // declare a course code string variable
                        string courseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;

                        BO.Booking bBooking = new BO.Booking();
                        bBooking.BOOK_BookingID = booking.BOOK_BookingID;
                        bBooking.COL_Name = college.COL_Name;
                        bBooking.CAMP_Name = campus.CAMP_Name;
                        bBooking.QUAL_Name = qualification.QUAL_Name;
                        bBooking.USER_Name = trainer.USER_Name + " " + trainer.USER_Surname;
                        bBooking.BOOK_StartDate = booking.BOOK_StartDate.ToShortDateString();
                        bBooking.BOOK_EndDate = booking.BOOK_EndDate.ToShortDateString();
                        bBooking.STATUS_StatusName = status.STATUS_StatusName;
                        bBooking.BOOK_StatusID = booking.BOOK_StatusID;
                        bBooking.CourseCode = courseCode;
                        bBooking.orderNumber = booking.BOOK_OrderNo;
                        bBooking.invoiceNumber = booking.BOOK_InvoiceNo;

                        if (status.STATUS_StatusName == "DONE")
                            bBooking.Coloring = "success";
                        else if (status.STATUS_StatusName == "CONFIRMED")
                            bBooking.Coloring = "info";
                        else if (status.STATUS_StatusName == "CANCELED")
                            bBooking.Coloring = "warning";
                        else if (status.STATUS_StatusName == "PENDING")
                            bBooking.Coloring = "danger";

                        result.Add(bBooking);
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get bookings all the bookings which is confirmed.
        /// </summary>
        /// <returns>
        /// Booking object
        /// </returns>
        public static List<BO.Booking> GetConfirmedBookings()
        {
            try
            {
                List<BO.Booking> result = new List<BO.Booking>();
                
                foreach(DALCMS.BOOKING booking in Core.CMS.BOOKING.Get().Where(i => i.BOOK_StatusID == (int)Core.CMS.BO.Enums.BookingStatus.CONFIRMED))
                {
                    Core.CMS.BO.Booking bookingList = Core.CMS.BOOKING.GetBookingByBookingID(booking.BOOK_BookingID);

                    BO.Booking b = new BO.Booking();
                    b.BOOK_BookingID = bookingList.BOOK_BookingID;
                    b.QUAL_Name = bookingList.COL_Name + " - " + bookingList.QUAL_Name;
                    b.CourseCode = bookingList.CourseCode;

                    result.Add(b);
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a record from the BOOKING table. Then get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="bookingID"></param>
        /// <returns>
        /// Returns a record from the DB matching the param value.
        /// </returns>
        public static BO.Booking GetBookingByBookingID(int bookingID)
        {
            try
            {
                // Returns data for...
                // booking matching bookingid
                DALCMS.BOOKING booking = Get().SingleOrDefault(b => b.BOOK_BookingID == bookingID);
                // campus matching campusID
                DALCMS.CAMPUS campus = Core.CMS.CAMPUS.Get().SingleOrDefault(c => c.CAMP_CampusID == booking.BOOK_CampusID);
                // college matching collegeID
                DALCMS.COLLEGE college = Core.CMS.COLLEGE.Get().SingleOrDefault(c => c.COL_CollegeID == campus.CAMP_CollegID);
                // qualification matching qualid
                DALCMS.QUALIFICATION qualification = Core.CMS.QUALIFICATION.Get().SingleOrDefault(q => q.QUAL_QualID == booking.BOOK_QualID);
                // trainter matching userid
                DALCMS.USERS trainer = Core.CMS.USERS.Get().SingleOrDefault(t => t.USER_UserID == booking.BOOK_UserID);
                // status mathcing the status id
                DALCMS.STATUS status = Core.CMS.STATUS.Get().SingleOrDefault(s => s.STATUS_StatusID == booking.BOOK_StatusID);
                // declare a course code string variable
                string courseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;

                BO.Booking bBooking = new BO.Booking();
                bBooking.BOOK_BookingID = booking.BOOK_BookingID;
                bBooking.COL_Name = college.COL_Name;
                bBooking.CAMP_Name = campus.CAMP_Name;
                bBooking.QUAL_Name = qualification.QUAL_Name;
                bBooking.USER_Name = trainer.USER_Name + " " + trainer.USER_Surname;
                bBooking.BOOK_StartDate = booking.BOOK_StartDate.ToShortDateString();
                bBooking.BOOK_EndDate = booking.BOOK_EndDate.ToShortDateString();
                bBooking.STATUS_StatusName = status.STATUS_StatusName;
                bBooking.BOOK_StatusID = booking.BOOK_StatusID;
                bBooking.CourseCode = courseCode;
                bBooking.orderNumber = booking.BOOK_OrderNo;
                bBooking.invoiceNumber = booking.BOOK_InvoiceNo;

                return bBooking;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets a list of bookings according to the param value(random course code).
        /// Then foreach Booking record get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns>
        /// returns a list of bookings
        /// </returns>
        public static List<BO.Booking> GetBookingByRCC(string searchValue)
        {
            try
            {
                List<BO.Booking> result = new List<BO.Booking>();

                DALCMS.BOOKING bookingByRCC = Core.CMS.BOOKING.Get().SingleOrDefault(b => b.BOOK_Year == searchValue.Substring(0, 2) && b.BOOK_Month == searchValue.Substring(2, 2) && b.BOOK_RandomCode == searchValue.Substring(4, 4));

                if (bookingByRCC != null)
                {
                    DALCMS.CAMPUS campus = Core.CMS.CAMPUS.Get().SingleOrDefault(c => c.CAMP_CampusID == bookingByRCC.BOOK_CampusID);
                    // college matching collegeID
                    DALCMS.COLLEGE college = Core.CMS.COLLEGE.Get().SingleOrDefault(c => c.COL_CollegeID == campus.CAMP_CollegID);
                    // qualification matching qualid
                    DALCMS.QUALIFICATION qualification = Core.CMS.QUALIFICATION.Get().SingleOrDefault(q => q.QUAL_QualID == bookingByRCC.BOOK_QualID);
                    // trainter matching userid
                    DALCMS.USERS trainer = Core.CMS.USERS.Get().SingleOrDefault(t => t.USER_UserID == bookingByRCC.BOOK_UserID);
                    // status mathcing the status id
                    DALCMS.STATUS status = Core.CMS.STATUS.Get().SingleOrDefault(s => s.STATUS_StatusID == bookingByRCC.BOOK_StatusID);

                    BO.Booking bBooking = new BO.Booking();
                    bBooking.BOOK_BookingID = bookingByRCC.BOOK_BookingID;
                    bBooking.COL_Name = college.COL_Name;
                    bBooking.CAMP_Name = campus.CAMP_Name;
                    bBooking.QUAL_Name = qualification.QUAL_Name;
                    bBooking.USER_Name = trainer.USER_Name + " " + trainer.USER_Surname;
                    bBooking.BOOK_StartDate = bookingByRCC.BOOK_StartDate.ToShortDateString();
                    bBooking.BOOK_EndDate = bookingByRCC.BOOK_EndDate.ToShortDateString();
                    bBooking.STATUS_StatusName = status.STATUS_StatusName;
                    bBooking.BOOK_StatusID = status.STATUS_StatusID;
                    bBooking.CourseCode = bookingByRCC.BOOK_Year + bookingByRCC.BOOK_Month + bookingByRCC.BOOK_RandomCode;
                    bBooking.orderNumber = bookingByRCC.BOOK_OrderNo;
                    bBooking.invoiceNumber = bookingByRCC.BOOK_InvoiceNo;

                    if (status.STATUS_StatusName == "DONE")
                        bBooking.Coloring = "success";
                    else if (status.STATUS_StatusName == "CONFIRMED")
                        bBooking.Coloring = "info";
                    else if (status.STATUS_StatusName == "CANCELED")
                        bBooking.Coloring = "warning";
                    else if (status.STATUS_StatusName == "PENDING")
                        bBooking.Coloring = "danger";

                    result.Add(bBooking);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// This function genrates a unique course code
        /// </summary>
        /// <returns>
        /// Returns die unique code in a string fromat
        /// </returns>
        public static string GenerateUniqueCode()
        {
            // This code generate the randomCourseCode.
            Random random = new Random();
            int randomValue = random.Next(0001, 9999);
            // Convert current date to string
            string dates = DateTime.Now.ToString();
            // Concatinate the Year month and 4 digit code into one string value
            string courseCode = dates.Substring(2, 2) + dates.Substring(5, 2) + randomValue.ToString().Substring(0, 4);
            // This is the return value;
            return courseCode;
        }

        /// <summary>
        /// Within the transaction scope is a function to save the new booking data.
        /// Data is instantiated and then asigned to the DAL properties.
        /// </summary>
        /// <param name="booking"></param>
        /// <returns>
        /// Return the saved data
        /// </returns>
        public static BO.Booking SaveBooking(BO.Booking booking)
        {
            try
            {
                if (booking.BOOK_BookingID > 0)
                {
                    // Create the TransactionScope to execute the commands, guaranteeing
                    // that both commands can commit or roll back as a single unit of work.
                    using (TransactionScope scope = new TransactionScope())
                    {
                        var bookUnchanged = Get(booking.BOOK_BookingID);

                        DALCMS.BOOKING bBooking = new DALCMS.BOOKING();
                        bBooking.BOOK_BookingID = booking.BOOK_BookingID;
                        bBooking.BOOK_StartDate = DateTime.Parse(booking.BOOK_StartDate);
                        bBooking.BOOK_EndDate = DateTime.Parse(booking.BOOK_EndDate);
                        bBooking.BOOK_CampusID = booking.CAMP_CampusID > 0 ? booking.CAMP_CampusID : bookUnchanged.BOOK_CampusID ;
                        bBooking.BOOK_QualID = booking.QUAL_QualID > 0 ? booking.QUAL_QualID : bookUnchanged.BOOK_QualID;
                        bBooking.BOOK_UserID = booking.USER_UserID > 0 ? booking.USER_UserID : bookUnchanged.BOOK_UserID;
                        bBooking.BOOK_StatusID = booking.selectedStatus > 0 ? booking.selectedStatus : bookUnchanged.BOOK_StatusID;
                        bBooking.BOOK_OrderNo = booking.orderNumber;
                        bBooking.BOOK_InvoiceNo = booking.invoiceNumber;

                        // Check if booking status is Confirmed
                        string existingRCC = bookUnchanged.BOOK_Year + bookUnchanged.BOOK_Month + bookUnchanged.BOOK_RandomCode;
                        if (booking.selectedStatus == (int)Core.CMS.BO.Enums.BookingStatus.CONFIRMED && String.IsNullOrEmpty(existingRCC))
                        {
                            // calls the generateUnique code function
                            string courseCode = GenerateUniqueCode();
                            // Check if the courseCode exists
                            var rcc = Get().SingleOrDefault(i => i.BOOK_Year == courseCode.Substring(0, 2) && i.BOOK_Month == courseCode.Substring(2, 2) && i.BOOK_RandomCode == courseCode.Substring(4, 4));
                            // While loop that checks the if the instantiated variable is not null it continious with the loop
                            while (rcc != null)
                            {
                                courseCode = GenerateUniqueCode();
                                rcc = Get().SingleOrDefault(i => i.BOOK_Year == courseCode.Substring(0, 2) && i.BOOK_Month == courseCode.Substring(2, 2) && i.BOOK_RandomCode == courseCode.Substring(4, 4));
                            }

                            // Assign the course code to the variables op the booking collection
                            bBooking.BOOK_Year = courseCode.Substring(0, 2);
                            bBooking.BOOK_Month = courseCode.Substring(2, 2);
                            bBooking.BOOK_RandomCode = courseCode.Substring(4, 4);
                        }
                        else
                        {
                            bBooking.BOOK_Year = bookUnchanged.BOOK_Year;
                            bBooking.BOOK_Month = bookUnchanged.BOOK_Month;
                            bBooking.BOOK_RandomCode = bookUnchanged.BOOK_RandomCode;
                        }

                        Save(bBooking);

                        // The Complete method commits the transaction. If an exception has been thrown, 
                        // Complete is not called and the transaction is rolled back.
                        scope.Complete();

                        return booking;
                    }
                }
                else
                {
                    // Create the TransactionScope to execute the commands, guaranteeing
                    // that both commands can commit or roll back as a single unit of work.
                    using (TransactionScope scope = new TransactionScope())
                    {
                        DALCMS.BOOKING bBooking = new DALCMS.BOOKING();
                        bBooking.BOOK_BookingID = booking.BOOK_BookingID;
                        bBooking.BOOK_CampusID = booking.CAMP_CampusID;
                        bBooking.BOOK_QualID = booking.QUAL_QualID;
                        bBooking.BOOK_UserID = booking.USER_UserID;
                        bBooking.BOOK_StartDate = DateTime.Parse(booking.BOOK_StartDate);
                        bBooking.BOOK_EndDate = DateTime.Parse(booking.BOOK_EndDate);
                        bBooking.BOOK_Year = null;
                        bBooking.BOOK_Month = null;
                        bBooking.BOOK_RandomCode = null;
                        // booking status is hardcoded, the status is pending(2)
                        bBooking.BOOK_StatusID = (int)BO.Enums.BookingStatus.PENDING;
                        bBooking.BOOK_OrderNo = booking.orderNumber;
                        bBooking.BOOK_InvoiceNo = booking.invoiceNumber;

                        Save(bBooking);

                        // The Complete method commits the transaction. If an exception has been thrown, 
                        // Complete is not called and the transaction is rolled back.
                        scope.Complete();

                        return booking;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
