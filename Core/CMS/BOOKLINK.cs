using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
    /// <summary>
    /// This class extends the BOOKLINK Business Layer methods.
    /// </summary>
    public partial class BOOKLINK
    {
        /// <summary>
        /// This code validates the bussiness logik of the BOOKLINK table.
        /// </summary>
        /// <param name="BOOKLINK">BOOKLINK Object.</param>
        /// <param name="blType">The action to validate.</param>
        /// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
        public static string BussinesLogik(DALCMS.BOOKLINK BOOKLINK, COMMON.Enums.BLType blType)
        {
            //NB: Please apply the BOOKLINK Business validation Logic here

            StringBuilder sb = new StringBuilder();

            switch (blType)
            {
                case COMMON.Enums.BLType.Save:
                    break;
                case COMMON.Enums.BLType.Read:
                    break;
                case COMMON.Enums.BLType.Delete:
                    break;
                default:
                    break;
            }

            return sb.ToString();
        }

        // Please add extended methods here

        /// <summary>
        ///  Delete Booking Link
        /// </summary>
        /// <param name="userQuals"></param>
        /// <returns></returns>
        public static List<BO.UserQual> DeleteBookLink(List<BO.UserQual> userQuals)
        {
            try
            {
                foreach (BO.UserQual userQual in userQuals)
                {
                    if(userQual._destroy == true)
                    {
                        DALCMS.BOOKLINK bookLink = Core.CMS.BOOKLINK.Get().SingleOrDefault(bl => bl.BOOKLINK_BookingID == userQual.CLIQUAL_BookingID && bl.BOOKLINK_UserID == userQual.USER_UserID);
                        if (bookLink != null)
                        {
                            Core.CMS.BOOKLINK.Delete(bookLink);
                        }
                    }
                }
                return userQuals;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}
