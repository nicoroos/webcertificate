using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace Core.CMS
{
    /// <summary>
    /// This class extends the TIMESHEET Business Layer methods.
    /// </summary>
    public partial class TIMESHEET
    {
        /// <summary>
        /// This code validates the bussiness logik of the TIMESHEET table.
        /// </summary>
        /// <param name="TIMESHEET">TIMESHEET Object.</param>
        /// <param name="blType">The action to validate.</param>
        /// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
        public static string BussinesLogik(DALCMS.TIMESHEET TIMESHEET, COMMON.Enums.BLType blType)
        {
            //NB: Please apply the TIMESHEET Business validation Logic here

            StringBuilder sb = new StringBuilder();

            switch (blType)
            {
                case COMMON.Enums.BLType.Save:
                    break;
                case COMMON.Enums.BLType.Read:
                    break;
                case COMMON.Enums.BLType.Delete:
                    break;
                default:
                    break;
            }

            return sb.ToString();
        }

        // Please add extended methods here

        /// <summary>
        /// Get TimeSheet By ID
        /// </summary>
        /// <param name="timeSheetID"></param>
        /// <returns>Returns the Timesheet by the ID</returns>
        public static BO.TimeSheet GetTimeSheetByID(int timeSheetID)
        {
            try
            {
                DALCMS.TIMESHEET timeSheet = Core.CMS.TIMESHEET.Get(timeSheetID);

                BO.TimeSheet tTimeSheet = new BO.TimeSheet();
                tTimeSheet.TIME_TimeSheetID = timeSheet.TIME_TimeSheetID;
                tTimeSheet.TIME_UserID = timeSheet.TIME_UserID;
                tTimeSheet.TIME_Date = timeSheet.TIME_Date.ToString("yyyy-MM-dd");
                tTimeSheet.TIME_StartTime = timeSheet.TIME_StartTime.ToString("HH:mm");
                tTimeSheet.TIME_EndTime = timeSheet.TIME_EndTime.ToString("HH:mm");

                TimeSpan totalTime = timeSheet.TIME_EndTime.Subtract(timeSheet.TIME_StartTime);
                tTimeSheet.TIME_Hours = timeSheet.TIME_Hours.ToString();//Calculation moet in knockout gemaak word

                tTimeSheet.COL_Name = timeSheet.CAMPUS.COLLEGE.COL_Name;
                tTimeSheet.CAMP_Name = timeSheet.CAMPUS.CAMP_Name;
                tTimeSheet.TIME_Nights = timeSheet.TIME_Nights;
                tTimeSheet.COST_Toll = timeSheet.COSTS.COST_Toll;
                tTimeSheet.COST_FlightTransport = timeSheet.COSTS.COST_FlightTransport;
                tTimeSheet.COST_FlightParking = timeSheet.COSTS.COST_FlightParking;
                tTimeSheet.COST_Other = timeSheet.COSTS.COST_Other;
                tTimeSheet.COST_Food = timeSheet.COSTS.COST_Food;

                tTimeSheet.RATES_RatePerKM = timeSheet.COSTS.RatePerKM.RATES_RatePerKM;
                tTimeSheet.RATES_Name = timeSheet.COSTS.RatePerKM.RATES_Name;
                tTimeSheet.TIME_ODOStart = int.Parse(timeSheet.TIME_ODOStart.Trim());
                tTimeSheet.TIME_ODOEnd = int.Parse(timeSheet.TIME_ODOEnd.Trim());
                int KMTraveled = tTimeSheet.TIME_ODOEnd - tTimeSheet.TIME_ODOStart;
                tTimeSheet.KMTraveled = KMTraveled.ToString();

                return tTimeSheet;
            }
            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Get Timesheet List By Date And UserID
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static List<BO.TimeSheet> GetTimesheetListByDateAndUserID(DateTime startDate, DateTime endDate, int userID)
        {

            try
            {
                List<BO.TimeSheet> result = new List<BO.TimeSheet>();

                foreach (DALCMS.TIMESHEET timeSheet in Core.CMS.TIMESHEET.Get().Where(tu => tu.TIME_Date >= startDate && tu.TIME_Date <= endDate && tu.TIME_UserID == userID)) //(startDate, endDate, userID))
                {


                    BO.TimeSheet tuTimeSheet = new BO.TimeSheet();
                    tuTimeSheet.TIME_TimeSheetID = timeSheet.TIME_TimeSheetID;
                    tuTimeSheet.TIME_UserID = timeSheet.TIME_UserID;
                    tuTimeSheet.TIME_Date = timeSheet.TIME_Date.ToString();
                    tuTimeSheet.TIME_StartTime = timeSheet.TIME_StartTime.ToString();
                    tuTimeSheet.TIME_EndTime = timeSheet.TIME_EndTime.ToString();

                    TimeSpan diff1 = timeSheet.TIME_EndTime.Subtract(timeSheet.TIME_StartTime);
                    tuTimeSheet.TIME_Hours = diff1.ToString();//Calculation moet in knockout gemaak word

                    tuTimeSheet.COL_Name = timeSheet.CAMPUS.COLLEGE.COL_Name;
                    tuTimeSheet.CAMP_Name = timeSheet.CAMPUS.CAMP_Name;
                    tuTimeSheet.TIME_Nights = timeSheet.TIME_Nights;
                    tuTimeSheet.COST_Toll = timeSheet.COSTS.COST_Toll;
                    tuTimeSheet.COST_FlightTransport = timeSheet.COSTS.COST_FlightTransport;
                    tuTimeSheet.COST_FlightParking = timeSheet.COSTS.COST_FlightParking;
                    tuTimeSheet.COST_Other = timeSheet.COSTS.COST_Other;
                    tuTimeSheet.COST_Food = timeSheet.COSTS.COST_Food;

                    tuTimeSheet.RATES_Name = timeSheet.COSTS.RatePerKM.RATES_Name;
                    tuTimeSheet.RATES_RatePerKM = timeSheet.COSTS.RatePerKM.RATES_RatePerKM;

                    tuTimeSheet.TIME_ODOStart = int.Parse(timeSheet.TIME_ODOStart.Trim());
                    tuTimeSheet.TIME_ODOEnd = int.Parse(timeSheet.TIME_ODOEnd.Trim());
                    int KMTraveled = tuTimeSheet.TIME_ODOEnd - tuTimeSheet.TIME_ODOStart;
                    tuTimeSheet.KMTraveled = KMTraveled.ToString();

                    result.Add(tuTimeSheet);

                }


                return result;
            }

            catch (Exception)
            {

                throw;
            }

        }

        /// <summary>
        /// Get TimeSheet By UserID
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static List<BO.TimeSheet> GetTimeSheetByUserID(int userID)
        {
            try
            {
                List<BO.TimeSheet> result = new List<BO.TimeSheet>();

                foreach (DALCMS.TIMESHEET timeSheet in Core.CMS.TIMESHEET.Get().Where(tl => tl.TIME_UserID == userID))
                {
                    BO.TimeSheet tlTimeSheet = new BO.TimeSheet();
                    tlTimeSheet.TIME_TimeSheetID = timeSheet.TIME_TimeSheetID;
                    tlTimeSheet.TIME_UserID = timeSheet.TIME_UserID;
                    tlTimeSheet.TIME_Date = timeSheet.TIME_Date.ToString();
                    tlTimeSheet.TIME_StartTime = timeSheet.TIME_StartTime.ToString();
                    tlTimeSheet.TIME_EndTime = timeSheet.TIME_EndTime.ToString();

                    TimeSpan diff1 = timeSheet.TIME_EndTime.Subtract(timeSheet.TIME_StartTime);
                    tlTimeSheet.TIME_Hours = diff1.ToString();//Calculation moet in knockout gemaak word

                    tlTimeSheet.COL_Name = timeSheet.CAMPUS.COLLEGE.COL_Name;
                    tlTimeSheet.CAMP_Name = timeSheet.CAMPUS.CAMP_Name;
                    tlTimeSheet.TIME_Nights = timeSheet.TIME_Nights;
                    tlTimeSheet.COST_Toll = timeSheet.COSTS.COST_Toll;
                    tlTimeSheet.COST_FlightTransport = timeSheet.COSTS.COST_FlightTransport;
                    tlTimeSheet.COST_FlightParking = timeSheet.COSTS.COST_FlightParking;
                    tlTimeSheet.COST_Other = timeSheet.COSTS.COST_Other;
                    tlTimeSheet.COST_Food = timeSheet.COSTS.COST_Food;

                    tlTimeSheet.RATES_Name = timeSheet.COSTS.RatePerKM.RATES_Name;
                    tlTimeSheet.RATES_RatePerKM = timeSheet.COSTS.RatePerKM.RATES_RatePerKM;

                    tlTimeSheet.TIME_ODOStart = int.Parse(timeSheet.TIME_ODOStart.Trim());
                    tlTimeSheet.TIME_ODOEnd = int.Parse(timeSheet.TIME_ODOEnd.Trim());
                    int KMTraveled = tlTimeSheet.TIME_ODOEnd - tlTimeSheet.TIME_ODOStart;
                    tlTimeSheet.KMTraveled = KMTraveled.ToString();

                    result.Add(tlTimeSheet);
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Save and/ or update TimeSheet
        /// </summary>
        /// <param name="tstimeSheet"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static BO.TimeSheet SaveTimeSheet(BO.TimeSheet timeSheet, int userID)
        {
            try
            {

                if (timeSheet.TIME_TimeSheetID > 0)
                {
                    // Create the TransactionScope to execute the commands, guaranteeing
                    // that both commands can commit or roll back as a single unit of work.
                    using (TransactionScope scope = new TransactionScope())
                    {
                        DALCMS.TIMESHEET getTimeSheet = Core.CMS.TIMESHEET.Get(timeSheet.TIME_TimeSheetID);
                        DALCMS.COSTS cCost = Core.CMS.COSTS.Get(getTimeSheet.TIME_CostID);
                        DALCMS.RatePerKM ratePerKM = Core.CMS.RatePerKM.Get().SingleOrDefault(r => r.RATES_RatePerKM == timeSheet.selectedRate);
                        DALCMS.CAMPUS campus = Core.CMS.CAMPUS.Get().SingleOrDefault(c => c.CAMP_Name == timeSheet.CAMP_Name);

                        DALCMS.COSTS cost = new DALCMS.COSTS(); 
                        cost.COST_CostID = timeSheet.TIME_CostID;
                        
                        if (ratePerKM == null)
                        {
                            cost.COST_RatesPerKMID = cCost.COST_RatesPerKMID;
                        }
                        else
                        {
                            cost.COST_RatesPerKMID = ratePerKM.RATES_RatePerKMID;
                        }
                        cost.COST_Toll = timeSheet.COST_Toll.HasValue ? timeSheet.COST_Toll.Value : getTimeSheet.COSTS.COST_Toll;
                        cost.COST_FlightParking = timeSheet.COST_FlightParking.HasValue ? timeSheet.COST_FlightParking.Value : getTimeSheet.COSTS.COST_FlightParking;
                        cost.COST_FlightTransport = timeSheet.COST_FlightTransport.HasValue ? timeSheet.COST_FlightTransport.Value : getTimeSheet.COSTS.COST_FlightTransport;
                        cost.COST_Other = timeSheet.COST_Other.HasValue ? timeSheet.COST_Other.Value : getTimeSheet.COSTS.COST_Other;
                        cost.COST_Food = timeSheet.COST_Food.HasValue ? timeSheet.COST_Food.Value : getTimeSheet.COSTS.COST_Food;

                        DALCMS.COSTS costs = Core.CMS.COSTS.Save(cost);

                        DALCMS.TIMESHEET tTimeSheet = new DALCMS.TIMESHEET();
                        tTimeSheet.TIME_TimeSheetID = timeSheet.TIME_TimeSheetID;
                        tTimeSheet.TIME_UserID = userID;
                        tTimeSheet.TIME_Date = DateTime.Parse(timeSheet.TIME_Date);
                        tTimeSheet.TIME_StartTime = DateTime.Parse(timeSheet.TIME_StartTime);
                        tTimeSheet.TIME_EndTime = DateTime.Parse(timeSheet.TIME_EndTime);
                        TimeSpan hours = tTimeSheet.TIME_EndTime.Subtract(tTimeSheet.TIME_StartTime);
                        string diff = hours.ToString();
                        tTimeSheet.TIME_Hours = Convert.ToDateTime(diff);//Calculation moet in knockout gemaak word
                        tTimeSheet.TIME_CampusID = timeSheet.selectedCampus < 1 ? campus.CAMP_CampusID : timeSheet.selectedCampus;
                        tTimeSheet.TIME_Nights = timeSheet.TIME_Nights;
                        tTimeSheet.TIME_CostID = costs.COST_CostID;
                        tTimeSheet.TIME_ODOStart = timeSheet.TIME_ODOStart.ToString();
                        tTimeSheet.TIME_ODOEnd = timeSheet.TIME_ODOEnd.ToString();

                        DALCMS.TIMESHEET.Save(tTimeSheet);

                        // The Complete method commits the transaction. If an exception has been thrown, 
                        // Complete is not called and the transaction is rolled back.
                        scope.Complete();

                        return timeSheet;
                    }
                }
                else
                {
                    // Create the TransactionScope to execute the commands, guaranteeing
                    // that both commands can commit or roll back as a single unit of work.
                    using (TransactionScope scope = new TransactionScope())
                    {
                        DALCMS.RatePerKM ratePerKM = Core.CMS.RatePerKM.Get().SingleOrDefault(r => r.RATES_RatePerKM == timeSheet.selectedRate);

                        DALCMS.COSTS cost = new DALCMS.COSTS();
                        cost.COST_CostID = timeSheet.TIME_CostID;
                        cost.COST_RatesPerKMID = ratePerKM.RATES_RatePerKMID;
                        cost.COST_Toll = timeSheet.COST_Toll;
                        cost.COST_FlightParking = timeSheet.COST_FlightParking;
                        cost.COST_FlightTransport = timeSheet.COST_FlightTransport;
                        cost.COST_Other = timeSheet.COST_Other;
                        cost.COST_Food = timeSheet.COST_Food;

                        DALCMS.COSTS costs = Core.CMS.COSTS.Save(cost);

                        DALCMS.CAMPUS campus = Core.CMS.CAMPUS.Get().SingleOrDefault(c => c.CAMP_Name == timeSheet.CAMP_Name);

                        DALCMS.TIMESHEET tTimeSheet = new DALCMS.TIMESHEET();
                        tTimeSheet.TIME_TimeSheetID = timeSheet.TIME_TimeSheetID;
                        tTimeSheet.TIME_UserID = userID;
                        tTimeSheet.TIME_Date = DateTime.Parse(timeSheet.TIME_Date);
                        tTimeSheet.TIME_StartTime = DateTime.Parse(timeSheet.TIME_StartTime);
                        tTimeSheet.TIME_EndTime = DateTime.Parse(timeSheet.TIME_EndTime);
                        TimeSpan hours = tTimeSheet.TIME_EndTime.Subtract(tTimeSheet.TIME_StartTime);
                        string diff = hours.ToString();
                        tTimeSheet.TIME_Hours = Convert.ToDateTime(diff);//Calculation moet in knockout gemaak word
                        tTimeSheet.TIME_CampusID = timeSheet.selectedCampus < 1 ? campus.CAMP_CampusID : timeSheet.selectedCampus;
                        tTimeSheet.TIME_Nights = timeSheet.TIME_Nights;
                        tTimeSheet.TIME_CostID = costs.COST_CostID;
                        tTimeSheet.TIME_ODOStart = timeSheet.TIME_ODOStart.ToString();
                        tTimeSheet.TIME_ODOEnd = timeSheet.TIME_ODOEnd.ToString();

                        Core.CMS.TIMESHEET.Save(tTimeSheet);

                        // The Complete method commits the transaction. If an exception has been thrown, 
                        // Complete is not called and the transaction is rolled back.
                        scope.Complete();

                        return timeSheet;
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
