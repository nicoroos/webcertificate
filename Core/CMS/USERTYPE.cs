using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class extends the USERTYPE Business Layer methods.
/// </summary>
public partial class USERTYPE
{
/// <summary>
/// This code validates the bussiness logik of the USERTYPE table.
/// </summary>
/// <param name="USERTYPE">USERTYPE Object.</param>
/// <param name="blType">The action to validate.</param>
/// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
public static string BussinesLogik(DALCMS.USERTYPE USERTYPE, COMMON.Enums.BLType blType)
{
//NB: Please apply the USERTYPE Business validation Logic here

StringBuilder sb = new StringBuilder();

switch (blType)
{
case COMMON.Enums.BLType.Save:
break;
case COMMON.Enums.BLType.Read:
break;
case COMMON.Enums.BLType.Delete:
break;
default:
break;
}

return sb.ToString();
}

// Please add extended methods here
}
}
