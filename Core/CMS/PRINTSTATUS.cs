using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class extends the PRINTSTATUS Business Layer methods.
/// </summary>
public partial class PRINTSTATUS
{
/// <summary>
/// This code validates the bussiness logik of the PRINTSTATUS table.
/// </summary>
/// <param name="PRINTSTATUS">PRINTSTATUS Object.</param>
/// <param name="blType">The action to validate.</param>
/// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
public static string BussinesLogik(DALCMS.PRINTSTATUS PRINTSTATUS, COMMON.Enums.BLType blType)
{
//NB: Please apply the PRINTSTATUS Business validation Logic here

StringBuilder sb = new StringBuilder();

switch (blType)
{
case COMMON.Enums.BLType.Save:
break;
case COMMON.Enums.BLType.Read:
break;
case COMMON.Enums.BLType.Delete:
break;
default:
break;
}

return sb.ToString();
}

// Please add extended methods here
}
}
