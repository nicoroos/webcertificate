﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Core.CMS
{
    /// <summary>
    /// This class handles all the methods and validations regarding the USERS and CONTACTDETAILS table of the DB.
    /// </summary>
    public static class UserDetials
    {
        /// <summary>
        /// Get all the detials of a specific user
        /// </summary>
        /// <param name="email"></param>
        /// <returns>
        /// Returns a user's with the specified email
        /// </returns>
        public static BO.UserDetails GetUserByEmail(string email)
        {
            try
            {
                DALCMS.CONTACTDETAILS contactdetails = Core.CMS.CONTACTDETAILS.GetUserByEmail(email.ToUpper());

                BO.UserDetails userDetails = new BO.UserDetails();
                userDetails.USER_UserID = contactdetails.CONDET_UserID;
                userDetails.USER_Name = contactdetails.USERS.USER_Name.Trim();
                userDetails.USER_Surname = contactdetails.USERS.USER_Surname.Trim();
                userDetails.USER_Initials = contactdetails.USERS.USER_Initials.Trim();

                userDetails.TITLE_Name = Core.CMS.TITLE.Get(Convert.ToInt32(contactdetails.USERS.USER_TitleID)).TITLE_Name;

                userDetails.USER_Password = contactdetails.USERS.USER_Password.Trim();
                userDetails.USER_TypeID = contactdetails.USERS.USER_TypeID;

                userDetails.CONDET_CONTACTDETAILSID = contactdetails.CONDET_CONTACTDETAILSID;
                userDetails.CONDET_StreetNo = contactdetails.CONDET_StreetNo;
                userDetails.CONDET_StreetName = contactdetails.CONDET_StreetName;
                userDetails.CONDET_Suburb = contactdetails.CONDET_Suburb;
                userDetails.CONDET_City = contactdetails.CONDET_City;
                userDetails.CONDET_PostalCode = contactdetails.CONDET_PostalCode;
                userDetails.CONDET_TelNo = contactdetails.CONDET_TelNo;
                userDetails.CONDET_FaxNo = contactdetails.CONDET_FaxNo;
                userDetails.CONDET_Email = contactdetails.CONDET_Email;
                userDetails.CONDET_CellNo = contactdetails.CONDET_CellNo;

                return userDetails;
            }
            catch (NullReferenceException)
            {
                BO.UserDetails userDetails = new BO.UserDetails();

                userDetails.USER_UserID = 0;
                userDetails.USER_Name = "";
                userDetails.USER_Surname = "";
                userDetails.USER_Initials = "";
                userDetails.selectedTitle = 0;
                userDetails.USER_Password = "";
                userDetails.USER_TypeID = 0;

                userDetails.CONDET_CONTACTDETAILSID = 0;
                userDetails.CONDET_StreetNo = "";
                userDetails.CONDET_StreetName = "";
                userDetails.CONDET_Suburb = "";
                userDetails.CONDET_City = "";
                userDetails.CONDET_PostalCode = "";
                userDetails.CONDET_TelNo = "";
                userDetails.CONDET_FaxNo = "";
                userDetails.CONDET_Email = "";
                userDetails.CONDET_CellNo = "";

                return userDetails;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get a specific users details from DB
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Retruns the specified user's Details
        /// </returns>
        public static BO.UserDetails GetUserByUserID(int userID)
        {
            try
            {
                DALCMS.CONTACTDETAILS contactdetails = Core.CMS.CONTACTDETAILS.GetUserByUserID(userID);
                DALCMS.USERS user = Core.CMS.USERS.Get(contactdetails.CONDET_UserID);

                BO.UserDetails userDetails = new BO.UserDetails();

                userDetails.USER_UserID = contactdetails.CONDET_UserID;
                userDetails.USER_Name = user.USER_Name.Trim();
                userDetails.USER_Surname = user.USER_Surname.Trim();
                userDetails.USER_Initials = user.USER_Initials.Trim();

                if(user.USER_TitleID == 0)
                {
                    userDetails.TITLE_Name = string.Empty;
                    userDetails.selectedTitle = 0;
                }
                else if(user.USER_TitleID.HasValue)
                {
                    DALCMS.TITLE title = TITLE.Get().Where(t => t.TITLE_TitleID == user.USER_TitleID.Value).SingleOrDefault();
                    userDetails.TITLE_Name = title.TITLE_Name.Trim();
                    userDetails.selectedTitle = title.TITLE_TitleID;
                }

                userDetails.USER_Password = user.USER_Password.Trim();

                DALCMS.USERTYPE usertype = USERTYPE.Get().Where(ut => ut.TYPE_TypeID == user.USER_TypeID).SingleOrDefault();
                userDetails.TYPE_Name = usertype.TYPE_Name;
                userDetails.selectedType = usertype.TYPE_TypeID;

                DALCMS.CAMPUSLINK campusLink = CAMPUSLINK.GetCampusLinkByUserID(contactdetails.CONDET_UserID);
                if(campusLink == null)
                {
                    userDetails.CAMP_Name = string.Empty;
                    userDetails.COL_Name = string.Empty;
                }
                else
                {
                    DALCMS.CAMPUS campus = CAMPUS.Get(campusLink.CAMPLINK_CampusID);
                    userDetails.selectedCampus = campus.CAMP_CampusID;
                    userDetails.selectedCboCampus = campus.CAMP_CampusID;
                    userDetails.CAMP_Name = campus.CAMP_Name;

                    DALCMS.COLLEGE college = COLLEGE.Get(campus.CAMP_CollegID);
                    userDetails.selectedCollege = college.COL_CollegeID;
                    userDetails.selectedCboCollege = college.COL_CollegeID;
                    userDetails.COL_Name = college.COL_Name;
                }

                userDetails.CONDET_CONTACTDETAILSID = contactdetails.CONDET_CONTACTDETAILSID;
                userDetails.CONDET_CellNo = contactdetails.CONDET_CellNo;
                userDetails.CONDET_Email = contactdetails.CONDET_Email;
                // Check if street name is null and if it is asigns a empty string
                if (string.IsNullOrEmpty(contactdetails.CONDET_StreetNo))
                {
                    userDetails.CONDET_StreetNo = string.Empty;
                }
                else
                {
                    userDetails.CONDET_StreetNo = contactdetails.CONDET_StreetNo.Trim();
                }
                // Check if street no is null and if it is asigns a empty string
                if (string.IsNullOrEmpty(contactdetails.CONDET_StreetName))
                {
                    userDetails.CONDET_StreetName = string.Empty;
                }
                else
                {
                    userDetails.CONDET_StreetName = contactdetails.CONDET_StreetName.Trim();
                }
                // Check if suburb is null and if it is asigns a empty string
                if (string.IsNullOrEmpty(contactdetails.CONDET_Suburb))
                {
                    userDetails.CONDET_Suburb = string.Empty;
                }
                else
                {
                    userDetails.CONDET_Suburb = contactdetails.CONDET_Suburb.Trim();
                }
                // Check if City is null and if it is asigns a empty string
                if (string.IsNullOrEmpty(contactdetails.CONDET_City))
                {
                    userDetails.CONDET_City = string.Empty;
                }
                else
                {
                    userDetails.CONDET_City = contactdetails.CONDET_City.Trim();
                }
                // Check if PostalCode is null and if it is asigns a empty string
                if (string.IsNullOrEmpty(contactdetails.CONDET_PostalCode))
                {
                    userDetails.CONDET_PostalCode = string.Empty;
                }
                else
                {
                    userDetails.CONDET_PostalCode = contactdetails.CONDET_PostalCode.Trim();
                }
                // Check if Tel no is null and if it is asigns a empty string
                if (string.IsNullOrEmpty(contactdetails.CONDET_TelNo))
                {
                    userDetails.CONDET_TelNo = string.Empty;
                }
                else
                {
                    userDetails.CONDET_TelNo = contactdetails.CONDET_TelNo.Trim();
                }
                // Check if Fax no is null and if it is asigns a empty string
                if (string.IsNullOrEmpty(contactdetails.CONDET_FaxNo))
                {
                    userDetails.CONDET_FaxNo = string.Empty;
                }
                else
                {
                    userDetails.CONDET_FaxNo = contactdetails.CONDET_FaxNo.Trim();
                }

                return userDetails;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Genereate a uniquer password
        /// </summary>
        /// <returns>
        /// Return the random password.
        /// </returns>
        private static string PasswordGenerator()
        {
            // This code generate the randomCourseCode.
            Random random = new Random();
            int randomValue = random.Next(10000, 99999);
            int randomValue2 = random.Next(100, 999);
            // Convert current date to string
            string date = DateTime.Now.ToString();
            // Concatinate the Year month and 4 digit code into one string value
            string courseCode = randomValue.ToString() + date.Substring(0, 4) + randomValue2.ToString();

            return courseCode;
        }

        /// <summary>
        /// Save the record to the DB
        /// </summary>
        /// <param name="userDetails"></param>
        /// <returns></returns>
        public static BO.UserDetails SaveUserDetails(BO.UserDetails userDetails)
        {
            try
            {
                var email = GetUserByEmail(userDetails.CONDET_Email);

                if (string.IsNullOrEmpty(email.CONDET_Email))
                {
                    // Create the TransactionScope to execute the commands, guaranteeing
                    // that both commands can commit or roll back as a single unit of work.
                    using (TransactionScope scope = new TransactionScope())
                    {
                        DALCMS.USERS user = new DALCMS.USERS();
                        user.USER_UserID = userDetails.USER_UserID;
                        user.USER_Name = userDetails.USER_Name;
                        user.USER_Surname = userDetails.USER_Surname;
                        user.USER_Initials = userDetails.USER_Initials;
                        user.USER_TitleID = userDetails.USER_TitleID;
                        user.USER_Password = PasswordGenerator();
                        user.USER_TypeID = userDetails.USER_TypeID;

                        DALCMS.USERS userID = Core.CMS.USERS.Save(user);

                        if (!string.IsNullOrEmpty(userDetails.selectedCourse))
                        {
                            DALCMS.BOOKING booking = Core.CMS.BOOKING.Get().SingleOrDefault(b => b.BOOK_Year == userDetails.selectedCourse.Substring(0, 2) && b.BOOK_Month == userDetails.selectedCourse.Substring(2, 2) && b.BOOK_RandomCode == userDetails.selectedCourse.Substring(4, 4));
                            DALCMS.BOOKLINK bBooking = new DALCMS.BOOKLINK();
                            bBooking.BOOKLINK_BookingID = booking.BOOK_BookingID;
                            bBooking.BOOKLINK_UserID = userID.USER_UserID;

                            Core.CMS.BOOKLINK.Save(bBooking);
                        }

                        DALCMS.CONTACTDETAILS contactDetails = new DALCMS.CONTACTDETAILS();
                        contactDetails.CONDET_CONTACTDETAILSID = userDetails.CONDET_CONTACTDETAILSID;
                        contactDetails.CONDET_UserID = userID.USER_UserID;
                        contactDetails.CONDET_StreetNo = string.IsNullOrEmpty(userDetails.CONDET_StreetNo) ? "" : userDetails.CONDET_StreetNo.ToUpper();
                        contactDetails.CONDET_StreetName = string.IsNullOrEmpty(userDetails.CONDET_StreetName) ? "" : userDetails.CONDET_StreetName.ToUpper();
                        contactDetails.CONDET_Suburb = string.IsNullOrEmpty(userDetails.CONDET_Suburb) ? "" : userDetails.CONDET_Suburb.ToUpper();
                        contactDetails.CONDET_City = string.IsNullOrEmpty(userDetails.CONDET_City) ? "" : userDetails.CONDET_City.ToUpper();
                        contactDetails.CONDET_PostalCode = string.IsNullOrEmpty(userDetails.CONDET_PostalCode) ? "" : userDetails.CONDET_PostalCode.ToUpper();
                        contactDetails.CONDET_TelNo = string.IsNullOrEmpty(userDetails.CONDET_TelNo) ? "" : userDetails.CONDET_TelNo;
                        contactDetails.CONDET_FaxNo = string.IsNullOrEmpty(userDetails.CONDET_FaxNo) ? "" : userDetails.CONDET_FaxNo;
                        contactDetails.CONDET_CellNo = userDetails.CONDET_CellNo;
                        contactDetails.CONDET_Email = string.IsNullOrEmpty(userDetails.CONDET_Email) ? "" : userDetails.CONDET_Email.ToUpper();

                        Core.CMS.CONTACTDETAILS.Save(contactDetails);

                        DALCMS.CAMPUSLINK campuslink = new DALCMS.CAMPUSLINK();
                        campuslink.CAMPLINK_UserID = userID.USER_UserID;
                        campuslink.CAMPLINK_CampusID = userDetails.selectedCampus;

                        Core.CMS.CAMPUSLINK.Save(campuslink);

                        // The Complete method commits the transaction. If an exception has been thrown, 
                        // Complete is not called and the transaction is rolled back.
                        scope.Complete();

                        return userDetails;
                    }
                }
                else
                {
                    // Create the TransactionScope to execute the commands, guaranteeing
                    // that both commands can commit or roll back as a single unit of work.
                    using (TransactionScope scope = new TransactionScope())
                    {
                        DALCMS.USERS user = new DALCMS.USERS();
                        user.USER_UserID = userDetails.USER_UserID;
                        user.USER_Name = userDetails.USER_Name;
                        user.USER_Surname = userDetails.USER_Surname;
                        user.USER_Initials = userDetails.USER_Initials;
                        user.USER_Password = userDetails.USER_Password.ToUpper();
                        user.USER_TitleID = userDetails.USER_TitleID > 0 ? userDetails.USER_TitleID : email.USER_TitleID;
                        user.USER_TypeID = userDetails.selectedType > 0 ? userDetails.selectedType : email.USER_TypeID;

                        DALCMS.USERS updateUser = Core.CMS.USERS.Save(user);

                        if (!string.IsNullOrEmpty(userDetails.selectedCourse))
                        {
                            DALCMS.BOOKING booking = Core.CMS.BOOKING.Get().SingleOrDefault(b => b.BOOK_Year == userDetails.selectedCourse.Substring(0, 2) && b.BOOK_Month == userDetails.selectedCourse.Substring(2, 2) && b.BOOK_RandomCode == userDetails.selectedCourse.Substring(4, 4));
                            DALCMS.BOOKLINK bBooking = new DALCMS.BOOKLINK();
                            bBooking.BOOKLINK_BookingID = booking.BOOK_BookingID;
                            bBooking.BOOKLINK_UserID = userDetails.USER_UserID;

                            Core.CMS.BOOKLINK.Save(bBooking);
                        }

                        if (userDetails.selectedCboCampus > 0)
                        {
                            DALCMS.CAMPUSLINK campusLink = Core.CMS.CAMPUSLINK.GetCampusLinkByUserID(userDetails.USER_UserID);
                            campusLink.CAMPLINK_CampLinkID = campusLink.CAMPLINK_CampLinkID;
                            campusLink.CAMPLINK_UserID = userDetails.USER_UserID;
                            campusLink.CAMPLINK_CampusID = userDetails.selectedCampus;

                            Core.CMS.CAMPUSLINK.Save(campusLink);
                        }

                        DALCMS.CONTACTDETAILS contactDetails = new DALCMS.CONTACTDETAILS();
                        contactDetails.CONDET_CONTACTDETAILSID = userDetails.CONDET_CONTACTDETAILSID;
                        contactDetails.CONDET_UserID = userDetails.USER_UserID;
                        contactDetails.CONDET_StreetNo = string.IsNullOrEmpty(userDetails.CONDET_StreetNo) ? "" : userDetails.CONDET_StreetNo.ToUpper();
                        contactDetails.CONDET_StreetName = string.IsNullOrEmpty(userDetails.CONDET_StreetName) ? "" : userDetails.CONDET_StreetName.ToUpper();
                        contactDetails.CONDET_Suburb = string.IsNullOrEmpty(userDetails.CONDET_Suburb) ? "" : userDetails.CONDET_Suburb.ToUpper();
                        contactDetails.CONDET_City = string.IsNullOrEmpty(userDetails.CONDET_City) ? "" : userDetails.CONDET_City.ToUpper();
                        contactDetails.CONDET_PostalCode = string.IsNullOrEmpty(userDetails.CONDET_PostalCode) ? "" : userDetails.CONDET_PostalCode.ToUpper();
                        contactDetails.CONDET_TelNo = string.IsNullOrEmpty(userDetails.CONDET_TelNo) ? "" : userDetails.CONDET_TelNo;
                        contactDetails.CONDET_FaxNo = string.IsNullOrEmpty(userDetails.CONDET_FaxNo) ? "" : userDetails.CONDET_FaxNo;
                        contactDetails.CONDET_Email = string.IsNullOrEmpty(userDetails.CONDET_Email) ? "" : userDetails.CONDET_Email.ToUpper();
                        contactDetails.CONDET_CellNo = userDetails.CONDET_CellNo;

                        Core.CMS.CONTACTDETAILS.Save(contactDetails);

                        // The Complete method commits the transaction. If an exception has been thrown, 
                        // Complete is not called and the transaction is rolled back.
                        scope.Complete();

                        return userDetails;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
