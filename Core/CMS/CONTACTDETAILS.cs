using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
    /// <summary>
    /// This class extends the CONTACTDETAILS Business Layer methods.
    /// </summary>
    public partial class CONTACTDETAILS
    {
        /// <summary>
        /// This code validates the bussiness logik of the CONTACTDETAILS table.
        /// </summary>
        /// <param name="CONTACTDETAILS">CONTACTDETAILS Object.</param>
        /// <param name="blType">The action to validate.</param>
        /// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
        public static string BussinesLogik(DALCMS.CONTACTDETAILS CONTACTDETAILS, COMMON.Enums.BLType blType)
        {
            //NB: Please apply the CONTACTDETAILS Business validation Logic here

            StringBuilder sb = new StringBuilder();

            switch (blType)
            {
                case COMMON.Enums.BLType.Save:
                    break;
                case COMMON.Enums.BLType.Read:
                    break;
                case COMMON.Enums.BLType.Delete:
                    break;
                default:
                    break;
            }

            return sb.ToString();
        }

        // Please add extended methods here
        /// <summary>
        /// Get a specific record from the CONTACTDETAILS table of the DB
        /// </summary>
        /// <param name="email"></param>
        /// <returns>
        /// Return a record from the DB matching the param
        /// </returns>
        public static DALCMS.CONTACTDETAILS GetUserByEmail(string email)
        {
            try
            {
                DALCMS.CONTACTDETAILS contactDetails = Core.CMS.CONTACTDETAILS.Get().SingleOrDefault(cd => cd.CONDET_Email == email);
                return contactDetails;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the CONTACTDETAILS table of the DB.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Return a record from the DB matching the param.
        /// </returns>
        public static DALCMS.CONTACTDETAILS GetUserByUserID(int userID)
        {
            try
            {
                return Core.CMS.CONTACTDETAILS.Get().Where(cd => cd.CONDET_UserID == userID).SingleOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
