using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace Core.CMS
{
    /// <summary>
    /// This class extends the CLIENTQUAL Business Layer methods.
    /// </summary>
    public partial class CLIENTQUAL
    {
        /// <summary>
        /// This code validates the bussiness logik of the CLIENTQUAL table.
        /// </summary>
        /// <param name="CLIENTQUAL">CLIENTQUAL Object.</param>
        /// <param name="blType">The action to validate.</param>
        /// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
        public static string BussinesLogik(DALCMS.CLIENTQUAL CLIENTQUAL, COMMON.Enums.BLType blType)
        {
            //NB: Please apply the CLIENTQUAL Business validation Logic here

            StringBuilder sb = new StringBuilder();

            switch (blType)
            {
                case COMMON.Enums.BLType.Save:
                    break;
                case COMMON.Enums.BLType.Read:
                    break;
                case COMMON.Enums.BLType.Delete:
                    break;
                default:
                    break;
            }

            return sb.ToString();
        }

        // Please add extended methods here

        /// <summary>
        /// Get a list of record from the DB table CLIENTQUAL matching the param
        /// </summary>
        /// <param name="userID">User ID</param>
        /// <returns>
        /// Returns a list of records matching the param
        /// </returns>
        public static List<BO.UserQual> GetUserQualByUserID(int userID)
        {
            try
            {
                List<BO.UserQual> result = new List<BO.UserQual>();

                foreach (DALCMS.CLIENTQUAL userQual in Core.CMS.CLIENTQUAL.Get().Where(uq => uq.CLIQUAL_UserID == userID))
                {
                    BO.UserQual uUserQual = new BO.UserQual();

                    DALCMS.USERS user = Core.CMS.USERS.Get(userQual.CLIQUAL_UserID);
                    DALCMS.QUALIFICATION qual = Core.CMS.QUALIFICATION.Get(userQual.CLIQUAL_QualID);
                    DALCMS.PRINTSTATUS printStatus = Core.CMS.PRINTSTATUS.Get(userQual.CLIQUAL_PrintID);
                    DALCMS.BOOKING booking = Core.CMS.BOOKING.Get(userQual.CLIQUAL_BookingID);

                    uUserQual.CLIQUAL_UserQualID = userQual.CLIQUAL_UserQualID;
                    uUserQual.CLIQUAL_BookingID = userQual.CLIQUAL_BookingID;
                    uUserQual.USER_Initials = user.USER_Initials;
                    uUserQual.USER_Surname = user.USER_Surname;
                    uUserQual.QUAL_Name = qual.QUAL_Name;
                    uUserQual.QUAL_Description = qual.QUAL_Description;
                    uUserQual.CLIQUAL_StartDate = userQual.CLIQUAL_StartDate.ToShortDateString();
                    uUserQual.CLIQUAL_EndDate = userQual.CLIQUAL_EndDate.ToShortDateString();
                    uUserQual.PRINT_PrintID = userQual.CLIQUAL_PrintID;
                    uUserQual.PRINT_Status = printStatus.PRINT_Status;
                    uUserQual.CourseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;

                    result.Add(uUserQual);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the BOOKLINK table that match the param.
        /// Then foreach booklink record get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="bookingID"></param>
        /// <returns>
        /// Returns a list of Booking to be populated in the bookings table that match the param.
        /// </returns>
        public static List<BO.UserQual> GetUserQualByBookingID(int bookingID)
        {
            try
            {
                List<BO.UserQual> result = new List<BO.UserQual>();

                foreach (DALCMS.BOOKLINK bookLink in Core.CMS.BOOKLINK.Get().Where(bl => bl.BOOKLINK_BookingID == bookingID))
                {
                    DALCMS.BOOKING booking = Core.CMS.BOOKING.Get(bookingID);
                    DALCMS.USERS user = Core.CMS.USERS.Get(bookLink.BOOKLINK_UserID);
                    DALCMS.QUALIFICATION qual = Core.CMS.QUALIFICATION.Get(booking.BOOK_QualID);
                    DALCMS.CLIENTQUAL userQual = Core.CMS.CLIENTQUAL.Get().SingleOrDefault(uq => uq.CLIQUAL_UserID == bookLink.BOOKLINK_UserID && uq.CLIQUAL_BookingID == bookingID);

                    if (userQual != null)
                    {
                        BO.UserQual uUserQual = new BO.UserQual();
                        uUserQual.CLIQUAL_UserQualID = userQual.CLIQUAL_UserQualID;
                        uUserQual.CLIQUAL_BookingID = userQual.CLIQUAL_BookingID;
                        uUserQual.USER_Initials = user.USER_Initials;
                        uUserQual.USER_UserID = user.USER_UserID;
                        uUserQual.USER_Surname = user.USER_Surname;
                        uUserQual.QUAL_Name = qual.QUAL_Name;
                        uUserQual.QUAL_Description = qual.QUAL_Description;
                        uUserQual.CLIQUAL_StartDate = userQual.CLIQUAL_StartDate.ToShortDateString();
                        uUserQual.CLIQUAL_EndDate = userQual.CLIQUAL_EndDate.ToShortDateString();
                        uUserQual.PRINT_PrintID = userQual.CLIQUAL_PrintID;
                        uUserQual.PRINT_Status = userQual.PRINTSTATUS.PRINT_Status;
                        uUserQual.CourseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;
                        result.Add(uUserQual);
                    }
                    else
                    {
                        BO.UserQual uUserQual = new BO.UserQual();
                        //uUserQual.CLIQUAL_UserQualID = userQual.CLIQUAL_UserQualID;
                        uUserQual.CLIQUAL_BookingID = booking.BOOK_BookingID;
                        uUserQual.USER_Initials = user.USER_Initials;
                        uUserQual.USER_UserID = user.USER_UserID;
                        uUserQual.USER_Surname = user.USER_Surname;
                        uUserQual.QUAL_Name = qual.QUAL_Name;
                        uUserQual.QUAL_Description = qual.QUAL_Description;
                        uUserQual.CLIQUAL_StartDate = booking.BOOK_StartDate.ToShortDateString();
                        uUserQual.CLIQUAL_EndDate = booking.BOOK_EndDate.ToShortDateString();
                        uUserQual.PRINT_PrintID = (int)Core.CMS.BO.Enums.PrintStatus.PRINTED;
                        DALCMS.PRINTSTATUS status = Core.CMS.PRINTSTATUS.Get(uUserQual.PRINT_PrintID);
                        uUserQual.PRINT_Status = status.PRINT_Status;
                        uUserQual.CourseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;
                        result.Add(uUserQual);
                    }
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Save a list of records.
        /// </summary>
        /// <param name="userQuals"></param>
        /// <returns>
        /// Returns the saved data.
        /// </returns>
        public static List<BO.UserQual> SaveClientQual(List<BO.UserQual> userQuals)
        {
            try
            {
                // Create the TransactionScope to execute the commands, guaranteeing
                // that both commands can commit or roll back as a single unit of work.
                using (TransactionScope scope = new TransactionScope())
                {

                    List<BO.UserQual> result = new List<BO.UserQual>();

                    foreach (BO.UserQual userQual in userQuals)
                    {
                        DALCMS.CLIENTQUAL checkUserQual = Core.CMS.CLIENTQUAL.Get().SingleOrDefault(uq => uq.CLIQUAL_UserID == userQual.USER_UserID && uq.CLIQUAL_BookingID == userQual.CLIQUAL_BookingID);

                        if (checkUserQual == null)
                        {
                            DALCMS.QUALIFICATION qualification = Core.CMS.QUALIFICATION.Get().SingleOrDefault(q => q.QUAL_Name == userQual.QUAL_Name);

                            if (userQual.PRINT_PrintID == 0 || userQual.PRINT_PrintID == 2)
                            {
                                userQual.PRINT_PrintID = 3;
                            }

                            DALCMS.CLIENTQUAL uUserQual = new DALCMS.CLIENTQUAL();
                            uUserQual.CLIQUAL_BookingID = userQual.CLIQUAL_BookingID;
                            uUserQual.CLIQUAL_UserQualID = userQual.CLIQUAL_UserQualID;
                            uUserQual.CLIQUAL_UserID = userQual.USER_UserID;
                            uUserQual.CLIQUAL_QualID = qualification.QUAL_QualID;
                            uUserQual.CLIQUAL_StartDate = DateTime.Parse(userQual.CLIQUAL_StartDate);
                            uUserQual.CLIQUAL_EndDate = DateTime.Parse(userQual.CLIQUAL_EndDate);
                            uUserQual.CLIQUAL_PrintID = userQual.PRINT_PrintID;

                            DALCMS.CLIENTQUAL userQualSaved = Core.CMS.CLIENTQUAL.Save(uUserQual);

                            BO.UserQual userQualReturn = new BO.UserQual();
                            userQualReturn.CLIQUAL_UserQualID = userQualSaved.CLIQUAL_UserQualID;
                            userQualReturn.USER_UserID = userQualSaved.CLIQUAL_UserID;
                            userQualReturn.USER_Initials = userQual.USER_Initials;
                            userQualReturn.USER_Surname = userQual.USER_Surname;
                            userQualReturn.QUAL_Name = userQual.QUAL_Name;
                            userQualReturn.CLIQUAL_StartDate = userQualSaved.CLIQUAL_StartDate.ToString();
                            userQualReturn.CLIQUAL_EndDate = userQualSaved.CLIQUAL_EndDate.ToString();
                            userQualReturn.PRINT_PrintID = 2;
                            userQualReturn.CLIQUAL_BookingID = userQualSaved.CLIQUAL_BookingID;

                            result.Add(userQualReturn);
                        }
                        else
                        {
                            DALCMS.CLIENTQUAL uq = Core.CMS.CLIENTQUAL.Get().SingleOrDefault(i => i.CLIQUAL_UserID == userQual.USER_UserID && i.CLIQUAL_BookingID == userQual.CLIQUAL_BookingID);

                            if (userQual.PRINT_PrintID == 0 || userQual.PRINT_PrintID == 2)
                            {
                                userQual.PRINT_PrintID = 3;
                            }

                            DALCMS.CLIENTQUAL uUserQual = new DALCMS.CLIENTQUAL();

                            uUserQual.CLIQUAL_BookingID = uq.CLIQUAL_BookingID;
                            uUserQual.CLIQUAL_UserQualID = uq.CLIQUAL_UserQualID;
                            uUserQual.CLIQUAL_UserID = uq.CLIQUAL_UserID;
                            uUserQual.CLIQUAL_QualID = uq.CLIQUAL_QualID;
                            uUserQual.CLIQUAL_StartDate = (uq.CLIQUAL_StartDate);
                            uUserQual.CLIQUAL_EndDate = (uq.CLIQUAL_EndDate);
                            uUserQual.CLIQUAL_PrintID = userQual.PRINT_PrintID;

                            DALCMS.CLIENTQUAL userQualSaved = Core.CMS.CLIENTQUAL.Save(uUserQual);

                            BO.UserQual userQualReturn = new BO.UserQual();
                            userQualReturn.CLIQUAL_UserQualID = userQualSaved.CLIQUAL_UserQualID;
                            userQualReturn.USER_UserID = userQualSaved.CLIQUAL_UserID;
                            userQualReturn.USER_Initials = uq.USERS.USER_Initials;
                            userQualReturn.USER_Surname = uq.USERS.USER_Surname;
                            userQualReturn.QUAL_Name = uq.QUALIFICATION.QUAL_Name;
                            userQualReturn.CLIQUAL_StartDate = userQualSaved.CLIQUAL_StartDate.ToString();
                            userQualReturn.CLIQUAL_EndDate = userQualSaved.CLIQUAL_EndDate.ToString();
                            userQualReturn.PRINT_PrintID = 2;
                            userQualReturn.CLIQUAL_BookingID = userQualSaved.CLIQUAL_BookingID;

                            result.Add(userQualReturn);
                        }

                    }

                    // The Complete method commits the transaction. If an exception has been thrown, 
                    // Complete is not called and the transaction is rolled back.
                    scope.Complete();

                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a list of records.
        /// </summary>
        /// <param name="userQuals"></param>
        /// <returns></returns>
        public static List<BO.UserQual> SaveClientQualPrintList(List<BO.UserQual> userQuals)
        {
            try
            {
                 List<BO.UserQual> result = new List<BO.UserQual>();

                 foreach (BO.UserQual userQual in userQuals)
                 {

                     DALCMS.CLIENTQUAL uq = Core.CMS.CLIENTQUAL.Get().SingleOrDefault(i => i.CLIQUAL_UserID == userQual.USER_UserID && i.CLIQUAL_BookingID == userQual.CLIQUAL_BookingID);

                     if (userQual.PRINT_PrintID == 3)
                     {
                         userQual.PRINT_PrintID = 2;
                     }

                     DALCMS.CLIENTQUAL uUserQual = new DALCMS.CLIENTQUAL();

                     uUserQual.CLIQUAL_BookingID = uq.CLIQUAL_BookingID;
                     uUserQual.CLIQUAL_UserQualID = uq.CLIQUAL_UserQualID;
                     uUserQual.CLIQUAL_UserID = uq.CLIQUAL_UserID;
                     uUserQual.CLIQUAL_QualID = uq.CLIQUAL_QualID;
                     uUserQual.CLIQUAL_StartDate = (uq.CLIQUAL_StartDate);
                     uUserQual.CLIQUAL_EndDate = (uq.CLIQUAL_EndDate);
                     uUserQual.CLIQUAL_PrintID = userQual.PRINT_PrintID;

                     DALCMS.CLIENTQUAL userQualSaved = Core.CMS.CLIENTQUAL.Save(uUserQual);

                     BO.UserQual userQualReturn = new BO.UserQual();
                     userQualReturn.CLIQUAL_UserQualID = userQualSaved.CLIQUAL_UserQualID;
                     userQualReturn.USER_UserID = userQualSaved.CLIQUAL_UserID;
                     userQualReturn.USER_Initials = uq.USERS.USER_Initials;
                     userQualReturn.USER_Surname = uq.USERS.USER_Surname;
                     userQualReturn.QUAL_Name = uq.QUALIFICATION.QUAL_Name;
                     userQualReturn.CLIQUAL_StartDate = userQualSaved.CLIQUAL_StartDate.ToString();
                     userQualReturn.CLIQUAL_EndDate = userQualSaved.CLIQUAL_EndDate.ToString();
                     userQualReturn.PRINT_PrintID = userQualSaved.CLIQUAL_PrintID;
                     userQualReturn.CLIQUAL_BookingID = userQualSaved.CLIQUAL_BookingID;

                     result.Add(userQualReturn);
                 }

                 return result;

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
