﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.CMS.BO
{
    public class UserDetails
    {
        // Users table properties
        /// <summary>
        /// User ID
        /// </summary>
        public int USER_UserID { get; set; }
        /// <summary>
        /// TitleID
        /// </summary>
        public int USER_TitleID { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string USER_Name { get; set; }
        /// <summary>
        /// Surname
        /// </summary>
        public string USER_Surname { get; set; }
        /// <summary>
        /// Initials
        /// </summary>
        public string USER_Initials { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        public string USER_Password { get; set; }
        /// <summary>
        /// Type ID
        /// </summary>
        public int USER_TypeID { get; set; }
        /// <summary>
        /// Title ID
        /// </summary>
        public int? selectedTitle { get; set; }
        /// <summary>
        /// User Type ID
        /// </summary>
        public int selectedType { get; set; }

        // Titles table
        /// <summary>
        /// Title
        /// </summary>
        public string TITLE_Name { get; set; }

        // User Type table
        /// <summary>
        /// User type Name
        /// </summary>
        public string TYPE_Name { get; set; }

        //ContactDetails Table
        /// <summary>
        /// Contact details ID
        /// </summary>
        public int CONDET_CONTACTDETAILSID { get; set; }
        /// <summary>
        /// Street No
        /// </summary>
        public string CONDET_StreetNo { get; set; }
        /// <summary>
        /// Street Name
        /// </summary>
        public string CONDET_StreetName { get; set; }
        /// <summary>
        /// Suburb
        /// </summary>
        public string CONDET_Suburb { get; set; }
        /// <summary>
        /// City
        /// </summary>
        public string CONDET_City { get; set; }
        /// <summary>
        /// Postal Code
        /// </summary>
        public string CONDET_PostalCode { get; set; }
        /// <summary>
        /// Telelphone Number
        /// </summary>
        public string CONDET_TelNo { get; set; }
        /// <summary>
        /// Fax Number
        /// </summary>
        public string CONDET_FaxNo { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string CONDET_Email { get; set; }
        /// <summary>
        /// Cellphone Number
        /// </summary>
        public string CONDET_CellNo { get; set; }

        // Campus Link
        /// <summary>
        /// Campus ID
        /// </summary>
        public int selectedCboCampus { get; set; }
        /// <summary>
        /// Campud ID
        /// </summary>
        public int selectedCampus { get; set; }
        /// <summary>
        /// College ID
        /// </summary>
        public int selectedCboCollege { get; set; }
        /// <summary>
        /// College ID
        /// </summary>
        public int selectedCollege { get; set; }
        /// <summary>
        /// course Code
        /// </summary>
        public string CourseCode { get; set; }
        /// <summary>
        /// Destroy
        /// </summary>
        public bool _destroy { get; set; }

        /// <summary>
        /// College Name
        /// </summary>
        public string COL_Name { get; set; }
        /// <summary>
        /// Campus Name
        /// </summary>
        public string CAMP_Name { get; set; }
        /// <summary>
        /// Selected course code
        /// </summary>
        public string selectedCourse { get; set; }
    }
}