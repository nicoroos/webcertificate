﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.CMS.BO
{
    /// <summary>
    /// Class that contains the timesheet properties
    /// </summary>
    public class TimeSheet
    {
        /// <summary>
        /// Time Sheet ID
        /// </summary>
        public int TIME_TimeSheetID { get; set; }
        /// <summary>
        /// User ID
        /// </summary>
        public int TIME_UserID { get; set; }
        /// <summary>
        /// Time Sheet Date
        /// </summary>
        public string TIME_Date { get; set; }
        /// <summary>
        /// Star Time
        /// </summary>
        public string TIME_StartTime { get; set; }
        /// <summary>
        /// End Time
        /// </summary>
        public string TIME_EndTime { get; set; }
        /// <summary>
        /// Hours
        /// </summary>
        public string TIME_Hours { get; set; }
        /// <summary>
        /// Campus ID
        /// </summary>
        public int TIME_CampusID { get; set; }
        /// <summary>
        /// Nights
        /// </summary>
        public int? TIME_Nights { get; set; }
        /// <summary>
        /// Cost ID
        /// </summary>
        public int TIME_CostID { get; set; }
        /// <summary>
        /// ODO start
        /// </summary>
        public int TIME_ODOStart { get; set; }
        /// <summary>
        /// ODO End
        /// </summary>
        public int TIME_ODOEnd { get; set; }
        /// <summary>
        /// KM Traveled
        /// </summary>
        public string KMTraveled { get; set; }
        /// <summary>
        /// Rate Per KM
        /// </summary>
        public decimal selectedRate { get; set; }
        /// <summary>
        /// College ID
        /// </summary>
        public int selectedCollege { get; set; }
        /// <summary>
        /// Campus ID
        /// </summary>
        public int selectedCampus { get; set; }
        /// <summary>
        /// College Name
        /// </summary>
        public string COL_Name { get; set; }
        /// <summary>
        /// Campus Name
        /// </summary>
        public string CAMP_Name { get; set; }
        /// <summary>
        /// Rates name
        /// </summary>
        public string RATES_Name { get; set; }
        /// <summary>
        /// Rate per KM
        /// </summary>
        public decimal? RATES_RatePerKM { get; set; }
        /// <summary>
        /// Toll Cost
        /// </summary>
        public decimal? COST_Toll { get; set; }
        /// <summary>
        /// Flight Transport cost
        /// </summary>
        public decimal? COST_FlightTransport { get; set; }
        /// <summary>
        /// Flight Parking Cost
        /// </summary>
        public decimal? COST_FlightParking { get; set; }
        /// <summary>
        /// Other Cost
        /// </summary>
        public decimal? COST_Other { get; set; }
        /// <summary>
        /// Food Cost
        /// </summary>
        public decimal? COST_Food { get; set; }
    }
}
