﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.CMS.BO
{
    /// <summary>
    /// Class that contains all the userQual properties
    /// </summary>
    public class UserQual
    {
        /// <summary>
        /// User Qualification ID
        /// </summary>
        public int CLIQUAL_UserQualID { get; set; }
        /// <summary>
        /// User Initials
        /// </summary>
        public string USER_Initials { get; set; }
        /// <summary>
        /// User Surname
        /// </summary>
        public string USER_Surname { get; set; }
        /// <summary>
        /// User ID
        /// </summary>
        public int USER_UserID { get; set; }
        /// <summary>
        /// Qualification Name
        /// </summary>
        public string QUAL_Name { get; set; }
        /// <summary>
        /// Qualification Description
        /// </summary>
        public string QUAL_Description { get; set; }
        /// <summary>
        /// User Qualfication Start Date
        /// </summary>
        public string CLIQUAL_StartDate { get; set; }
        /// <summary>
        /// User Qualification End Date
        /// </summary>
        public string CLIQUAL_EndDate { get; set; }
        /// <summary>
        /// Print Status
        /// </summary>
        public string PRINT_Status { get; set; }
        /// <summary>
        /// Print ID
        /// </summary>
        public int PRINT_PrintID { get; set; }
        /// <summary>
        /// Booking ID
        /// </summary>
        public int CLIQUAL_BookingID { get; set; }
        /// <summary>
        /// Course Code
        /// </summary>
        public string CourseCode { get; set; }
        /// <summary>
        /// destroy property
        /// </summary>
        public bool _destroy { get; set; }

    }
}
