﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.CMS.BO.Enums
{
    public enum PrintStatus
    {
        PENDING = 1,
        PRINTED = 2,
        BUSY = 3
    }
}
