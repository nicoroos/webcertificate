﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.CMS.BO.Enums
{
    // _NB: Sit code comments in.
    public enum BookingStatus
    {
        DONE = 1,
        PENDING = 2,
        CANCELED = 3,
        CONFIRMED = 4,
        PRINTED = 5,
        SEND = 6
    }
}
