﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.CMS.BO
{
    /// <summary>
    /// This class containt the properties for the booking GetMethod
    /// </summary>
    public class Booking
    {
        /// <summary>
        /// Booking ID
        /// </summary>
        public int BOOK_BookingID { get; set; }
        /// <summary>
        /// College ID
        /// </summary>
        public int COL_CollegeID { get; set; }
        /// <summary>
        /// College Name
        /// </summary>
        public string COL_Name { get; set; }
        /// <summary>
        /// Campus ID
        /// </summary>
        public int CAMP_CampusID { get; set; }
        /// <summary>
        /// Campus Name
        /// </summary>
        public string CAMP_Name { get; set; }
        /// <summary>
        /// Qualificaiton ID
        /// </summary>
        public int QUAL_QualID { get; set; }
        /// <summary>
        /// Qaulification Name
        /// </summary>
        public string QUAL_Name { get; set; }
        /// <summary>
        /// user ID
        /// </summary>
        public int USER_UserID { get; set; }
        /// <summary>
        /// User Name
        /// </summary>
        public string USER_Name { get; set; }
        /// <summary>
        /// Booking Start date
        /// </summary>
        public string BOOK_StartDate { get; set; }
        /// <summary>
        /// Booking End Date
        /// </summary>
        public string BOOK_EndDate { get; set; }
        /// <summary>
        /// Unique CourseCode
        /// </summary>
        public string CourseCode { get; set; }
        /// <summary>
        /// Status Name
        /// </summary>
        public string STATUS_StatusName { get; set; }
        /// <summary>
        /// Status ID
        /// </summary>
        public int BOOK_StatusID { get; set; }
        /// <summary>
        /// Destroy
        /// </summary>
        public bool _destroy { get; set; }
        /// <summary>
        /// Coloring
        /// </summary>
        public string Coloring { get; set; }
        /// <summary>
        /// Status ID
        /// </summary>
        public int selectedStatus { get; set; }
        /// <summary>
        /// Selected Course Code
        /// </summary>
        public string selectedCourse { get; set; }
        /// <summary>
        /// Order Number
        /// </summary>
        public string orderNumber { get; set; }
        /// <summary>
        /// Invoive Number
        /// </summary>
        public string invoiceNumber { get; set; }
    }
}
