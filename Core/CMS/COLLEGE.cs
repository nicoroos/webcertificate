using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace Core.CMS
{
    /// <summary>
    /// This class extends the COLLEGE Business Layer methods.
    /// </summary>
    public partial class COLLEGE
    {
        /// <summary>
        /// This code validates the bussiness logik of the COLLEGE table.
        /// </summary>
        /// <param name="COLLEGE">COLLEGE Object.</param>
        /// <param name="blType">The action to validate.</param>
        /// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
        public static string BussinesLogik(DALCMS.COLLEGE COLLEGE, COMMON.Enums.BLType blType)
        {
            //NB: Please apply the COLLEGE Business validation Logic here

            StringBuilder sb = new StringBuilder();

            switch (blType)
            {
                case COMMON.Enums.BLType.Save:
                    break;
                case COMMON.Enums.BLType.Read:
                    break;
                case COMMON.Enums.BLType.Delete:
                    break;
                default:
                    break;
            }

            return sb.ToString();
        }

        // Please add extended methods here

        /// <summary>
        /// Get a list of records from the COLLEGE table through the DAL.
        /// </summary>
        /// <param name="collegeName"></param>
        /// <returns>
        /// Get a list of records from the COLLEGE table through the DAL.
        /// </returns>
        public static List<DALCMS.COLLEGE> GetCollegeByName(string collegeName)
        {
            try
            {
                List<DALCMS.COLLEGE> result = new List<DALCMS.COLLEGE>();

                foreach (DALCMS.COLLEGE college in Get().Where(c => c.COL_Name.Contains(collegeName)))
                {
                    result.Add(college);
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Save College
        /// </summary>
        /// <param name="colleges"></param>
        /// <returns>Saved Data</returns>
        public static DALCMS.COLLEGE SaveCollege(DALCMS.COLLEGE college)
        {
            try
            {
                return Core.CMS.COLLEGE.Save(college);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
