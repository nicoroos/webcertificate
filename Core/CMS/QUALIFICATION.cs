using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
    /// <summary>
    /// This class extends the QUALIFICATION Business Layer methods.
    /// </summary>
    public partial class QUALIFICATION
    {
        /// <summary>
        /// This code validates the bussiness logik of the QUALIFICATION table.
        /// </summary>
        /// <param name="QUALIFICATION">QUALIFICATION Object.</param>
        /// <param name="blType">The action to validate.</param>
        /// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
        public static string BussinesLogik(DALCMS.QUALIFICATION QUALIFICATION, COMMON.Enums.BLType blType)
        {
            //NB: Please apply the QUALIFICATION Business validation Logic here

            StringBuilder sb = new StringBuilder();

            switch (blType)
            {
                case COMMON.Enums.BLType.Save:
                    break;
                case COMMON.Enums.BLType.Read:
                    break;
                case COMMON.Enums.BLType.Delete:
                    break;
                default:
                    break;
            }

            return sb.ToString();
        }

        // Please add extended methods here

        /// <summary>
        /// Get a list of records from the QUALIFICATION table through DAL.
        /// </summary>
        /// <param name="qualName"></param>
        /// <returns>
        /// Return a list of records matching the param.
        /// </returns>
        public static List<DALCMS.QUALIFICATION> GetQualByName(string qualName)
        {
            try
            {
                List<DALCMS.QUALIFICATION> result = new List<DALCMS.QUALIFICATION>();

                foreach (DALCMS.QUALIFICATION qualifiaction in Get().Where(q => q.QUAL_Name.Contains(qualName)))
                {
                    result.Add(qualifiaction);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// save the qualification
        /// </summary>
        /// <param name="qualification"></param>
        /// <returns>
        /// Returns the saved data.
        /// </returns>
        public static DALCMS.QUALIFICATION SaveQualification(DALCMS.QUALIFICATION qualification)
        {
            try
            {
                return Core.CMS.QUALIFICATION.Save(qualification); 
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
