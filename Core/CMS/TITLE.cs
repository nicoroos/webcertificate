using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class extends the TITLE Business Layer methods.
/// </summary>
public partial class TITLE
{
/// <summary>
/// This code validates the bussiness logik of the TITLE table.
/// </summary>
/// <param name="TITLE">TITLE Object.</param>
/// <param name="blType">The action to validate.</param>
/// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
public static string BussinesLogik(DALCMS.TITLE TITLE, COMMON.Enums.BLType blType)
{
//NB: Please apply the TITLE Business validation Logic here

StringBuilder sb = new StringBuilder();

switch (blType)
{
case COMMON.Enums.BLType.Save:
break;
case COMMON.Enums.BLType.Read:
break;
case COMMON.Enums.BLType.Delete:
break;
default:
break;
}

return sb.ToString();
}

// Please add extended methods here
}
}
