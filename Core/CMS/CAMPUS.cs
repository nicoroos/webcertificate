using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
    /// <summary>
    /// This class extends the CAMPUS Business Layer methods.
    /// </summary>
    public partial class CAMPUS
    {
        /// <summary>
        /// This code validates the bussiness logik of the CAMPUS table.
        /// </summary>
        /// <param name="CAMPUS">CAMPUS Object.</param>
        /// <param name="blType">The action to validate.</param>
        /// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
        public static string BussinesLogik(DALCMS.CAMPUS CAMPUS, COMMON.Enums.BLType blType)
        {
            //NB: Please apply the CAMPUS Business validation Logic here

            StringBuilder sb = new StringBuilder();

            switch (blType)
            {
                case COMMON.Enums.BLType.Save:
                    break;
                case COMMON.Enums.BLType.Read:
                    break;
                case COMMON.Enums.BLType.Delete:
                    break;
                default:
                    break;
            }

            return sb.ToString();
        }

        // Please add extended methods here

        /// <summary>
        /// Get Campus By CollegeID and return list
        /// </summary>
        /// <param name="collegeID"></param>
        /// <returns>List of Campus</returns>
        public static List<DALCMS.CAMPUS> GetCampusByCollegeID(int collegeID)
        {
            try
            {
                return Core.CMS.CAMPUS.Get().Where(c => c.CAMP_CollegID == collegeID).ToList();
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Save Campus
        /// </summary>
        /// <param name="campus"></param>
        /// <returns>campus</returns>
        public static DALCMS.CAMPUS SaveCampus(DALCMS.CAMPUS campus)
        {
            try
            {
                return Core.CMS.CAMPUS.Save(campus);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

    }
}
