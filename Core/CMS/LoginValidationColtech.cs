﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.CMS
{
    /// <summary>
    /// This class contains the methods to validate the login credentials.
    /// </summary>
    public static class LoginValidationColtech
    {
        /// <summary>
        /// Validates the user login credetials
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>
        /// Returns a bool value to indicate if login is valid.
        /// </returns>
        public static bool Validation(string username, string password)
        {
            BO.UserDetails loginObject = UserDetials.GetUserByEmail(username);

            if (password == loginObject.USER_Password && password != string.Empty && (loginObject.USER_TypeID == 1 || loginObject.USER_TypeID == 2 || loginObject.USER_TypeID == 3))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
