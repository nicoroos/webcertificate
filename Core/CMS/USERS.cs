using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
    /// <summary>
    /// This class extends the USERS Business Layer methods.
    /// </summary>
    public partial class USERS
    {
        /// <summary>
        /// This code validates the bussiness logik of the USERS table.
        /// </summary>
        /// <param name="USERS">USERS Object.</param>
        /// <param name="blType">The action to validate.</param>
        /// <returns>Empty string if validations has passed, else a string describing the failed validations.</returns>
        public static string BussinesLogik(DALCMS.USERS USERS, COMMON.Enums.BLType blType)
        {
            //NB: Please apply the USERS Business validation Logic here

            StringBuilder sb = new StringBuilder();

            switch (blType)
            {
                case COMMON.Enums.BLType.Save:
                    break;
                case COMMON.Enums.BLType.Read:
                    break;
                case COMMON.Enums.BLType.Delete:
                    break;
                default:
                    break;
            }

            return sb.ToString();
        }

        // Please add extended methods here
        /// <summary>
        /// Get a list of records from the USER table matching the param.
        /// </summary>
        /// <param name="campusID"></param>
        /// <returns>
        /// Returns a list of records from DAL matching the param
        /// </returns>
        public static List<DALCMS.USERS> GetUserByCampusID(int campusID)
        {
            try
            {
                List<DALCMS.USERS> result = new List<DALCMS.USERS>();

                foreach (DALCMS.CAMPUSLINK campusLink in Core.CMS.CAMPUSLINK.Get().Where(c => c.CAMPLINK_CampusID == campusID))
                {
                    DALCMS.USERS user = USERS.Get().Where(u => u.USER_UserID == campusLink.CAMPLINK_UserID).SingleOrDefault();
                    result.Add(user);
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USER table matching the param
        /// </summary>
        /// <param name="collegeID">College ID</param>
        /// <returns>
        /// returns a users list object .
        /// </returns>
        public static List<DALCMS.USERS> GetUserByCollegeID(int collegeID)
        {
            try
            {
                List<DALCMS.USERS> result = new List<DALCMS.USERS>();

                foreach (DALCMS.CAMPUS campus in Core.CMS.CAMPUS.GetCampusByCollegeID(collegeID))
                {
                    foreach (DALCMS.CAMPUSLINK campusLink in Core.CMS.CAMPUSLINK.Get().Where(c => c.CAMPLINK_CampusID == campus.CAMP_CampusID))
                    {
                        DALCMS.USERS user = USERS.Get().Where(u => u.USER_UserID == campusLink.CAMPLINK_UserID).SingleOrDefault();
                        result.Add(user);
                    }
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USER table matching the param. 
        /// </summary>
        /// <param name="surname"></param>
        /// <returns>
        /// Returns a list of records matching the param.
        /// </returns>
        public static List<DALCMS.USERS> GetUserBySurname(string surname)
        {
            try
            {
                List<DALCMS.USERS> result = new List<DALCMS.USERS>();

                foreach (DALCMS.USERS user in Get().Where(u => u.USER_Surname.Contains(surname)))
                {
                    result.Add(user);
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get a list of all records from the USER table.
        /// </summary>
        /// <param name="campusID"></param>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns data from the USER table mathcing both the params.
        /// </returns>
        public static List<DALCMS.USERS> GetUserByCampusIDAndSurname(int campusID, string surname)
        {
            try
            {
                List<DALCMS.USERS> result = new List<DALCMS.USERS>();

                foreach (DALCMS.USERS user in Get().Where(u => u.USER_Surname.Contains(surname)))
                {
                    DALCMS.CAMPUSLINK campLink = Core.CMS.CAMPUSLINK.Get().SingleOrDefault(cl => cl.CAMPLINK_CampusID == campusID && cl.CAMPLINK_UserID == user.USER_UserID);

                    if (campLink != null)
                    {
                        DALCMS.USERS u = Get(campLink.CAMPLINK_UserID);

                        result.Add(u);
                    }
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
