using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the CLIENTQUAL Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.CLIENTQUAL"/>
public partial class CLIENTQUAL : DALCMS.CLIENTQUAL
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the CLIENTQUAL object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.CLIENTQUAL.Save"/> or <see cref="Core.CMS.CLIENTQUAL.Delete"/> methods.
/// </summary>
/// <param name="CLIQUAL_UserQualID">The CLIENTQUAL auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the CLIENTQUAL Object.</returns>
/// <seealso cref="Core.CMS.CLIENTQUAL.Save"/>
/// <seealso cref="Core.CMS.CLIENTQUAL.Delete"/>
public new static DALCMS.CLIENTQUAL Get(int CLIQUAL_UserQualID)
{
try
{
DALCMS.CLIENTQUAL c = DALCMS.CLIENTQUAL.Get(CLIQUAL_UserQualID);

if (c == null)
{
return new DALCMS.CLIENTQUAL();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of CLIENTQUAL objects.</returns>
/// <remarks>Returns all the records from Table CLIENTQUAL</remarks>
public new static List<DALCMS.CLIENTQUAL> Get()
{
try
{
return DALCMS.CLIENTQUAL.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an CLIENTQUAL entry in the databse.
/// To obtain an instance of the CLIENTQUAL object use the <see cref="Core.CMS.CLIENTQUAL.Get(int)"/> method.
/// </summary>
/// <param name="CLIENTQUAL">A Single instance of the CLIENTQUAL object.</param>
/// <returns>A Single instance of the CLIENTQUAL object.</returns>
/// <seealso cref="Core.CMS.CLIENTQUAL.Get(int)"/>
public new static DALCMS.CLIENTQUAL Save(DALCMS.CLIENTQUAL CLIENTQUAL)
{
try
{
string bl = CMS.CLIENTQUAL.BussinesLogik(CLIENTQUAL, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.CLIENTQUAL.Save(CLIENTQUAL);
}
else
{
throw new Exception("There was a problem saving the CLIENTQUAL Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the CLIENTQUAL object use the <see cref="Core.CMS.CLIENTQUAL.Get(int)"/> method.
/// </summary>
/// <param name="CLIENTQUAL">A Single instance of the CLIENTQUAL object.</param>
/// <seealso cref="Core.CMS.CLIENTQUAL.Get(int)"/>
public new static void Delete(DALCMS.CLIENTQUAL CLIENTQUAL)
{
try
{
string bl = CMS.CLIENTQUAL.BussinesLogik(CLIENTQUAL, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.CLIENTQUAL.Delete(CLIENTQUAL);
}
else
{
throw new Exception("There was a problem deleting the CLIENTQUAL Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
