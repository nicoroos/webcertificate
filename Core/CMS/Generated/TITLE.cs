using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the TITLE Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.TITLE"/>
public partial class TITLE : DALCMS.TITLE
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the TITLE object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.TITLE.Save"/> or <see cref="Core.CMS.TITLE.Delete"/> methods.
/// </summary>
/// <param name="TITLE_TitleID">The TITLE auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the TITLE Object.</returns>
/// <seealso cref="Core.CMS.TITLE.Save"/>
/// <seealso cref="Core.CMS.TITLE.Delete"/>
public new static DALCMS.TITLE Get(int TITLE_TitleID)
{
try
{
DALCMS.TITLE c = DALCMS.TITLE.Get(TITLE_TitleID);

if (c == null)
{
return new DALCMS.TITLE();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of TITLE objects.</returns>
/// <remarks>Returns all the records from Table TITLE</remarks>
public new static List<DALCMS.TITLE> Get()
{
try
{
return DALCMS.TITLE.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an TITLE entry in the databse.
/// To obtain an instance of the TITLE object use the <see cref="Core.CMS.TITLE.Get(int)"/> method.
/// </summary>
/// <param name="TITLE">A Single instance of the TITLE object.</param>
/// <returns>A Single instance of the TITLE object.</returns>
/// <seealso cref="Core.CMS.TITLE.Get(int)"/>
public new static DALCMS.TITLE Save(DALCMS.TITLE TITLE)
{
try
{
string bl = CMS.TITLE.BussinesLogik(TITLE, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.TITLE.Save(TITLE);
}
else
{
throw new Exception("There was a problem saving the TITLE Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the TITLE object use the <see cref="Core.CMS.TITLE.Get(int)"/> method.
/// </summary>
/// <param name="TITLE">A Single instance of the TITLE object.</param>
/// <seealso cref="Core.CMS.TITLE.Get(int)"/>
public new static void Delete(DALCMS.TITLE TITLE)
{
try
{
string bl = CMS.TITLE.BussinesLogik(TITLE, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.TITLE.Delete(TITLE);
}
else
{
throw new Exception("There was a problem deleting the TITLE Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
