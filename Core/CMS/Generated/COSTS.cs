using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the COSTS Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.COSTS"/>
public partial class COSTS : DALCMS.COSTS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the COSTS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.COSTS.Save"/> or <see cref="Core.CMS.COSTS.Delete"/> methods.
/// </summary>
/// <param name="COST_CostID">The COSTS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the COSTS Object.</returns>
/// <seealso cref="Core.CMS.COSTS.Save"/>
/// <seealso cref="Core.CMS.COSTS.Delete"/>
public new static DALCMS.COSTS Get(int COST_CostID)
{
try
{
DALCMS.COSTS c = DALCMS.COSTS.Get(COST_CostID);

if (c == null)
{
return new DALCMS.COSTS();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of COSTS objects.</returns>
/// <remarks>Returns all the records from Table COSTS</remarks>
public new static List<DALCMS.COSTS> Get()
{
try
{
return DALCMS.COSTS.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an COSTS entry in the databse.
/// To obtain an instance of the COSTS object use the <see cref="Core.CMS.COSTS.Get(int)"/> method.
/// </summary>
/// <param name="COSTS">A Single instance of the COSTS object.</param>
/// <returns>A Single instance of the COSTS object.</returns>
/// <seealso cref="Core.CMS.COSTS.Get(int)"/>
public new static DALCMS.COSTS Save(DALCMS.COSTS COSTS)
{
try
{
string bl = CMS.COSTS.BussinesLogik(COSTS, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.COSTS.Save(COSTS);
}
else
{
throw new Exception("There was a problem saving the COSTS Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the COSTS object use the <see cref="Core.CMS.COSTS.Get(int)"/> method.
/// </summary>
/// <param name="COSTS">A Single instance of the COSTS object.</param>
/// <seealso cref="Core.CMS.COSTS.Get(int)"/>
public new static void Delete(DALCMS.COSTS COSTS)
{
try
{
string bl = CMS.COSTS.BussinesLogik(COSTS, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.COSTS.Delete(COSTS);
}
else
{
throw new Exception("There was a problem deleting the COSTS Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
