using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the CONTACTDETAILS Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.CONTACTDETAILS"/>
public partial class CONTACTDETAILS : DALCMS.CONTACTDETAILS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the CONTACTDETAILS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.CONTACTDETAILS.Save"/> or <see cref="Core.CMS.CONTACTDETAILS.Delete"/> methods.
/// </summary>
/// <param name="CONDET_CONTACTDETAILSID">The CONTACTDETAILS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the CONTACTDETAILS Object.</returns>
/// <seealso cref="Core.CMS.CONTACTDETAILS.Save"/>
/// <seealso cref="Core.CMS.CONTACTDETAILS.Delete"/>
public new static DALCMS.CONTACTDETAILS Get(int CONDET_CONTACTDETAILSID)
{
try
{
DALCMS.CONTACTDETAILS c = DALCMS.CONTACTDETAILS.Get(CONDET_CONTACTDETAILSID);

if (c == null)
{
return new DALCMS.CONTACTDETAILS();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of CONTACTDETAILS objects.</returns>
/// <remarks>Returns all the records from Table CONTACTDETAILS</remarks>
public new static List<DALCMS.CONTACTDETAILS> Get()
{
try
{
return DALCMS.CONTACTDETAILS.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an CONTACTDETAILS entry in the databse.
/// To obtain an instance of the CONTACTDETAILS object use the <see cref="Core.CMS.CONTACTDETAILS.Get(int)"/> method.
/// </summary>
/// <param name="CONTACTDETAILS">A Single instance of the CONTACTDETAILS object.</param>
/// <returns>A Single instance of the CONTACTDETAILS object.</returns>
/// <seealso cref="Core.CMS.CONTACTDETAILS.Get(int)"/>
public new static DALCMS.CONTACTDETAILS Save(DALCMS.CONTACTDETAILS CONTACTDETAILS)
{
try
{
string bl = CMS.CONTACTDETAILS.BussinesLogik(CONTACTDETAILS, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.CONTACTDETAILS.Save(CONTACTDETAILS);
}
else
{
throw new Exception("There was a problem saving the CONTACTDETAILS Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the CONTACTDETAILS object use the <see cref="Core.CMS.CONTACTDETAILS.Get(int)"/> method.
/// </summary>
/// <param name="CONTACTDETAILS">A Single instance of the CONTACTDETAILS object.</param>
/// <seealso cref="Core.CMS.CONTACTDETAILS.Get(int)"/>
public new static void Delete(DALCMS.CONTACTDETAILS CONTACTDETAILS)
{
try
{
string bl = CMS.CONTACTDETAILS.BussinesLogik(CONTACTDETAILS, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.CONTACTDETAILS.Delete(CONTACTDETAILS);
}
else
{
throw new Exception("There was a problem deleting the CONTACTDETAILS Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
