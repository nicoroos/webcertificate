using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the USERTYPE Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.USERTYPE"/>
public partial class USERTYPE : DALCMS.USERTYPE
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the USERTYPE object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.USERTYPE.Save"/> or <see cref="Core.CMS.USERTYPE.Delete"/> methods.
/// </summary>
/// <param name="TYPE_TypeID">The USERTYPE auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the USERTYPE Object.</returns>
/// <seealso cref="Core.CMS.USERTYPE.Save"/>
/// <seealso cref="Core.CMS.USERTYPE.Delete"/>
public new static DALCMS.USERTYPE Get(int TYPE_TypeID)
{
try
{
DALCMS.USERTYPE c = DALCMS.USERTYPE.Get(TYPE_TypeID);

if (c == null)
{
return new DALCMS.USERTYPE();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of USERTYPE objects.</returns>
/// <remarks>Returns all the records from Table USERTYPE</remarks>
public new static List<DALCMS.USERTYPE> Get()
{
try
{
return DALCMS.USERTYPE.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an USERTYPE entry in the databse.
/// To obtain an instance of the USERTYPE object use the <see cref="Core.CMS.USERTYPE.Get(int)"/> method.
/// </summary>
/// <param name="USERTYPE">A Single instance of the USERTYPE object.</param>
/// <returns>A Single instance of the USERTYPE object.</returns>
/// <seealso cref="Core.CMS.USERTYPE.Get(int)"/>
public new static DALCMS.USERTYPE Save(DALCMS.USERTYPE USERTYPE)
{
try
{
string bl = CMS.USERTYPE.BussinesLogik(USERTYPE, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.USERTYPE.Save(USERTYPE);
}
else
{
throw new Exception("There was a problem saving the USERTYPE Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the USERTYPE object use the <see cref="Core.CMS.USERTYPE.Get(int)"/> method.
/// </summary>
/// <param name="USERTYPE">A Single instance of the USERTYPE object.</param>
/// <seealso cref="Core.CMS.USERTYPE.Get(int)"/>
public new static void Delete(DALCMS.USERTYPE USERTYPE)
{
try
{
string bl = CMS.USERTYPE.BussinesLogik(USERTYPE, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.USERTYPE.Delete(USERTYPE);
}
else
{
throw new Exception("There was a problem deleting the USERTYPE Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
