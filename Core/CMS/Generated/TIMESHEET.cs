using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the TIMESHEET Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.TIMESHEET"/>
public partial class TIMESHEET : DALCMS.TIMESHEET
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the TIMESHEET object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.TIMESHEET.Save"/> or <see cref="Core.CMS.TIMESHEET.Delete"/> methods.
/// </summary>
/// <param name="TIME_TimeSheetID">The TIMESHEET auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the TIMESHEET Object.</returns>
/// <seealso cref="Core.CMS.TIMESHEET.Save"/>
/// <seealso cref="Core.CMS.TIMESHEET.Delete"/>
public new static DALCMS.TIMESHEET Get(int TIME_TimeSheetID)
{
try
{
DALCMS.TIMESHEET c = DALCMS.TIMESHEET.Get(TIME_TimeSheetID);

if (c == null)
{
return new DALCMS.TIMESHEET();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of TIMESHEET objects.</returns>
/// <remarks>Returns all the records from Table TIMESHEET</remarks>
public new static List<DALCMS.TIMESHEET> Get()
{
try
{
return DALCMS.TIMESHEET.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an TIMESHEET entry in the databse.
/// To obtain an instance of the TIMESHEET object use the <see cref="Core.CMS.TIMESHEET.Get(int)"/> method.
/// </summary>
/// <param name="TIMESHEET">A Single instance of the TIMESHEET object.</param>
/// <returns>A Single instance of the TIMESHEET object.</returns>
/// <seealso cref="Core.CMS.TIMESHEET.Get(int)"/>
public new static DALCMS.TIMESHEET Save(DALCMS.TIMESHEET TIMESHEET)
{
try
{
string bl = CMS.TIMESHEET.BussinesLogik(TIMESHEET, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.TIMESHEET.Save(TIMESHEET);
}
else
{
throw new Exception("There was a problem saving the TIMESHEET Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the TIMESHEET object use the <see cref="Core.CMS.TIMESHEET.Get(int)"/> method.
/// </summary>
/// <param name="TIMESHEET">A Single instance of the TIMESHEET object.</param>
/// <seealso cref="Core.CMS.TIMESHEET.Get(int)"/>
public new static void Delete(DALCMS.TIMESHEET TIMESHEET)
{
try
{
string bl = CMS.TIMESHEET.BussinesLogik(TIMESHEET, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.TIMESHEET.Delete(TIMESHEET);
}
else
{
throw new Exception("There was a problem deleting the TIMESHEET Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
