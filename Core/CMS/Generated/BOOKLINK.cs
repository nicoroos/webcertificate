using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the BOOKLINK Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.BOOKLINK"/>
public partial class BOOKLINK : DALCMS.BOOKLINK
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the BOOKLINK object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.BOOKLINK.Save"/> or <see cref="Core.CMS.BOOKLINK.Delete"/> methods.
/// </summary>
/// <param name="BOOKLINK_BooklinkID">The BOOKLINK auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the BOOKLINK Object.</returns>
/// <seealso cref="Core.CMS.BOOKLINK.Save"/>
/// <seealso cref="Core.CMS.BOOKLINK.Delete"/>
public new static DALCMS.BOOKLINK Get(int BOOKLINK_BooklinkID)
{
try
{
DALCMS.BOOKLINK c = DALCMS.BOOKLINK.Get(BOOKLINK_BooklinkID);

if (c == null)
{
return new DALCMS.BOOKLINK();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of BOOKLINK objects.</returns>
/// <remarks>Returns all the records from Table BOOKLINK</remarks>
public new static List<DALCMS.BOOKLINK> Get()
{
try
{
return DALCMS.BOOKLINK.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an BOOKLINK entry in the databse.
/// To obtain an instance of the BOOKLINK object use the <see cref="Core.CMS.BOOKLINK.Get(int)"/> method.
/// </summary>
/// <param name="BOOKLINK">A Single instance of the BOOKLINK object.</param>
/// <returns>A Single instance of the BOOKLINK object.</returns>
/// <seealso cref="Core.CMS.BOOKLINK.Get(int)"/>
public new static DALCMS.BOOKLINK Save(DALCMS.BOOKLINK BOOKLINK)
{
try
{
string bl = CMS.BOOKLINK.BussinesLogik(BOOKLINK, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.BOOKLINK.Save(BOOKLINK);
}
else
{
throw new Exception("There was a problem saving the BOOKLINK Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the BOOKLINK object use the <see cref="Core.CMS.BOOKLINK.Get(int)"/> method.
/// </summary>
/// <param name="BOOKLINK">A Single instance of the BOOKLINK object.</param>
/// <seealso cref="Core.CMS.BOOKLINK.Get(int)"/>
public new static void Delete(DALCMS.BOOKLINK BOOKLINK)
{
try
{
string bl = CMS.BOOKLINK.BussinesLogik(BOOKLINK, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.BOOKLINK.Delete(BOOKLINK);
}
else
{
throw new Exception("There was a problem deleting the BOOKLINK Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
