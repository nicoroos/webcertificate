using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the BOOKING Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.BOOKING"/>
public partial class BOOKING : DALCMS.BOOKING
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the BOOKING object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.BOOKING.Save"/> or <see cref="Core.CMS.BOOKING.Delete"/> methods.
/// </summary>
/// <param name="BOOK_BookingID">The BOOKING auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the BOOKING Object.</returns>
/// <seealso cref="Core.CMS.BOOKING.Save"/>
/// <seealso cref="Core.CMS.BOOKING.Delete"/>
public new static DALCMS.BOOKING Get(int BOOK_BookingID)
{
try
{
DALCMS.BOOKING c = DALCMS.BOOKING.Get(BOOK_BookingID);

if (c == null)
{
return new DALCMS.BOOKING();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of BOOKING objects.</returns>
/// <remarks>Returns all the records from Table BOOKING</remarks>
public new static List<DALCMS.BOOKING> Get()
{
try
{
return DALCMS.BOOKING.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an BOOKING entry in the databse.
/// To obtain an instance of the BOOKING object use the <see cref="Core.CMS.BOOKING.Get(int)"/> method.
/// </summary>
/// <param name="BOOKING">A Single instance of the BOOKING object.</param>
/// <returns>A Single instance of the BOOKING object.</returns>
/// <seealso cref="Core.CMS.BOOKING.Get(int)"/>
public new static DALCMS.BOOKING Save(DALCMS.BOOKING BOOKING)
{
try
{
string bl = CMS.BOOKING.BussinesLogik(BOOKING, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.BOOKING.Save(BOOKING);
}
else
{
throw new Exception("There was a problem saving the BOOKING Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the BOOKING object use the <see cref="Core.CMS.BOOKING.Get(int)"/> method.
/// </summary>
/// <param name="BOOKING">A Single instance of the BOOKING object.</param>
/// <seealso cref="Core.CMS.BOOKING.Get(int)"/>
public new static void Delete(DALCMS.BOOKING BOOKING)
{
try
{
string bl = CMS.BOOKING.BussinesLogik(BOOKING, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.BOOKING.Delete(BOOKING);
}
else
{
throw new Exception("There was a problem deleting the BOOKING Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
