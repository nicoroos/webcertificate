using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the PRINTSTATUS Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.PRINTSTATUS"/>
public partial class PRINTSTATUS : DALCMS.PRINTSTATUS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the PRINTSTATUS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.PRINTSTATUS.Save"/> or <see cref="Core.CMS.PRINTSTATUS.Delete"/> methods.
/// </summary>
/// <param name="PRINT_PrintID">The PRINTSTATUS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the PRINTSTATUS Object.</returns>
/// <seealso cref="Core.CMS.PRINTSTATUS.Save"/>
/// <seealso cref="Core.CMS.PRINTSTATUS.Delete"/>
public new static DALCMS.PRINTSTATUS Get(int PRINT_PrintID)
{
try
{
DALCMS.PRINTSTATUS c = DALCMS.PRINTSTATUS.Get(PRINT_PrintID);

if (c == null)
{
return new DALCMS.PRINTSTATUS();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of PRINTSTATUS objects.</returns>
/// <remarks>Returns all the records from Table PRINTSTATUS</remarks>
public new static List<DALCMS.PRINTSTATUS> Get()
{
try
{
return DALCMS.PRINTSTATUS.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an PRINTSTATUS entry in the databse.
/// To obtain an instance of the PRINTSTATUS object use the <see cref="Core.CMS.PRINTSTATUS.Get(int)"/> method.
/// </summary>
/// <param name="PRINTSTATUS">A Single instance of the PRINTSTATUS object.</param>
/// <returns>A Single instance of the PRINTSTATUS object.</returns>
/// <seealso cref="Core.CMS.PRINTSTATUS.Get(int)"/>
public new static DALCMS.PRINTSTATUS Save(DALCMS.PRINTSTATUS PRINTSTATUS)
{
try
{
string bl = CMS.PRINTSTATUS.BussinesLogik(PRINTSTATUS, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.PRINTSTATUS.Save(PRINTSTATUS);
}
else
{
throw new Exception("There was a problem saving the PRINTSTATUS Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the PRINTSTATUS object use the <see cref="Core.CMS.PRINTSTATUS.Get(int)"/> method.
/// </summary>
/// <param name="PRINTSTATUS">A Single instance of the PRINTSTATUS object.</param>
/// <seealso cref="Core.CMS.PRINTSTATUS.Get(int)"/>
public new static void Delete(DALCMS.PRINTSTATUS PRINTSTATUS)
{
try
{
string bl = CMS.PRINTSTATUS.BussinesLogik(PRINTSTATUS, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.PRINTSTATUS.Delete(PRINTSTATUS);
}
else
{
throw new Exception("There was a problem deleting the PRINTSTATUS Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
