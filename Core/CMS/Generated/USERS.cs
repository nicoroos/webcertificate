using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the USERS Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.USERS"/>
public partial class USERS : DALCMS.USERS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the USERS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.USERS.Save"/> or <see cref="Core.CMS.USERS.Delete"/> methods.
/// </summary>
/// <param name="USER_UserID">The USERS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the USERS Object.</returns>
/// <seealso cref="Core.CMS.USERS.Save"/>
/// <seealso cref="Core.CMS.USERS.Delete"/>
public new static DALCMS.USERS Get(int USER_UserID)
{
try
{
DALCMS.USERS c = DALCMS.USERS.Get(USER_UserID);

if (c == null)
{
return new DALCMS.USERS();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of USERS objects.</returns>
/// <remarks>Returns all the records from Table USERS</remarks>
public new static List<DALCMS.USERS> Get()
{
try
{
return DALCMS.USERS.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an USERS entry in the databse.
/// To obtain an instance of the USERS object use the <see cref="Core.CMS.USERS.Get(int)"/> method.
/// </summary>
/// <param name="USERS">A Single instance of the USERS object.</param>
/// <returns>A Single instance of the USERS object.</returns>
/// <seealso cref="Core.CMS.USERS.Get(int)"/>
public new static DALCMS.USERS Save(DALCMS.USERS USERS)
{
try
{
string bl = CMS.USERS.BussinesLogik(USERS, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.USERS.Save(USERS);
}
else
{
throw new Exception("There was a problem saving the USERS Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the USERS object use the <see cref="Core.CMS.USERS.Get(int)"/> method.
/// </summary>
/// <param name="USERS">A Single instance of the USERS object.</param>
/// <seealso cref="Core.CMS.USERS.Get(int)"/>
public new static void Delete(DALCMS.USERS USERS)
{
try
{
string bl = CMS.USERS.BussinesLogik(USERS, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.USERS.Delete(USERS);
}
else
{
throw new Exception("There was a problem deleting the USERS Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
