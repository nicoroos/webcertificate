using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the CAMPUSLINK Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.CAMPUSLINK"/>
public partial class CAMPUSLINK : DALCMS.CAMPUSLINK
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the CAMPUSLINK object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.CAMPUSLINK.Save"/> or <see cref="Core.CMS.CAMPUSLINK.Delete"/> methods.
/// </summary>
/// <param name="CAMPLINK_CampLinkID">The CAMPUSLINK auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the CAMPUSLINK Object.</returns>
/// <seealso cref="Core.CMS.CAMPUSLINK.Save"/>
/// <seealso cref="Core.CMS.CAMPUSLINK.Delete"/>
public new static DALCMS.CAMPUSLINK Get(int CAMPLINK_CampLinkID)
{
try
{
DALCMS.CAMPUSLINK c = DALCMS.CAMPUSLINK.Get(CAMPLINK_CampLinkID);

if (c == null)
{
return new DALCMS.CAMPUSLINK();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of CAMPUSLINK objects.</returns>
/// <remarks>Returns all the records from Table CAMPUSLINK</remarks>
public new static List<DALCMS.CAMPUSLINK> Get()
{
try
{
return DALCMS.CAMPUSLINK.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an CAMPUSLINK entry in the databse.
/// To obtain an instance of the CAMPUSLINK object use the <see cref="Core.CMS.CAMPUSLINK.Get(int)"/> method.
/// </summary>
/// <param name="CAMPUSLINK">A Single instance of the CAMPUSLINK object.</param>
/// <returns>A Single instance of the CAMPUSLINK object.</returns>
/// <seealso cref="Core.CMS.CAMPUSLINK.Get(int)"/>
public new static DALCMS.CAMPUSLINK Save(DALCMS.CAMPUSLINK CAMPUSLINK)
{
try
{
string bl = CMS.CAMPUSLINK.BussinesLogik(CAMPUSLINK, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.CAMPUSLINK.Save(CAMPUSLINK);
}
else
{
throw new Exception("There was a problem saving the CAMPUSLINK Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the CAMPUSLINK object use the <see cref="Core.CMS.CAMPUSLINK.Get(int)"/> method.
/// </summary>
/// <param name="CAMPUSLINK">A Single instance of the CAMPUSLINK object.</param>
/// <seealso cref="Core.CMS.CAMPUSLINK.Get(int)"/>
public new static void Delete(DALCMS.CAMPUSLINK CAMPUSLINK)
{
try
{
string bl = CMS.CAMPUSLINK.BussinesLogik(CAMPUSLINK, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.CAMPUSLINK.Delete(CAMPUSLINK);
}
else
{
throw new Exception("There was a problem deleting the CAMPUSLINK Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
