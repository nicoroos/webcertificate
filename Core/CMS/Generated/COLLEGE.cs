using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the COLLEGE Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.COLLEGE"/>
public partial class COLLEGE : DALCMS.COLLEGE
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the COLLEGE object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.COLLEGE.Save"/> or <see cref="Core.CMS.COLLEGE.Delete"/> methods.
/// </summary>
/// <param name="COL_CollegeID">The COLLEGE auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the COLLEGE Object.</returns>
/// <seealso cref="Core.CMS.COLLEGE.Save"/>
/// <seealso cref="Core.CMS.COLLEGE.Delete"/>
public new static DALCMS.COLLEGE Get(int COL_CollegeID)
{
try
{
DALCMS.COLLEGE c = DALCMS.COLLEGE.Get(COL_CollegeID);

if (c == null)
{
return new DALCMS.COLLEGE();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of COLLEGE objects.</returns>
/// <remarks>Returns all the records from Table COLLEGE</remarks>
public new static List<DALCMS.COLLEGE> Get()
{
try
{
return DALCMS.COLLEGE.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an COLLEGE entry in the databse.
/// To obtain an instance of the COLLEGE object use the <see cref="Core.CMS.COLLEGE.Get(int)"/> method.
/// </summary>
/// <param name="COLLEGE">A Single instance of the COLLEGE object.</param>
/// <returns>A Single instance of the COLLEGE object.</returns>
/// <seealso cref="Core.CMS.COLLEGE.Get(int)"/>
public new static DALCMS.COLLEGE Save(DALCMS.COLLEGE COLLEGE)
{
try
{
string bl = CMS.COLLEGE.BussinesLogik(COLLEGE, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.COLLEGE.Save(COLLEGE);
}
else
{
throw new Exception("There was a problem saving the COLLEGE Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the COLLEGE object use the <see cref="Core.CMS.COLLEGE.Get(int)"/> method.
/// </summary>
/// <param name="COLLEGE">A Single instance of the COLLEGE object.</param>
/// <seealso cref="Core.CMS.COLLEGE.Get(int)"/>
public new static void Delete(DALCMS.COLLEGE COLLEGE)
{
try
{
string bl = CMS.COLLEGE.BussinesLogik(COLLEGE, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.COLLEGE.Delete(COLLEGE);
}
else
{
throw new Exception("There was a problem deleting the COLLEGE Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
