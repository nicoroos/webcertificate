using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the QUALIFICATION Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.QUALIFICATION"/>
public partial class QUALIFICATION : DALCMS.QUALIFICATION
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the QUALIFICATION object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.QUALIFICATION.Save"/> or <see cref="Core.CMS.QUALIFICATION.Delete"/> methods.
/// </summary>
/// <param name="QUAL_QualID">The QUALIFICATION auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the QUALIFICATION Object.</returns>
/// <seealso cref="Core.CMS.QUALIFICATION.Save"/>
/// <seealso cref="Core.CMS.QUALIFICATION.Delete"/>
public new static DALCMS.QUALIFICATION Get(int QUAL_QualID)
{
try
{
DALCMS.QUALIFICATION c = DALCMS.QUALIFICATION.Get(QUAL_QualID);

if (c == null)
{
return new DALCMS.QUALIFICATION();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of QUALIFICATION objects.</returns>
/// <remarks>Returns all the records from Table QUALIFICATION</remarks>
public new static List<DALCMS.QUALIFICATION> Get()
{
try
{
return DALCMS.QUALIFICATION.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an QUALIFICATION entry in the databse.
/// To obtain an instance of the QUALIFICATION object use the <see cref="Core.CMS.QUALIFICATION.Get(int)"/> method.
/// </summary>
/// <param name="QUALIFICATION">A Single instance of the QUALIFICATION object.</param>
/// <returns>A Single instance of the QUALIFICATION object.</returns>
/// <seealso cref="Core.CMS.QUALIFICATION.Get(int)"/>
public new static DALCMS.QUALIFICATION Save(DALCMS.QUALIFICATION QUALIFICATION)
{
try
{
string bl = CMS.QUALIFICATION.BussinesLogik(QUALIFICATION, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.QUALIFICATION.Save(QUALIFICATION);
}
else
{
throw new Exception("There was a problem saving the QUALIFICATION Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the QUALIFICATION object use the <see cref="Core.CMS.QUALIFICATION.Get(int)"/> method.
/// </summary>
/// <param name="QUALIFICATION">A Single instance of the QUALIFICATION object.</param>
/// <seealso cref="Core.CMS.QUALIFICATION.Get(int)"/>
public new static void Delete(DALCMS.QUALIFICATION QUALIFICATION)
{
try
{
string bl = CMS.QUALIFICATION.BussinesLogik(QUALIFICATION, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.QUALIFICATION.Delete(QUALIFICATION);
}
else
{
throw new Exception("There was a problem deleting the QUALIFICATION Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion



}
}
