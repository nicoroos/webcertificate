using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
    /// <summary>
    /// This class exposes the CAMPUS Business Layer methods.
    /// </summary>
    /// <seealso cref="DALCMS.CAMPUS"/>
    public partial class CAMPUS : DALCMS.CAMPUS
    {
        #region Generated
        // This code was generated using the CodeGen

        /// <summary>
        /// Use this method to return a single instance of the CAMPUS object for the specified parameter value.
        /// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.CAMPUS.Save"/> or <see cref="Core.CMS.CAMPUS.Delete"/> methods.
        /// </summary>
        /// <param name="CAMP_CampusID">The CAMPUS auto no value for which to retrieve the databse entry.</param>
        /// <returns>A Single instance of the CAMPUS Object.</returns>
        /// <seealso cref="Core.CMS.CAMPUS.Save"/>
        /// <seealso cref="Core.CMS.CAMPUS.Delete"/>
        public new static DALCMS.CAMPUS Get(int CAMP_CampusID)
        {
            try
            {
                DALCMS.CAMPUS c = DALCMS.CAMPUS.Get(CAMP_CampusID);

                if (c == null)
                {
                    return new DALCMS.CAMPUS();
                }
                else
                {
                    return c;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Returns a List of all the records.
        /// </summary>
        /// <returns>A Generic List of CAMPUS objects.</returns>
        /// <remarks>Returns all the records from Table CAMPUS</remarks>
        public new static List<DALCMS.CAMPUS> Get()
        {
            try
            {
                return DALCMS.CAMPUS.Get().ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Use this method to Add or Update an CAMPUS entry in the databse.
        /// To obtain an instance of the CAMPUS object use the <see cref="Core.CMS.CAMPUS.GetCampusByCollegeID(int)"/> method.
        /// </summary>
        /// <param name="CAMPUS">A Single instance of the CAMPUS object.</param>
        /// <returns>A Single instance of the CAMPUS object.</returns>
        /// <seealso cref="Core.CMS.CAMPUS.GetCampusByCollegeID(int)"/>
        public new static DALCMS.CAMPUS Save(DALCMS.CAMPUS CAMPUS)
        {
            try
            {
                string bl = CMS.CAMPUS.BussinesLogik(CAMPUS, COMMON.Enums.BLType.Save);

                if (String.IsNullOrWhiteSpace(bl))
                {
                    return DALCMS.CAMPUS.Save(CAMPUS);
                }
                else
                {
                    throw new Exception("There was a problem saving the CAMPUS Record.", new Exception(bl));
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Use this method to Delete an entry from the database.
        /// To obtain an instance of the CAMPUS object use the <see cref="Core.CMS.CAMPUS.GetCampusByCollegeID(int)"/> method.
        /// </summary>
        /// <param name="CAMPUS">A Single instance of the CAMPUS object.</param>
        /// <seealso cref="Core.CMS.CAMPUS.GetCampusByCollegeID(int)"/>
        public new static void Delete(DALCMS.CAMPUS CAMPUS)
        {
            try
            {
                string bl = CMS.CAMPUS.BussinesLogik(CAMPUS, COMMON.Enums.BLType.Delete);

                if (String.IsNullOrWhiteSpace(bl))
                {
                    DALCMS.CAMPUS.Delete(CAMPUS);
                }
                else
                {
                    throw new Exception("There was a problem deleting the CAMPUS Record.", new Exception(bl));
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

    }
}
