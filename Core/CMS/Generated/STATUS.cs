using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the STATUS Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.STATUS"/>
public partial class STATUS : DALCMS.STATUS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the STATUS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.STATUS.Save"/> or <see cref="Core.CMS.STATUS.Delete"/> methods.
/// </summary>
/// <param name="STATUS_StatusID">The STATUS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the STATUS Object.</returns>
/// <seealso cref="Core.CMS.STATUS.Save"/>
/// <seealso cref="Core.CMS.STATUS.Delete"/>
public new static DALCMS.STATUS Get(int STATUS_StatusID)
{
try
{
DALCMS.STATUS c = DALCMS.STATUS.Get(STATUS_StatusID);

if (c == null)
{
return new DALCMS.STATUS();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of STATUS objects.</returns>
/// <remarks>Returns all the records from Table STATUS</remarks>
public new static List<DALCMS.STATUS> Get()
{
try
{
return DALCMS.STATUS.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an STATUS entry in the databse.
/// To obtain an instance of the STATUS object use the <see cref="Core.CMS.STATUS.Get(int)"/> method.
/// </summary>
/// <param name="STATUS">A Single instance of the STATUS object.</param>
/// <returns>A Single instance of the STATUS object.</returns>
/// <seealso cref="Core.CMS.STATUS.Get(int)"/>
public new static DALCMS.STATUS Save(DALCMS.STATUS STATUS)
{
try
{
string bl = CMS.STATUS.BussinesLogik(STATUS, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.STATUS.Save(STATUS);
}
else
{
throw new Exception("There was a problem saving the STATUS Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the STATUS object use the <see cref="Core.CMS.STATUS.Get(int)"/> method.
/// </summary>
/// <param name="STATUS">A Single instance of the STATUS object.</param>
/// <seealso cref="Core.CMS.STATUS.Get(int)"/>
public new static void Delete(DALCMS.STATUS STATUS)
{
try
{
string bl = CMS.STATUS.BussinesLogik(STATUS, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.STATUS.Delete(STATUS);
}
else
{
throw new Exception("There was a problem deleting the STATUS Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
