using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.CMS
{
/// <summary>
/// This class exposes the RatePerKM Business Layer methods.
/// </summary>
/// <seealso cref="DALCMS.RatePerKM"/>
public partial class RatePerKM : DALCMS.RatePerKM
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Use this method to return a single instance of the RatePerKM object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="Core.CMS.RatePerKM.Save"/> or <see cref="Core.CMS.RatePerKM.Delete"/> methods.
/// </summary>
/// <param name="RATES_RatePerKMID">The RatePerKM auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the RatePerKM Object.</returns>
/// <seealso cref="Core.CMS.RatePerKM.Save"/>
/// <seealso cref="Core.CMS.RatePerKM.Delete"/>
public new static DALCMS.RatePerKM Get(int RATES_RatePerKMID)
{
try
{
DALCMS.RatePerKM c = DALCMS.RatePerKM.Get(RATES_RatePerKMID);

if (c == null)
{
return new DALCMS.RatePerKM();
}
else
{
return c;
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Returns a List of all the records.
/// </summary>
/// <returns>A Generic List of RatePerKM objects.</returns>
/// <remarks>Returns all the records from Table RatePerKM</remarks>
public new static List<DALCMS.RatePerKM> Get()
{
try
{
return DALCMS.RatePerKM.Get().ToList();
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an RatePerKM entry in the databse.
/// To obtain an instance of the RatePerKM object use the <see cref="Core.CMS.RatePerKM.Get(int)"/> method.
/// </summary>
/// <param name="RatePerKM">A Single instance of the RatePerKM object.</param>
/// <returns>A Single instance of the RatePerKM object.</returns>
/// <seealso cref="Core.CMS.RatePerKM.Get(int)"/>
public new static DALCMS.RatePerKM Save(DALCMS.RatePerKM RatePerKM)
{
try
{
string bl = CMS.RatePerKM.BussinesLogik(RatePerKM, COMMON.Enums.BLType.Save);

if (String.IsNullOrWhiteSpace(bl))
{
return DALCMS.RatePerKM.Save(RatePerKM);
}
else
{
throw new Exception("There was a problem saving the RatePerKM Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the RatePerKM object use the <see cref="Core.CMS.RatePerKM.Get(int)"/> method.
/// </summary>
/// <param name="RatePerKM">A Single instance of the RatePerKM object.</param>
/// <seealso cref="Core.CMS.RatePerKM.Get(int)"/>
public new static void Delete(DALCMS.RatePerKM RatePerKM)
{
try
{
string bl = CMS.RatePerKM.BussinesLogik(RatePerKM, COMMON.Enums.BLType.Delete);

if (String.IsNullOrWhiteSpace(bl))
{
DALCMS.RatePerKM.Delete(RatePerKM);
}
else
{
throw new Exception("There was a problem deleting the RatePerKM Record.", new Exception(bl));
}
}
catch (Exception)
{
throw;
}
}

#endregion

}
}
