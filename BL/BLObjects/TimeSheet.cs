﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class TimeSheet
    {
        public int TimeSheetID { get; set; }
        public int UserID { get; set; }
        public string Date { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Hours { get; set; }
        public int CampusID { get; set; }
        public int? Nights { get; set; }
        public int CostID { get; set; }
        public int ODOStart { get; set; }
        public int ODOEnd { get; set; }
        public string KMTraveled { get; set; }

        public decimal selectedRate { get; set; }
        public int selectedCollege { get; set; }
        public int selectedCampus { get; set; }

        public string College { get; set; }
        public string Campus { get; set; }

        public string RatesName { get; set; }
        public decimal? RatesPerKM { get; set; }
        public decimal? Toll { get; set; }
        public decimal? FlightTransport { get; set; }
        public decimal? FlightParking { get; set; }
        public decimal? Other { get; set; }
        public decimal? Food { get; set; }
    }
}
