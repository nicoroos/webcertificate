﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class PrintStatus
    {
        public int PrintID { get; set; }
        public string Status { get; set; }
    }
}
