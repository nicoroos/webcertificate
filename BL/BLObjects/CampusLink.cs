﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class CampusLink
    {
        public int CampLinkID { get; set; }
        public int UserID { get; set; }
        public int CampusID { get; set; }
    }
}
