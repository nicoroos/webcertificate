﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class Campus
    {
        public int CampusID { get; set; }
        public string CampusName { get; set; }
        public string CampusDescription { get; set; }
        public int CollegeID { get; set; }
        public bool _destroy { get; set; }
    }
}
