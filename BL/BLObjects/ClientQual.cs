﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class ClientQual
    {
        public int UserQualID { get; set; }
        public int UserID { get; set; }
        public string Qual { get; set; }
        public string QualDescrip { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string PrintStatus { get; set; }
        public int PrintID { get; set; }
        public string ReferNum { get; set; }
        public bool _destroy { get; set; }

        public string CInitials { get; set; }
        public string CSurname { get; set; }

        public int BookingID { get; set; }
    }
}
