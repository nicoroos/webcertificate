﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class RatePerKM
    {
        public int RatePerKMID { get; set; }
        public string RateName { get; set; }
        public decimal Rate { get; set; }
    }
}
