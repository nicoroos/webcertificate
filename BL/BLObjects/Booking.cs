﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class Booking
    {
        public int BookingID { get; set; }
        public string College { get; set; }
        public string Campus { get; set; }
        public string Qualification { get; set; }
        public string Trainer { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string CourseCode { get; set; }
        public string StatusName { get; set; }
        public int StatusID { get; set; }
        public bool _destroy { get; set; }
        public string Coloring { get; set; }
    }
}
