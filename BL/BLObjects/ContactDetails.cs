﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class ContactDetails
    {
        public int ContactDetailsID { get; set; }
        public int ClientID { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Street3 { get; set; }
        public string Street4 { get; set; }
        public string PostalCode { get; set; }
        public string TelNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string CellNo { get; set; }
    }
}
