﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class Title
    {
        public int? TitleID { get; set; }
        public string TitleName { get; set; }
    }
}
