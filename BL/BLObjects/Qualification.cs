﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class Qualification
    {
        public int QualID { get; set; }
        public string QualName { get; set; }
        public string QualDescrip { get; set; }
        public bool _destroy { get; set; }
    }
}
