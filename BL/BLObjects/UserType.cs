﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class UserType
    {
        public int TypeID { get; set; }
        public string TypeName { get; set; }
    }
}
