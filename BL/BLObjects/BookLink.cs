﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class BookLink
    {
        public int BookLinkID { get; set; }
        public int UserID { get; set; }
        public int BookingID { get; set; }
    }
}
