﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class College
    {
        public int CollegeID { get; set; }
        public string CollegeName { get; set; }
        public string CollegeDescription { get; set; }
        public bool _destroy { get; set; }
    }
}
