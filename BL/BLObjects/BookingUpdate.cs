﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class BookingUpdate
    {
        public int BookingID { get; set; }
        public int selectedCampus { get; set; }
        public int selectedQualification { get; set; }
        public int selectedTrainer { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int selectedStatus { get; set; }
        public bool _destroy { get; set; }

        public string Campus { get; set; }
        public string Qualification { get; set; }
        public string Trainer { get; set; }
        public string CourseCode { get; set; }
        public string StatusName { get; set; }
    }
}
