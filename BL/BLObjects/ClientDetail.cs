﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class ClientDetail
    {
        //Client Table
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Initials { get; set; }
        public string Title { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }


        //ContactDetails Table
        public int ContactDetailsID { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Street3 { get; set; }
        public string Street4 { get; set; }
        public string PostalCode { get; set; }
        public string TelNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string CellNo { get; set; }

        public string College { get; set; }
        public string Campus { get; set; }
        public bool _destroy { get; set; }

        public int selectedCboCollege { get; set; }
        public int selectedCboCampus { get; set; }
        public int selectedTitle { get; set; }
        public int selectedType { get; set; }
    }
}
