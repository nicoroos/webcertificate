﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class BookingSave
    {
        public int BookingID { get; set; }
        public int selectedCampus { get; set; }
        public int selectedQualification { get; set; }
        public int selectedTrainer { get; set; }
        public DateTime SStartDate { get; set; }
        public DateTime SEndDate { get; set; }
        public int selectedStatus { get; set; }
        public bool _destroy { get; set; }
    }
}
