﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.BLObjects
{
    public class ClientDetails
    {
        //Client Table
        public int UserID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Initials { get; set; }
        public int? selectedTitle { get; set; }
        public string Password { get; set; }
        public int TypeID { get; set; }
        public int selectedType { get; set; }
        

        //ContactDetails Table
        public int ContactDetailsID { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Street3 { get; set; }
        public string Street4 { get; set; }
        public string PostalCode { get; set; }
        public string TelNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string CellNo { get; set; }

        // Campus Link
        public int selectedCboCampus { get; set; }
        public int selectedCampus { get; set; }
        public bool _dstroy { get; set; }

        // Booking link
        public string ReferNum { get; set; }
    }
}
