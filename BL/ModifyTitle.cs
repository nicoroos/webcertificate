﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the Title table of the DB.
    /// </summary>
    public static class ModifyTitle
    {
        /// <summary>
        /// Get a specific record from the TITLE table matching the param.
        /// </summary>
        /// <param name="titleID"></param>
        /// <returns>
        /// return the Title name and titleID according to param
        /// </returns>
        public static BLObjects.Title GetTitleByID(int titleID)
        {
            try
            {
                var title = DAL.Entities.TITLE.GetTitleByID(titleID);
                BLObjects.Title t = new BLObjects.Title();
                t.TitleID = title.TITLE_TitleID;
                t.TitleName = title.TITLE_Name.ToUpper();

                return t;
            }
            catch (NullReferenceException)
            {
                BLObjects.Title t = new BLObjects.Title();
                t.TitleID = 0;
                t.TitleName = "";

                return t;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the titles from the TITLE table through the DAL.
        /// </summary>
        /// <returns>
        /// Returns a list of all the Titles in the Title Table.
        /// </returns>
        public static List<BLObjects.Title> GetAll()
        {
            try
            {
                List<BLObjects.Title> result = new List<BLObjects.Title>();

                foreach (DAL.Entities.TITLE ti in DAL.Entities.TITLE.GetAll())
                {
                    BLObjects.Title title = new BLObjects.Title();

                    title.TitleID = ti.TITLE_TitleID;
                    title.TitleName = ti.TITLE_Name.ToUpper();

                    result.Add(title);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
