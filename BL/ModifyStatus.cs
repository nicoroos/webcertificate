﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the STATUS table of the DB.
    /// </summary>
    public static class ModifyStatus
    {
        /// <summary>
        /// Get a specific record from the STATUS table through the DAL.
        /// </summary>
        /// <param name="statusID"></param>
        /// <returns>
        /// Returns a record from the DAL matching param
        /// </returns>
        public static BLObjects.Status GetID(int statusID)
        {
            try
            {
                var title = DAL.Entities.STATUS.GetStatusByID(statusID);
                BLObjects.Status s = new BLObjects.Status();
                s.StatusID = title.STATUS_StatusID;
                s.StatusName = title.STATUS_StatusName;

                return s;
            }
            catch (NullReferenceException)
            {
                BLObjects.Status t = new BLObjects.Status();
                t.StatusID = 0;
                t.StatusName = "";

                return t;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of all the records records in the Status table.
        /// </summary>
        /// <returns>
        /// Returns all the records from the Status table.
        /// </returns>
        public static List<BLObjects.Status> GetAll()
        {
            try
            {
                List<BLObjects.Status> result = new List<BLObjects.Status>();

                foreach (DAL.Entities.STATUS st in DAL.Entities.STATUS.GetAll())
                {
                    BLObjects.Status status = new BLObjects.Status();

                    status.StatusID = st.STATUS_StatusID;
                    status.StatusName = st.STATUS_StatusName;

                    result.Add(status);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
