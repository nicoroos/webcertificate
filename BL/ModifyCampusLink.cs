﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the CAMPUSLINK table of the DB.
    /// </summary>
    public static class ModifyCampusLink
    {
        /// <summary>
        /// Get all the records from CAMPUSLINK table through the DAL.
        /// </summary>
        /// <returns>
        /// Returns a list of all the records from the CAMPUSLINk Table.
        /// </returns>
        public static List<BLObjects.CampusLink> GetAll()
        {
            try
            {
                List<BLObjects.CampusLink> result = new List<BLObjects.CampusLink>();

                foreach (DAL.Entities.CAMPUSLINK cl in DAL.Entities.CAMPUSLINK.GetAll())
                {
                    BLObjects.CampusLink campuslink = new BLObjects.CampusLink();

                    campuslink.CampLinkID = cl.CAMPLINK_CampLinkID;
                    campuslink.UserID = cl.CAMPLINK_UserID;
                    campuslink.CampusID = cl.CAMPLINK_CampusID;

                    result.Add(campuslink);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
