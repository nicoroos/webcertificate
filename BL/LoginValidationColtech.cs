﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BL
{
    public static class LoginValidationColtech
    {
        /// <summary>
        /// validates the login from the DB
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>
        /// Returns a boolean value
        /// </returns>
        public static bool Validation(string username, string password)
        {
            var loginObject = BL.ModifyUserDetails.GetUserByEmail(username);

            if (password == loginObject.Password.Trim() && password != "" && (loginObject.TypeID == 1 || loginObject.TypeID == 2 || loginObject.TypeID == 3))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
