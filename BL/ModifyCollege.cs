﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the College table of the DB.
    /// </summary>
    public static class ModifyCollege
    {
        /// <summary>
        /// Get a specific record from COLLEGE table through the DAL.
        /// </summary>
        /// <param name="collegeID"></param>
        /// <returns>
        /// Returns a record matching the param.
        /// </returns>
        public static BLObjects.College GetCollegeByID(int collegeID)
        {
            try
            {
                var college = DAL.Entities.COLLEGE.GetCollegeByID(collegeID);

                BLObjects.College col = new BLObjects.College();
                col.CollegeID = college.COL_CollegeID;
                col.CollegeName = college.COL_Name;
                col.CollegeDescription = college.COL_Description;

                return col;
            }
            catch (NullReferenceException)
            {
                BLObjects.College col = new BLObjects.College();
                col.CollegeID = 0;
                col.CollegeName = "";
                col.CollegeDescription = "";

                return col;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from the CAMPUSLINK table, then use that record to get the CollegeID from CAMPUS table and then the Get a specific record from COLLEGE table through the DAL.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns the record matching the param from the COLLEGE table.
        /// </returns>
        public static BLObjects.College GetCollegeByUserID(int userID)
        {
            try
            {
                var campuslink = DAL.Entities.CAMPUSLINK.GetCampusLinkByUserID(userID);
                var campus = DAL.Entities.CAMPUS.GetCampusByID(campuslink.CAMPLINK_CampusID);
                var college = DAL.Entities.COLLEGE.GetCollegeByID(campus.CAMP_CollegID);

                BLObjects.College c = new BLObjects.College();

                c.CollegeID = college.COL_CollegeID;
                c.CollegeName = college.COL_Name;
                c.CollegeDescription = college.COL_Description;

                return c;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the the record from the COLLLEGE table through the DAL.
        /// </summary>
        /// <returns>
        /// Returns a list of all the dat in the COLLEGE table.
        /// </returns>
        public static List<BLObjects.College> GetAll()
        {
            try
            {
                List<BLObjects.College> result = new List<BLObjects.College>();

                foreach (DAL.Entities.COLLEGE col in DAL.Entities.COLLEGE.GetAll())
                {
                    BLObjects.College college = new BLObjects.College();

                    college.CollegeID = col.COL_CollegeID;
                    college.CollegeName = col.COL_Name;
                    college.CollegeDescription = col.COL_Description;

                    result.Add(college);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a new records and update existing records in the COLLEGE table through the DAL.
        /// </summary>
        /// <param name="colleges"></param>
        /// <returns>
        /// Returns a list of saved records.
        /// </returns>
        public static List<BLObjects.College> SaveCollegeList(List<BLObjects.College> colleges)
        {
            try
            {
                // Create the TransactionScope to execute the commands, guaranteeing
                // that both commands can commit or roll back as a single unit of work.
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (BLObjects.College college in colleges)
                    {
                        //var checkExistance = DAL.Entities.COLLEGE.GetCollegeByName(college.CollegeName);

                        if (college.CollegeName != "")// && checkExistance == null)
                        {
                            DAL.Entities.COLLEGE cCollege = new DAL.Entities.COLLEGE();

                            cCollege.COL_CollegeID = college.CollegeID;
                            cCollege.COL_Name = college.CollegeName.ToUpper();
                            cCollege.COL_Description = college.CollegeDescription.ToUpper();

                            if (college._destroy == true)
                            {
                                // first deletes all the linked campuses 
                                foreach (BLObjects.Campus campus in ModifyCampus.GetCampusByCollegeID(cCollege.COL_CollegeID))
                                {
                                    DAL.Entities.CAMPUS c = new DAL.Entities.CAMPUS();
                                    c.CAMP_CollegID = campus.CollegeID;
                                    c.CAMP_CampusID = campus.CampusID;
                                    c.CAMP_Name = campus.CampusName;
                                    c.CAMP_Description = campus.CampusDescription;

                                    DAL.Entities.CAMPUS.Delete(c);
                                }
                                // Delete the college
                                DAL.Entities.COLLEGE.Delete(cCollege);
                            }
                            else
                            {
                                // Calls the the save method in the DAL to save the Data to the Database
                                DAL.Entities.COLLEGE.SaveCollege(cCollege);
                            }
                        }
                    }

                    // The Complete method commits the transaction. If an exception has been thrown, 
                    // Complete is not called and the transaction is rolled back.
                    scope.Complete();

                    return colleges;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the COLLEGE table through the DAL.
        /// </summary>
        /// <param name="collegeName"></param>
        /// <returns>
        /// Returns a list of records matching the param.
        /// </returns>
        public static List<BLObjects.College> GetCollegeByName(string collegeName)
        {
            try
            {
                List<BLObjects.College> result = new List<BLObjects.College>();

                foreach (var col in GetAll().Where(c => c.CollegeName.Contains(collegeName)))
                {
                    BLObjects.College college = new BLObjects.College();

                    college.CollegeID = col.CollegeID;
                    college.CollegeName = col.CollegeName.ToUpper();
                    college.CollegeDescription = col.CollegeDescription.ToUpper();

                    result.Add(college);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}