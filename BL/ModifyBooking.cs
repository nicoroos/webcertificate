﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the BOOKING table of the DB.
    /// </summary>
    public class ModifyBooking
    {
        /// <summary>
        /// Get a record from the BOOKING table. Then get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="bookingID"></param>
        /// <returns>
        /// Returns a record from the DB matching the param value.
        /// </returns>
        public static BLObjects.Booking GetBookingByBookingID(int bookingID)
        {
            try
            {
                // Get the details from the DAL(entity framework).
                // Booking details
                DAL.Entities.BOOKING booking = DAL.Entities.BOOKING.GetBookingByID(bookingID);
                // Campus details 
                DAL.Entities.CAMPUS campus = DAL.Entities.CAMPUS.GetCampusByID(booking.BOOK_CampusID);
                // College details
                DAL.Entities.COLLEGE college = DAL.Entities.COLLEGE.GetCollegeByID(campus.CAMP_CollegID);
                // Qualification details 
                DAL.Entities.QUALIFICATION qual = DAL.Entities.QUALIFICATION.GetQualByQualID(booking.BOOK_QualID);
                // User details
                var trainer = DAL.Entities.USERS.GetUserByID(booking.BOOK_UserID);
                // Status details
                var status = DAL.Entities.STATUS.GetStatusByID(booking.BOOK_StatusID);
                // declare a course code string variable
                string courseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;

                // Instantiate a new variable for the data collection.
                BLObjects.Booking b = new BLObjects.Booking();
                b.BookingID = booking.BOOK_BookingID;
                b.College = college.COL_Name;
                b.Campus = campus.CAMP_Name;
                b.Qualification = qual.QUAL_Name;
                b.Trainer = trainer.USER_Name + " " + trainer.USER_Surname;
                b.StartDate = booking.BOOK_StartDate.ToShortDateString();
                b.EndDate = booking.BOOK_EndDate.ToShortDateString();
                b.CourseCode = courseCode;
                b.StatusName = status.STATUS_StatusName;

                return b;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the CAMPUS table that match the collegeID param, then get a list of records from the BOOKING table that match the campusID.
        /// Then foreach booking record get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="collegeID"></param>
        /// <returns>
        /// Returns a list of Booking to be populated in the bookings table that match the param.
        /// </returns>
        public static List<BLObjects.Booking> GetBookingByCollegeID(int collegeID)
        {
            try
            {
                List<BLObjects.Booking> result = new List<BLObjects.Booking>();

                foreach (DAL.Entities.CAMPUS campusid in DAL.Entities.CAMPUS.GetCampusByCollegeID(collegeID))
                {
                    foreach (DAL.Entities.BOOKING booking in DAL.Entities.BOOKING.GetBookingListByCampusID(campusid.CAMP_CampusID))
                    {
                        var campus = ModifyCampus.GetCampusByCampusID(booking.BOOK_CampusID);
                        var college = ModifyCollege.GetCollegeByID(campus.CollegeID);
                        var qualification = ModifyQualification.GetQualByQualID(booking.BOOK_QualID);
                        var trainer = ModifyUser.GetID(booking.BOOK_UserID);
                        var status = DAL.Entities.STATUS.GetStatusByID(booking.BOOK_StatusID);
                        // declare a course code string variable
                        string courseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;

                        BLObjects.Booking b = new BLObjects.Booking();
                        b.BookingID = booking.BOOK_BookingID;
                        b.College = college.CollegeName;
                        b.Campus = campus.CampusName;
                        b.Qualification = qualification.QualName;
                        b.Trainer = trainer.Name + " " + trainer.Surname;
                        b.StartDate = booking.BOOK_StartDate.ToShortDateString();
                        b.EndDate = booking.BOOK_EndDate.ToShortDateString();
                        b.CourseCode = courseCode;
                        b.StatusName = status.STATUS_StatusName;

                        if (status.STATUS_StatusName == "DONE")
                            b.Coloring = "success";
                        else if (status.STATUS_StatusName == "CONFIRMED")
                            b.Coloring = "warning";
                        else if (status.STATUS_StatusName == "CANCELED")
                            b.Coloring = "danger";
                        else if (status.STATUS_StatusName == "PENDING")
                            b.Coloring = "";

                        result.Add(b);
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the booking table matching the param value.
        /// Then foreach booking record get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="campusID"></param>
        /// <returns>
        /// Returns a list of bookings to be populated in the bookings table matching the param
        /// </returns>
        public static List<BLObjects.Booking> GetBookingByCampusID(int campusID)
        {
            try
            {
                List<BLObjects.Booking> result = new List<BLObjects.Booking>();

                foreach (DAL.Entities.BOOKING booking in DAL.Entities.BOOKING.GetBookingListByCampusID(campusID))
                {
                    var campus = ModifyCampus.GetCampusByCampusID(booking.BOOK_CampusID);
                    var college = ModifyCollege.GetCollegeByID(campus.CollegeID);
                    var qualification = ModifyQualification.GetQualByQualID(booking.BOOK_QualID);
                    var trainer = ModifyUser.GetID(booking.BOOK_UserID);
                    var status = DAL.Entities.STATUS.GetStatusByID(booking.BOOK_StatusID);
                    // declare a course code string variable
                    string CourseCode = CourseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;

                    BLObjects.Booking b = new BLObjects.Booking();
                    b.BookingID = booking.BOOK_BookingID;
                    b.College = college.CollegeName;
                    b.Campus = campus.CampusName;
                    b.Qualification = qualification.QualName;
                    b.Trainer = trainer.Name + " " + trainer.Surname;
                    b.StartDate = booking.BOOK_StartDate.ToShortDateString();
                    b.EndDate = booking.BOOK_EndDate.ToShortDateString();
                    b.CourseCode = CourseCode;
                    b.StatusName = status.STATUS_StatusName;

                    if (status.STATUS_StatusName == "DONE")
                        b.Coloring = "success";
                    else if (status.STATUS_StatusName == "CONFIRMED")
                        b.Coloring = "warning";
                    else if (status.STATUS_StatusName == "CANCELED")
                        b.Coloring = "danger";
                    else if (status.STATUS_StatusName == "PENDING")
                        b.Coloring = "";

                    result.Add(b);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the booklink table matching the param value.
        /// Then foreach booklink record get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of bookings to be populated in the bookings table matching the param
        /// </returns>
        public static List<BLObjects.Booking> GeBookingBytUserID(int userID)
        {
            try
            {
                List<BLObjects.Booking> result = new List<BLObjects.Booking>();

                // Get all the booking for the UserID
                foreach (DAL.Entities.BOOKLINK booklink in DAL.Entities.BOOKLINK.GetUserIDList(userID))
                {
                    // Returns data for...
                    // booking matching bookingid
                    var booking = DAL.Entities.BOOKING.GetBookingByID(booklink.BOOKLINK_BookingID);
                    // campus matching campusID
                    var campus = DAL.Entities.CAMPUS.GetCampusByID(booking.BOOK_CampusID);
                    // College matching collegID
                    var college = DAL.Entities.COLLEGE.GetCollegeByID(campus.CAMP_CollegID);
                    // qualification matching qualid
                    var qual = DAL.Entities.QUALIFICATION.GetQualByQualID(booking.BOOK_QualID);
                    // trainer matching userid
                    var trainer = DAL.Entities.USERS.GetUserByID(booking.BOOK_UserID);
                    // Matching the status id
                    var status = DAL.Entities.STATUS.GetStatusByID(booking.BOOK_StatusID);
                    // declare a course code string variable
                    string courseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;

                    BLObjects.Booking b = new BLObjects.Booking();
                    b.BookingID = booking.BOOK_BookingID;
                    b.College = college.COL_Name;
                    b.Campus = campus.CAMP_Name;
                    b.Qualification = qual.QUAL_Name;
                    b.Trainer = trainer.USER_Name + " " + trainer.USER_Surname;
                    b.StartDate = booking.BOOK_StartDate.ToShortDateString();
                    b.EndDate = booking.BOOK_EndDate.ToShortDateString();
                    b.StatusName = status.STATUS_StatusName;
                    b.CourseCode = courseCode;

                    if (status.STATUS_StatusName == "DONE")
                        b.Coloring = "success";
                    else if (status.STATUS_StatusName == "CONFIRMED")
                        b.Coloring = "warning";
                    else if (status.STATUS_StatusName == "CANCELED")
                        b.Coloring = "danger";
                    else if (status.STATUS_StatusName == "PENDING")
                        b.Coloring = "active";
                    
                    result.Add(b);
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of all records from the booklink table.
        /// Then foreach Booking record get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <returns>
        /// Returns a list of of the bookings
        /// </returns>
        public static List<BLObjects.Booking> GetAll()
        {
            try
            {
                List<BLObjects.Booking> result = new List<BLObjects.Booking>();

                foreach (DAL.Entities.BOOKING booking in DAL.Entities.BOOKING.GetAllBookings())
                {
                    var campus = ModifyCampus.GetCampusByCampusID(booking.BOOK_CampusID);
                    var college = ModifyCollege.GetCollegeByID(campus.CollegeID);
                    var qualification = ModifyQualification.GetQualByQualID(booking.BOOK_QualID);
                    var trainer = ModifyUser.GetID(booking.BOOK_UserID);
                    var status = DAL.Entities.STATUS.GetStatusByID(booking.BOOK_StatusID);

                    // instantiate booking class and assign value to the properties
                    BLObjects.Booking b = new BLObjects.Booking();
                    b.BookingID = booking.BOOK_BookingID;
                    b.College = college.CollegeName;
                    b.Campus = campus.CampusName;
                    b.Qualification = qualification.QualName;
                    b.Trainer = trainer.Name + " " + trainer.Surname;
                    b.StartDate = booking.BOOK_StartDate.ToShortDateString();
                    b.EndDate = booking.BOOK_EndDate.ToShortDateString();
                    b.CourseCode = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;
                    b.StatusName = status.STATUS_StatusName;
                    b.StatusID = booking.BOOK_StatusID;

                    if (status.STATUS_StatusName == "DONE")
                        b.Coloring = "success";
                    else if (status.STATUS_StatusName == "CONFIRMED")
                        b.Coloring = "warning";
                    else if (status.STATUS_StatusName == "CANCELED")
                        b.Coloring = "danger";
                    else if (status.STATUS_StatusName == "PENDING")
                        b.Coloring = "";

                    result.Add(b);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// This function genrates a unique course code
        /// </summary>
        /// <returns>
        /// Returns die unique code in a string fromat
        /// </returns>
        public static string GenerateUniqueCode()
        {
            // This code generate the randomCourseCode.
            Random random = new Random();
            int randomValue = random.Next(0001, 9999);
            // Convert current date to string
            string dates = DateTime.Now.ToString();
            // Concatinate the Year month and 4 digit code into one string value
            string courseCode = dates.Substring(2, 2) + dates.Substring(5, 2) + randomValue.ToString().Substring(0, 4);
            // This is the return value;
            return courseCode;
        }

        /// <summary>
        /// Within the transaction scope is a function to save the new booking data.
        /// Data is instantiated and then asigned to the DAL properties.
        /// </summary>
        /// <param name="booking"></param>
        /// <returns>
        /// Return the saved data
        /// </returns>
        public static BLObjects.BookingSave SaveBooking(BLObjects.BookingSave booking)
        {
            try
            {
                // Create the TransactionScope to execute the commands, guaranteeing
                // that both commands can commit or roll back as a single unit of work.
                using (TransactionScope scope = new TransactionScope())
                {
                    // Save the booking details with the random Courese code
                    DAL.Entities.BOOKING bBooking = new DAL.Entities.BOOKING();
                    bBooking.BOOK_BookingID = booking.BookingID;
                    bBooking.BOOK_CampusID = booking.selectedCampus;
                    bBooking.BOOK_QualID = booking.selectedQualification;
                    bBooking.BOOK_UserID = booking.selectedTrainer;
                    bBooking.BOOK_StartDate = booking.SStartDate;
                    bBooking.BOOK_EndDate = booking.SEndDate;
                    bBooking.BOOK_Year = null;
                    bBooking.BOOK_Month = null;
                    bBooking.BOOK_RandomCode = null;
                    // booking status is hardcoded, the status is pending(2)
                    bBooking.BOOK_StatusID = 2;

                    DAL.Entities.BOOKING.SaveBooking(bBooking);

                    // The Complete method commits the transaction. If an exception has been thrown, 
                    // Complete is not called and the transaction is rolled back.
                    scope.Complete();

                    return booking;
                }
            }
            catch (TransactionAbortedException)
            {
                throw;
            }
            catch (ApplicationException)
            {
                throw;
            }
            catch (NullReferenceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Within the transaction scope is a function to updates the new booking data.
        /// Data is instantiated and then asigned to the DAL properties.
        /// </summary>
        /// <param name="booking"></param>
        /// <returns>
        /// Returns the saved data.
        /// </returns>
        public static BLObjects.BookingUpdate UpdateBooking(BLObjects.BookingUpdate booking)
        {
            try
            {
                // Create the TransactionScope to execute the commands, guaranteeing
                // that both commands can commit or roll back as a single unit of work.
                using (TransactionScope scope = new TransactionScope())
                {
                    // Instaniate the booking
                    DAL.Entities.BOOKING bBooking = new DAL.Entities.BOOKING();

                    bBooking.BOOK_BookingID = booking.BookingID;

                    var bookunchanged = DAL.Entities.BOOKING.GetBookingByID(booking.BookingID);

                    // Checks if the Campus is changed and if not it gets the original value
                    if (booking.selectedCampus == 0)
                    {
                        bBooking.BOOK_CampusID = bookunchanged.BOOK_CampusID;
                    }
                    else
                    {
                        bBooking.BOOK_CampusID = booking.selectedCampus;
                    }

                    // Checks if the Qualification is changed and if not it gets the original value
                    if (booking.selectedQualification == 0)
                    {
                        bBooking.BOOK_QualID = bookunchanged.BOOK_QualID;
                    }
                    else
                    {
                        bBooking.BOOK_QualID = booking.selectedQualification;
                    }

                    // Check if the Trainer has changed and if not it gets the original value
                    if (booking.selectedTrainer == 0)
                    {
                        bBooking.BOOK_UserID = bookunchanged.BOOK_UserID;
                    }
                    else
                    {
                        bBooking.BOOK_UserID = booking.selectedTrainer;
                    }

                    // Check if the Status has changed and if not it get the original value
                    if (booking.selectedStatus == 0)
                    {
                        bBooking.BOOK_StatusID = bookunchanged.BOOK_StatusID;
                    }
                    else
                    {
                        bBooking.BOOK_StatusID = booking.selectedStatus;
                    }

                    // Chech if booking status is Confirmed
                    string existingRCC = bookunchanged.BOOK_Year + bookunchanged.BOOK_Month + bookunchanged.BOOK_RandomCode;
                    if (booking.selectedStatus == 4)
                    {
                        // calls the generateUnique code function
                        string courseCode = GenerateUniqueCode();
                        // Check if the courseCode exists
                        var rcc = DAL.Entities.BOOKING.GetBookingByRCC(courseCode.Substring(0, 2), courseCode.Substring(2, 2), courseCode.Substring(4, 4));
                        // While loop that checks the if the instantiated variable is not null it continious with the loop
                        while (rcc != null)
                        {
                            courseCode = GenerateUniqueCode();
                            rcc = DAL.Entities.BOOKING.GetBookingByRCC(courseCode.Substring(0, 2), courseCode.Substring(2, 2), courseCode.Substring(4, 4));
                        }

                        // Assign the course code to the variables op the booking collection
                        bBooking.BOOK_Year = courseCode.Substring(0, 2);
                        bBooking.BOOK_Month = courseCode.Substring(2, 2);
                        bBooking.BOOK_RandomCode = courseCode.Substring(4, 4);
                    }
                    else
                    {
                        bBooking.BOOK_Year = bookunchanged.BOOK_Year;
                        bBooking.BOOK_Month = bookunchanged.BOOK_Month;
                        bBooking.BOOK_RandomCode = bookunchanged.BOOK_RandomCode;
                    }

                    bBooking.BOOK_StartDate = DateTime.Parse(booking.StartDate);
                    bBooking.BOOK_EndDate = DateTime.Parse(booking.EndDate);

                    DAL.Entities.BOOKING.SaveBooking(bBooking);

                    // The Complete method commits the transaction. If an exception has been thrown, 
                    // Complete is not called and the transaction is rolled back.
                    scope.Complete();

                    return booking;
                }
            }
            catch (TransactionAbortedException)
            {
                throw;
            }
            catch (ApplicationException)
            {
                throw;
            }
            catch (NullReferenceException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets a list of bookings according to the param value(userid or course code).
        /// Then foreach Booking record get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="searchValue"></param>
        /// <returns>
        /// returns a list of bookings
        /// </returns>
        public static List<BLObjects.Booking> Search(string searchValue)
        {
            try
            {
                List<BLObjects.Booking> result = new List<BLObjects.Booking>();

                // Get all the bookings to matching the 
                var bookingByRCC = DAL.Entities.BOOKING.GetBookingByRCC(searchValue.Substring(0, 2), searchValue.Substring(2, 2), searchValue.Substring(4, 4));
                if (bookingByRCC != null)
                {
                    var campus = DAL.Entities.CAMPUS.GetCampusByID(bookingByRCC.BOOK_CampusID);
                    var college = DAL.Entities.COLLEGE.GetCollegeByID(campus.CAMP_CollegID);
                    var qual = DAL.Entities.QUALIFICATION.GetQualByQualID(bookingByRCC.BOOK_QualID);
                    var trainer = DAL.Entities.USERS.GetUserByID(bookingByRCC.BOOK_UserID);

                    BLObjects.Booking bBooking = new BLObjects.Booking();
                    bBooking.BookingID = bookingByRCC.BOOK_BookingID;
                    bBooking.College = college.COL_Name;
                    bBooking.Campus = campus.CAMP_Name;
                    bBooking.Qualification = qual.QUAL_Name;
                    bBooking.Trainer = trainer.USER_Name + " " + trainer.USER_Surname;
                    bBooking.StartDate = bookingByRCC.BOOK_StartDate.ToString();
                    bBooking.EndDate = bookingByRCC.BOOK_EndDate.ToString();
                    bBooking.CourseCode = bookingByRCC.BOOK_Year + bookingByRCC.BOOK_Month + bookingByRCC.BOOK_RandomCode;

                    result.Add(bBooking);
                }
                //else
                //{
                //    foreach (DAL.Entities.USERS t in DAL.Entities.USERS.GetUserLIstByNameAndSurname(searchValue))
                //    {
                //        foreach (DAL.Entities.BOOKING bookingByTrainer in DAL.Entities.BOOKING.GetBookingListByUserID(t.USER_UserID))
                //        {
                //            var campus = DAL.Entities.CAMPUS.GetCampusByID(bookingByTrainer.BOOK_CampusID);
                //            var college = DAL.Entities.COLLEGE.GetCollegeByID(campus.CAMP_CollegID);
                //            var qual = DAL.Entities.QUALIFICATION.GetQualByQualID(bookingByTrainer.BOOK_QualID);
                //            var trainer = DAL.Entities.USERS.GetUserByID(bookingByTrainer.BOOK_UserID);
                //            // declare a course code string variable
                //            string CourseCode = bookingByTrainer.BOOK_Year + bookingByTrainer.BOOK_Month + bookingByTrainer.BOOK_RandomCode;

                //            // Instaniate a the bookings class and asign data to the properties
                //            BLObjects.Booking booking = new BLObjects.Booking();
                //            booking.BookingID = bookingByTrainer.BOOK_BookingID;
                //            booking.College = college.COL_Name;
                //            booking.Campus = campus.CAMP_Name;
                //            booking.Qualification = qual.QUAL_Name;
                //            booking.Trainer = trainer.USER_Name + " " + trainer.USER_Surname;
                //            booking.StartDate = bookingByTrainer.BOOK_StartDate.ToString();
                //            booking.EndDate = bookingByTrainer.BOOK_EndDate.ToString();
                //            booking.CourseCode = CourseCode;

                //            result.Add(booking);
                //        }
                //    }
                //}

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
