﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the TimeSheet table of the DB.
    /// </summary>
    public static class ModifyTimeSheet
    {
        /// <summary>
        /// Get a specific record from the TIMESHEET table of the DB
        /// </summary>
        /// <param name="timeSheetID"></param>
        /// <returns>
        /// Returns a single record matching the param.
        /// </returns>
        public static BLObjects.TimeSheet GetTimeSheetByID(int timeSheetID)
        {
            try
            {
                var timeSheet = DAL.Entities.TIMESHEET.GetTimeSheetByID(timeSheetID);
                var campus = DAL.Entities.CAMPUS.GetCampusByID(timeSheet.TIME_CampusID);
                var college = DAL.Entities.COLLEGE.GetCollegeByID(campus.CAMP_CollegID);
                var cost = DAL.Entities.COSTS.GetCostByCostID(timeSheet.TIME_CostID);

                BLObjects.TimeSheet t = new BLObjects.TimeSheet();
                t.TimeSheetID = timeSheet.TIME_TimeSheetID;
                t.UserID = timeSheet.TIME_UserID;
                t.Date = timeSheet.TIME_Date.ToString("yyyy-MM-dd");
                t.StartTime = timeSheet.TIME_StartTime.ToString("HH:mm");
                t.EndTime = timeSheet.TIME_EndTime.ToString("HH:mm");

                TimeSpan totalTime = timeSheet.TIME_EndTime.Subtract(timeSheet.TIME_StartTime);
                t.Hours = totalTime.ToString();
                t.College = college.COL_Name;
                t.Campus = campus.CAMP_Name;
                t.Nights = timeSheet.TIME_Nights;
                t.Toll = cost.COST_Toll;
                t.FlightTransport = cost.COST_FlightTransport;
                t.FlightParking = cost.COST_FlightParking;
                t.Other = cost.COST_Other;
                t.Food = cost.COST_Food;

                if (cost.COST_RatesPerKMID.HasValue)
                {
                    var rpkm = DAL.Entities.RatePerKM.GetRatePerKMByID(cost.COST_RatesPerKMID.Value);
                    t.RatesName = rpkm.RATES_Name;
                    t.RatesPerKM = rpkm.RATES_RatePerKM;
                }

                t.ODOStart = int.Parse(timeSheet.TIME_ODOStart.Trim());
                t.ODOEnd = int.Parse(timeSheet.TIME_ODOEnd.Trim());
                int KMTraveld = t.ODOEnd - t.ODOStart;
                t.KMTraveled = KMTraveld.ToString();

                return t;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the TIMESHEET table matching the param.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of records which match the param.
        /// </returns>
        public static List<BLObjects.TimeSheet> GetTimeSheetByUserID(int userID)
        {
            try
            {
                List<BLObjects.TimeSheet> result = new List<BLObjects.TimeSheet>();

                foreach (DAL.Entities.TIMESHEET timeSheet in DAL.Entities.TIMESHEET.GetTimeSheetListByUserID(userID))
                {
                    var campus = DAL.Entities.CAMPUS.GetCampusByID(timeSheet.TIME_CampusID);
                    var college = DAL.Entities.COLLEGE.GetCollegeByID(campus.CAMP_CollegID);
                    var cost = DAL.Entities.COSTS.GetCostByCostID(timeSheet.TIME_CostID);
                    
                    BLObjects.TimeSheet t = new BLObjects.TimeSheet();
                    t.TimeSheetID = timeSheet.TIME_TimeSheetID;
                    t.UserID = timeSheet.TIME_UserID;
                    t.Date = timeSheet.TIME_Date.ToString("yyyy-MM-dd");
                    t.StartTime = timeSheet.TIME_StartTime.ToString("HH:mm");
                    t.EndTime = timeSheet.TIME_EndTime.ToString("HH:mm");

                    TimeSpan totalTime = timeSheet.TIME_EndTime.Subtract(timeSheet.TIME_StartTime);
                    t.Hours = totalTime.ToString();
                    t.College = college.COL_Name;
                    t.Campus = campus.CAMP_Name;
                    t.Nights = timeSheet.TIME_Nights;
                    t.Toll = cost.COST_Toll;
                    t.FlightTransport = cost.COST_FlightTransport;
                    t.FlightParking = cost.COST_FlightParking;
                    t.Other = cost.COST_Other;
                    t.Food = cost.COST_Food;

                    if (cost.COST_RatesPerKMID.HasValue)
                    {
                        var rpkm = DAL.Entities.RatePerKM.GetRatePerKMByID(cost.COST_RatesPerKMID.Value);
                        t.RatesName = rpkm.RATES_Name;
                        t.RatesPerKM = rpkm.RATES_RatePerKM;
                    }

                    t.ODOStart = int.Parse(timeSheet.TIME_ODOStart.Trim());
                    t.ODOEnd = int.Parse(timeSheet.TIME_ODOEnd.Trim());
                    int KMTraveld = t.ODOEnd - t.ODOStart;
                    t.KMTraveled = KMTraveld.ToString();

                    result.Add(t);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records form the TIMESHEET table matching all the params.
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of records mathcing all the params
        /// </returns>
        public static List<BLObjects.TimeSheet> GetTimesheetListByDateAndUserID(DateTime startDate, DateTime endDate, int userID)
        {
            try 
	        {	        
		        List<BLObjects.TimeSheet> result = new List<BLObjects.TimeSheet>();

                foreach (DAL.Entities.TIMESHEET timeSheet in DAL.Entities.TIMESHEET.GetTimeSheetListByDatesAndUserID(startDate, endDate, userID))
                {
                    var campus = DAL.Entities.CAMPUS.GetCampusByID(timeSheet.TIME_CampusID);
                    var college = DAL.Entities.COLLEGE.GetCollegeByID(campus.CAMP_CollegID);
                    var cost = DAL.Entities.COSTS.GetCostByCostID(timeSheet.TIME_CostID);

                    BLObjects.TimeSheet t = new BLObjects.TimeSheet();
                    t.TimeSheetID = timeSheet.TIME_TimeSheetID;
                    t.UserID = timeSheet.TIME_UserID;
                    t.Date = timeSheet.TIME_Date.ToString("yyyy-MM-dd");
                    t.StartTime = timeSheet.TIME_StartTime.ToString("HH:mm");
                    t.EndTime = timeSheet.TIME_EndTime.ToString("HH:mm");
                    TimeSpan diff1 = timeSheet.TIME_EndTime.Subtract(timeSheet.TIME_StartTime);
                    t.Hours = diff1.ToString();//Calculation moet in knockout gemaak word
                    t.College = college.COL_Name;
                    t.Campus = campus.CAMP_Name;
                    t.Nights = timeSheet.TIME_Nights;
                    t.Toll = cost.COST_Toll;
                    t.FlightTransport = cost.COST_FlightTransport;
                    t.FlightParking = cost.COST_FlightParking;
                    t.Other = cost.COST_Other;
                    t.Food = cost.COST_Food;
                    
                    if (cost.COST_RatesPerKMID.HasValue)
                    {
                        var rpkm = DAL.Entities.RatePerKM.GetRatePerKMByID(cost.COST_RatesPerKMID.Value);
                        t.RatesName = rpkm.RATES_Name;
                        t.RatesPerKM = rpkm.RATES_RatePerKM;
                    }

                    t.ODOStart = int.Parse(timeSheet.TIME_ODOStart.Trim());
                    t.ODOEnd = int.Parse(timeSheet.TIME_ODOEnd.Trim());
                    int KMTraveld = t.ODOEnd - t.ODOStart;
                    t.KMTraveled = KMTraveld.ToString();

                    result.Add(t);
                }

                return result;
	        }
	        catch (Exception)
	        {
		        throw;
	        }
        }

       /// <summary>
       /// Save a new record to the TIMESHEET table through the DAL.
       /// </summary>
       /// <param name="timeSheet"></param>
       /// <param name="userID"></param>
       /// <returns>
       /// Returns the saved data.
       /// </returns>
        public static BLObjects.TimeSheet SaveTimeSheet(BLObjects.TimeSheet timeSheet, int userID)
        {
            try
            {
                DAL.Entities.COSTS c = new DAL.Entities.COSTS();
                c.COST_CostID = timeSheet.CostID;
                c.COST_Toll = timeSheet.Toll;
                c.COST_FlightParking = timeSheet.FlightParking;
                c.COST_FlightTransport = timeSheet.FlightTransport;
                c.COST_Other = timeSheet.Other;
                c.COST_Food = timeSheet.Food;
               
                var rate = DAL.Entities.RatePerKM.GetRatePerKMByRate(timeSheet.selectedRate);
                c.COST_RatesPerKMID = rate.RATES_RatePerKMID;

                var cost = DAL.Entities.COSTS.SaveCosts(c);
                DAL.Entities.TIMESHEET t = new DAL.Entities.TIMESHEET();
                t.TIME_TimeSheetID = timeSheet.TimeSheetID;
                t.TIME_UserID = userID;
                t.TIME_Date = DateTime.Parse(timeSheet.Date);
                t.TIME_StartTime = DateTime.Parse(timeSheet.StartTime);
                t.TIME_EndTime = DateTime.Parse(timeSheet.EndTime);
                
                TimeSpan diff1 = t.TIME_EndTime.Subtract(t.TIME_StartTime);
                string Hours = diff1.ToString();//Calculation moet in knockout gemaak word
                t.TIME_Hours = Convert.ToDateTime(Hours);

                if (timeSheet.selectedCampus < 1)
                {
                    var campus = DAL.Entities.CAMPUS.GetCampusByName(timeSheet.Campus);
                    t.TIME_CampusID = campus.CAMP_CampusID;
                }
                else
                {
                    t.TIME_CampusID = timeSheet.selectedCampus;
                }

                t.TIME_Nights = timeSheet.Nights;
                t.TIME_CostID = cost.COST_CostID;
                t.TIME_ODOStart = timeSheet.ODOStart.ToString();
                t.TIME_ODOEnd = timeSheet.ODOEnd.ToString();

                DAL.Entities.TIMESHEET.Save(t);

                return timeSheet;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Upadate an existing record to the TIMESHEET table through the DAL.
        /// </summary>
        /// <param name="timeSheet"></param>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns the Updated records
        /// </returns>
        public static BLObjects.TimeSheet UpdateTimeSheet(BLObjects.TimeSheet timeSheet, int userID)
        {
            try
            {
                var gettimesheet = GetTimeSheetByID(timeSheet.TimeSheetID);

                DAL.Entities.COSTS c = new DAL.Entities.COSTS();

                c.COST_CostID = gettimesheet.CostID;

                // Check if value has changed
                if (timeSheet.Toll.HasValue)
                {
                    c.COST_Toll = timeSheet.Toll;
                }
                else
                {
                    c.COST_Toll = gettimesheet.Toll;
                }

                if (timeSheet.FlightParking.HasValue)
                {
                    c.COST_FlightParking = timeSheet.FlightParking;
                }
                else
                {
                    c.COST_FlightParking = gettimesheet.FlightParking;
                }

                if (timeSheet.FlightTransport.HasValue)
                {
                    c.COST_FlightTransport = timeSheet.FlightTransport;
                }
                else
                {
                    c.COST_FlightTransport = gettimesheet.FlightTransport;
                }

                if (timeSheet.Other.HasValue)
                {
                    c.COST_Other = timeSheet.Other;
                }
                else
                {
                    c.COST_Other = gettimesheet.Other;
                }

                if (timeSheet.Food.HasValue)
                {
                    c.COST_Food = timeSheet.Food;
                }
                else
                {
                    c.COST_Food = gettimesheet.Food;
                }

                if (timeSheet.RatesPerKM == null)
                {
                    var rate = DAL.Entities.RatePerKM.GetRatePerKMByRate(timeSheet.selectedRate);
                    c.COST_RatesPerKMID = rate.RATES_RatePerKMID;
                }
                else
                {
                    var rate = DAL.Entities.RatePerKM.GetRatePerKMByRate((decimal)timeSheet.RatesPerKM);
                    c.COST_RatesPerKMID = rate.RATES_RatePerKMID;
                }


                var cost = DAL.Entities.COSTS.SaveCosts(c);

                DAL.Entities.TIMESHEET t = new DAL.Entities.TIMESHEET();
                t.TIME_TimeSheetID = timeSheet.TimeSheetID;
                t.TIME_UserID = userID;
                t.TIME_Date = DateTime.Parse(timeSheet.Date);
                t.TIME_StartTime = DateTime.Parse(timeSheet.StartTime);
                t.TIME_EndTime = DateTime.Parse(timeSheet.EndTime);
                

                TimeSpan totalTIme = t.TIME_EndTime.Subtract(t.TIME_StartTime);
                string Hours = totalTIme.ToString();//Calculation moet in knockout gemaak word
                t.TIME_Hours = Convert.ToDateTime(Hours);

                if (timeSheet.selectedCampus < 1)
                {
                    var campus = DAL.Entities.CAMPUS.GetCampusByName(timeSheet.Campus);
                    t.TIME_CampusID = campus.CAMP_CampusID;
                }
                else
                {
                    t.TIME_CampusID = timeSheet.selectedCampus;
                }

                t.TIME_Nights = timeSheet.Nights;
                t.TIME_CostID = cost.COST_CostID;
                t.TIME_ODOStart = timeSheet.ODOStart.ToString();
                t.TIME_ODOEnd = timeSheet.ODOEnd.ToString();

                DAL.Entities.TIMESHEET.Save(t);

                return timeSheet;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
