﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the CAMPUS table of the DB.
    /// </summary>
    public static class ModifyCampus
    {
        /// <summary>
        /// Get a specific record from the campus table through the DAL.
        /// </summary>
        /// <param name="campusID"></param>
        /// <returns>
        /// Returns a specific record matching the param.
        /// </returns>
        public static BLObjects.Campus GetCampusByCampusID(int campusID)
        {
            try
            {
                var campus = DAL.Entities.CAMPUS.GetCampusByID(campusID);

                BLObjects.Campus c = new BLObjects.Campus();
                c.CampusID = campus.CAMP_CampusID;
                c.CampusName = campus.CAMP_Name;
                c.CampusDescription = campus.CAMP_Description;
                c.CollegeID = campus.CAMP_CollegID;

                return c;
            }
            catch (NullReferenceException)
            {
                BLObjects.Campus c = new BLObjects.Campus();
                c.CampusID = 0;
                c.CampusName = "";
                c.CampusDescription = "";
                c.CollegeID = 0;

                return c;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of data from the CAMPUS table through the DAL(enityframework)
        /// </summary>
        /// <param name="collegeID"></param>
        /// <returns>
        /// Returns a list of data that match the param
        /// </returns>
        public static List<BLObjects.Campus> GetCampusByCollegeID(int collegeID)
        {
            try
            {
                List<BLObjects.Campus> result = new List<BLObjects.Campus>();

                foreach (DAL.Entities.CAMPUS camp in DAL.Entities.CAMPUS.GetCampusByCollegeID(collegeID))
                {
                    BLObjects.Campus campus = new BLObjects.Campus();

                    campus.CollegeID = camp.CAMP_CollegID;
                    campus.CampusID = camp.CAMP_CampusID;
                    campus.CampusName = camp.CAMP_Name;
                    campus.CampusDescription = camp.CAMP_Description;

                    result.Add(campus);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of data from the CAMPUS table through the DAL(enityframework)Get all the the record from the DAL
        /// </summary>
        /// <returns>
        /// Retuns a list of all the records in the CAMPUS table.
        /// </returns>
        public static List<BLObjects.Campus> GetAll()
        {
            try
            {
                List<BLObjects.Campus> result = new List<BLObjects.Campus>();

                foreach (DAL.Entities.CAMPUS camp in DAL.Entities.CAMPUS.GetAll())
                {
                    BLObjects.Campus campus = new BLObjects.Campus();

                    campus.CollegeID = camp.CAMP_CollegID;
                    campus.CampusID = camp.CAMP_CampusID;
                    campus.CampusName = camp.CAMP_Name;
                    campus.CampusDescription = camp.CAMP_Description;

                    result.Add(campus);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a list of campuses to the CAMPUS table and updates the existing records if any changes were made.
        /// </summary>
        /// <param name="campuses"></param>
        /// <returns>
        /// Returns the saved records.
        /// </returns>
        public static List<BLObjects.Campus> SaveCampusList(List<BLObjects.Campus> campuses)
        {
            try
            {
                // Create the TransactionScope to execute the commands, guaranteeing
                // that both commands can commit or roll back as a single unit of work.
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (BLObjects.Campus campus in campuses)
                    {
                        //var checkExistance = DAL.Entities.CAMPUS.GetCampusByName(campus.CampusName);

                        if (campus.CampusName != "")// && checkExistance == null)
                        {
                            DAL.Entities.CAMPUS cCampus = new DAL.Entities.CAMPUS();

                            cCampus.CAMP_CollegID = campus.CollegeID;
                            cCampus.CAMP_CampusID = campus.CampusID;
                            cCampus.CAMP_Name = campus.CampusName.ToUpper();
                            cCampus.CAMP_Description = campus.CampusDescription.ToUpper();

                            if (campus._destroy == true)
                            {
                                DAL.Entities.CAMPUS.Delete(cCampus);
                            }
                            else
                            {
                                DAL.Entities.CAMPUS.CampusSave(cCampus);
                            }
                        }
                    }

                    // The Complete method commits the transaction. If an exception has been thrown, 
                    // Complete is not called and the transaction is rolled back.
                    scope.Complete();

                    return campuses;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
