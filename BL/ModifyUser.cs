﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the USER table of the DB.
    /// </summary>
    public static class ModifyUser
    {
        /// <summary>
        /// Get a specific record from the USER table.
        /// </summary>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a record matching the param.
        /// </returns>
        public static BLObjects.User GetID(int userID)
        {
            try
            {
                var user = DAL.Entities.USERS.GetUserByID(userID);

                if (user.USER_TitleID.HasValue)
                {
                    int TitleID = (int)user.USER_TitleID;
                    var title = ModifyTitle.GetTitleByID(TitleID);

                    BLObjects.User c = new BLObjects.User();
                    c.UserID = user.USER_UserID;
                    c.Name = user.USER_Name;
                    c.Surname = user.USER_Surname;
                    c.Initials = user.USER_Initials;
                    c.Title = title.TitleName;
                    c.Password = user.USER_Password;
                    c.UserType = user.USER_TypeID;

                    return c;
                }
                else
                {
                    BLObjects.User c = new BLObjects.User();
                    c.UserID = user.USER_UserID;
                    c.Name = user.USER_Name;
                    c.Surname = user.USER_Surname;
                    c.Initials = user.USER_Initials;
                    c.Title = string.Empty;
                    c.Password = user.USER_Password;
                    c.UserType = user.USER_TypeID;

                    return c;
                }
            }
            catch (NullReferenceException)
            {
                BLObjects.User c = new BLObjects.User();
                c.UserID = 0;
                c.Name = string.Empty;
                c.Surname = string.Empty;
                c.Initials = string.Empty;
                c.Title = string.Empty;
                c.Password = string.Empty;
                c.UserType = 0;

                return c;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of all records from the USER table.
        /// </summary>
        /// <returns>
        /// Returns all the records from the USER table.
        /// </returns>
        public static List<BLObjects.User> GetAll()
        {
            try
            {
                List<BLObjects.User> result = new List<BLObjects.User>();

                //Create 'n BusinessLayer Client Object van die Entity Framework Object. (Convert die een na die ander)
                foreach (DAL.Entities.USERS cli in DAL.Entities.USERS.GetAllUsers())
                {
                    if (cli.USER_TitleID.HasValue)
                    {
                        int titleID = (int)cli.USER_TitleID;
                        var title = ModifyTitle.GetTitleByID(titleID);

                        BLObjects.User client = new BLObjects.User();

                        client.UserID = cli.USER_UserID;
                        client.Title = title.TitleName;
                        client.Initials = cli.USER_Initials;
                        client.Name = cli.USER_Name;
                        client.Surname = cli.USER_Surname;
                        client.UserType = cli.USER_TypeID;

                        result.Add(client);
                    }
                    else
                    {
                        BLObjects.User client = new BLObjects.User();

                        client.UserID = cli.USER_UserID;
                        client.Title = "";
                        client.Initials = cli.USER_Initials;
                        client.Name = cli.USER_Name;
                        client.Surname = cli.USER_Surname;
                        client.UserType = cli.USER_TypeID;

                        result.Add(client);
                    }
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get a list of all records from the USER table.
        /// </summary>
        /// <param name="campusID"></param>
        /// <param name="surname"></param>
        /// <returns>
        /// Returns data from the USER table mathcing both the params.
        /// </returns>
        public static List<BLObjects.User> GetUserByCampusIDAndSurname(int campusID, string surname)
        {
            List<BLObjects.User> result = new List<BLObjects.User>();

            foreach (var u in DAL.Entities.USERS.GetUserListBySurname(surname))
            {
                var camplink = DAL.Entities.CAMPUSLINK.GetCampusLinkByUserIDAndeCampusID(u.USER_UserID, campusID);

                if (camplink != null)
                {
                    var users = GetID(camplink.CAMPLINK_UserID);

                    BLObjects.User user = new BLObjects.User();

                    user.UserID = users.UserID;
                    user.Title = users.Title;
                    user.Initials = users.Initials;
                    user.Name = users.Name;
                    user.Surname = users.Surname;
                    user.UserType = users.UserType;

                    result.Add(user);
                }
            }

            return result;
        }

        /// <summary>
        /// Get a list of records from the USER table matching the param.
        /// </summary>
        /// <param name="campusID"></param>
        /// <returns>
        /// Returns a list of records from DAL matching the param
        /// </returns>
        public static List<BLObjects.User> GetUserByCampID(int campusID)
        {
            try
            {
                List<BLObjects.User> result = new List<BLObjects.User>();

                //Create 'n BusinessLayer Client Object van die Entity Framework Object. (Convert die een na die ander)
                foreach (DAL.Entities.CAMPUSLINK cli in DAL.Entities.CAMPUSLINK.GetCampusID(campusID))
                {
                    var users = GetID(cli.CAMPLINK_UserID);
                    BLObjects.User user = new BLObjects.User();

                    user.UserID = users.UserID;
                    user.Title = users.Title;
                    user.Initials = users.Initials;
                    user.Name = users.Name;
                    user.Surname = users.Surname;
                    user.UserType = users.UserType;

                    result.Add(user);
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USER table matching the param. 
        /// </summary>
        /// <param name="surname"></param>
        /// <returns>
        /// Returns a list of records matching the param.
        /// </returns>
        public static List<BLObjects.User> GetUserBySurname(string surname)
        {
            try
            {
                List<BLObjects.User> result = new List<BLObjects.User>();

                foreach (var cli in GetAll().Where(c => c.Surname.Contains(surname)))
                {

                    BLObjects.User client = new BLObjects.User();
                    client.UserID = cli.UserID;
                    client.Title = cli.Title;
                    client.Initials = cli.Initials;
                    client.Name = cli.Name;
                    client.Surname = cli.Surname;
                    client.UserType = cli.UserType;

                    result.Add(client);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USER table matching all the params. 
        /// </summary>
        /// <param name="surname"></param>
        /// <param name="userID"></param>
        /// <returns>
        /// Returns a list of records matching the param
        /// </returns>
        public static List<BLObjects.User> GetUserBySurnameAndUserID(string surname, int userID)
        {
            try
            {
                var campusLink = DAL.Entities.CAMPUSLINK.GetCampusLinkByUserID(userID);

                List<BLObjects.User> result = new List<BLObjects.User>();

                foreach (var cli in GetAll().Where(c => c.Surname.Contains(surname)))
                {
                    var campus = DAL.Entities.CAMPUSLINK.GetCampusLinkByUserID(cli.UserID);

                    if (campus.CAMPLINK_CampusID == campusLink.CAMPLINK_CampusID)
                    {
                        BLObjects.User client = new BLObjects.User();
                        client.UserID = cli.UserID;
                        client.Title = cli.Title;
                        client.Initials = cli.Initials;
                        client.Name = cli.Name;
                        client.Surname = cli.Surname;
                        client.UserType = cli.UserType;

                        result.Add(client);
                    }
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USER table matching the params 
        /// </summary>
        /// <param name="userType"></param>
        /// <returns>
        /// Returns all the users matching the param.
        /// </returns>
        public static List<BLObjects.User> GetUserByType(int userType)
        {
            try
            {
                List<BLObjects.User> result = new List<BLObjects.User>();

                foreach (var cli in DAL.Entities.USERS.GetUserListByType(userType))
                {
                    var booking = DAL.Entities.BOOKING.GetBookingByUserID(cli.USER_UserID);

                    if (booking == null)
                    {
                        if (cli.USER_TitleID.HasValue)
                        {
                            int titleID = (int)cli.USER_TitleID;
                            var title = ModifyTitle.GetTitleByID(titleID);

                            BLObjects.User client = new BLObjects.User();

                            client.UserID = cli.USER_UserID;
                            client.Title = title.TitleName;
                            client.Initials = cli.USER_Initials;
                            client.Name = cli.USER_Name;
                            client.Surname = cli.USER_Surname;
                            client.UserType = cli.USER_TypeID;

                            result.Add(client);
                        }
                        else
                        {
                            BLObjects.User client = new BLObjects.User();

                            client.UserID = cli.USER_UserID;
                            client.Title = "";
                            client.Initials = cli.USER_Initials;
                            client.Name = cli.USER_Name;
                            client.Surname = cli.USER_Surname;
                            client.UserType = cli.USER_TypeID;

                            result.Add(client);
                        }
                    }
                    else
                    {
                        if (cli.USER_TitleID.HasValue)
                        {
                            int titleID = (int)cli.USER_TitleID;
                            var title = ModifyTitle.GetTitleByID(titleID);

                            BLObjects.User client = new BLObjects.User();

                            client.UserID = cli.USER_UserID;
                            client.ReferNum = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;
                            client.Title = title.TitleName;
                            client.Initials = cli.USER_Initials;
                            client.Name = cli.USER_Name;
                            client.Surname = cli.USER_Surname;
                            client.UserType = cli.USER_TypeID;

                            result.Add(client);
                        }
                        else
                        {
                            BLObjects.User client = new BLObjects.User();

                            client.UserID = cli.USER_UserID;
                            client.ReferNum = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;
                            client.Title = string.Empty;
                            client.Initials = cli.USER_Initials;
                            client.Name = cli.USER_Name;
                            client.Surname = cli.USER_Surname;
                            client.UserType = cli.USER_TypeID;

                            result.Add(client);
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a list of records from the USER table matching the params 
        /// </summary>
        /// <param name="userType"></param>
        /// <returns>
        /// Returns a list of the trainers
        /// </returns>
        public static List<BLObjects.User> GetUserListByTrainer(int userType)
        {
            try
            {
                List<BLObjects.User> result = new List<BLObjects.User>();

                foreach (var ut in DAL.Entities.USERS.GetUserListByType(userType))
                {
                    if (ut.USER_TitleID.HasValue)
                    {
                        int titleID = (int)ut.USER_TitleID;
                        var title = ModifyTitle.GetTitleByID(titleID);

                        BLObjects.User trainer = new BLObjects.User();

                        trainer.UserID = ut.USER_UserID;
                        trainer.Title = title.TitleName;
                        trainer.Initials = ut.USER_Initials;
                        trainer.Name = ut.USER_Name;
                        trainer.Surname = ut.USER_Surname;
                        trainer.UserType = ut.USER_TypeID;

                        result.Add(trainer);
                    }
                    else
                    {
                        BLObjects.User trainer = new BLObjects.User();

                        trainer.UserID = ut.USER_UserID;
                        trainer.Title = "";
                        trainer.Initials = ut.USER_Initials;
                        trainer.Name = ut.USER_Name;
                        trainer.Surname = ut.USER_Surname;
                        trainer.UserType = ut.USER_TypeID;

                        result.Add(trainer);
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}