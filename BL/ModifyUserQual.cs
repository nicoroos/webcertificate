﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the CLIENTQUAL table of the DB.
    /// </summary>
    public static class ModifyUserQual
    {
        /// <summary>
        /// Get all record with this param from DAL
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns>
        /// Returns a list of records matching the param
        /// </returns>
        public static List<BLObjects.ClientQual> GetID(int UserID)
        {
            try
            {
                List<BLObjects.ClientQual> result = new List<BLObjects.ClientQual>();

                foreach (DAL.Entities.CLIENTQUAL cq in DAL.Entities.CLIENTQUAL.GetClientQualListByUserID(UserID))
                {
                    BLObjects.ClientQual clientqual = new BLObjects.ClientQual();

                    clientqual.UserQualID = cq.CLIQUAL_UserQualID;
                    clientqual.UserID = cq.CLIQUAL_UserID;

                    // Get the qual name according to the id from the DB
                    BLObjects.Qualification qualification = new BLObjects.Qualification();
                    qualification = BL.ModifyQualification.GetQualByQualID(cq.CLIQUAL_QualID);
                    clientqual.Qual = qualification.QualName;

                    clientqual.StartDate = (cq.CLIQUAL_StartDate).ToShortDateString();
                    clientqual.EndDate = (cq.CLIQUAL_EndDate).ToShortDateString();

                    var printstatus = DAL.Entities.PRINTSTATUS.GetPrintStatusByPrintID(cq.CLIQUAL_PrintID);
                    clientqual.PrintStatus = printstatus.PRINT_Status;

                    var book = DAL.Entities.BOOKING.GetBookingByID(cq.CLIQUAL_BookingID);
                    clientqual.ReferNum = book.BOOK_Year + book.BOOK_Month + book.BOOK_RandomCode;

                    result.Add(clientqual);
                }

                foreach (DAL.Entities.BOOKLINK bl in DAL.Entities.BOOKLINK.GetUserIDList(UserID))
                {
                    var cliqual = DAL.Entities.CLIENTQUAL.GetClientQualByUserIDAndBookingID(UserID, bl.BOOKLINK_BookingID);
                    if (cliqual == null)
                    {
                        var booking = DAL.Entities.BOOKING.GetBookingByID(bl.BOOKLINK_BookingID);
                        BLObjects.ClientQual clientqual = new BLObjects.ClientQual();

                        //clientqual.ClientQualID = b.;
                        clientqual.UserID = UserID;

                        // Get the qual name according to the id from the DB
                        BLObjects.Qualification qualification = new BLObjects.Qualification();
                        qualification = BL.ModifyQualification.GetQualByQualID(booking.BOOK_QualID);
                        clientqual.Qual = qualification.QualName;

                        clientqual.StartDate = (booking.BOOK_StartDate).ToShortDateString();
                        clientqual.EndDate = (booking.BOOK_EndDate).ToShortDateString();

                        clientqual.PrintStatus = "N/A";


                        if (booking.BOOK_Year == null)
                        {
                            clientqual.ReferNum = "N/A";
                        }
                        else
                        {

                            clientqual.ReferNum = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;
                        }

                        result.Add(clientqual);
                    }
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get all the records from the DAL.
        /// </summary>
        /// <returns></returns>
        public static List<BLObjects.ClientQual> GetALL()
        {
            try
            {
                List<BLObjects.ClientQual> result = new List<BLObjects.ClientQual>();

                foreach (DAL.Entities.CLIENTQUAL cq in DAL.Entities.CLIENTQUAL.GetALL())
                {
                    BLObjects.ClientQual clientqual = new BLObjects.ClientQual();

                    clientqual.UserQualID = cq.CLIQUAL_UserQualID;
                    clientqual.UserID = cq.CLIQUAL_UserID;

                    BLObjects.Qualification qualification = new BLObjects.Qualification();
                    qualification = BL.ModifyQualification.GetQualByQualID(cq.CLIQUAL_QualID);

                    clientqual.Qual = qualification.QualName;
                    clientqual.StartDate = (cq.CLIQUAL_StartDate).ToString();
                    clientqual.EndDate = (cq.CLIQUAL_EndDate).ToString();

                    var printstatus = DAL.Entities.STATUS.GetStatusByID(cq.CLIQUAL_PrintID);
                    clientqual.PrintStatus = printstatus.STATUS_StatusName;

                    result.Add(clientqual);
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// save a single record to the DB
        /// </summary>
        /// <param name="clientQual"></param>
        /// <returns>
        /// </returns>
        public static BLObjects.ClientQual SaveSingle(BLObjects.ClientQual clientQual)
        {
            try
            {
                DAL.Entities.CLIENTQUAL cq = new DAL.Entities.CLIENTQUAL();

                cq.CLIQUAL_UserQualID = clientQual.UserQualID;
                cq.CLIQUAL_UserID = clientQual.UserID;

                var qual = DAL.Entities.QUALIFICATION.GetQualByName(clientQual.Qual);
                cq.CLIQUAL_QualID = qual.QUAL_QualID;

                cq.CLIQUAL_StartDate = DateTime.Parse(clientQual.StartDate);
                cq.CLIQUAL_EndDate = DateTime.Parse(clientQual.EndDate);
                var printstatus = DAL.Entities.PRINTSTATUS.GetPrintStatusByStatusName(clientQual.PrintStatus);
                cq.CLIQUAL_PrintID = printstatus.PRINT_PrintID;

                DAL.Entities.CLIENTQUAL.SaveClientQual(cq);

                return clientQual;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a list of records marked.
        /// </summary>
        /// <param name="clientsQual"></param>
        /// <returns></returns>
        public static List<BLObjects.ClientQual> Save(List<BLObjects.ClientQual> clientsQual)
        {
            try
            {
                List<BLObjects.ClientQual> result = new List<BLObjects.ClientQual>();

                foreach (BLObjects.ClientQual clientqual in clientsQual)
                {
                    if (clientqual._destroy == false)
                    {
                        var check = DAL.Entities.CLIENTQUAL.GetClientQualByUserIDAndBookingID(clientqual.UserID, clientqual.BookingID);

                        if (check == null)
                        {

                            if (clientqual.PrintID == 0)
                            {
                                clientqual.PrintID = 3;
                            }

                            DAL.Entities.CLIENTQUAL cClientQual = new DAL.Entities.CLIENTQUAL();

                            cClientQual.CLIQUAL_BookingID = clientqual.BookingID;

                            cClientQual.CLIQUAL_UserQualID = clientqual.UserQualID;
                            cClientQual.CLIQUAL_UserID = clientqual.UserID;

                            var qual = DAL.Entities.QUALIFICATION.GetQualByName(clientqual.Qual);
                            cClientQual.CLIQUAL_QualID = qual.QUAL_QualID;

                            cClientQual.CLIQUAL_StartDate = DateTime.Parse(clientqual.StartDate);
                            cClientQual.CLIQUAL_EndDate = DateTime.Parse(clientqual.EndDate);
                            cClientQual.CLIQUAL_PrintID = clientqual.PrintID;

                            var saved = DAL.Entities.CLIENTQUAL.SaveClientQual(cClientQual);

                            BLObjects.ClientQual data = new BLObjects.ClientQual();
                            data.UserQualID = saved.CLIQUAL_UserQualID;
                            data.UserID = clientqual.UserID;
                            data.CInitials = clientqual.CInitials;
                            data.CSurname = clientqual.CSurname;
                            data.Qual = clientqual.Qual;
                            data.StartDate = clientqual.StartDate;
                            data.EndDate = clientqual.EndDate;
                            data.PrintID = 2;
                            data.BookingID = clientqual.BookingID;

                            result.Add(data);
                        }
                        else
                        {
                            var cliql = DAL.Entities.CLIENTQUAL.GetClientQualByUserIDAndBookingID(clientqual.UserID, clientqual.BookingID);

                            if (clientqual.PrintID == 0)
                            {
                                clientqual.PrintID = 3;
                            }

                            DAL.Entities.CLIENTQUAL cClientQual = new DAL.Entities.CLIENTQUAL();


                            cClientQual.CLIQUAL_BookingID = cliql.CLIQUAL_BookingID;

                            cClientQual.CLIQUAL_UserQualID = cliql.CLIQUAL_UserQualID;
                            cClientQual.CLIQUAL_UserID = cliql.CLIQUAL_UserID;
                            cClientQual.CLIQUAL_QualID = cliql.CLIQUAL_QualID;

                            cClientQual.CLIQUAL_StartDate = cliql.CLIQUAL_StartDate;
                            cClientQual.CLIQUAL_EndDate = cliql.CLIQUAL_EndDate;
                            cClientQual.CLIQUAL_PrintID = clientqual.PrintID;

                            var saved = DAL.Entities.CLIENTQUAL.SaveClientQual(cClientQual);

                            BLObjects.ClientQual data = new BLObjects.ClientQual();
                            data.UserQualID = saved.CLIQUAL_UserQualID;
                            data.UserID = clientqual.UserID;
                            data.CInitials = clientqual.CInitials;
                            data.CSurname = clientqual.CSurname;
                            data.Qual = clientqual.Qual;
                            data.StartDate = clientqual.StartDate;
                            data.EndDate = clientqual.EndDate;
                            data.PrintID = 2;
                            data.BookingID = clientqual.BookingID;

                            result.Add(data);

                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}