﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the RatePerKM table of the DB.
    /// </summary>
    public static class ModifyRatePerKM
    {
        /// <summary>
        /// Get all the records from the RatePerKM table through DAL.
        /// </summary>
        /// <returns>
        /// Return all the records from the RatePerKM.
        /// </returns>
        public static List<BLObjects.RatePerKM> GetAll()
        {
            try
            {
                List<BLObjects.RatePerKM> result = new List<BLObjects.RatePerKM>();

                foreach (DAL.Entities.RatePerKM rpkm in DAL.Entities.RatePerKM.GetAll())
                {
                    BLObjects.RatePerKM rateperkm = new BLObjects.RatePerKM();

                    rateperkm.RatePerKMID = rpkm.RATES_RatePerKMID;
                    rateperkm.RateName = rpkm.RATES_Name;
                    rateperkm.Rate = rpkm.RATES_RatePerKM;

                    result.Add(rateperkm);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
