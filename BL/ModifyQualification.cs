﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the QUALIFICATION table of the DB.
    /// </summary>
    public static class ModifyQualification
    {
        /// <summary>
        /// Get a specific record form the QUALIFICATION table through the DAL.
        /// </summary>
        /// <param name="qualID"></param>
        /// <returns>
        /// returns a specific record matching the param
        /// </returns>
        public static BLObjects.Qualification GetQualByQualID(int qualID)
        {
            try
            {
                var qualification = DAL.Entities.QUALIFICATION.GetQualByQualID(qualID);
                BLObjects.Qualification qual = new BLObjects.Qualification();
                qual.QualID = qualification.QUAL_QualID;
                qual.QualName = qualification.QUAL_Name;
                qual.QualDescrip = qualification.QUAL_Description;

                return qual;
            }
            catch (NullReferenceException)
            {
                BLObjects.Qualification qual = new BLObjects.Qualification();
                qual.QualID = 0;
                qual.QualName = "";
                qual.QualDescrip = "";

                return qual;
            }
        }
        
        /// <summary>
        /// Get a list of records from the QUALIFICATION table through DAL.
        /// </summary>
        /// <param name="qualName"></param>
        /// <returns>
        /// Return a list of records matching the param.
        /// </returns>
        public static List<BLObjects.Qualification> GetQualName(string qualName)
        {
            try
            {
                List<BLObjects.Qualification> result = new List<BLObjects.Qualification>();

                foreach (var qual in GetAll().Where(q => q.QualName.Contains(qualName)))
                {
                    BLObjects.Qualification qualification = new BLObjects.Qualification();

                    qualification.QualID = qual.QualID;
                    qualification.QualName = qual.QualName.ToUpper();
                    qualification.QualDescrip = qual.QualDescrip.ToUpper();

                    result.Add(qualification);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
  
        /// <summary>
        /// Get a list of all the the records from the QUALILIFICATION table through DAL.
        /// </summary>
        /// <returns>
        /// Returns a list of all the records in the QUALIFICATION table.
        /// </returns>
        public static List<BLObjects.Qualification> GetAll()
        {
            try
            {
                List<BLObjects.Qualification> result = new List<BLObjects.Qualification>();

                foreach (DAL.Entities.QUALIFICATION qual in DAL.Entities.QUALIFICATION.GetAllQualifications())
                {
                    BLObjects.Qualification qualification = new BLObjects.Qualification();

                    qualification.QualID = qual.QUAL_QualID;
                    qualification.QualName = qual.QUAL_Name;
                    qualification.QualDescrip = qual.QUAL_Description;

                    result.Add(qualification);
                }
                return result;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Save a new records and update existing records in the COLLEGE table through the DAL.
        /// </summary>
        /// <param name="quals"></param>
        /// <returns>
        /// Returns a list of saved records.
        /// </returns>
        public static List<BLObjects.Qualification> SaveQualificationList(List<BLObjects.Qualification> quals)
        {
            try
            {
                // Create the TransactionScope to execute the commands, guaranteeing
                // that both commands can commit or roll back as a single unit of work.
                using (TransactionScope scope = new TransactionScope())
                {
                    foreach (BLObjects.Qualification qual in quals)
                    {
                        //var checkexistance = DAL.Entities.QUALIFICATION.GetQualByName(qual.QualName);

                        if (qual.QualName != "")// && checkexistance == null)
                        {
                            DAL.Entities.QUALIFICATION qQualification = new DAL.Entities.QUALIFICATION();

                            qQualification.QUAL_QualID = qual.QualID;
                            qQualification.QUAL_Name = qual.QualName.ToUpper();
                            qQualification.QUAL_Description = qual.QualDescrip.ToUpper();

                            if (qual._destroy == true)
                            {
                                DAL.Entities.QUALIFICATION.Delete(qQualification);
                            }
                            else
                            {
                                DAL.Entities.QUALIFICATION.SaveQual(qQualification);
                            }
                        }
                    }
                    // The Complete method commits the transaction. If an exception has been thrown, 
                    // Complete is not called and the transaction is rolled back.
                    scope.Complete();

                    return quals;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}

