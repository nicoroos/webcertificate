﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the USERS and CONTACTDETAILS table of the DB.
    /// </summary>
    public static class ModifyUserDetails
    {
        /// <summary>
        /// Get all the detials of a specific client
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static BLObjects.ClientDetails GetUserByEmail(string email)
        {
            try
            {
                var contactdetails = DAL.Entities.CONTACTDETAILS.GetClientByEmail(email);
                var client = DAL.Entities.USERS.GetUserByID(contactdetails.CONDET_UserID);

                //BLObjects.Login login = new BLObjects.Login();
                //login.UserID = contactdetails.CONDET_UserID;
                //login.username = contactdetails.CONDET_Email;
                //login.password = client.USER_Password;

                BLObjects.ClientDetails clientdetails = new BLObjects.ClientDetails();

                clientdetails.UserID = contactdetails.CONDET_UserID;
                clientdetails.Name = client.USER_Name;
                clientdetails.Surname = client.USER_Surname;
                clientdetails.Initials = client.USER_Initials;
                clientdetails.selectedTitle = client.USER_TitleID;
                clientdetails.Password = client.USER_Password;
                clientdetails.TypeID = client.USER_TypeID;

                clientdetails.ContactDetailsID = contactdetails.CONDET_CONTACTDETAILSID;
                clientdetails.Street1 = contactdetails.CONDET_StreetNo;
                clientdetails.Street2 = contactdetails.CONDET_StreetName;
                clientdetails.Street3 = contactdetails.CONDET_Suburb;
                clientdetails.Street4 = contactdetails.CONDET_City;
                clientdetails.PostalCode = contactdetails.CONDET_PostalCode;
                clientdetails.TelNo = contactdetails.CONDET_TelNo;
                clientdetails.Fax = contactdetails.CONDET_FaxNo;
                clientdetails.Email = contactdetails.CONDET_Email;
                clientdetails.CellNo = contactdetails.CONDET_CellNo;

                return clientdetails;


            }
            catch (NullReferenceException)
            {
                BLObjects.ClientDetails clientdetails = new BLObjects.ClientDetails();

                clientdetails.UserID = 0;
                clientdetails.Name = "";
                clientdetails.Surname = "";
                clientdetails.Initials = "";
                clientdetails.selectedTitle = 0;
                clientdetails.Password = "";
                clientdetails.TypeID = 0;

                clientdetails.ContactDetailsID = 0;
                clientdetails.Street1 = "";
                clientdetails.Street2 = "";
                clientdetails.Street3 = "";
                clientdetails.Street4 = "";
                clientdetails.PostalCode = "";
                clientdetails.TelNo = "";
                clientdetails.Fax = "";
                clientdetails.Email = "";
                clientdetails.CellNo = "";

                return clientdetails;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a specific record from DB
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public static BLObjects.ClientDetail GetUserByUserID(int userID)
        {
            try
            {
                var contactdetails = DAL.Entities.CONTACTDETAILS.GetClientByUserID(userID);
                var client = DAL.Entities.USERS.GetUserByID(userID);

                BLObjects.ClientDetail clientdetails = new BLObjects.ClientDetail();

                clientdetails.UserID = contactdetails.CONDET_UserID;
                clientdetails.Name = client.USER_Name;
                clientdetails.Surname = client.USER_Surname;
                clientdetails.Initials = client.USER_Initials;
                if (client.USER_TitleID == null || client.USER_TitleID == 0)
                {
                    clientdetails.Title = "EMPTY";
                }
                else
                {
                    if (client.USER_TitleID.HasValue)
                    {
                        var title = DAL.Entities.TITLE.GetTitleByID(client.USER_TitleID.Value);
                        clientdetails.Title = title.TITLE_Name;
                        clientdetails.selectedTitle = client.USER_TitleID.Value;
                    }
                }

                clientdetails.Password = client.USER_Password.Trim();

                var usertype = DAL.Entities.USERTYPE.GetUserTypeByID(client.USER_TypeID);
                clientdetails.UserType = usertype.TYPE_Name;
                clientdetails.selectedType = client.USER_TypeID;

                var campuslink = DAL.Entities.CAMPUSLINK.GetCampusLinkByUserID(contactdetails.CONDET_UserID);
                if (campuslink == null)
                {
                    clientdetails.Campus = "EMPTY";
                    clientdetails.College = "EMPTY";
                }
                else
                {
                    var campus = DAL.Entities.CAMPUS.GetCampusByID(campuslink.CAMPLINK_CampusID);
                    clientdetails.Campus = campus.CAMP_Name;
                    clientdetails.selectedCboCampus = campus.CAMP_CampusID;
                    var college = DAL.Entities.COLLEGE.GetCollegeByID(campus.CAMP_CollegID);
                    clientdetails.College = college.COL_Name;
                    clientdetails.selectedCboCollege = college.COL_CollegeID;
                }

                clientdetails.ContactDetailsID = contactdetails.CONDET_CONTACTDETAILSID;
                clientdetails.CellNo = contactdetails.CONDET_CellNo;
                clientdetails.Email = contactdetails.CONDET_Email;
                // Check if street name is null and if it is asigns a empty string
                if (contactdetails.CONDET_StreetNo == null || contactdetails.CONDET_StreetNo.Trim() == "")
                {
                    clientdetails.Street1 = "EMPTY";
                }
                else
                {
                    clientdetails.Street1 = contactdetails.CONDET_StreetNo;
                }
                // Check if street no is null and if it is asigns a empty string
                if (contactdetails.CONDET_StreetName == null || contactdetails.CONDET_StreetName.Trim() == "")
                {
                    clientdetails.Street2 = "EMPTY";
                }
                else
                {
                    clientdetails.Street2 = contactdetails.CONDET_StreetName;
                }
                // Check if suburb is null and if it is asigns a empty string
                if (contactdetails.CONDET_Suburb == null || contactdetails.CONDET_Suburb.Trim() == "")
                {
                    clientdetails.Street3 = "EMPTY";
                }
                else
                {
                    clientdetails.Street3 = contactdetails.CONDET_Suburb;
                }
                // Check if City is null and if it is asigns a empty string
                if (contactdetails.CONDET_City == null || contactdetails.CONDET_City.Trim() == "")
                {
                    clientdetails.Street4 = "EMPTY";
                }
                else
                {
                    clientdetails.Street4 = contactdetails.CONDET_City;
                }
                // Check if PostalCode is null and if it is asigns a empty string
                if (contactdetails.CONDET_PostalCode == null || contactdetails.CONDET_PostalCode.Trim() == "")
                {
                    clientdetails.PostalCode = "EMPTY";
                }
                else
                {
                    clientdetails.PostalCode = contactdetails.CONDET_PostalCode;
                }
                // Check if Tel no is null and if it is asigns a empty string
                if (contactdetails.CONDET_TelNo == null || contactdetails.CONDET_TelNo.Trim() == "")
                {
                    clientdetails.TelNo = "EMPTY";
                }
                else
                {
                    clientdetails.TelNo = contactdetails.CONDET_TelNo;
                }
                // Check if Fax no is null and if it is asigns a empty string
                if (contactdetails.CONDET_FaxNo == null || contactdetails.CONDET_FaxNo.Trim() == "")
                {
                    clientdetails.Fax = "EMPTY";
                }
                else
                {
                    clientdetails.Fax = contactdetails.CONDET_FaxNo;
                }

                return clientdetails;

            }
            catch (NullReferenceException)
            {
                BLObjects.ClientDetail clientdetails = new BLObjects.ClientDetail();

                clientdetails.UserID = 0;
                clientdetails.Name = "";
                clientdetails.Surname = "";
                clientdetails.Initials = "";
                clientdetails.Title = "";
                clientdetails.Password = "";
                clientdetails.UserType = "";

                clientdetails.ContactDetailsID = 0;
                clientdetails.Street1 = "";
                clientdetails.Street2 = "";
                clientdetails.Street3 = "";
                clientdetails.Street4 = "";
                clientdetails.PostalCode = "";
                clientdetails.TelNo = "";
                clientdetails.Fax = "";
                clientdetails.Email = "";
                clientdetails.CellNo = "";

                return clientdetails;

            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Genereate a uniquer password
        /// </summary>
        /// <returns>
        /// Return the random password.
        /// </returns>
        private static string PasswordGenerator()
        {
            // This code generate the randomCourseCode.
            Random random = new Random();
            int randomValue = random.Next(10000, 99999);
            // Convert current date to string
            string date = DateTime.Now.ToString();
            // Concatinate the Year month and 4 digit code into one string value
            string courseCode = randomValue.ToString() + date.Substring(0, 4) + randomValue.ToString();

            return courseCode;
        }

        /// <summary>
        /// Save the record to the DB
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public static BLObjects.ClientDetails Save(BLObjects.ClientDetails client)
        {
            try
            {
                var email = GetUserByEmail(client.Email);

                if (email.Email == "")
                {
                    // Create the TransactionScope to execute the commands, guaranteeing
                    // that both commands can commit or roll back as a single unit of work.
                    using (TransactionScope scope = new TransactionScope())
                    {
                        DAL.Entities.USERS c = new DAL.Entities.USERS();
                        c.USER_UserID = client.UserID;
                        c.USER_Name = client.Name.ToUpper();
                        c.USER_Surname = client.Surname.ToUpper();
                        c.USER_Initials = client.Initials.ToUpper();
                        c.USER_TitleID = client.selectedTitle;
                        c.USER_TypeID = client.selectedType;
                        c.USER_Password = PasswordGenerator();

                        var clientid = DAL.Entities.USERS.SaveUser(c);

                        DAL.Entities.CONTACTDETAILS contactdetails = new DAL.Entities.CONTACTDETAILS();
                        contactdetails.CONDET_UserID = clientid.USER_UserID;
                        contactdetails.CONDET_CONTACTDETAILSID = client.ContactDetailsID;
                        if (client.Street1 == null)
                        {
                            contactdetails.CONDET_StreetNo = null;
                        }
                        else
                        {
                            contactdetails.CONDET_StreetNo = client.Street1.ToUpper();
                        }

                        if (client.Street2 == null)
                        {
                            contactdetails.CONDET_StreetName = null;
                        }
                        else
                        {
                            contactdetails.CONDET_StreetName = client.Street2.ToUpper();
                        }

                        if (client.Street3 == null)
                        {
                            contactdetails.CONDET_Suburb = null;
                        }
                        else
                        {
                            contactdetails.CONDET_Suburb = client.Street3.ToUpper();
                        }

                        if (client.Street4 == null)
                        {
                            contactdetails.CONDET_City = null;
                        }
                        else
                        {
                            contactdetails.CONDET_City = client.Street4.ToUpper();
                        }

                        if (client.PostalCode == null)
                        {
                            contactdetails.CONDET_PostalCode = null;
                        }
                        else
                        {
                            contactdetails.CONDET_PostalCode = client.PostalCode.ToUpper();
                        }

                        if (client.TelNo == null)
                        {
                            contactdetails.CONDET_TelNo = null;
                        }
                        else
                        {
                            contactdetails.CONDET_TelNo = client.TelNo;
                        }

                        if (client.Fax == null)
                        {
                            contactdetails.CONDET_FaxNo = "";
                        }
                        else
                        {
                            contactdetails.CONDET_FaxNo = client.Fax;
                        }

                        if (client.Email == null)
                        {
                            contactdetails.CONDET_Email = null;
                        }
                        else
                        {
                            contactdetails.CONDET_Email = client.Email.ToUpper();
                        }

                        if (client.CellNo == null)
                        {
                            contactdetails.CONDET_CellNo = null;
                        }
                        else
                        {
                            contactdetails.CONDET_CellNo = client.CellNo;
                        }


                        DAL.Entities.CONTACTDETAILS.SaveContactDetails(contactdetails);

                        DAL.Entities.CAMPUSLINK campuslink = new DAL.Entities.CAMPUSLINK();
                        campuslink.CAMPLINK_UserID = clientid.USER_UserID;
                        campuslink.CAMPLINK_CampusID = client.selectedCampus;

                        DAL.Entities.CAMPUSLINK.CampusLinkSave(campuslink);

                        // The Complete method commits the transaction. If an exception has been thrown, 
                        // Complete is not called and the transaction is rolled back.
                        scope.Complete();

                        return client;
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// update the records to the 
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public static BLObjects.ClientDetailUpdate Update(BLObjects.ClientDetailUpdate client)
        {
            try
            {
                var email = GetUserByEmail(client.Email);

                if (email.Email != "" || email.Email == null)
                {
                    // Create the TransactionScope to execute the commands, guaranteeing
                    // that both commands can commit or roll back as a single unit of work.
                    using (TransactionScope scope = new TransactionScope())
                    {
                        DAL.Entities.USERS clientdetailupdate = new DAL.Entities.USERS();
                        clientdetailupdate.USER_UserID = client.UserID;
                        clientdetailupdate.USER_Name = client.Name.ToUpper();
                        clientdetailupdate.USER_Surname = client.Surname.ToUpper();
                        clientdetailupdate.USER_Initials = client.Initials.ToUpper();

                        if (client.ReferNum != null)
                        {
                            var booking = DAL.Entities.BOOKING.GetBookingByRCC(client.ReferNum.Substring(0, 2), client.ReferNum.Substring(2, 2), client.ReferNum.Substring(4, 4));
                            DAL.Entities.BOOKLINK b = new DAL.Entities.BOOKLINK();
                            b.BOOKLINK_BookingID = booking.BOOK_BookingID;
                            b.BOOKLINK_UserID = client.UserID;

                            DAL.Entities.BOOKLINK.Save(b);
                        }
                        var unchanged = DAL.Entities.USERS.GetUserByID(client.UserID);
                        if (client.selectedTitle > 0)
                        {
                            clientdetailupdate.USER_TitleID = client.selectedTitle;
                        }
                        else
                        {
                            if (unchanged.USER_TitleID.HasValue)
                            {
                                var title = DAL.Entities.TITLE.GetTitleByID(unchanged.USER_TitleID.Value);
                                clientdetailupdate.USER_TitleID = title.TITLE_TitleID;
                            }
                        }

                        if (client.selectedType > 0)
                        {
                            clientdetailupdate.USER_TypeID = client.selectedType;
                        }
                        else
                        {
                            clientdetailupdate.USER_TypeID = unchanged.USER_TypeID;
                        }


                        if (client.selectedCboCampus > 0)
                        {
                            var campus = DAL.Entities.CAMPUSLINK.GetCampusLinkByUserID(client.UserID);

                            DAL.Entities.CAMPUSLINK campuslink = new DAL.Entities.CAMPUSLINK();
                            campuslink.CAMPLINK_CampLinkID = campus.CAMPLINK_CampLinkID;
                            campuslink.CAMPLINK_CampusID = client.selectedCboCampus;
                            campuslink.CAMPLINK_UserID = client.UserID;

                            DAL.Entities.CAMPUSLINK.CampusLinkSave(campuslink);
                        }

                        clientdetailupdate.USER_Password = client.Password;

                        var update = DAL.Entities.USERS.SaveUser(clientdetailupdate);

                        //var contdetid = DAL.Entities.CONTACTDETAILS.GetClientID(update.USER_UserID);

                        DAL.Entities.CONTACTDETAILS contactdetails = new DAL.Entities.CONTACTDETAILS();
                        contactdetails.CONDET_UserID = client.UserID;
                        contactdetails.CONDET_CONTACTDETAILSID = client.ContactDetailsID;
                        if (client.Street1 == "BLANK")
                        {
                            contactdetails.CONDET_StreetNo = null;
                        }
                        else
                        {
                            contactdetails.CONDET_StreetNo = client.Street1.ToUpper();
                        }

                        if (client.Street2 == "BLANK")
                        {
                            contactdetails.CONDET_StreetName = null;
                        }
                        else
                        {
                            contactdetails.CONDET_StreetName = client.Street2.ToUpper();
                        }

                        if (client.Street3 == "BLANK")
                        {
                            contactdetails.CONDET_Suburb = null;
                        }
                        else
                        {
                            contactdetails.CONDET_Suburb = client.Street3.ToUpper();
                        }

                        if (client.Street4 == "BLANK")
                        {
                            contactdetails.CONDET_City = "";
                        }
                        else
                        {
                            contactdetails.CONDET_City = client.Street4.ToUpper();
                        }


                        if (client.PostalCode == "BLANK")
                        {
                            contactdetails.CONDET_PostalCode = null;
                        }
                        else
                        {
                            contactdetails.CONDET_PostalCode = client.PostalCode.ToUpper();
                        }

                        if (client.TelNo == "blank")
                        {
                            contactdetails.CONDET_TelNo = null;
                        }
                        else
                        {
                            contactdetails.CONDET_TelNo = client.TelNo;
                        }

                        if (client.Fax == "blank")
                        {
                            contactdetails.CONDET_FaxNo = null;
                        }
                        else
                        {
                            contactdetails.CONDET_FaxNo = client.Fax;
                        }

                        if (client.CellNo == "blank")
                        {
                            contactdetails.CONDET_CellNo = null;
                        }
                        else
                        {
                            contactdetails.CONDET_CellNo = client.CellNo;
                        }

                        if (client.Email == "blank")
                        {
                            contactdetails.CONDET_Email = null;
                        }
                        else
                        {
                            contactdetails.CONDET_Email = client.Email.ToUpper();
                        }

                        DAL.Entities.CONTACTDETAILS.SaveContactDetails(contactdetails);

                        // The Complete method commits the transaction. If an exception has been thrown, 
                        // Complete is not called and the transaction is rolled back.
                        scope.Complete();

                        return client;
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
