﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the QUALIFICATION table of the DB.
    /// </summary>
    public static class ModifyUserType
    {
        /// <summary>
        /// Get a list of all the records from the DAL
        /// </summary>
        /// <returns>
        /// Returns all the records form the Usertype Table
        /// </returns>
        public static List<BLObjects.UserType> GetAll()
        {
            try
            {
                List<BLObjects.UserType> result = new List<BLObjects.UserType>();

                foreach (DAL.Entities.USERTYPE ut in DAL.Entities.USERTYPE.GetAll())
                {
                    BLObjects.UserType usertype = new BLObjects.UserType();

                    usertype.TypeID = ut.TYPE_TypeID;
                    usertype.TypeName = ut.TYPE_Name;

                    result.Add(usertype);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<BLObjects.UserType> GetAllCollege()
        {
            try
            {
                List<BLObjects.UserType> result = new List<BLObjects.UserType>();

                foreach (DAL.Entities.USERTYPE ut in DAL.Entities.USERTYPE.GetAll().Where(c => c.TYPE_TypeID >= 4))
                {
                    BLObjects.UserType usertype = new BLObjects.UserType();

                    usertype.TypeID = ut.TYPE_TypeID;
                    usertype.TypeName = ut.TYPE_Name;

                    result.Add(usertype);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static List<BLObjects.UserType> GetAllCollegeUsers()
        {
            try
            {
                List<BLObjects.UserType> result = new List<BLObjects.UserType>();

                foreach (DAL.Entities.USERTYPE ut in DAL.Entities.USERTYPE.GetAll().Where(c => c.TYPE_TypeID >= 6))
                {
                    BLObjects.UserType usertype = new BLObjects.UserType();

                    usertype.TypeID = ut.TYPE_TypeID;
                    usertype.TypeName = ut.TYPE_Name;

                    result.Add(usertype);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
