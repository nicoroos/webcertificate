﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    /// <summary>
    /// This class handles all the methods and validations regarding the BOOKINGLINK table of the DB.
    /// </summary>
    public static class ModifyBooklink
    {
        /// <summary>
        /// Get a list of records from the BOOKLINK table that match the param.
        /// Then foreach booklink record get all the data of the booking from all the linked tables of the BOOKING table.
        /// </summary>
        /// <param name="bookingID"></param>
        /// <returns>
        /// Returns a list of Booking to be populated in the bookings table that match the param.
        /// </returns>
        public static List<BLObjects.ClientQual> GetBookLinkByBookingID(int bookingID)
        {
            try
            {
                List<BLObjects.ClientQual> result = new List<BLObjects.ClientQual>();

                foreach (DAL.Entities.BOOKLINK bl in DAL.Entities.BOOKLINK.GetBookIDList(bookingID))
                {
                    var booking = DAL.Entities.BOOKING.GetBookingByID(bookingID);
                    var user = DAL.Entities.USERS.GetUserByID(bl.BOOKLINK_UserID);
                    var qual = DAL.Entities.QUALIFICATION.GetQualByQualID(booking.BOOK_QualID);
                    var cq = DAL.Entities.CLIENTQUAL.GetClientQualByUserIDAndBookingID(bl.BOOKLINK_UserID, bookingID);

                    string printStatus = null;

                    if (cq != null)
                    {
                        DAL.Entities.PRINTSTATUS printSts = new DAL.Entities.PRINTSTATUS();
                        printSts = DAL.Entities.PRINTSTATUS.GetPrintStatusByPrintID(cq.CLIQUAL_PrintID);
                        printStatus = printSts.PRINT_Status;
                    }

                    BLObjects.ClientQual clientQual = new BLObjects.ClientQual();
                    clientQual.BookingID = bookingID;
                    clientQual.UserID = user.USER_UserID;
                    clientQual.CInitials = user.USER_Initials.Trim();
                    clientQual.CSurname = user.USER_Surname.Trim();
                    clientQual.Qual = qual.QUAL_Name.Trim();
                    clientQual.QualDescrip = qual.QUAL_Description.Trim();
                    clientQual.StartDate = booking.BOOK_StartDate.ToShortDateString();
                    clientQual.EndDate = booking.BOOK_EndDate.ToShortDateString();
                    clientQual.ReferNum = booking.BOOK_Year + booking.BOOK_Month + booking.BOOK_RandomCode;
                    clientQual.PrintStatus = printStatus;

                    result.Add(clientQual);
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Run through a list of data and check if the data is marked to be deleted and if so the data is removed
        /// </summary>
        /// <param name="clientQuals"></param>
        /// <returns></returns>
        public static List<BLObjects.ClientQual> Delete(List<BLObjects.ClientQual> clientQuals)
        {
            try
            {
                foreach (BLObjects.ClientQual clientqual in clientQuals)
                {
                    if (clientqual._destroy == true)
                    {
                        var booklink = DAL.Entities.BOOKLINK.GetBookIDAndUserID(clientqual.BookingID, clientqual.UserID);
                        if (booklink != null)
                        {
                            DAL.Entities.BOOKLINK.Delete(booklink);
                        }
                    }
                }
                return clientQuals;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
