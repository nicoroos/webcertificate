using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the TIMESHEET Object.
/// </summary>
public partial class TIMESHEET
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the TIMESHEET records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table TIMESHEET</remarks>
protected static IQueryable<TIMESHEET> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.TIMESHEET;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an TIMESHEET entry in the databse.
/// To obtain an instance of the TIMESHEET object use the <see cref="DALCMS.TIMESHEET.Get(int)"/> method.
/// </summary>
/// <param name="TIMESHEET">A Single instance of the TIMESHEET object.</param>
/// <returns>A Single instance of the TIMESHEET object.</returns>
/// <seealso cref="DALCMS.TIMESHEET.Get(int)"/>
protected static TIMESHEET Save(TIMESHEET TIMESHEET)
{
try
{
CMSEntities ctx = Context.Create();
TIMESHEET c = ctx.TIMESHEET.SingleOrDefault(s => s.TIME_TimeSheetID == TIMESHEET.TIME_TimeSheetID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.TIMESHEET.Add(TIMESHEET);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(TIMESHEET);
}
ctx.SaveChanges();
c = TIMESHEET;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the TIMESHEET object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.TIMESHEET.Save"/> or <see cref="DALCMS.TIMESHEET.Delete"/> methods.
/// </summary>
/// <param name="TIME_TimeSheetID">The TIMESHEET auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the TIMESHEET Object.</returns>
/// <seealso cref="DALCMS.TIMESHEET.Save"/>
/// <seealso cref="DALCMS.TIMESHEET.Delete"/>
protected static TIMESHEET Get(int TIME_TimeSheetID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.TIMESHEET.SingleOrDefault(s => s.TIME_TimeSheetID == TIME_TimeSheetID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the TIMESHEET object use the <see cref="DALCMS.TIMESHEET.Get(int)"/> method.
/// </summary>
/// <param name="TIMESHEET">A Single instance of the TIMESHEET object.</param>
/// <seealso cref="DALCMS.TIMESHEET.Get(int)"/>
protected static void Delete(TIMESHEET TIMESHEET)
{
try
{
CMSEntities ctx = Context.Create();
TIMESHEET c = ctx.TIMESHEET.Where(i => i.TIME_TimeSheetID == TIMESHEET.TIME_TimeSheetID).SingleOrDefault();
ctx.TIMESHEET.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
