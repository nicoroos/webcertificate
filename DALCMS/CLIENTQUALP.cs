using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the CLIENTQUAL Object.
/// </summary>
public partial class CLIENTQUAL
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the CLIENTQUAL records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table CLIENTQUAL</remarks>
protected static IQueryable<CLIENTQUAL> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.CLIENTQUAL;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an CLIENTQUAL entry in the databse.
/// To obtain an instance of the CLIENTQUAL object use the <see cref="DALCMS.CLIENTQUAL.Get(int)"/> method.
/// </summary>
/// <param name="CLIENTQUAL">A Single instance of the CLIENTQUAL object.</param>
/// <returns>A Single instance of the CLIENTQUAL object.</returns>
/// <seealso cref="DALCMS.CLIENTQUAL.Get(int)"/>
protected static CLIENTQUAL Save(CLIENTQUAL CLIENTQUAL)
{
try
{
CMSEntities ctx = Context.Create();
CLIENTQUAL c = ctx.CLIENTQUAL.SingleOrDefault(s => s.CLIQUAL_UserQualID == CLIENTQUAL.CLIQUAL_UserQualID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.CLIENTQUAL.Add(CLIENTQUAL);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(CLIENTQUAL);
}
ctx.SaveChanges();
c = CLIENTQUAL;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the CLIENTQUAL object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.CLIENTQUAL.Save"/> or <see cref="DALCMS.CLIENTQUAL.Delete"/> methods.
/// </summary>
/// <param name="CLIQUAL_UserQualID">The CLIENTQUAL auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the CLIENTQUAL Object.</returns>
/// <seealso cref="DALCMS.CLIENTQUAL.Save"/>
/// <seealso cref="DALCMS.CLIENTQUAL.Delete"/>
protected static CLIENTQUAL Get(int CLIQUAL_UserQualID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.CLIENTQUAL.SingleOrDefault(s => s.CLIQUAL_UserQualID == CLIQUAL_UserQualID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the CLIENTQUAL object use the <see cref="DALCMS.CLIENTQUAL.Get(int)"/> method.
/// </summary>
/// <param name="CLIENTQUAL">A Single instance of the CLIENTQUAL object.</param>
/// <seealso cref="DALCMS.CLIENTQUAL.Get(int)"/>
protected static void Delete(CLIENTQUAL CLIENTQUAL)
{
try
{
CMSEntities ctx = Context.Create();
CLIENTQUAL c = ctx.CLIENTQUAL.Where(i => i.CLIQUAL_UserQualID == CLIENTQUAL.CLIQUAL_UserQualID).SingleOrDefault();
ctx.CLIENTQUAL.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
