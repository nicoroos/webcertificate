using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the COSTS Object.
/// </summary>
public partial class COSTS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the COSTS records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table COSTS</remarks>
protected static IQueryable<COSTS> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.COSTS;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an COSTS entry in the databse.
/// To obtain an instance of the COSTS object use the <see cref="DALCMS.COSTS.Get(int)"/> method.
/// </summary>
/// <param name="COSTS">A Single instance of the COSTS object.</param>
/// <returns>A Single instance of the COSTS object.</returns>
/// <seealso cref="DALCMS.COSTS.Get(int)"/>
protected static COSTS Save(COSTS COSTS)
{
try
{
CMSEntities ctx = Context.Create();
COSTS c = ctx.COSTS.SingleOrDefault(s => s.COST_CostID == COSTS.COST_CostID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.COSTS.Add(COSTS);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(COSTS);
}
ctx.SaveChanges();
c = COSTS;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the COSTS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.COSTS.Save"/> or <see cref="DALCMS.COSTS.Delete"/> methods.
/// </summary>
/// <param name="COST_CostID">The COSTS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the COSTS Object.</returns>
/// <seealso cref="DALCMS.COSTS.Save"/>
/// <seealso cref="DALCMS.COSTS.Delete"/>
protected static COSTS Get(int COST_CostID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.COSTS.SingleOrDefault(s => s.COST_CostID == COST_CostID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the COSTS object use the <see cref="DALCMS.COSTS.Get(int)"/> method.
/// </summary>
/// <param name="COSTS">A Single instance of the COSTS object.</param>
/// <seealso cref="DALCMS.COSTS.Get(int)"/>
protected static void Delete(COSTS COSTS)
{
try
{
CMSEntities ctx = Context.Create();
COSTS c = ctx.COSTS.Where(i => i.COST_CostID == COSTS.COST_CostID).SingleOrDefault();
ctx.COSTS.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
