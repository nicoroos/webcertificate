using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the CONTACTDETAILS Object.
/// </summary>
public partial class CONTACTDETAILS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the CONTACTDETAILS records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table CONTACTDETAILS</remarks>
protected static IQueryable<CONTACTDETAILS> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.CONTACTDETAILS;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an CONTACTDETAILS entry in the databse.
/// To obtain an instance of the CONTACTDETAILS object use the <see cref="DALCMS.CONTACTDETAILS.Get(int)"/> method.
/// </summary>
/// <param name="CONTACTDETAILS">A Single instance of the CONTACTDETAILS object.</param>
/// <returns>A Single instance of the CONTACTDETAILS object.</returns>
/// <seealso cref="DALCMS.CONTACTDETAILS.Get(int)"/>
protected static CONTACTDETAILS Save(CONTACTDETAILS CONTACTDETAILS)
{
try
{
CMSEntities ctx = Context.Create();
CONTACTDETAILS c = ctx.CONTACTDETAILS.SingleOrDefault(s => s.CONDET_CONTACTDETAILSID == CONTACTDETAILS.CONDET_CONTACTDETAILSID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.CONTACTDETAILS.Add(CONTACTDETAILS);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(CONTACTDETAILS);
}
ctx.SaveChanges();
c = CONTACTDETAILS;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the CONTACTDETAILS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.CONTACTDETAILS.Save"/> or <see cref="DALCMS.CONTACTDETAILS.Delete"/> methods.
/// </summary>
/// <param name="CONDET_CONTACTDETAILSID">The CONTACTDETAILS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the CONTACTDETAILS Object.</returns>
/// <seealso cref="DALCMS.CONTACTDETAILS.Save"/>
/// <seealso cref="DALCMS.CONTACTDETAILS.Delete"/>
protected static CONTACTDETAILS Get(int CONDET_CONTACTDETAILSID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.CONTACTDETAILS.SingleOrDefault(s => s.CONDET_CONTACTDETAILSID == CONDET_CONTACTDETAILSID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the CONTACTDETAILS object use the <see cref="DALCMS.CONTACTDETAILS.Get(int)"/> method.
/// </summary>
/// <param name="CONTACTDETAILS">A Single instance of the CONTACTDETAILS object.</param>
/// <seealso cref="DALCMS.CONTACTDETAILS.Get(int)"/>
protected static void Delete(CONTACTDETAILS CONTACTDETAILS)
{
try
{
CMSEntities ctx = Context.Create();
CONTACTDETAILS c = ctx.CONTACTDETAILS.Where(i => i.CONDET_CONTACTDETAILSID == CONTACTDETAILS.CONDET_CONTACTDETAILSID).SingleOrDefault();
ctx.CONTACTDETAILS.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
