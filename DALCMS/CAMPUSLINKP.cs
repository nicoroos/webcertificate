using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the CAMPUSLINK Object.
/// </summary>
public partial class CAMPUSLINK
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the CAMPUSLINK records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table CAMPUSLINK</remarks>
protected static IQueryable<CAMPUSLINK> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.CAMPUSLINK;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an CAMPUSLINK entry in the databse.
/// To obtain an instance of the CAMPUSLINK object use the <see cref="DALCMS.CAMPUSLINK.Get(int)"/> method.
/// </summary>
/// <param name="CAMPUSLINK">A Single instance of the CAMPUSLINK object.</param>
/// <returns>A Single instance of the CAMPUSLINK object.</returns>
/// <seealso cref="DALCMS.CAMPUSLINK.Get(int)"/>
protected static CAMPUSLINK Save(CAMPUSLINK CAMPUSLINK)
{
try
{
CMSEntities ctx = Context.Create();
CAMPUSLINK c = ctx.CAMPUSLINK.SingleOrDefault(s => s.CAMPLINK_CampLinkID == CAMPUSLINK.CAMPLINK_CampLinkID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.CAMPUSLINK.Add(CAMPUSLINK);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(CAMPUSLINK);
}
ctx.SaveChanges();
c = CAMPUSLINK;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the CAMPUSLINK object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.CAMPUSLINK.Save"/> or <see cref="DALCMS.CAMPUSLINK.Delete"/> methods.
/// </summary>
/// <param name="CAMPLINK_CampLinkID">The CAMPUSLINK auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the CAMPUSLINK Object.</returns>
/// <seealso cref="DALCMS.CAMPUSLINK.Save"/>
/// <seealso cref="DALCMS.CAMPUSLINK.Delete"/>
protected static CAMPUSLINK Get(int CAMPLINK_CampLinkID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.CAMPUSLINK.SingleOrDefault(s => s.CAMPLINK_CampLinkID == CAMPLINK_CampLinkID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the CAMPUSLINK object use the <see cref="DALCMS.CAMPUSLINK.Get(int)"/> method.
/// </summary>
/// <param name="CAMPUSLINK">A Single instance of the CAMPUSLINK object.</param>
/// <seealso cref="DALCMS.CAMPUSLINK.Get(int)"/>
protected static void Delete(CAMPUSLINK CAMPUSLINK)
{
try
{
CMSEntities ctx = Context.Create();
CAMPUSLINK c = ctx.CAMPUSLINK.Where(i => i.CAMPLINK_CampLinkID == CAMPUSLINK.CAMPLINK_CampLinkID).SingleOrDefault();
ctx.CAMPUSLINK.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
