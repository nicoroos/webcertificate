using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the STATUS Object.
/// </summary>
public partial class STATUS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the STATUS records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table STATUS</remarks>
protected static IQueryable<STATUS> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.STATUS;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an STATUS entry in the databse.
/// To obtain an instance of the STATUS object use the <see cref="DALCMS.STATUS.Get(int)"/> method.
/// </summary>
/// <param name="STATUS">A Single instance of the STATUS object.</param>
/// <returns>A Single instance of the STATUS object.</returns>
/// <seealso cref="DALCMS.STATUS.Get(int)"/>
protected static STATUS Save(STATUS STATUS)
{
try
{
CMSEntities ctx = Context.Create();
STATUS c = ctx.STATUS.SingleOrDefault(s => s.STATUS_StatusID == STATUS.STATUS_StatusID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.STATUS.Add(STATUS);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(STATUS);
}
ctx.SaveChanges();
c = STATUS;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the STATUS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.STATUS.Save"/> or <see cref="DALCMS.STATUS.Delete"/> methods.
/// </summary>
/// <param name="STATUS_StatusID">The STATUS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the STATUS Object.</returns>
/// <seealso cref="DALCMS.STATUS.Save"/>
/// <seealso cref="DALCMS.STATUS.Delete"/>
protected static STATUS Get(int STATUS_StatusID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.STATUS.SingleOrDefault(s => s.STATUS_StatusID == STATUS_StatusID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the STATUS object use the <see cref="DALCMS.STATUS.Get(int)"/> method.
/// </summary>
/// <param name="STATUS">A Single instance of the STATUS object.</param>
/// <seealso cref="DALCMS.STATUS.Get(int)"/>
protected static void Delete(STATUS STATUS)
{
try
{
CMSEntities ctx = Context.Create();
STATUS c = ctx.STATUS.Where(i => i.STATUS_StatusID == STATUS.STATUS_StatusID).SingleOrDefault();
ctx.STATUS.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
