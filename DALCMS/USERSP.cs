using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the USERS Object.
/// </summary>
public partial class USERS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the USERS records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table USERS</remarks>
protected static IQueryable<USERS> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.USERS;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an USERS entry in the databse.
/// To obtain an instance of the USERS object use the <see cref="DALCMS.USERS.Get(int)"/> method.
/// </summary>
/// <param name="USERS">A Single instance of the USERS object.</param>
/// <returns>A Single instance of the USERS object.</returns>
/// <seealso cref="DALCMS.USERS.Get(int)"/>
protected static USERS Save(USERS USERS)
{
try
{
CMSEntities ctx = Context.Create();
USERS c = ctx.USERS.SingleOrDefault(s => s.USER_UserID == USERS.USER_UserID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.USERS.Add(USERS);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(USERS);
}
ctx.SaveChanges();
c = USERS;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the USERS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.USERS.Save"/> or <see cref="DALCMS.USERS.Delete"/> methods.
/// </summary>
/// <param name="USER_UserID">The USERS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the USERS Object.</returns>
/// <seealso cref="DALCMS.USERS.Save"/>
/// <seealso cref="DALCMS.USERS.Delete"/>
protected static USERS Get(int USER_UserID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.USERS.SingleOrDefault(s => s.USER_UserID == USER_UserID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the USERS object use the <see cref="DALCMS.USERS.Get(int)"/> method.
/// </summary>
/// <param name="USERS">A Single instance of the USERS object.</param>
/// <seealso cref="DALCMS.USERS.Get(int)"/>
protected static void Delete(USERS USERS)
{
try
{
CMSEntities ctx = Context.Create();
USERS c = ctx.USERS.Where(i => i.USER_UserID == USERS.USER_UserID).SingleOrDefault();
ctx.USERS.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
