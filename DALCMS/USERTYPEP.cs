using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the USERTYPE Object.
/// </summary>
public partial class USERTYPE
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the USERTYPE records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table USERTYPE</remarks>
protected static IQueryable<USERTYPE> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.USERTYPE;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an USERTYPE entry in the databse.
/// To obtain an instance of the USERTYPE object use the <see cref="DALCMS.USERTYPE.Get(int)"/> method.
/// </summary>
/// <param name="USERTYPE">A Single instance of the USERTYPE object.</param>
/// <returns>A Single instance of the USERTYPE object.</returns>
/// <seealso cref="DALCMS.USERTYPE.Get(int)"/>
protected static USERTYPE Save(USERTYPE USERTYPE)
{
try
{
CMSEntities ctx = Context.Create();
USERTYPE c = ctx.USERTYPE.SingleOrDefault(s => s.TYPE_TypeID == USERTYPE.TYPE_TypeID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.USERTYPE.Add(USERTYPE);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(USERTYPE);
}
ctx.SaveChanges();
c = USERTYPE;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the USERTYPE object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.USERTYPE.Save"/> or <see cref="DALCMS.USERTYPE.Delete"/> methods.
/// </summary>
/// <param name="TYPE_TypeID">The USERTYPE auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the USERTYPE Object.</returns>
/// <seealso cref="DALCMS.USERTYPE.Save"/>
/// <seealso cref="DALCMS.USERTYPE.Delete"/>
protected static USERTYPE Get(int TYPE_TypeID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.USERTYPE.SingleOrDefault(s => s.TYPE_TypeID == TYPE_TypeID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the USERTYPE object use the <see cref="DALCMS.USERTYPE.Get(int)"/> method.
/// </summary>
/// <param name="USERTYPE">A Single instance of the USERTYPE object.</param>
/// <seealso cref="DALCMS.USERTYPE.Get(int)"/>
protected static void Delete(USERTYPE USERTYPE)
{
try
{
CMSEntities ctx = Context.Create();
USERTYPE c = ctx.USERTYPE.Where(i => i.TYPE_TypeID == USERTYPE.TYPE_TypeID).SingleOrDefault();
ctx.USERTYPE.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
