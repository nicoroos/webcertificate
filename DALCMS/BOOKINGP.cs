using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
    /// <summary>
    /// This class is used to handle database actions for the BOOKING Object.
    /// </summary>
    public partial class BOOKING
    {
        #region Generated
        // This code was generated using the CodeGen

        /// <summary>
        /// Get all the BOOKING records.
        /// </summary>
        /// <returns>IQueryable collection.</returns>
        /// <remarks>Returns all the records from Table BOOKING</remarks>
        protected static IQueryable<BOOKING> Get()
        {
            try
            {
                CMSEntities ctx = Context.Create();
                return ctx.BOOKING;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Use this method to Add or Update an BOOKING entry in the databse.
        /// To obtain an instance of the BOOKING object use the <see cref="DALCMS.BOOKING.Get(int)"/> method.
        /// </summary>
        /// <param name="BOOKING">A Single instance of the BOOKING object.</param>
        /// <returns>A Single instance of the BOOKING object.</returns>
        /// <seealso cref="DALCMS.BOOKING.Get(int)"/>
        protected static BOOKING Save(BOOKING BOOKING)
        {
            try
            {
                CMSEntities ctx = Context.Create();
                BOOKING c = ctx.BOOKING.SingleOrDefault(s => s.BOOK_BookingID == BOOKING.BOOK_BookingID);

                if (c == null)
                {
                    // record doesnt exist, create a new record
                    ctx.BOOKING.Add(BOOKING);
                }
                else
                {
                    // record exists, update existing record
                    ctx.Entry(c).CurrentValues.SetValues(BOOKING);
                }
                ctx.SaveChanges();
                c = BOOKING;
                return c;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Use this method to return a single instance of the BOOKING object for the specified parameter value.
        /// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.BOOKING.Save"/> or <see cref="DALCMS.BOOKING.Delete"/> methods.
        /// </summary>
        /// <param name="BOOK_BookingID">The BOOKING auto no value for which to retrieve the databse entry.</param>
        /// <returns>A Single instance of the BOOKING Object.</returns>
        /// <seealso cref="DALCMS.BOOKING.Save"/>
        /// <seealso cref="DALCMS.BOOKING.Delete"/>
        protected static BOOKING Get(int BOOK_BookingID)
        {
            try
            {
                CMSEntities ctx = Context.Create();
                return ctx.BOOKING.SingleOrDefault(s => s.BOOK_BookingID == BOOK_BookingID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Use this method to Delete an entry from the database.
        /// To obtain an instance of the BOOKING object use the <see cref="DALCMS.BOOKING.Get(int)"/> method.
        /// </summary>
        /// <param name="BOOKING">A Single instance of the BOOKING object.</param>
        /// <seealso cref="DALCMS.BOOKING.Get(int)"/>
        protected static void Delete(BOOKING BOOKING)
        {
            try
            {
                CMSEntities ctx = Context.Create();
                BOOKING c = ctx.BOOKING.Where(i => i.BOOK_BookingID == BOOKING.BOOK_BookingID).SingleOrDefault();
                ctx.BOOKING.Remove(c);
                ctx.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
