using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace DALCMS
{
/// <summary>
/// This class contains methods commonly used by all the DAL Objects.
/// </summary>
public static class Common
{
/// <summary>
/// Use this method to make a Deep copy of an object instead of a shallow copy.
/// </summary>
/// <typeparam name="T">Any</typeparam>
/// <param name="from">The object that has the values.</param>
/// <param name="to">The new Instance of the object you want to populate.</param>
public static void DeepCopy<T>(T from, object to)
{
if (from == null)
{
throw new ArgumentNullException("Object cannot be null");
}

Process(from, to);
}

static object Process(object from, object to)
{
if (from == null)
{
return null;
}

Type type = from.GetType().BaseType;

if (type.IsValueType || type == typeof(string))
{
return from;
}
if (type.IsClass)
{
object copy = to;

FieldInfo[] fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

foreach (FieldInfo field in fields)
{
object fieldValue = field.GetValue(from);

if (fieldValue == null)
{
continue;
}

field.SetValue(copy, Process(fieldValue, fieldValue));
}

return copy;
}
else if (type.IsArray)
{

Type elementType = Type.GetType(type.FullName.Replace("[]", string.Empty));

var array = from as Array;

Array copied = Array.CreateInstance(elementType, array.Length);

for (int i = 0; i < array.Length; i++)
{
copied.SetValue(Process(array.GetValue(i), to), i);
}

return Convert.ChangeType(copied, from.GetType());

}
else
{
return from;
}

}

}
}
