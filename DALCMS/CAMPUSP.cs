using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the CAMPUS Object.
/// </summary>
public partial class CAMPUS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the CAMPUS records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table CAMPUS</remarks>
protected static IQueryable<CAMPUS> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.CAMPUS;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an CAMPUS entry in the databse.
/// To obtain an instance of the CAMPUS object use the <see cref="DALCMS.CAMPUS.Get(int)"/> method.
/// </summary>
/// <param name="CAMPUS">A Single instance of the CAMPUS object.</param>
/// <returns>A Single instance of the CAMPUS object.</returns>
/// <seealso cref="DALCMS.CAMPUS.Get(int)"/>
protected static CAMPUS Save(CAMPUS CAMPUS)
{
try
{
CMSEntities ctx = Context.Create();
CAMPUS c = ctx.CAMPUS.SingleOrDefault(s => s.CAMP_CampusID == CAMPUS.CAMP_CampusID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.CAMPUS.Add(CAMPUS);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(CAMPUS);
}
ctx.SaveChanges();
c = CAMPUS;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the CAMPUS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.CAMPUS.Save"/> or <see cref="DALCMS.CAMPUS.Delete"/> methods.
/// </summary>
/// <param name="CAMP_CampusID">The CAMPUS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the CAMPUS Object.</returns>
/// <seealso cref="DALCMS.CAMPUS.Save"/>
/// <seealso cref="DALCMS.CAMPUS.Delete"/>
protected static CAMPUS Get(int CAMP_CampusID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.CAMPUS.SingleOrDefault(s => s.CAMP_CampusID == CAMP_CampusID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the CAMPUS object use the <see cref="DALCMS.CAMPUS.Get(int)"/> method.
/// </summary>
/// <param name="CAMPUS">A Single instance of the CAMPUS object.</param>
/// <seealso cref="DALCMS.CAMPUS.Get(int)"/>
protected static void Delete(CAMPUS CAMPUS)
{
try
{
CMSEntities ctx = Context.Create();
CAMPUS c = ctx.CAMPUS.Where(i => i.CAMP_CampusID == CAMPUS.CAMP_CampusID).SingleOrDefault();
ctx.CAMPUS.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
