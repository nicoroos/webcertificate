using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the TITLE Object.
/// </summary>
public partial class TITLE
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the TITLE records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table TITLE</remarks>
protected static IQueryable<TITLE> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.TITLE;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an TITLE entry in the databse.
/// To obtain an instance of the TITLE object use the <see cref="DALCMS.TITLE.Get(int)"/> method.
/// </summary>
/// <param name="TITLE">A Single instance of the TITLE object.</param>
/// <returns>A Single instance of the TITLE object.</returns>
/// <seealso cref="DALCMS.TITLE.Get(int)"/>
protected static TITLE Save(TITLE TITLE)
{
try
{
CMSEntities ctx = Context.Create();
TITLE c = ctx.TITLE.SingleOrDefault(s => s.TITLE_TitleID == TITLE.TITLE_TitleID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.TITLE.Add(TITLE);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(TITLE);
}
ctx.SaveChanges();
c = TITLE;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the TITLE object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.TITLE.Save"/> or <see cref="DALCMS.TITLE.Delete"/> methods.
/// </summary>
/// <param name="TITLE_TitleID">The TITLE auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the TITLE Object.</returns>
/// <seealso cref="DALCMS.TITLE.Save"/>
/// <seealso cref="DALCMS.TITLE.Delete"/>
protected static TITLE Get(int TITLE_TitleID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.TITLE.SingleOrDefault(s => s.TITLE_TitleID == TITLE_TitleID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the TITLE object use the <see cref="DALCMS.TITLE.Get(int)"/> method.
/// </summary>
/// <param name="TITLE">A Single instance of the TITLE object.</param>
/// <seealso cref="DALCMS.TITLE.Get(int)"/>
protected static void Delete(TITLE TITLE)
{
try
{
CMSEntities ctx = Context.Create();
TITLE c = ctx.TITLE.Where(i => i.TITLE_TitleID == TITLE.TITLE_TitleID).SingleOrDefault();
ctx.TITLE.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
