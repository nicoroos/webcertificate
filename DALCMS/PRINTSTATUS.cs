//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DALCMS
{
    using System;
    using System.Collections.Generic;
    
    using Newtonsoft.Json;
    public partial class PRINTSTATUS
    {
        public PRINTSTATUS()
        {
            this.CLIENTQUAL = new HashSet<CLIENTQUAL>();
        }
    
        public int PRINT_PrintID { get; set; }
        public string PRINT_Status { get; set; }
    
    [JsonIgnore]
        public virtual ICollection<CLIENTQUAL> CLIENTQUAL { get; set; }
    }
}
