using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the RatePerKM Object.
/// </summary>
public partial class RatePerKM
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the RatePerKM records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table RatePerKM</remarks>
protected static IQueryable<RatePerKM> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.RatePerKM;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an RatePerKM entry in the databse.
/// To obtain an instance of the RatePerKM object use the <see cref="DALCMS.RatePerKM.Get(int)"/> method.
/// </summary>
/// <param name="RatePerKM">A Single instance of the RatePerKM object.</param>
/// <returns>A Single instance of the RatePerKM object.</returns>
/// <seealso cref="DALCMS.RatePerKM.Get(int)"/>
protected static RatePerKM Save(RatePerKM RatePerKM)
{
try
{
CMSEntities ctx = Context.Create();
RatePerKM c = ctx.RatePerKM.SingleOrDefault(s => s.RATES_RatePerKMID == RatePerKM.RATES_RatePerKMID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.RatePerKM.Add(RatePerKM);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(RatePerKM);
}
ctx.SaveChanges();
c = RatePerKM;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the RatePerKM object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.RatePerKM.Save"/> or <see cref="DALCMS.RatePerKM.Delete"/> methods.
/// </summary>
/// <param name="RATES_RatePerKMID">The RatePerKM auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the RatePerKM Object.</returns>
/// <seealso cref="DALCMS.RatePerKM.Save"/>
/// <seealso cref="DALCMS.RatePerKM.Delete"/>
protected static RatePerKM Get(int RATES_RatePerKMID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.RatePerKM.SingleOrDefault(s => s.RATES_RatePerKMID == RATES_RatePerKMID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the RatePerKM object use the <see cref="DALCMS.RatePerKM.Get(int)"/> method.
/// </summary>
/// <param name="RatePerKM">A Single instance of the RatePerKM object.</param>
/// <seealso cref="DALCMS.RatePerKM.Get(int)"/>
protected static void Delete(RatePerKM RatePerKM)
{
try
{
CMSEntities ctx = Context.Create();
RatePerKM c = ctx.RatePerKM.Where(i => i.RATES_RatePerKMID == RatePerKM.RATES_RatePerKMID).SingleOrDefault();
ctx.RatePerKM.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
