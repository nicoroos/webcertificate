using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the BOOKLINK Object.
/// </summary>
public partial class BOOKLINK
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the BOOKLINK records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table BOOKLINK</remarks>
protected static IQueryable<BOOKLINK> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.BOOKLINK;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an BOOKLINK entry in the databse.
/// To obtain an instance of the BOOKLINK object use the <see cref="DALCMS.BOOKLINK.Get(int)"/> method.
/// </summary>
/// <param name="BOOKLINK">A Single instance of the BOOKLINK object.</param>
/// <returns>A Single instance of the BOOKLINK object.</returns>
/// <seealso cref="DALCMS.BOOKLINK.Get(int)"/>
protected static BOOKLINK Save(BOOKLINK BOOKLINK)
{
try
{
CMSEntities ctx = Context.Create();
BOOKLINK c = ctx.BOOKLINK.SingleOrDefault(s => s.BOOKLINK_BooklinkID == BOOKLINK.BOOKLINK_BooklinkID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.BOOKLINK.Add(BOOKLINK);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(BOOKLINK);
}
ctx.SaveChanges();
c = BOOKLINK;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the BOOKLINK object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.BOOKLINK.Save"/> or <see cref="DALCMS.BOOKLINK.Delete"/> methods.
/// </summary>
/// <param name="BOOKLINK_BooklinkID">The BOOKLINK auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the BOOKLINK Object.</returns>
/// <seealso cref="DALCMS.BOOKLINK.Save"/>
/// <seealso cref="DALCMS.BOOKLINK.Delete"/>
protected static BOOKLINK Get(int BOOKLINK_BooklinkID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.BOOKLINK.SingleOrDefault(s => s.BOOKLINK_BooklinkID == BOOKLINK_BooklinkID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the BOOKLINK object use the <see cref="DALCMS.BOOKLINK.Get(int)"/> method.
/// </summary>
/// <param name="BOOKLINK">A Single instance of the BOOKLINK object.</param>
/// <seealso cref="DALCMS.BOOKLINK.Get(int)"/>
protected static void Delete(BOOKLINK BOOKLINK)
{
try
{
CMSEntities ctx = Context.Create();
BOOKLINK c = ctx.BOOKLINK.Where(i => i.BOOKLINK_BooklinkID == BOOKLINK.BOOKLINK_BooklinkID).SingleOrDefault();
ctx.BOOKLINK.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
