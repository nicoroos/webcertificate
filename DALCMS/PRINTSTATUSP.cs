using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the PRINTSTATUS Object.
/// </summary>
public partial class PRINTSTATUS
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the PRINTSTATUS records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table PRINTSTATUS</remarks>
protected static IQueryable<PRINTSTATUS> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.PRINTSTATUS;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an PRINTSTATUS entry in the databse.
/// To obtain an instance of the PRINTSTATUS object use the <see cref="DALCMS.PRINTSTATUS.Get(int)"/> method.
/// </summary>
/// <param name="PRINTSTATUS">A Single instance of the PRINTSTATUS object.</param>
/// <returns>A Single instance of the PRINTSTATUS object.</returns>
/// <seealso cref="DALCMS.PRINTSTATUS.Get(int)"/>
protected static PRINTSTATUS Save(PRINTSTATUS PRINTSTATUS)
{
try
{
CMSEntities ctx = Context.Create();
PRINTSTATUS c = ctx.PRINTSTATUS.SingleOrDefault(s => s.PRINT_PrintID == PRINTSTATUS.PRINT_PrintID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.PRINTSTATUS.Add(PRINTSTATUS);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(PRINTSTATUS);
}
ctx.SaveChanges();
c = PRINTSTATUS;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the PRINTSTATUS object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.PRINTSTATUS.Save"/> or <see cref="DALCMS.PRINTSTATUS.Delete"/> methods.
/// </summary>
/// <param name="PRINT_PrintID">The PRINTSTATUS auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the PRINTSTATUS Object.</returns>
/// <seealso cref="DALCMS.PRINTSTATUS.Save"/>
/// <seealso cref="DALCMS.PRINTSTATUS.Delete"/>
protected static PRINTSTATUS Get(int PRINT_PrintID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.PRINTSTATUS.SingleOrDefault(s => s.PRINT_PrintID == PRINT_PrintID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the PRINTSTATUS object use the <see cref="DALCMS.PRINTSTATUS.Get(int)"/> method.
/// </summary>
/// <param name="PRINTSTATUS">A Single instance of the PRINTSTATUS object.</param>
/// <seealso cref="DALCMS.PRINTSTATUS.Get(int)"/>
protected static void Delete(PRINTSTATUS PRINTSTATUS)
{
try
{
CMSEntities ctx = Context.Create();
PRINTSTATUS c = ctx.PRINTSTATUS.Where(i => i.PRINT_PrintID == PRINTSTATUS.PRINT_PrintID).SingleOrDefault();
ctx.PRINTSTATUS.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
