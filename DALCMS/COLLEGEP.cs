using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the COLLEGE Object.
/// </summary>
public partial class COLLEGE
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the COLLEGE records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table COLLEGE</remarks>
protected static IQueryable<COLLEGE> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.COLLEGE;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an COLLEGE entry in the databse.
/// To obtain an instance of the COLLEGE object use the <see cref="DALCMS.COLLEGE.Get(int)"/> method.
/// </summary>
/// <param name="COLLEGE">A Single instance of the COLLEGE object.</param>
/// <returns>A Single instance of the COLLEGE object.</returns>
/// <seealso cref="DALCMS.COLLEGE.Get(int)"/>
protected static COLLEGE Save(COLLEGE COLLEGE)
{
try
{
CMSEntities ctx = Context.Create();
COLLEGE c = ctx.COLLEGE.SingleOrDefault(s => s.COL_CollegeID == COLLEGE.COL_CollegeID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.COLLEGE.Add(COLLEGE);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(COLLEGE);
}
ctx.SaveChanges();
c = COLLEGE;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the COLLEGE object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.COLLEGE.Save"/> or <see cref="DALCMS.COLLEGE.Delete"/> methods.
/// </summary>
/// <param name="COL_CollegeID">The COLLEGE auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the COLLEGE Object.</returns>
/// <seealso cref="DALCMS.COLLEGE.Save"/>
/// <seealso cref="DALCMS.COLLEGE.Delete"/>
protected static COLLEGE Get(int COL_CollegeID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.COLLEGE.SingleOrDefault(s => s.COL_CollegeID == COL_CollegeID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the COLLEGE object use the <see cref="DALCMS.COLLEGE.Get(int)"/> method.
/// </summary>
/// <param name="COLLEGE">A Single instance of the COLLEGE object.</param>
/// <seealso cref="DALCMS.COLLEGE.Get(int)"/>
protected static void Delete(COLLEGE COLLEGE)
{
try
{
CMSEntities ctx = Context.Create();
COLLEGE c = ctx.COLLEGE.Where(i => i.COL_CollegeID == COLLEGE.COL_CollegeID).SingleOrDefault();
ctx.COLLEGE.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
