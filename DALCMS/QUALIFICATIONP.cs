using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DALCMS
{
/// <summary>
/// This class is used to handle database actions for the QUALIFICATION Object.
/// </summary>
public partial class QUALIFICATION
{
#region Generated
// This code was generated using the CodeGen

/// <summary>
/// Get all the QUALIFICATION records.
/// </summary>
/// <returns>IQueryable collection.</returns>
/// <remarks>Returns all the records from Table QUALIFICATION</remarks>
protected static IQueryable<QUALIFICATION> Get()
{
try
{
CMSEntities ctx = Context.Create();
return ctx.QUALIFICATION;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Add or Update an QUALIFICATION entry in the databse.
/// To obtain an instance of the QUALIFICATION object use the <see cref="DALCMS.QUALIFICATION.Get(int)"/> method.
/// </summary>
/// <param name="QUALIFICATION">A Single instance of the QUALIFICATION object.</param>
/// <returns>A Single instance of the QUALIFICATION object.</returns>
/// <seealso cref="DALCMS.QUALIFICATION.Get(int)"/>
protected static QUALIFICATION Save(QUALIFICATION QUALIFICATION)
{
try
{
CMSEntities ctx = Context.Create();
QUALIFICATION c = ctx.QUALIFICATION.SingleOrDefault(s => s.QUAL_QualID == QUALIFICATION.QUAL_QualID);

if (c == null)
{
// record doesnt exist, create a new record
ctx.QUALIFICATION.Add(QUALIFICATION);
}
else
{
// record exists, update existing record
ctx.Entry(c).CurrentValues.SetValues(QUALIFICATION);
}
ctx.SaveChanges();
c = QUALIFICATION;
return c;
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to return a single instance of the QUALIFICATION object for the specified parameter value.
/// This retrieved single instance can be used to Update or Delete the data by passing it as a parameter to the <see cref="DALCMS.QUALIFICATION.Save"/> or <see cref="DALCMS.QUALIFICATION.Delete"/> methods.
/// </summary>
/// <param name="QUAL_QualID">The QUALIFICATION auto no value for which to retrieve the databse entry.</param>
/// <returns>A Single instance of the QUALIFICATION Object.</returns>
/// <seealso cref="DALCMS.QUALIFICATION.Save"/>
/// <seealso cref="DALCMS.QUALIFICATION.Delete"/>
protected static QUALIFICATION Get(int QUAL_QualID)
{
try
{
CMSEntities ctx = Context.Create();
return ctx.QUALIFICATION.SingleOrDefault(s => s.QUAL_QualID == QUAL_QualID);
}
catch (Exception)
{
throw;
}
}

/// <summary>
/// Use this method to Delete an entry from the database.
/// To obtain an instance of the QUALIFICATION object use the <see cref="DALCMS.QUALIFICATION.Get(int)"/> method.
/// </summary>
/// <param name="QUALIFICATION">A Single instance of the QUALIFICATION object.</param>
/// <seealso cref="DALCMS.QUALIFICATION.Get(int)"/>
protected static void Delete(QUALIFICATION QUALIFICATION)
{
try
{
CMSEntities ctx = Context.Create();
QUALIFICATION c = ctx.QUALIFICATION.Where(i => i.QUAL_QualID == QUALIFICATION.QUAL_QualID).SingleOrDefault();
ctx.QUALIFICATION.Remove(c);
ctx.SaveChanges();
}
catch (Exception)
{
throw;
}
}

#endregion
}
}
