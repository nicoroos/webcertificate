using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity.Core.EntityClient;

namespace DALCMS
{
/// <summary>
/// This class contain methods interacting with entity framework.
/// </summary>
public static class Context
{
/// <summary>
/// Use this method to create a Context of entity framework
/// </summary>
/// <returns>Context</returns>
/// <seealso cref="DALCMSEntities.globals.Connection"/>
public static CMSEntities Create()
{
CMSEntities ctx = new CMSEntities();
if (!string.IsNullOrWhiteSpace(globals.Connection))
{
ctx.Database.Connection.ConnectionString = globals.Connection;
}
return ctx;
}
}
}
